﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

namespace Haeger2018
{
    public partial class BTMTW : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            // Check the User Agent
            var agent = Request.UserAgent.ToLower();

            // Check if iOS, Android, or other
            if (agent.Contains("ios"))
            {
                Response.Redirect("https://itunes.apple.com/us/app/haeger-wizard/id625478700?mt=8&uo=4");
            }
            else if (agent.Contains("android"))
            {
                Response.Redirect("https://play.google.com/store/apps/details?id=edu.haeger.haegerwizard&hl=en");
            }
            else if (Request.Browser.IsMobileDevice)
            {
                Response.Redirect("https://itunes.apple.com/us/app/haeger-wizard/id625478700?mt=8&uo=4");
            }

            BTMWizard.PreRender += new EventHandler(wzd_PreRender);
        }

        protected void wzd_PreRender(object sender, EventArgs e)
        {
           

        }

        public string GetClassForWizardStep(object wizardStep)
        {
            WizardStep step = wizardStep as WizardStep;

            if (step == null)
            {
                return "";
            }

            int stepIndex = BTMWizard.WizardSteps.IndexOf(step);

            if (stepIndex < BTMWizard.ActiveStepIndex)
            {
                return "stepCompleted";
            }
            else if (stepIndex > BTMWizard.ActiveStepIndex)
            {
                return "stepNotCompleted";
            }
            else
            {
                return "stepCurrent";
            }
        }

        protected void rblBTMMachineType_SelectedIndexChanged(object sender, EventArgs e)
        {
            Session["btmMachineSelected"] = rblBTMMachineType.SelectedItem.Text;
            var btmMachineSelectedImage = rblBTMMachineType.SelectedItem.Value;

            imgBTMMachineSelected.ImageUrl = "images/" + btmMachineSelectedImage + ".png";
            lblBTMMachineType.Text = "Machine Type:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + rblBTMMachineType.SelectedItem.Text;
            imgBTMMachineSelected.Visible = true;
            lblBTMMachineType.Visible = true;

            // Retrieve data from query
            DataView dvBTMold = (DataView)sqlBTMResultsOld.Select(DataSourceSelectArguments.Empty);
            DataView dvBTMnew = (DataView)sqlBTMResultsNew.Select(DataSourceSelectArguments.Empty);

            SqlDataSource sql1 = new SqlDataSource();

            string strMachineChosen = Session["btmMachineSelected"].ToString();

            if (rblBTMMachineType.SelectedItem.Text == "Old Machines")
            {
                grdBTMResultOld.Visible = true;
            }
            else if (rblBTMMachineType.SelectedItem.Text == "Dash 4 & MSPe")
            {
                grdBTMResultNew.Visible = true;
            }

            BTMWizard.MoveTo(this.WizardStep2);


        }


        protected void rblBTMPunchThick_SelectedIndexChanged(object sender, EventArgs e)
        {
            Session["btmPunchThickSelected"] = rblBTMPunchThick.SelectedItem.Text;

            lblPunchThickness.Text = "Punch Thickness:&nbsp;&nbsp;&nbsp;&nbsp;" + rblBTMPunchThick.SelectedItem.Text;
            lblPunchThickness.Visible = true;

            BTMWizard.MoveTo(this.WizardStep3);
        }

        protected void rblBTMDieThick_SelectedIndexChanged(object sender, EventArgs e)
        {
            Session["btmDieThickSelected"] = rblBTMDieThick.SelectedItem.Text;

            lblDieThickness.Text = "Die Thickness:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + rblBTMDieThick.SelectedItem.Text;
            lblDieThickness.Visible = true;

            BTMWizard.MoveTo(this.WizardStep4);
        }

        protected void rblBTMPunchDia_SelectedIndexChanged(object sender, EventArgs e)
        {
            Session["btmPunchDiameterSelected"] = rblBTMPunchDia.SelectedItem.Text;

            lblPunchDiameter.Text = "Punch Diameter:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + rblBTMPunchDia.SelectedItem.Text;
            lblPunchDiameter.Visible = true;
            BTMWizard.MoveTo(this.WizardStep5);
        }

     
        protected void btnBTMReset_Click(object sender, EventArgs e)
        {
            Response.Redirect("BTMTW.aspx");
        }


    }
}