﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Haeger2018
{
    public partial class DistLocator : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        protected void Page_Init()
        {
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
        }
        protected void foo(object sender, EventArgs e)
        {

        }
        protected void GridView1_SelectedIndexChanged(object sender, EventArgs e)
        {
            GridView2.Visible = false;
        }

        protected void GridView2_SelectedIndexChanged(object sender, EventArgs e)
        {
            GridView1.Visible = false;
        }

        protected void txtUserZip_TextChanged(object sender, EventArgs e)
        {

            if (txtUserZip.Text == "")
            {
                GridView1.Visible = true;
            }
            else
            {
                GridView1.Visible = false;
            }

        }
    }
}