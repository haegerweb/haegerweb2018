﻿<%@ Page Title="Sales Request" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="SalesRequest.aspx.cs" Inherits="Haeger2018.SalesRequest" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
    <style>
     
        .requiredRed
        {
            width:auto;
            text-align:right;
            color:red;
            font-size:14px;
            font-weight:bold;
        }

        h7
        {
            font:normal;
            color:black;
            font-size:18px;

        }

        .red
        {
            color:red;
        }

        .rbl input[type="radio"]
        {
           margin-left: 20px;
           margin-right: 1px;
        }

        .rbl2 input[type=radio]
        {
            margin-left:10px;
            margin-right:1px;
        }
         span1{
            color:#0085ca;
        }
          /* The container */
        .container1 {
            display:block;
            position:center;
            padding-left: 35px;
            margin-bottom: 12px;
            cursor: pointer;
            font-size: 22px;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
            font-size: 16px;
        }

        /* Hide the browser's default checkbox */
        .container1 input {
            position: absolute;
            opacity: 0;
            cursor: pointer;
    
        }

         .checkbox .btn, .checkbox-inline .btn {
            padding-left: 2em;
            min-width: 8em;
            }
            .checkbox label, .checkbox-inline label {
            text-align: left;
            padding-left: 0.5em;
            }
            .checkbox input[type="checkbox"]{
                float:none;
            }


        /* Create a custom checkbox */
        .checkmark {
            position: absolute;
            top: 0;
            left: 0;
            height: 25px;
            width: 25px;
            background-color: red;
    
        }

        .container1:hover {
            background-color:greenyellow;
        }

        /* On mouse-over, add a grey background color */
        .container1:hover input ~ .checkmark {
            background-color: #ccc;
        }

        /* When the checkbox is checked, add a blue background */
        .container1 input:checked ~ .checkmark {
            background-color: #0085ca;
        }

        /* Create the checkmark/indicator (hidden when not checked) */
        .checkmark:after {
            content: "";
            position: absolute;
            display: none;
        }

        /* Show the checkmark when checked */
        .container1 input:checked ~ .checkmark:after {
            display: block;
        }

        /* Style the checkmark/indicator */
        .container1 .checkmark:after {
            left: 9px;
            top: 5px;
            width: 5px;
            height: 10px;
            border: solid white;
            border-width: 0 3px 3px 0;
            -webkit-transform: rotate(45deg);
            -ms-transform: rotate(45deg);
            transform: rotate(45deg);
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div class="container-fluid">
        <div class="form-group">
            <div class="row">
                <div class="section-title">
                    <h2 style="margin: 0;">Haeger Sales Requests</h2>
                    <h3 style="text-transform:initial; color:black; margin:0;">A <span1> PennEngineering&reg</span1> Company</h3>
                   
               </div>
            <br />
            </div>
            <br />
             <div class="row">
                <div class="col-sm-3 col-sx-12">

                </div>
                <div class="col-sm-6 col-sx-12 text-center">
                    <h7>Items in <h7 class="red">RED</h7> must be filled in</h7>
                    <hr />
                </div>
            </div>
            <div class="row">
                 <div class="col-sm-2">
                    <asp:Label ID="lblSlsRCompany" CssClass="control-label requiredRed" runat="server" Text="" Style="text-align:right">Company Name:&nbsp&nbsp</asp:Label>
                </div>
                <div class="col-sm-4 col-sm-pull-1">
                    <asp:TextBox ID="txtSlsRCompany" runat="server" CssClass="form-control"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfvSlsRCompany" runat="server" CssClass="requiredRed" 
                        ErrorMessage="* Required" ControlToValidate="txtSlsRCompany">

                    </asp:RequiredFieldValidator>
                </div>

            </div>
            <div class="row">
                <div class="col-sm-2">
                    <asp:Label ID="lblSlsRFirstName" CssClass="control-label requiredRed" runat="server" Text="">First Name:</asp:Label>
                </div>
                <div class="col-sm-4 col-sm-pull-1">
                    <asp:TextBox ID="txtSlsRFirstName" runat="server" CssClass="form-control"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfvSlsRFirstName" runat="server" CssClass="requiredRed"  
                        ErrorMessage="* Required" ControlToValidate="txtSlsRFirstName">

                    </asp:RequiredFieldValidator>
                </div>
                <div class="col-sm-1">
                    <asp:Label ID="lblSlsLastName" CssClass="control-label requiredRed" runat="server" Text="">Last Name:</asp:Label>
                </div>
                <div class="col-sm-4">
                    <asp:TextBox ID="txtSlsLastName" runat="server" CssClass="form-control"></asp:TextBox>
                     <asp:RequiredFieldValidator ID="rfvSlsLastName" runat="server" CssClass="requiredRed" 
                                            ErrorMessage="* Required" ControlToValidate="txtSlsLastName">

                    </asp:RequiredFieldValidator>
                </div>

            </div>
            <div class="row">
                <div class="col-sm-2">
                    <asp:Label ID="lblSlsRPhone" CssClass="control-label requiredRed" runat="server" Text="">Phone Number:</asp:Label>
                </div>
                <div class="col-sm-4 col-sm-pull-1">
                    <asp:TextBox ID="txtSlsRPhone" runat="server" CssClass="form-control"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfvSlsRPhone" runat="server" CssClass="requiredRed" 
                                        ControlToValidate="txtSlsRPhone" ErrorMessage="* Required">

                    </asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator ID="revSlsRPhone" runat="server" CssClass="requiredRed"
                        ErrorMessage="* Phone Number Format Invalid" ControlToValidate="txtSlsRPhone" SetFocusOnError="True"
                        ValidationExpression="^([0-9]( |-)?)?(\(?[0-9]{3}\)?|[0-9]{3})( |-)?([0-9]{3}( |-)?[0-9]{4}|[a-zA-Z0-9]{7})$">
                    </asp:RegularExpressionValidator>
                </div>

                <div class="col-sm-1">
                    <asp:Label ID="lblSlsREmail" CssClass="control-label requiredRed" runat="server" Text="">Email:</asp:Label>
                </div>
                <div class="col-sm-4">
                    <asp:TextBox ID="txtSlsREmail" runat="server" CssClass="form-control"></asp:TextBox>
                     <asp:RequiredFieldValidator ID="rfvSksREmail" runat="server" CssClass="requiredRed" 
                                        ErrorMessage="* Required" ControlToValidate="txtSlsREmail">

                    </asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator ID="revSlsREmail" runat="server"
                                     ErrorMessage="  * Invalid Email Format" CssClass="requiredRed" ControlToValidate="txtSlsREmail"
                                     SetFocusOnError="True"
                                     ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*">
                    </asp:RegularExpressionValidator>
                </div>

            </div>

            <div class="row">
                <div class="col-sm-2">
                    <asp:Label ID="lblSlsRAddress" CssClass="control-label requiredRed" runat="server" Text="">Address:</asp:Label>
                </div>
                <div class="col-sm-4 col-sm-pull-1">
                    <asp:TextBox ID="txtSlsRAddress" runat="server" CssClass="form-control"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" CssClass="requiredRed" 
                                        ControlToValidate="txtSlsRAddress" ErrorMessage="* Required">

                    </asp:RequiredFieldValidator>
                   
                </div>

                <div class="col-sm-1">
                    <asp:Label ID="lblSlsRCity" CssClass="control-label requiredRed" runat="server" Text="">City:</asp:Label>
                </div>
                <div class="col-sm-4">
                    <asp:TextBox ID="txtSlsRCity" runat="server" CssClass="form-control"></asp:TextBox>
                     <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" CssClass="requiredRed" 
                                        ErrorMessage="* Required" ControlToValidate="txtSlsRCity">

                    </asp:RequiredFieldValidator>
                   
                </div>

            </div>

            <div class="row">
                <div class="col-sm-1">
                    <asp:Label ID="lblSlsRState" CssClass="control-label requiredRed" runat="server" Text="">State:</asp:Label>
                    
                </div>
                <div class="col-sm-3">
                    <asp:TextBox ID="txtSlsRState" runat="server" CssClass="form-control"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfvSlsRState" runat="server" CssClass="requiredRed" 
                                        ErrorMessage="* Required" ControlToValidate="txtSlsRState"></asp:RequiredFieldValidator>
                </div>
                

                <div class="col-sm-1">
                    <asp:Label ID="lblSlsRZip" CssClass="control-label requiredRed" runat="server" Text="">Zip Code:</asp:Label>
                    
                </div>
                <div class="col-sm-3">
                    <asp:TextBox ID="txtSlsRZip" runat="server" CssClass="form-control"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfvSlsRZip" runat="server" CssClass="requiredRed" 
                                        ErrorMessage="* Required" ControlToValidate="txtSlsRZip"></asp:RequiredFieldValidator>
                </div>
                


                <div class="col-sm-1">
                    <asp:Label ID="lblSlsRCountry" CssClass="control-label requiredRed" runat="server" Text="">Country:</asp:Label>
                    
                </div>
                <div class="col-sm-3">
                    <asp:TextBox ID="txtSlsRCountry" runat="server" CssClass="form-control"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfvSlsRCountry" runat="server" CssClass="requiredRed" 
                                        ErrorMessage="* Required" ControlToValidate="txtSlsRCountry"></asp:RequiredFieldValidator>
                </div>
                

            </div>
            <br />
            

            <div class="row">
                <div class="col-sm-4 col-sm-push-3">
                    <asp:Label ID="lblSlsRDetails" CssClass="control-label requiredRed" runat="server" Text="">Sales Inquiry Type:</asp:Label>
                    <asp:RequiredFieldValidator ID="rfvSlsRDetails" runat="server" CssClass="requiredRed"
                                            ErrorMessage="* Required" ControlToValidate="rblSlsInquiry" ></asp:RequiredFieldValidator>
                </div>
                
                
            </div>
            <div class="row">
                <div class="col-sm-4 col-sm-push-4">
                    <asp:RadioButtonList ID="rblSlsInquiry" runat="server">
                        <asp:ListItem>Machine Inquiry</asp:ListItem>
                        <asp:ListItem>Tooling Inquiry</asp:ListItem>
                        <asp:ListItem>Fastener Inquiry</asp:ListItem>
                        <asp:ListItem>Other</asp:ListItem>
                    </asp:RadioButtonList>
                    <asp:Label ID="lblSlsInqOther" runat="server"  Text="">Other:</asp:Label>
                    <asp:TextBox ID="txtSlsInqOther" runat="server" CssClass="form-control"></asp:TextBox>
                </div>
                
            </div>
            <br />
            <br />

            <%--Comments--%>
        <div class="row">
            <div class="col-xs-12">
                <asp:Label runat="server"  AssociatedControlID="txtSlsRComments" CssClass="control-label ">
                    Comments:
                </asp:Label>
            </div>
            <div class="col-xs-12">
                <asp:TextBox ID="txtSlsRComments" CssClass="form-control center-block" runat="server" TextMode="MultiLine" Width="100%" Columns="100" Rows="10"></asp:TextBox>
            </div>    
        </div>

            <br />
            <br />

             <%--GDPR Consent Fail Reminder--%>
            <div class="row">
                 <div class="col-md-1 col-lg-1 col-sm-1 col-xs-12 text-nowrap">
                    
                </div>
                <div class="col-md-8 col-lg-8 col-sm-8 col-xs-12">
                    <span>
                        <asp:Label ID="lblConsentWrn" CssClass="pull-left" runat="server" BorderStyle="Solid" BorderColor="GreenYellow" Text="" Visible="false">
                             <i class="fa fa-arrow-down"></i> Consent must be checked <i class="fa fa-arrow-down"></i>
                        </asp:Label>
                    </span>
                </div>
            </div>
            <%--END GDPR Consent Fail Reminder--%>

            <%--GDPR Consent Checkbox--%>
            <div class="row">
                <div class="col-md-2 col-lg-2 col-sm-2 col-xs-12 text-nowrap">
                    
                </div>
                <div class="col-md-8 col-lg-8 col-sm-8 col-xs-12">
                    <label class="container1">
                         <p>I have read the 
                            <asp:HyperLink ID="hlPrivacy" runat="server" NavigateUrl="/PrivacyPolicy" Target="_blank">Privacy Policy</asp:HyperLink>
                            /
                            <asp:HyperLink ID="hlTOS" runat="server" NavigateUrl="/TOS" Target="_blank">Terms of Use</asp:HyperLink>. 
                            I consent to the collection and use of the personal data submitted on the form above.</p>
                        <input id="gdprConsentBox" runat="server" type="checkbox">
                        <span class="checkmark"></span>
                    </label>
                </div>
            </div>
            <%--END GDPR Consent Checkbox--%>


            <br />
            <br />

            <div class="row">
                <div class="col-sm-4 col-centered">
                    <asp:Button ID="btnSlsRSubmit" runat="server" Text="Submit" CssClass="btn btn-large btn-block btn-primary" Width="100%" OnClick="btnSlsRSubmit_Click" />
                </div>
                
            </div>
            <div class="row">
                <div class="col-sm-4 col-sm-push-4">
                                        <asp:Label ID="lblResult" runat="server" Text=""></asp:Label>


                </div>
            </div>
        </div>
    </div>





</asp:Content>

