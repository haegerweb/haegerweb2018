﻿<%@ Page Title="Distributor Locator" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="DistLocator.aspx.cs" 
    Inherits="Haeger2018.DistLocator" MaintainScrollPositionOnPostback="true" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
     <style>
        span1{
            color:#0085ca;
        }

    </style>



</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
     <div class="container-fluid">
         <div class="row">
        <div class="col-md-8 animated fadeIn animation-delay-3">
                    <h2 class="section-title margin-bottom-0" style="border-bottom: none;">Haeger Distributor Locator</h2>
                    <h3 style="text-transform:initial; color:black; margin:0;">A <span1> PennEngineering&reg</span1> Company</h3>
                </div>
             </div>
        <hr />
        <br />
        <div class="row">
    <div class="col-sm-6 col-sm-push-4 col-md-6 col-md-push-4 col-lg-6 col-lg-push-4 col-xs-12 animated fadeIn animation-delay-5">
         <asp:Label ID="lblUserZip" runat="server" Text="Enter Zip Code:"></asp:Label>
        <asp:TextBox ID="txtUserZip" runat="server" OnTextChanged="txtUserZip_TextChanged"></asp:TextBox>

    </div>
            </div>
        <br />
        <br />
         <div class="row">
     <div class="col-sm-12 col-md-12 col-lg-12">
        <div class="col-sm-12 col-md-12 col-lg-12 hidden-xs animated fadeIn animation-delay-5">
            
  
            
            <div id="map" style="width: 630px; height: 430px; border: solid 3px #0085ca; margin-left:auto; margin-right:auto"></div>
            <!--Use "alert" for debugging javascript-->
            <div id="alert" contenteditable="true" runat="server" Visible="false">Click alerts</div>
            <asp:HiddenField ID="HiddenField1" runat="server" />    

            <a id="anchorId" runat="server" onclick="return true" onserverclick="foo"></a>
            
        </div>
         </div>
             </div>
        <br />
         <br />
             <div class="row">
        <div class="col-sm-12 col-md-12 col-lg-12 hidden-xs">
            <asp:GridView ID="GridView1" CssClass="table table-hover table-striped" runat="server" AutoGenerateColumns="False" DataSourceID="SqlDataSource1" OnSelectedIndexChanged="GridView1_SelectedIndexChanged" >
                <Columns>
                  <%--<asp:ImageField DataImageUrlField="distImageLocation" HeaderText="" ItemStyle-HorizontalAlign="Center" 
                        ItemStyle-VerticalAlign="Middle" ControlStyle-Width="200px" ControlStyle-Height="50px" ItemStyle-Height="" />--%>
                    <asp:BoundField DataField="dist" HeaderText="Distributor" SortExpression="dist" />
                    <asp:BoundField DataField="salesContact" HeaderText="Sales Contact" SortExpression="salesContact" />
                    <asp:BoundField DataField="salesContactEmail" HeaderText="Email" SortExpression="salesContactEmail" DataFormatString="<a href=mailto:{0}>{0}</a>" HtmlEncodeFormatString="False"  />
                    <asp:BoundField DataField="salesContactPhone" HeaderText="Phone Number" SortExpression="salesContactPhone" DataFormatString="{0:(###) ###-####}" />
                    
                   

                </Columns>
            </asp:GridView>
            <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:DistributorZipsConnectionString %>" 
                                SelectCommand="SELECT DISTINCT [dist], [salesContact], [salesContactEmail], [salesContactPhone], [distImageLocation] 
                                                    FROM [DistributorByZip$] WHERE ([stateAB] = @stateAB)">
                <SelectParameters>
                    <asp:ControlParameter ControlID="HiddenField1" Name="stateAB" PropertyName="Value" Type="String" />
                </SelectParameters>
            </asp:SqlDataSource>
        </div>
                 </div>
         <div class="row">
          <div class="col-sm-12 col-md-12 col-lg-12 col-xs-6">
        <asp:GridView ID="GridView2"  CssClass="table table-hover table-striped col-centered" runat="server" AutoGenerateColumns="False" DataSourceID="SqlDataSource2" OnSelectedIndexChanged="GridView2_SelectedIndexChanged">
            <Columns>
                <asp:BoundField DataField="dist" HeaderText="Distributor" SortExpression="dist" />
                <asp:BoundField DataField="salesContact" HeaderText="Sales Contact" SortExpression="salesContact" />
                <asp:BoundField DataField="salesContactEmail" HeaderText="Email" SortExpression="salesContactEmail" DataFormatString="<a href=mailto:{0}>{0}</a>" HtmlEncodeFormatString="False" />
                <asp:BoundField DataField="salesContactPhone" HeaderText="Phone Number" SortExpression="salesContactPhone" DataFormatString="{0:(###) ###-####}" />
            </Columns>
        </asp:GridView>


        <asp:SqlDataSource ID="SqlDataSource2" runat="server" ConnectionString="<%$ ConnectionStrings:DistributorZipsConnectionString %>" SelectCommand="SELECT DISTINCT [dist], [salesContact], [salesContactEmail], [salesContactPhone] FROM [DistributorByZip$] WHERE ([zipCode] = @zipCode)">
            <SelectParameters>
                <asp:ControlParameter ControlID="txtUserZip" Name="zipCode" PropertyName="Text" Type="Double" />
            </SelectParameters>
        </asp:SqlDataSource>

    </div>
             </div>
    </div>
    
</asp:Content>
