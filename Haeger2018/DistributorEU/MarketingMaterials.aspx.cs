﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Haeger2018.DistributorEU
{
    public partial class MarketingMaterials : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void lbtnLogoP2D_Click(object sender, EventArgs e)
        {
            Response.AddHeader("Content-Type", "application/octet-stream");
            Response.AddHeader("Content-Transfer-Encoding", "Binary");
            Response.AddHeader("Content-disposition", "attachment; filename=\"Haeger Logo White No Bkg PE Co.p2d\"");
            Response.WriteFile(HttpRuntime.AppDomainAppPath + @"/DistributorEU/marketing/Logo Haeger/Haeger Logo White No Bkg PE Co.p2d");
            Response.End();
        }
    }
}