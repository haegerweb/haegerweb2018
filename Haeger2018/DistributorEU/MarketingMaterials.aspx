﻿<%@ Page Title="Marketing Materials" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="MarketingMaterials.aspx.cs" Inherits="Haeger2018.DistributorEU.MarketingMaterials" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
    <style>
        span1
        {
            color:#0085ca;
        }

    </style>


</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
     <div class="container-fluid">    
        <div class="section-title">
            <h2 style="margin: 0;">Haeger Marketing Materials - Europe</h2>
            <h3 style="text-transform:initial; color:black; margin:0;">A <span1>PennEngineering&reg</span1> Company</h3>
        </div>
        <div class="row">
            <div class="col-sm-4">
               <h3>Logos</h3>
                <hr />
                 <a href="/DistributorEU/marketing/Logo Haeger/Haeger Bar Red_White Letters_PE Blue_No Bkg.eps">Haeger Bar Red_White Letters_PE Blue_No Bkg.eps</a>
                <br />
                <a href="/DistributorEU/marketing/Logo Haeger/Haeger Bar Red_White Letters_PE Blue_No Bkg.png" target="_blank">Haeger Bar Red_White Letters_PE Blue_No Bkg.png</a>
                <br />
                 <a href="/DistributorEU/marketing/Logo Haeger/Haeger Bar Red_White Letters_PE White_No Bkg.eps">Haeger Bar Red_White Letters_PE White_No Bkg.eps</a>
                <br />
                 <a href="/DistributorEU/marketing/Logo Haeger/Haeger Bar Red_White Letters_PE White_No Bkg.png" target="_blank">Haeger Bar Red_White Letters_PE White_No Bkg.png</a>
                <br />
                 <a href="/DistributorEU/marketing/Logo Haeger/Haeger Logo White No Bkg.png" target="_blank">Haeger Logo White No Bkg.png</a>
                <br />
                 <a href="/DistributorEU/marketing/Logo Haeger/Haeger PEM Logo.jpg" target="_blank">Haeger PEM Logo.jpg</a>
                <br />
                 <a href="/DistributorEU/marketing/Logo Haeger/Haeger Red Bar_Black Letters_PE Blue.eps">Haeger Red Bar_Black Letters_PE Blue.eps</a>
                <br />
                 <a href="/DistributorEU/marketing/Logo Haeger/Haeger Red Bar_Black Letters_PE Blue_No Bkg.png" target="_blank">Haeger Red Bar_Black Letters_PE Blue_No Bkg.png</a>
                <br />
                <a href="/DistributorEU/marketing/Logo Haeger/Haeger_Penn Logo.eps">Haeger_Penn Logo.eps</a>
                <br />
                 <a href="/DistributorEU/marketing/Logo Haeger/Haeger_Penn Logo.jpg" target="_blank">Haeger_Penn Logo.jpg</a>
                <br />
                <a href="/DistributorEU/marketing/Logo Haeger/Haeger_Penn Logo.png" target="_blank">Haeger_Penn Logo.png</a>
                <br />
                 <a href="/DistributorEU/marketing/Logo Haeger/LogoHaeger_Penn_2018_NEW.eps">LogoHaeger_Penn_2018_NEW.eps</a>
                <br />
                <a href="/DistributorEU/marketing/Logo Haeger/LogoHaeger_Penn_2018_NEW.pdf" target="_blank">LogoHaeger_Penn_2018_NEW.pdf</a>
                <br />
                <asp:LinkButton ID="lbtnLogoP2D" runat="server" OnClick="lbtnLogoP2D_Click">Haeger Logo White No Bkg PE Co.p2d</asp:LinkButton>
                <hr />
              
                <%-- <h3>Letterhead</h3>
                <hr />
                 <a href="/DistributorEU/marketing/letterhead/Haeger PEM Letterhead.doc">Haeger PEM Letterhead.doc</a>
                <hr />--%>
                
                <h3>Machine Images</h3>
                <hr />
                <a href="/DistributorEU/marketing/Pictures Machines/350 AUTO FEEDS ON 824OT -4     4457.jpg" target="_blank">350 AUTO FEEDS ON 824OT -4     4457.jpg</a>
                <br />
                <a href="/DistributorEU/marketing/Pictures Machines/350 BOWL & NEW MODULE  4330.jpg" target="_blank">350 BOWL & NEW MODULE  4330.jpg</a>
                <br />
                <a href="/DistributorEU/marketing/Pictures Machines/618_mspE_Insert.jpg" target="_blank">618_mspE_Insert.jpg</a>
                <br />
                <a href="/DistributorEU/marketing/Pictures Machines/618_mspE_Insert.psd">618_mspE_Insert.psd</a>
                <br />
                <a href="/DistributorEU/marketing/Pictures Machines/618_mspE_totaal linksvoor.jpg" target="_blank">618_mspE_totaal linksvoor.jpg</a>
                <br />
                <a href="/DistributorEU/marketing/Pictures Machines/618_mspE_totaal linksvoor.psd">618_mspE_totaal linksvoor.psd</a>
                <br />
                <a href="/DistributorEU/marketing/Pictures Machines/618_mspE_totaal rechtsvoor.jpg" target="_blank">618_mspE_totaal rechtsvoor.jpg</a>
                <br />
                <a href="/DistributorEU/marketing/Pictures Machines/618_mspE_totaal rechtsvoor.psd">618_mspE_totaal rechtsvoor.psd</a>
                <br />
                <a href="/DistributorEU/marketing/Pictures Machines/824-WT-4 RIGHT VIEW heager machine10.jpg" target="_blank">824-WT-4 RIGHT VIEW Haeger machine10.jpg</a>
                <br />
                <a href="/DistributorEU/marketing/Pictures Machines/824-WT-4 RIGHT VIEW heager machine10.psd">824-WT-4 RIGHT VIEW Haeger machine10.psd</a>
                <br />
                <a href="/DistributorEU/marketing/Pictures Machines/824 0T-4  LEFT SIDE heager machine12.jpg" target="_blank">824 0T-4  LEFT SIDE Haeger machine12.jpg</a>
                <br />
                <a href="/DistributorEU/marketing/Pictures Machines/824 0T-4  LEFT SIDE heager machine12.psd">824 0T-4  LEFT SIDE Haeger machine12.psd</a>
                <br />
                <a href="/DistributorEU/marketing/Pictures Machines/824 0T-4  LEFT SIDE heager machine22.jpg" target="_blank">824 0T-4  LEFT SIDE Haeger machine22.jpg</a>
                <br />
                <a href="/DistributorEU/marketing/Pictures Machines/824 0T-4  LEFT SIDE heager machine22.psd">824 0T-4  LEFT SIDE Haeger machine22.psd</a>
                <br />
                <a href="/DistributorEU/marketing/Pictures Machines/824 0T-4 heager machine11.jpg" target="_blank">824 0T-4 Haeger machine11.jpg</a>
                <br />
                <a href="/DistributorEU/marketing/Pictures Machines/824 0T-4 heager machine11.psd">824 0T-4 Haeger machine11.psd</a>
                <br />
                <a href="/DistributorEU/marketing/Pictures Machines/824 One Touch Lite  Dash -4 .jpg" target="_blank">824 One Touch Lite  Dash -4 .jpg</a>
                <br />
                <a href="/DistributorEU/marketing/Pictures Machines/824 One Touch Lite  Dash -4 .psd">824 One Touch Lite  Dash -4 .psd</a>
                <br />
                <a href="/DistributorEU/marketing/Pictures Machines/824 OT-4 CLOSE UP CENTER heager machine27.jpg" target="_blank">824 OT-4 CLOSE UP CENTER Haeger machine27.jpg</a>
                <br />
                <a href="/DistributorEU/marketing/Pictures Machines/824 OT-4 CLOSE UP CENTER heager machine27.psd">824 OT-4 CLOSE UP CENTER Haeger machine27.psd</a>
                <br />
                <a href="/DistributorEU/marketing/Pictures Machines/824 OT-4 RIGHT SIDE heager machine01.jpg" target="_blank">824 OT-4 RIGHT SIDE Haeger machine01.jpg</a>
                <br />
                <a href="/DistributorEU/marketing/Pictures Machines/824 OT-4 RIGHT SIDE heager machine01.psd">824 OT-4 RIGHT SIDE Haeger machine01.psd</a>
                <br />
                <a href="/DistributorEU/marketing/Pictures Machines/824 WT-4   RIGHT SIDE heager machine03.jpg" target="_blank">824 WT-4   RIGHT SIDE Haeger machine03.jpg</a>
                <br />
                <a href="/DistributorEU/marketing/Pictures Machines/824 WT-4   RIGHT SIDE heager machine03.psd">824 WT-4   RIGHT SIDE Haeger machine03.psd</a>
                <br />
                <a href="/DistributorEU/marketing/Pictures Machines/824 WT-4  front Img4429 small.jpg" target="_blank">824 WT-4  front Img4429 small.jpg</a>
                <br />
                <a href="/DistributorEU/marketing/Pictures Machines/824 WT-4  front Img4429.jpg" target="_blank">824 WT-4  front Img4429.jpg</a>
                <br />
                <a href="/DistributorEU/marketing/Pictures Machines/824 WT-4  front Img4429.psd">824 WT-4  front Img4429.psd</a>
                <br />
                <a href="/DistributorEU/marketing/Pictures Machines/824 WT-4 LEFT SIDE heager machine14.jpg" target="_blank">824 WT-4 LEFT SIDE Haeger machine14.jpg</a>
                <br />
                <a href="/DistributorEU/marketing/Pictures Machines/824 WT-4 LEFT SIDE heager machine14.psd ">824 WT-4 LEFT SIDE Haeger machine14.psd</a>
                <br />
                <a href="/DistributorEU/marketing/Pictures Machines/824_mspE_Insert.jpg" target="_blank">824_mspE_Insert.jpg</a>
                <br />
                <a href="/DistributorEU/marketing/Pictures Machines/824_mspE_Insert.psd">824_mspE_Insert.psd</a>
                <br />
                <a href="/DistributorEU/marketing/Pictures Machines/824_mspE_totaal linksvoor.jpg" target="_blank">824_mspE_totaal linksvoor.jpg  </a>
                <br />
                <a href="/DistributorEU/marketing/Pictures Machines/824_mspE_totaal linksvoor.psd">824_mspE_totaal linksvoor.psd  </a>
                <br />
                <a href="/DistributorEU/marketing/Pictures Machines/824_mspE_totaal linksvoorV2.jpg" target="_blank">824_mspE_totaal linksvoorV2.jpg  </a>
                <br />
                <a href="/DistributorEU/marketing/Pictures Machines/824_mspE_totaal linksvoorV2.psd">824_mspE_totaal linksvoorV2.psd  </a>
                <br />
                <a href="/DistributorEU/marketing/Pictures Machines/824_mspE_totaal rechtsvoor.jpg" target="_blank">824_mspE_totaal rechtsvoor.jpg  </a>
                <br />
                <a href="/DistributorEU/marketing/Pictures Machines/824_mspE_totaal rechtsvoor.psd ">824_mspE_totaal rechtsvoor.psd  </a>
                <br />
                <a href="/DistributorEU/marketing/Pictures Machines/824WT -4 MACHINE  4423.jpg" target="_blank">824WT -4 MACHINE  4423.jpg  </a>
                <br />
                <a href="/DistributorEU/marketing/Pictures Machines/842 WT-4 LEFT SIDE heager machine23.jpg" target="_blank">842 WT-4 LEFT SIDE Haeger machine23.jpg  </a>
                <br />
                <a href="/DistributorEU/marketing/Pictures Machines/842 WT-4 LEFT SIDE heager machine23.psd">842 WT-4 LEFT SIDE Haeger machine23.psd  </a>
                <br />
                <a href="/DistributorEU/marketing/Pictures Machines/CLOSE UP OF 824WT-4   4363.jpg" target="_blank">CLOSE UP OF 824WT-4   4363.jpg  </a>
                <br />
                <a href="/DistributorEU/marketing/Pictures Machines/CLOSE UP OF MAS 350   4443.jpg" target="_blank">CLOSE UP OF MAS 350   4443.jpg  </a>
                <br />
                <a href="/DistributorEU/marketing/Pictures Machines/CLOSE UP SHOT SHOWING NEW T-BRACKET CONNECTOR 4346.jpg" target="_blank">CLOSE UP SHOT SHOWING NEW T-BRACKET CONNECTOR 4346.jpg  </a>
                <br />
                <a href="/DistributorEU/marketing/Lineup/Line up machines Haeger.jpg" target="_blank">Line up machines Haeger.jpg</a>
                <br />
                <a href="/DistributorEU/marketing/Lineup/Line up machines Haeger.pdf" target="_blank">Line up machines Haeger.pdf</a>
                <br />
                

            </div>
            <div class="col-sm-4">
               
                <h3>Brochures - High Resolution</h3>
                <hr />
                <a href="/pdf/Machine Brochures HighResolution/618 MSPe - English - HR.pdf" target="_blank">618 MSPe - English - HR.pdf  </a>
                <br />
                <a href="/pdf/Machine Brochures HighResolution/618 MSPe - French - HR.pdf" target="_blank">618 MSPe - French - HR.pdf  </a>
                <br />
                <a href="/pdf/Machine Brochures HighResolution/618 MSPe - German - HR.pdf" target="_blank">618 MSPe - German - HR.pdf  </a>
                <br />
                <a href="/pdf/Machine Brochures HighResolution/618 MSPe - Italian - HR.pdf" target="_blank">618 MSPe - Italian - HR.pdf  </a>
                <br />
                <a href="/pdf/Machine Brochures HighResolution/618 MSPe - Spanish - HR.pdf" target="_blank">618 MSPe - Spanish - HR.pdf  </a>
                <hr />
                <a href="/pdf/Machine Brochures HighResolution/824 MSPe - English - HR.pdf" target="_blank">824 MSPe - English - HR.pdf  </a>
                <br />
                <a href="/pdf/Machine Brochures HighResolution/824 MSPe - French - HR.pdf" target="_blank">824 MSPe - French - HR.pdf  </a>
                <br />
                <a href="/pdf/Machine Brochures HighResolution/824 MSPe - German - HR.pdf" target="_blank">824 MSPe - German - HR.pdf  </a>
                <br />
                <a href="/pdf/Machine Brochures HighResolution/824 MSPe - Italian - HR.pdf" target="_blank">824 MSPe - Italian - HR.pdf  </a>
                <br />
                <a href="/pdf/Machine Brochures HighResolution/824 MSPe - Spanish - HR.pdf" target="_blank">824 MSPe - Spanish - HR.pdf  </a>
                <hr />
                <a href="/pdf/Machine Brochures HighResolution/824 OT-Lite - English - HR.pdf" target="_blank">824 OT-Lite - English - HR.pdf  </a>
                <br />
                <a href="/pdf/Machine Brochures HighResolution/824 OT-Lite - French - HR.pdf" target="_blank">824 OT-Lite - French - HR.pdf  </a>
                <br />
                <a href="/pdf/Machine Brochures HighResolution/824 OT-Lite - German - HR.pdf" target="_blank">824 OT-Lite - German - HR.pdf  </a>
                <br />
                <a href="/pdf/Machine Brochures HighResolution/824 OT-Lite - Italian - HR.pdf" target="_blank">824 OT-Lite - Italian - HR.pdf  </a>
                <br />
                <a href="/pdf/Machine Brochures HighResolution/824 OT-Lite - Spanish - HR.pdf" target="_blank">824 OT-Lite - Spanish - HR.pdf  </a>
                <hr />
                <a href="/pdf/Machine Brochures HighResolution/824 OT4 - English - HR.pdf" target="_blank">824 OT4 - English - HR.pdf  </a>
                <br />
                <a href="/pdf/Machine Brochures HighResolution/824 OT4 - French - HR.pdf" target="_blank">824 OT4 - French - HR.pdf  </a>
                <br />
                <a href="/pdf/Machine Brochures HighResolution/824 OT4 - German - HR.pdf" target="_blank">824 OT4 - German - HR.pdf  </a>
                <br />
                <a href="/pdf/Machine Brochures HighResolution/824 OT4 - Italian - HR.pdf" target="_blank">824 OT4 - Italian - HR.pdf  </a>
                <br />
                <a href="/pdf/Machine Brochures HighResolution/824 OT4 - Spanish - HR.pdf" target="_blank">824 OT4 - Spanish - HR.pdf  </a>
                <hr />
                <a href="/pdf/Machine Brochures HighResolution/824 WT4 - English - HR.pdf" target="_blank">824 WT4 - English - HR.pdf  </a>
                <br />
                <a href="/pdf/Machine Brochures HighResolution/824 WT4 - French - HR.pdf" target="_blank">824 WT4 - French - HR.pdf  </a>
                <br />
                <a href="/pdf/Machine Brochures HighResolution/824 WT4 - German - HR.pdf" target="_blank">824 WT4 - German - HR.pdf  </a>
                <br />
                <a href="/pdf/Machine Brochures HighResolution/824 WT4 - Italian - HR.pdf" target="_blank">824 WT4 - Italian - HR.pdf  </a>
                <br />
                <a href="/pdf/Machine Brochures HighResolution/824 WT4 - Spanish - HR.pdf" target="_blank">824 WT4 - Spanish - HR.pdf  </a>
                <hr />
                <a href="/pdf/Machine Brochures HighResolution/824 XYZ - English - HR.pdf" target="_blank">824 XYZ - English - HR.pdf  </a>
                <br />
                <a href="/pdf/Machine Brochures HighResolution/824 XYZ - French - HR.pdf" target="_blank">824 XYZ - French - HR.pdf  </a>
                <br />
                <a href="/pdf/Machine Brochures HighResolution/824 XYZ - German - HR.pdf" target="_blank">824 XYZ - German - HR.pdf  </a>
                <br />
                <a href="/pdf/Machine Brochures HighResolution/824 XYZ - Italian - HR.pdf" target="_blank">824 XYZ - Italian - HR.pdf  </a>
                <br />
                <a href="/pdf/Machine Brochures HighResolution/824 XYZ - Spanish - HR.pdf" target="_blank">824 XYZ - Spanish - HR.pdf  </a>
                <br />
                <br />




            </div>
            <div class="col-sm-4">
               
                 
                
                 <h3>Brochures - Low Resolution</h3>
                <hr />
                <a href="/pdf/Machine Brochures LowResolution/618 MSPe - English - LR.pdf" target="_blank">618 MSPe - English - LR.pdf  </a>
                <br />
                <a href="/pdf/Machine Brochures LowResolution/618 MSPe - French - LR.pdf" target="_blank">618 MSPe - French - LR.pdf  </a>
                <br />
                <a href="/pdf/Machine Brochures LowResolution/618 MSPe - German - LR.pdf" target="_blank">618 MSPe - German - LR.pdf  </a>
                <br />
                <a href="/pdf/Machine Brochures LowResolution/618 MSPe - Italian - LR.pdf" target="_blank">618 MSPe - Italian - LR.pdf  </a>
                <br />
                <a href="/pdf/Machine Brochures LowResolution/618 MSPe - Spanish - LR.pdf" target="_blank">618 MSPe - Spanish - LR.pdf  </a>
                <hr />
                <a href="/pdf/Machine Brochures LowResolution/824 MSPe - English - LR.pdf" target="_blank">824 MSPe - English - LR.pdf  </a>
                <br />
                <a href="/pdf/Machine Brochures LowResolution/824 MSPe - French - LR.pdf" target="_blank">824 MSPe - French - LR.pdf  </a>
                <br />
                <a href="/pdf/Machine Brochures LowResolution/824 MSPe - German - LR.pdf" target="_blank">824 MSPe - German - LR.pdf  </a>
                <br />
                <a href="/pdf/Machine Brochures LowResolution/824 MSPe - Italian - LR.pdf" target="_blank">824 MSPe - Italian - LR.pdf  </a>
                <br />
                <a href="/pdf/Machine Brochures LowResolution/824 MSPe - Spanish - LR.pdf" target="_blank">824 MSPe - Spanish - LR.pdf  </a>
                <hr />
                <a href="/pdf/Machine Brochures LowResolution/824 OT-Lite - English - LR.pdf" target="_blank">824 OT-Lite - English - LR.pdf  </a>
                <br />
                <a href="/pdf/Machine Brochures LowResolution/824 OT-Lite - French - LR.pdf" target="_blank">824 OT-Lite - French - LR.pdf  </a>
                <br />
                <a href="/pdf/Machine Brochures LowResolution/824 OT-Lite - German - LR.pdf" target="_blank">824 OT-Lite - German - LR.pdf  </a>
                <br />
                <a href="/pdf/Machine Brochures LowResolution/824 OT-Lite - Italian - LR.pdf" target="_blank">824 OT-Lite - Italian - LR.pdf  </a>
                <br />
                <a href="/pdf/Machine Brochures LowResolution/824 OT-Lite - Spanish - LR.pdf" target="_blank">824 OT-Lite - Spanish - LR.pdf  </a>
                <hr />
                <a href="/pdf/Machine Brochures LowResolution/824 OT4 - English - LR.pdf" target="_blank">824 OT4 - English - LR.pdf  </a>
                <br />
                <a href="/pdf/Machine Brochures LowResolution/824 OT4 - French - LR.pdf" target="_blank">824 OT4 - French - LR.pdf  </a>
                <br />
                <a href="/pdf/Machine Brochures LowResolution/824 OT4 - German - LR.pdf" target="_blank">824 OT4 - German - LR.pdf  </a>
                <br />
                <a href="/pdf/Machine Brochures LowResolution/824 OT4 - Italian - LR.pdf" target="_blank">824 OT4 - Italian - LR.pdf  </a>
                <br />
                <a href="/pdf/Machine Brochures LowResolution/824 OT4 - Spanish - LR.pdf" target="_blank">824 OT4 - Spanish - LR.pdf  </a>
                <hr />
                <a href="/pdf/Machine Brochures LowResolution/824 WT4 - English - LR.pdf" target="_blank">824 WT4 - English - LR.pdf  </a>
                <br />
                <a href="/pdf/Machine Brochures LowResolution/824 WT4 - French - LR.pdf" target="_blank">824 WT4 - French - LR.pdf  </a>
                <br />
                <a href="/pdf/Machine Brochures LowResolution/824 WT4 - German - LR.pdf" target="_blank">824 WT4 - German - LR.pdf  </a>
                <br />
                <a href="/pdf/Machine Brochures LowResolution/824 WT4 - Italian - LR.pdf" target="_blank">824 WT4 - Italian - LR.pdf </a>
                <br />
                <a href="/pdf/Machine Brochures LowResolution/824 WT4 - Spanish - LR.pdf" target="_blank">824 WT4 - Spanish - LR.pdf </a>
                <hr />
                <a href="/pdf/Machine Brochures LowResolution/824 XYZ - English - LR.pdf" target="_blank">824 XYZ - English - LR.pdf  </a>
                <br />
                <a href="/pdf/Machine Brochures LowResolution/824 XYZ - French - LR.pdf" target="_blank">824 XYZ - French - LR.pdf  </a>
                <br />
                <a href="/pdf/Machine Brochures LowResolution/824 XYZ - German - LR.pdf" target="_blank">824 XYZ - German - LR.pdf  </a>
                <br />
                <a href="/pdf/Machine Brochures LowResolution/824 XYZ - Italian - LR.pdf" target="_blank">824 XYZ - Italian - LR.pdf  </a>
                <br />
                <a href="/pdf/Machine Brochures LowResolution/824 XYZ - Spanish - LR.pdf" target="_blank">824 XYZ - Spanish - LR.pdf  </a>
                <br />
                <br />
                
                <br />
                <br />
                <br />
                <hr />
                


            </div>
        </div>
    </div>




</asp:Content>
