﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Haeger2018.DistributorEU
{
    public partial class DistributorEUPage : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnMarketing_Click(object sender, EventArgs e)
        {
            Response.Redirect("/DistributorEU/MarketingMaterials");
        }

        protected void btnStdProp_Click(object sender, EventArgs e)
        {
            Response.Redirect("/DistributorEU/StandardProposalsEU");
        }
    }
}