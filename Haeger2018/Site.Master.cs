﻿using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Security.Principal;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.AspNet.Identity;

namespace Haeger2018
{
    public partial class SiteMaster : MasterPage
    {
        private const string AntiXsrfTokenKey = "__AntiXsrfToken";
        private const string AntiXsrfUserNameKey = "__AntiXsrfUserName";
        private string _antiXsrfTokenValue;

        protected void Page_Init(object sender, EventArgs e)
        {
            // The code below helps to protect against XSRF attacks
            var requestCookie = Request.Cookies[AntiXsrfTokenKey];
            Guid requestCookieGuidValue;
            if (requestCookie != null && Guid.TryParse(requestCookie.Value, out requestCookieGuidValue))
            {
                // Use the Anti-XSRF token from the cookie
                _antiXsrfTokenValue = requestCookie.Value;
                Page.ViewStateUserKey = _antiXsrfTokenValue;
            }
            else
            {
                // Generate a new Anti-XSRF token and save to the cookie
                _antiXsrfTokenValue = Guid.NewGuid().ToString("N");
                Page.ViewStateUserKey = _antiXsrfTokenValue;

                var responseCookie = new HttpCookie(AntiXsrfTokenKey)
                {
                    HttpOnly = true,
                    Value = _antiXsrfTokenValue
                };
                if (FormsAuthentication.RequireSSL && Request.IsSecureConnection)
                {
                    responseCookie.Secure = true;
                }
                Response.Cookies.Set(responseCookie);
            }

            Page.PreLoad += master_Page_PreLoad;
        }

        protected void master_Page_PreLoad(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                // Set Anti-XSRF token
                ViewState[AntiXsrfTokenKey] = Page.ViewStateUserKey;
                ViewState[AntiXsrfUserNameKey] = Context.User.Identity.Name ?? String.Empty;
            }
            else
            {
                // Validate the Anti-XSRF token
                if ((string)ViewState[AntiXsrfTokenKey] != _antiXsrfTokenValue
                    || (string)ViewState[AntiXsrfUserNameKey] != (Context.User.Identity.Name ?? String.Empty))
                {
                    throw new InvalidOperationException("Validation of Anti-XSRF token failed.");
                }
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (HttpContext.Current.User.IsInRole("RootAdmin"))
            {
                adminLink.Visible = true;
            }
            else if (HttpContext.Current.User.IsInRole("Admin"))
            {
                adminLink.Visible = true;
            }
            else if (HttpContext.Current.User.IsInRole("DistAdminNA"))
            {
                distAdminNALink.Visible = true;
                
            }
            else if (HttpContext.Current.User.IsInRole("PartOutSalesNA"))
            {
                ptrSalesOutNALink.Visible = true;

            }
            else if (HttpContext.Current.User.IsInRole("PartnerNA"))
            {
                ParnterNALink.Visible = true;

            }
            else if (HttpContext.Current.User.IsInRole("ServiceNA"))
            {
                ServiceCommunityLink.Visible = true;

            }
            else if (HttpContext.Current.User.IsInRole("ServiceEU"))
            {
                ServiceCommunityLink.Visible = true;

            }
            else if (HttpContext.Current.User.IsInRole("ServiceAS"))
            {
                ServiceCommunityLink.Visible = true;

            }
            else if (HttpContext.Current.User.IsInRole("ServiceNACoordinator"))
            {
                ServiceNACoordinatorLink.Visible = true;

            }
            else if (HttpContext.Current.User.IsInRole("DistAdminEU"))
            {
                distAdminEULink.Visible = true;

            }
            else if (HttpContext.Current.User.IsInRole("DistAdminAS"))
            {
                distAdminASLink.Visible = true;

            }

            else if (HttpContext.Current.User.IsInRole("DistributorNA"))
            {
                distNALink.Visible = true;

            }
            else if (HttpContext.Current.User.IsInRole("DistributorEU"))
            {
                distEULink.Visible = true;
            }
            else if (HttpContext.Current.User.IsInRole("DistributorAS"))
            {
                distASLink.Visible = true;
            }
            else if (HttpContext.Current.User.IsInRole("CustomerNA"))
            {
                custNALink.Visible = true;

            }
            else if (HttpContext.Current.User.IsInRole("CustomerEU"))
            {
                custEULink.Visible = true;
            }
            else if (HttpContext.Current.User.IsInRole("CustomerAS"))
            {
                custASLink.Visible = true;
            }

        }

        protected void Unnamed_LoggingOut(object sender, LoginCancelEventArgs e)
        {
            Context.GetOwinContext().Authentication.SignOut(DefaultAuthenticationTypes.ApplicationCookie);
        }
    }

}