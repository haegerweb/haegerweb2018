﻿<%@ Page Title="Email Sent" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="SubmitSucess.aspx.cs" Inherits="Haeger2018.SubmitSucess" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <div class="container-fluid">
        <br />
        <br />
        <div class="row">
             <div class="col-sm-5 col-centered animated zoomIn animation-delay-3" style="text-align:center">
                <h1><i class="fa fa-envelope"></i>&nbsp;&nbsp;&nbsp;Your Email Has Been Sent</h1>
            </div>
        </div>
       <br />
       <div class="row">
            <div class="col-sm-5 col-centered animated zoomIn animation-delay-3" style="text-align:center">
                <asp:Literal ID="ltrConfirmation" runat="server"></asp:Literal>

            </div>
       </div>
       <br />
       <br />
        <div class="row">
            <div class="col-sm-5 col-centered">
                
                <asp:Button ID="btnSuccessOK" runat="server" Text="OK" 
                                class="btn btn-large btn-block btn-primary animated zoomIn animation-delay-7" OnClick="btnSuccessOK_Click" />
                   
            </div>


        </div>
       

    </div>
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />

</asp:Content>
