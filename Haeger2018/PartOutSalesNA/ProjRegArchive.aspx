﻿<%@ Page Title="Project Archive" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ProjRegArchive.aspx.cs" Inherits="Haeger2018.PartOutSalesNA.ProjRegArchive" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <div class="container-fluid">    
        <div class="section-title">
            <h2 style="margin: 0;">Haeger North America Project Archive</h2>
            <h3 style="text-transform:initial; color:black; margin:0;">A <span1> PennEngineering&reg</span1> Company</h3>
        </div>
        <br />
          <br />
          <div class="row">
             <div class="col-xs-12">
                <asp:Button ID="btnBackMain" class="btn btn-primary" runat ="server" Text="Back to Main" OnClick="btnBackMain_Click" Width="220px"  />
             </div>
         </div>
        <br />
        <div class="row">
            <div class="col-xs-12">
                <asp:GridView ID="grdProjArch" CssClass="table table-striped table-bordered table-condensed" EmptyDataText="No Projects Archived" style="align-content:flex-start" runat="server" AutoPostBack="True" AutoGenerateColumns="False" DataKeyNames="column1" DataSourceID="sqlProjArch" OnSelectedIndexChanged="grdProjArch_SelectedIndexChanged">
                    <Columns>
                        <asp:CommandField ShowDeleteButton="True" DeleteText="Restore"/>
                        <asp:BoundField DataField="column1" HeaderText="column1" ReadOnly="True" SortExpression="column1" Visible="false" />
                        <asp:BoundField DataField="company" HeaderText="Company" SortExpression="company" />
                        <asp:BoundField DataField="distributorName" HeaderText="Distributor" SortExpression="distributorName" />
                        <asp:BoundField DataField="contactName" HeaderText="Contact Name" SortExpression="contactName" />
                        <asp:BoundField DataField="salesPerson" HeaderText="Sales Person" SortExpression="salesPerson" />
                        <asp:BoundField DataField="projectMachineType" HeaderText="Machine Type" SortExpression="projectMachineType" />
                        <asp:BoundField DataField="priorityMonthToClose" HeaderText="Priority Month To Close" SortExpression="priorityMonthToClose" />
                        
                    </Columns>
                    
                </asp:GridView>
                <asp:SqlDataSource ID="sqlProjArch" runat="server" ConnectionString="<%$ ConnectionStrings:ProjRegConnectionString %>" 
                    SelectCommand="SELECT [_projID] AS column1, [company], [distributorName], [contactName], [salesPerson], [projectMachineType], [priorityMonthToClose] 
                                    FROM [ProjRegData] WHERE ([showNoShow] = @showNoShow)" 
                    DeleteCommand="UPDATE [ProjRegData] SET [showNoShow] = 'Show'  WHERE [_projID] = @original_column1"
                    OldValuesParameterFormatString="original_{0}" 
                    InsertCommand="INSERT INTO [ProjRegData] ([_projID], [company], [distributorName], [contactName], [salesPerson], [projectMachineType], [priorityMonthToClose]) 
                                    VALUES (@column1, @company, @distributorName, @contactName, @salesPerson, @projectMachineType, @priorityMonthToClose)" 
                    UpdateCommand="UPDATE [ProjRegData] 
                                    SET [company] = @company, [distributorName] = @distributorName, [contactName] = @contactName, 
                                        [salesPerson] = @salesPerson, [projectMachineType] = @projectMachineType, [priorityMonthToClose] = @priorityMonthToClose 
                                    WHERE [_projID] = @column1">
                    <DeleteParameters>
                        <asp:Parameter Name="column1" Type="String" />
                    </DeleteParameters>
                    <InsertParameters>
                        <asp:Parameter Name="column1" Type="String" />
                        <asp:Parameter Name="company" Type="String" />
                        <asp:Parameter Name="distributorName" Type="String" />
                        <asp:Parameter Name="contactName" Type="String" />
                        <asp:Parameter Name="salesPerson" Type="String" />
                        <asp:Parameter Name="projectMachineType" Type="String" />
                        <asp:Parameter Name="priorityMonthToClose" Type="String" />
                    </InsertParameters>
                    <SelectParameters>
                        <asp:Parameter DefaultValue="NoShow" Name="showNoShow" Type="String" />
                    </SelectParameters>
                    <UpdateParameters>
                        <asp:Parameter Name="company" Type="String" />
                        <asp:Parameter Name="distributorName" Type="String" />
                        <asp:Parameter Name="contactName" Type="String" />
                        <asp:Parameter Name="salesPerson" Type="String" />
                        <asp:Parameter Name="projectMachineType" Type="String" />
                        <asp:Parameter Name="priorityMonthToClose" Type="String" />
                        <asp:Parameter Name="column1" Type="String" />
                    </UpdateParameters>
                </asp:SqlDataSource>


            </div>


        </div>













    </div>



</asp:Content>
