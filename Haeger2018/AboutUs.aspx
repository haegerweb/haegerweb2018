﻿<%@ Page Title="About Us" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="AboutUs.aspx.cs" Inherits="Haeger2018.AboutUs" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
     <style>

        span1
        {
            color:#0085ca;
        }
        hr { 
                display: block;
                margin-top: 0.5em;
                margin-bottom: 0.5em;
                margin-left: auto;
                margin-right: auto;
                border-style: inset;
                border-width: 1px;
            }
    </style>

   

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
     <!--About Haeger -->
    <section class="animated fadeIn margin-bottom-10">
      <div class="container">
        <div class="row">
          <div class="col-md-8 col-sm-6">
            <div data-menu="airModeMenuOpt"></div>
            <div class="summernote">
               <div class="section-title">
                    <h2 style="margin: 0;">Haeger</h2>
                    <h3 style="text-transform:initial; color:black; margin:0;">A <span1> PennEngineering&reg</span1> Company</h3>
                    <hr />
                   <img src="/images/haeger-office.jpg" class="alignleft" alt="Image" />
               </div>
             
              <div class="clearfix" style="margin:0">
                
                  
                
              </div>
                <div class="clearfix">
                    <p>
                      Haeger, <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl="https://www.pemnet.com/" Target="_blank">a <span>PennEngineering&reg Company</span></asp:HyperLink>,  manufactures hardware insertion machines, we are the world leader in the development of innovative fastener 
                      insertion technology solutions. Haeger has introduced its full line of machines which provide our SingleTouch Part Handling Technology. 
                      These machines allow the insertion of up to four different fasteners in a single handling of a part. 
                      The Technology offers the single most significant boost to hardware insertion productivity since the introduction of automatic tooling systems.
                      Haeger machines Create Hardware Insertion Profit Centers™ by optimizing labor, improving quality, and increasing productivity through the use of 
                      technology.
                      
                  </p>
                </div>
              <div class="clearfix">
                 <%-- <a href="https://www.pemnet.com/" target="_blank">
                <img src="/images/PEMlogo.png" class="alignright imageborder" alt="Image" width="200"/>
                      </a>--%>
                  <p>
                      For nearly 40 years, Haeger has pioneered and manufactured machines and tooling systems for inserting nearly every type and size of self-clinching 
                      fastener imaginable. And, the development continues – we’re constantly creating new technologies for inserting nearly every type and size of 
                      self-clinching fasteners into all varieties of materials. Haeger’s engineers can solve insertion challenges that no one else can, whether 
                      it’s engineering, production, technical service or sales and marketing. Our staff brings experience, expertise, and diversity of talent 
                      second to none.

                  </p>
                  <p>
                      In 2018 Haeger, Inc. became part of <asp:HyperLink ID="HyperLink2" runat="server" NavigateUrl="https://www.pemnet.com/" Target="_blank"><span1>PennEngineering&reg</span1></asp:HyperLink>. 

                  </p>
                
              </div>
            </div>

             </div>
              
          <div class="col-md-4 ">
            <a class="btn btn-primary btn-block animated bounceInRight animation-delay-5 margin-bottom-10" href="ContactUs.aspx">
              <div class="row row-vertical-align row-no-pad">
                <div class="col-md-3">
                </div>
                <div class="col-md-1">
                  <i class="fa fa-phone-square pull-right fa-2x"></i>
                </div>
                <div class="col-md-5">
                  <div style="margin-left: 10px; text-align: left;">
                    Request Information Now<br> USA 1-800-878-4343 <br> Europe +31-541-530-230 <br> China +86-21-5695-4988
                  </div>
                </div>
                <div class="col-md-3">
                </div>
              </div>
            </a>

            <a class="btn btn-black btn-block animated bounceInRight animation-delay-7" href="https://itunes.apple.com/us/app/haeger-wizard/id625478700?mt=8&amp;uo=4" target="_itunes">
              <div class="row row-vertical-align row-no-pad">
                <div class="col-md-6">
                  <span style="font-size: 18px;">Haeger Wizard</span>
                </div>
                <div class="col-md-5">
                  <img src="/images/appstore.png">
                </div>
                <div class="col-md-1">
                </div>
              </div>
            </a>
               <a class="btn btn-black btn-block animated bounceInRight animation-delay-8" href="https://play.google.com/store/apps/details?id=edu.haeger.haegerwizard&hl=en" target="_googlePlay">
              <div class="row row-vertical-align row-no-pad">
                <div class="col-md-6">
                  <span style="font-size: 18px;">Haeger Wizard</span>
                </div>
                <div class="col-md-5">
                  <img src="/images/googleplay.png">
                </div>
                <div class="col-md-1">
                </div>
              </div>
            </a>
           <%-- <div class="tile nosize green animated bounceInRight animation-delay-9 margin-top-10">
              <div id="mc_embed_signup">
                  <h4 style="padding:10px;">Sign Up for the Haeger E-Newsletter</h4>
                  <h5><b><span class="asterisk">*</span> indicates required</b></h5>
                  
                    <div class="form-group row col-centered" >
                        <div class="col-xs-4">
                            <asp:Label ID="lblEmail" runat="server" Text="">Email:</asp:Label>
                        </div>
                        <div class="col-xs-8">
                            <asp:TextBox ID="txtNLEmail" runat="server" Cssclass="form-control"  placeholder="Enter email" ></asp:TextBox>
                        </div>
                    </div>
                   <br />
                    <div class="form-group row col-centered">
                        <div class="col-xs-4">
                            <asp:Label ID="lblNLFirstName" runat="server" Text="">First Name:</asp:Label>
                        </div>
                        <div class="col-xs-8">
                            <asp:TextBox ID="txtNLFirstName" runat="server" Cssclass="form-control" placeholder="Enter First Name" ></asp:TextBox>
                        </div>
                    </div>
                    <br />
                    <div class="form-group row col-centered">
                        <div class="col-xs-4">
                            <asp:Label ID="lblNLLastName" runat="server" Text="">Last Name:</asp:Label>
                        </div>
                        <div class="col-xs-8">
                            <asp:TextBox ID="txtNLLastName" runat="server" Cssclass="form-control" placeholder="Enter Last Name" ></asp:TextBox>
                        </div>
                    </div>
                     <br />
                    <div class="form-group row col-centered">
                        <div class="col-xs-4">
                            <asp:Label ID="lblNLCompany" runat="server" Text="">Company:</asp:Label>
                        </div>
                        <div class="col-xs-8">
                            <asp:TextBox ID="txtNLCompany" runat="server" Cssclass="form-control" placeholder="Enter Company" ></asp:TextBox>
                        </div>
                    </div>
                   <br />
                    <div class="form-group row col-centered">
                        <div class="col-xs-4">
                            <asp:Label ID="lblNLCountry" runat="server" Text="">Country:</asp:Label>
                        </div>
                        <div class="col-xs-8">
                            <asp:TextBox ID="txtNLCountry" runat="server" Cssclass="form-control" placeholder="Enter Country" ></asp:TextBox>
                        </div>
                    </div>
                   <br />
                    <div class="form-group row col-centered">
                        <div class="col-xs-4">
                            <asp:Label ID="lblNLCity" runat="server" Text="">City:</asp:Label>
                        </div>
                        <div class="col-xs-8">
                            <asp:TextBox ID="txtNLCity" runat="server" Cssclass="form-control" placeholder="Enter Country" ></asp:TextBox>
                        </div>
                    </div>
                   <br />
                    <div class="form-group row col-centered">
                        <div class="col-xs-4">
                            <asp:Label ID="lblNLState" runat="server" Text="">State:</asp:Label>
                        </div>
                        <div class="col-xs-8">
                            <asp:TextBox ID="txtNLState" runat="server" Cssclass="form-control" placeholder="Enter State" ></asp:TextBox>
                        </div>
                    </div>
                  <br />
                    <div class="form-group row col-centered">
                        <div class="col-xs-4">
                            <asp:Label ID="lblPosition" runat="server" Text="">Position:</asp:Label>
                        </div>
                        <div class="col-xs-8">
                            <asp:TextBox ID="txtPosition" runat="server" Cssclass="form-control" placeholder="Enter Position" ></asp:TextBox>
                        </div>
                    </div>
                 <br />
                  <div class="form-group row">
                    <span class="input-group-btn">
                        <asp:Button ID="btnNewsLetterSubmit" class="btn btn-success" runat="server" Text="Submit" OnClick="btnNewsLetterSubmit_Click" />
                    </span>
                  </div>
              </div>
            </div>--%>
          </div>
        </div>
      </div>
      <!-- container -->
    </section>

    <section class="animated fadeIn margin-bottom-10">
      <div class="container">
      </div>
    </section>


</asp:Content>
