﻿<%@ Page Title="Standard Proposals USA" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="StandardProposalsUS.aspx.cs" Inherits="Haeger2018.DistributorNA.StandardProposalsUS" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
         <style>
        span1{
            color:#0085ca;
        }

        .distLinks{
            font-weight:bold;
            font-size:18px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div class="container-fluid">
        <div class="section-title">
            <h2 style="margin: 0;">Haeger North American Standard Proposals</h2>
            <h3 style="text-transform:initial; color:black; margin:0;">A <span1> PennEngineering&reg</span1> Company</h3>
        </div>
        <div class="row">
            <div class="col-sm-4">

            </div>
            <div class="col-sm-4">

            </div>
            <div class="col-sm-4">
                <h3>Proposals</h3>
                <hr />
                <a href="/DistributorNA/excelNA/2018MachineStandardProposalsNA/PS4-A-Master-February-2019.xlsx">PS4-A February 2019</a>
                <br />
                <a href="/DistributorNA/excelNA/2018MachineStandardProposalsNA/618MSPe-February-2019.xlsx">618 MSPe February 2019</a>
                <br />
                <a href="/DistributorNA/excelNA/2018MachineStandardProposalsNA/618MSPe-He-575-for-Canada-March-2018.xlsx">618 MSPe He 575 for Canada  March 2018</a>
                <br />
                <a href="/DistributorNA/excelNA/2018MachineStandardProposalsNA/618Plus-February-2019.xlsx">618 Plus February 2019</a>
                <br />
                <a href="/DistributorNA/excelNA/2018MachineStandardProposalsNA/618Plus-575-March-2018.xlsx">618 Plus 575 March 2018</a>
                <br />
                <a href="/DistributorNA/excelNA/2018MachineStandardProposalsNA/736MSPe-March-2018.xlsx">736 MSPe March 2018</a>
                <br />
                <a href="/DistributorNA/excelNA/2018MachineStandardProposalsNA/736Plus-March-2018.xlsx">736 Plus March 2018</a>
                <br />
                <a href="/DistributorNA/excelNA/2018MachineStandardProposalsNA/824MSPe-February-2019.xlsx">824 MSPe February 2019</a>
                <br />
                <a href="/DistributorNA/excelNA/2018MachineStandardProposalsNA/824WT4e-February-2019.xlsx">824 WT4e February 2019</a>
                <br />
                <a href="/DistributorNA/excelNA/2018MachineStandardProposalsNA/824OTL-4e-Lite-February-2019.xlsx">824 OT4e Lite February 2019</a>
                <br />
                <a href="/DistributorNA/excelNA/2018MachineStandardProposalsNA/824OT4e-February-2019.xlsx">824 OT4e February 2019</a>
                <br />
                <a href="/DistributorNA/excelNA/2018MachineStandardProposalsNA/840MSPe-March-2018.xlsx">840 OT4e March 2018</a>
                <br />
                <a href="/DistributorNA/excelNA/2018MachineStandardProposalsNA/2015MSPe-March-2018.xlsx">2015 MSPe March 2018</a>
                <br />
                <a href="/DistributorNA/excelNA/2018MachineStandardProposalsNA/2015-PLUS-March-2018.xls">2015 PLUS March 2018</a>
                <br />
                <a href="/DistributorNA/excelNA/2018MachineStandardProposalsNA/2015MSPe-March-2018.xlsx">2015 MSPe March 2018</a>
                <br />
                <a href="/DistributorNA/excelNA/2018MachineStandardProposalsNA/Retrofit-9-inch-Auto-Feed-System-March-2018.xlsx">Retrofit 9 inch Auto Feed System March 2018</a>
                <br />
                <a href="/DistributorNA/excelNA/2018MachineStandardProposalsNA/Standard-Proposal-TIS-1-March-2018.xls">TIS-1 March 2018</a>
                <br />
                <h3>Brochures</h3>
                <hr />
                <a href="/pdf/Machine Brochures LowResolution/618 MSPe - English - LR.pdf" target="_blank">618 MSPe Brochure</a>
                <br />
                <a href="/pdf/Machine Brochures LowResolution/824 XYZ - English - LR.pdf" target="_blank">824 MSPe Brochure</a>
                <br />
                <a href="/pdf/Brochures/Folder-824-WT-8-pag-LR1.pdf" target="_blank">824 WT4e Brochure</a>
                <br />
                <a href="/pdf/Brochures/Folder-824-OT-LITE-LR.pdf" target="_blank">824 OT4e Lite Brochure</a>
                <br />
                <a href="/pdf/Brochures/Folder-824-OT-4-pag-LR1.pdf" target="_blank">824 OT4e Brochure</a>
                <br />
                <a href="/pdf/Brochures/Folder-824-4E-XYZ-R.pdf" target="_blank">XYZ-R Brochure</a>
                <br />
                <hr />
                <a href="/DistributorNA/MachineDimensions.aspx">Machine Dimensions</a>
                <br />
                <hr />
                <a href="/DistributorNA/miscNA/824-WT-4e-and-824-OT-4e-rev.ppt">Dash 4 Benefits PowerPoint</a>
                <br />
                <a href="/DistributorNA/miscNA/Energy-savings-calculation-usa.xls">Energy Savings</a>
                <br />
                <a href="/DistributorNA/miscNA/Time-Study-Haeger-2016-All.xls">Time Study - All</a>
                <br />
            </div>
        </div>
    </div>
</asp:Content>
