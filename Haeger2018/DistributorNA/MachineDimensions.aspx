﻿<%@ Page Title="Machine Dimensions" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="MachineDimensions.aspx.cs" Inherits="Haeger2018.DistributorNA.MachineDimensions" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
    <style>
        span1{
            color:#0085ca;
        }

        .distLinks{
            font-weight:bold;
            font-size:18px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div class="container-fluid">
        <div class="section-title">
            <h2 style="margin: 0;">Haeger North American Machine Dimensions</h2>
            <h3 style="text-transform:initial; color:black; margin:0;">A <span1> PennEngineering&reg</span1> Company</h3>
        </div>
        <div class="row">
            <div class="col-sm-4">

            </div>
            <div class="col-sm-4">

            </div>
            <div class="col-sm-4">
                <h3>Quick Directory</h3>
                <hr />
                <a href="/DistributorNA/machineDimensions/618-GENERAL-DIMENSIONS.pdf" target="_blank">618 General Dimensions</a>
                <br />
                <a href="/DistributorNA/machineDimensions/618Plus-machine-layout.pdf" target="_blank">618 Plus Machine Layout</a>
                <br />
                <a href="/DistributorNA/machineDimensions/824-OT-3-ASSY.pdf" target="_blank">824 OT-3 Assembly</a>
                <br />
                <a href="/DistributorNA/machineDimensions/824PlusMain.pdf" target="_blank">824 Plus</a>
                <br />
                <a href="/DistributorNA/machineDimensions/824wt4etop.pdf">824 WT4e Top</a>
                <br />
                <a href="/DistributorNA/machineDimensions/824wt4eside.pdf" target="_blank">824 WT4e Side</a>
                <br />

                
            </div>
        </div>
    </div>
</asp:Content>
