﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;


namespace Haeger2018
{
    public partial class RequestRMA : System.Web.UI.Page
    {
        SqlConnection rmaConn = new SqlConnection(ConfigurationManager.ConnectionStrings["OptInConsentConnectionString"].ConnectionString);
        DateTime localDate = DateTime.Now;

        protected void Page_Load(object sender, EventArgs e)
        {
            Page.MaintainScrollPositionOnPostBack = true;

        }

        protected void btnRMASubmit_Click(object sender, EventArgs e)
        {
            var msg = "Without your consent we cannot process your request.";

            // First check to warn user 
            if (!gdprConsentBox.Checked)
            {
                lblConsentWrn.Visible = true;

                Response.Write("<script>alert('" + msg + "')</script>");

                return;
            }


            // *************** RFQ Form Variables ***************
            string name = txtName.Text.Trim();
            string email = txtEmail.Text.Trim();
            string shipAddress = txtShipAddress.Text.Trim();
            string machSerial = txtMachineSerial.Text.Trim();


            // *************** INSERT DATABASE ***************

            rmaConn.Open();

            // Query and commands to retrieve max tracking number
            string trackNumQuery = "SELECT MAX(trackingNum) FROM RmaConsent";
            SqlCommand trackNumCommand = new SqlCommand(trackNumQuery, rmaConn);
            string trackNum = "";
            using (SqlDataReader tn = trackNumCommand.ExecuteReader())
            {
                while (tn.Read())
                {
                    trackNum = tn[0].ToString();
                }
            }

            // Add one to max tracking number and creating new _projID
            int nextIDnum = (Int32.Parse(trackNum)) + 1;
            string rmaID = "rfq" + nextIDnum.ToString();

            string insertQuery =
            "INSERT INTO [dbo].[RmaConsent] " +
            "(_rmaID, name, email, shipAddress, machSerial, gdprConsent, dateCreated) " +
            "VALUES (@rmaID, @name, @email, @shipAddress, @machSerial, @gdprConsent, @dateCreated)";

            SqlCommand insertCommand = new SqlCommand(insertQuery, rmaConn);
            insertCommand.Parameters.AddWithValue("@rmaID", rmaID);
            insertCommand.Parameters.AddWithValue("@name", name);
            insertCommand.Parameters.AddWithValue("@email", email);
            insertCommand.Parameters.AddWithValue("@shipAddress", shipAddress);
            insertCommand.Parameters.AddWithValue("@machSerial", machSerial);
            insertCommand.Parameters.AddWithValue("@gdprConsent", 1);
            insertCommand.Parameters.AddWithValue("@dateCreated", localDate);

            insertCommand.ExecuteNonQuery();
            rmaConn.Close();

            // *************** EMAIL ***************
            var consentConfirm = "";

            if (gdprConsentBox.Checked)
            {
                consentConfirm = "Confirmed";
            }

            string date = localDate.ToLongDateString();

            //Fetching Email Body Text from EmailTemplate File.
            string FilePath = @"D:/EmailTemplates/RequestRMA.html";
            string FilePath2 = @"D:/EmailTemplates/rmaPart.html";
            FileStream sourceStream = new FileStream(FilePath, FileMode.Open, FileAccess.ReadWrite, FileShare.ReadWrite);
            FileStream sourceStream2 = new FileStream(FilePath2, FileMode.Open, FileAccess.ReadWrite, FileShare.ReadWrite);
            FileStream sourceStream3 = new FileStream(FilePath2, FileMode.Open, FileAccess.ReadWrite, FileShare.ReadWrite);
            FileStream sourceStream4 = new FileStream(FilePath2, FileMode.Open, FileAccess.ReadWrite, FileShare.ReadWrite);
            FileStream sourceStream5 = new FileStream(FilePath2, FileMode.Open, FileAccess.ReadWrite, FileShare.ReadWrite);
            StreamReader str = new StreamReader(sourceStream);
            StreamReader part2 = new StreamReader(sourceStream2);
            StreamReader part3 = new StreamReader(sourceStream3);
            StreamReader part4 = new StreamReader(sourceStream4);
            StreamReader part5 = new StreamReader(sourceStream5);


            string MailText = str.ReadToEnd();
            string PartText2 = part2.ReadToEnd();
            string PartText3 = part3.ReadToEnd();
            string PartText4 = part4.ReadToEnd();
            string PartText5 = part5.ReadToEnd();
            str.Close();
            part2.Close();
            part3.Close();
            part4.Close();
            part5.Close();

            // ********** Repalce variables **********
            MailText = MailText.Replace("[currentDate]", date);
            MailText = MailText.Replace("[newName]", txtName.Text.Trim());
            MailText = MailText.Replace("[newEmailAddress]", txtEmail.Text.Trim());
            MailText = MailText.Replace("[newSoldTo]", txtSoldTo.Text.Trim());
            MailText = MailText.Replace("[newShipTo]", txtShipTo.Text.Trim());
            MailText = MailText.Replace("[newOrderNumber]", txtOrderNum.Text.Trim());
            MailText = MailText.Replace("[newMachSerial]", txtMachineSerial.Text.Trim());

            MailText = MailText.Replace("[newShipAddress]", txtShipAddress.Text);
            MailText = MailText.Replace("[newPartWarranty]", rblUnderWarranty.SelectedItem.Text);
            MailText = MailText.Replace("[newReasonReturn]", drpReasonReturn.SelectedItem.Text);
            MailText = MailText.Replace("[newReturnDetail]", txtReasonDetail.Text.Trim());

            MailText = MailText.Replace("[newPartNumber]", txtPartNumReq.Text.Trim());
            MailText = MailText.Replace("[newPartDescription]", txtPartDescReq.Text.Trim());
            MailText = MailText.Replace("[newQuantity]", txtPartQuanReq.Text.Trim());
            MailText = MailText.Replace("[newConsent]", consentConfirm);

            // ********** Part 2 **********
           
            string hideOpenTag = "<tr><td style=\"visibility: hidden;\">";
            string openTag = "<tr align=\"left\"><td>";
            string closeTag = "</td></tr>";
            if (txtPartNum2.Text != String.Empty || txtPartDesc2.Text != String.Empty || txtPartQuan2.Text != String.Empty)
            {
                MailText = MailText.Replace("[openTag2]", openTag);
                MailText = MailText.Replace("[closeTag2]", closeTag);
                PartText2 = PartText2.Replace("[partNumber]", txtPartNum2.Text.Trim());
                PartText2 = PartText2.Replace("[partDescription]", txtPartDesc2.Text.Trim());
                PartText2 = PartText2.Replace("[partQuantity]", txtPartQuan2.Text.Trim());
                MailText = MailText.Replace("[part2]", PartText2);
            }
            else
            {
                MailText = MailText.Replace("[openTag2]", hideOpenTag);
                MailText = MailText.Replace("[closeTag2]", closeTag);
                MailText = MailText.Replace("[part2]", "");
            }

            // ********** Part 3 **********

            if (txtPartNum3.Text != String.Empty || txtPartDesc3.Text != String.Empty || txtPartQuan3.Text != String.Empty)
            {
                MailText = MailText.Replace("[openTag3]", openTag);
                MailText = MailText.Replace("[closeTag3]", closeTag);
                PartText3 = PartText3.Replace("[partNumber]", txtPartNum3.Text.Trim());
                PartText3 = PartText3.Replace("[partDescription]", txtPartDesc3.Text.Trim());
                PartText3 = PartText3.Replace("[partQuantity]", txtPartQuan3.Text.Trim());
                MailText = MailText.Replace("[part3]", PartText3);
            }
            else
            {
                MailText = MailText.Replace("[openTag3]", hideOpenTag);
                MailText = MailText.Replace("[closeTag3]", closeTag);
                MailText = MailText.Replace("[part3]", "");
            }

            // ********** Part 4 **********

            if (txtPartNum4.Text != String.Empty || txtPartDesc4.Text != String.Empty || txtPartQuan4.Text != String.Empty)
            {
                MailText = MailText.Replace("[openTag4]", openTag);
                MailText = MailText.Replace("[closeTag4]", closeTag);
                PartText4 = PartText4.Replace("[partNumber]", txtPartNum4.Text.Trim());
                PartText4 = PartText4.Replace("[partDescription]", txtPartDesc4.Text.Trim());
                PartText4 = PartText4.Replace("[partQuantity]", txtPartQuan4.Text.Trim());
                MailText = MailText.Replace("[part4]", PartText4);
            }
            else
            {
                MailText = MailText.Replace("[openTag4]", hideOpenTag);
                MailText = MailText.Replace("[closeTag4]", closeTag);
                MailText = MailText.Replace("[part4]", "");
            }

            // ********** Part 5 **********
            if (txtPartNum5.Text != String.Empty || txtPartDesc5.Text != String.Empty || txtPartQuan5.Text != String.Empty)
            {
                MailText = MailText.Replace("[openTag5]", openTag);
                MailText = MailText.Replace("[closeTag5]", closeTag);
                PartText5 = PartText5.Replace("[partNumber]", txtPartNum5.Text.Trim());
                PartText5 = PartText5.Replace("[partDescription]", txtPartDesc5.Text.Trim());
                PartText5 = PartText5.Replace("[partQuantity]", txtPartQuan5.Text.Trim());
                MailText = MailText.Replace("[part5]", PartText5);
            }
            else
            {
                MailText = MailText.Replace("[openTag5]", hideOpenTag);
                MailText = MailText.Replace("[closeTag5]", closeTag);
                MailText = MailText.Replace("[part5]", "");
            }

            // Session variable to pass email information to confirmation page.
            Session["emailConfirm"] = MailText;

            string subject = "Web RMA: " + txtSoldTo.Text + " / " + txtShipTo.Text + " / " + txtOrderNum.Text;

            var customerEmail = txtEmail.Text.Trim();

            try
            {


                // Create the msg object to be sent
                MailMessage _mailmsg = new MailMessage();

                //Make TRUE because our body text is html
                _mailmsg.IsBodyHtml = true;

                // Configure the address we are sending the mail from
                MailAddress address = new MailAddress("webadmin@haeger.com");

                _mailmsg.From = address;

                //Add your email address to the recipients
                _mailmsg.To.Add("ronboggs@haeger.com");
                _mailmsg.To.Add("jtrott@haeger.com");
                _mailmsg.To.Add("sstanley@haeger.com");
                _mailmsg.To.Add("rcosta@haeger.com");
                _mailmsg.To.Add("sales@haeger.com");
                _mailmsg.CC.Add(customerEmail);
                _mailmsg.Bcc.Add("hmeyer@haeger.com");
                _mailmsg.Bcc.Add("mruma@haeger.com");
                _mailmsg.Bcc.Add("regeah811@gmail.com");
                _mailmsg.Bcc.Add("webadmin@haeger.com");

                //Set Subject
                _mailmsg.Subject = subject;

                //Set Body Text of Email
                _mailmsg.Body = MailText;

                // Configure an SmtpClient to send the mail.
                SmtpClient _smtp = new SmtpClient("smtp-mail.outlook.com");
                _smtp.Port = 587;
                _smtp.DeliveryMethod = SmtpDeliveryMethod.Network;
                _smtp.UseDefaultCredentials = false;
                System.Net.NetworkCredential credentials =
                    new System.Net.NetworkCredential("webadmin@haeger.com", "Crazycat75");
                _smtp.EnableSsl = true;
                _smtp.Credentials = credentials;



                //_smtp.DeliveryMethod = SmtpDeliveryMethod.SpecifiedPickupDirectory;
                //_smtp.PickupDirectoryLocation = @targetDirectory;

                // Send the msg.
                _smtp.Send(_mailmsg);

                // Display some feedback to the user to let them know it was sent.
                //lblResult.Text = "Your message was sent!";

                clearControls();

                Response.Redirect("/SubmitSucess.aspx");

            }
            catch
            {
                // If the message failed at some point, let the user know.
                lblResult.Text = "Your message failed to send, please try again.";

            }

        }

        private void clearControls()
        {
            // Clearing all controls.
            txtName.Text = "";
            txtEmail.Text = "";
            txtSoldTo.Text = "";
            txtShipTo.Text = "";
            txtOrderNum.Text = "";
            txtMachineSerial.Text = "";

            txtShipAddress.Text = "";
            rblUnderWarranty.ClearSelection();
            drpReasonReturn.SelectedIndex = 0;
            txtReasonDetail.Text = "";

        }
    }
}