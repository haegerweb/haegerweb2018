﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;


namespace Haeger2018
{
    public partial class ServiceRequest : System.Web.UI.Page
    {
        SqlConnection srvRqConn = new SqlConnection(ConfigurationManager.ConnectionStrings["OptInConsentConnectionString"].ConnectionString);
        

        protected void Page_Load(object sender, EventArgs e)
        {
            Page.MaintainScrollPositionOnPostBack = true;

        }

        protected void btnSrvRSubmit_Click(object sender, EventArgs e)
        {
            DateTime localDate = DateTime.Now;
            string date = localDate.ToLongDateString();

            var msg = "Without your consent we cannot process your request.";

            // First check to warn user 
            if (!gdprConsentBox.Checked)
            {
                lblConsentWrn.Visible = true;

                Response.Write("<script>alert('" + msg + "')</script>");

                return;
            }

            // *************** RFQ Form Variables ***************
            string companyName = txtSrvRCompany.Text.Trim();
            string firstName = txtSrvRFirstName.Text.Trim();
            string lastName = txtSrvLastName.Text.Trim();
            string phone = txtSrvRPhone.Text.Trim();
            string zipCode = txtSrvRZip.Text.Trim();
            string country = txtSrvRCountry.Text.Trim();
            string email = txtSrvREmail.Text.Trim();

            // *************** INSERT DATABASE ***************

            srvRqConn.Open();

            // Query and commands to retrieve max tracking number
            string trackNumQuery = "SELECT MAX(trackingNum) FROM SrvRqConsent";
            SqlCommand trackNumCommand = new SqlCommand(trackNumQuery, srvRqConn);
            string trackNum = "";
            using (SqlDataReader tn = trackNumCommand.ExecuteReader())
            {
                while (tn.Read())
                {
                    trackNum = tn[0].ToString();
                }
            }

            // Add one to max tracking number and creating new _projID
            int nextIDnum = (Int32.Parse(trackNum)) + 1;
            string srvRqID = "srvRq" + nextIDnum.ToString();

            string insertQuery =
            "INSERT INTO [dbo].[srvRqConsent] " +
            "(_srvRqID, companyName, firstName, lastName, phone, zipCode, country, gdprConsent, dateCreated, email) " +
            "VALUES (@srvRqID, @companyName, @firstName, @lastName, @phone, @zipCode, @country, @gdprConsent, @dateCreated, @email)";

            SqlCommand insertCommand = new SqlCommand(insertQuery, srvRqConn);
            insertCommand.Parameters.AddWithValue("@srvRqID", srvRqID);
            insertCommand.Parameters.AddWithValue("@companyName", companyName);
            insertCommand.Parameters.AddWithValue("@firstName", firstName);
            insertCommand.Parameters.AddWithValue("@lastName", lastName);
            insertCommand.Parameters.AddWithValue("@phone", phone);
            insertCommand.Parameters.AddWithValue("@zipCode", zipCode);
            insertCommand.Parameters.AddWithValue("@country", country);
            insertCommand.Parameters.AddWithValue("@gdprConsent", 1);
            insertCommand.Parameters.AddWithValue("@dateCreated", localDate);
            insertCommand.Parameters.AddWithValue("@email", email);

            insertCommand.ExecuteNonQuery();
            srvRqConn.Close();

            // *************** EMAIL ***************

            var consentConfirm = "";

            if (gdprConsentBox.Checked)
            {
                consentConfirm = "Confirmed";
            }


            //Fetching Email Body Text from EmailTemplate File.
            string FilePath = @"D:/EmailTemplates/ServiceRequestTemplate.html";
            FileStream sourceStream = new FileStream(FilePath, FileMode.Open, FileAccess.ReadWrite, FileShare.ReadWrite);
            StreamReader str = new StreamReader(sourceStream);
            string MailText = str.ReadToEnd();
            str.Close();

            //Repalce variables
            MailText = MailText.Replace("[currentDate]", date);
            MailText = MailText.Replace("[newCompany]", txtSrvRCompany.Text.Trim());
            MailText = MailText.Replace("[newFirstName]", txtSrvRFirstName.Text.Trim());
            MailText = MailText.Replace("[newLastName]", txtSrvLastName.Text.Trim());
            MailText = MailText.Replace("[newPhone]", txtSrvRPhone.Text.Trim());
            MailText = MailText.Replace("[newEmailAddress]", txtSrvREmail.Text.Trim());
            MailText = MailText.Replace("[newState]", txtSrvRState.Text.Trim());
            MailText = MailText.Replace("[newZipCode]", txtSrvRZip.Text.Trim());
            MailText = MailText.Replace("[newCountry]", txtSrvRCountry.Text.Trim());
            MailText = MailText.Replace("[newModelNumber]", txtSrvRModelNum.Text.Trim());
            MailText = MailText.Replace("[newMachSerial]", txtSrvRSerialNum.Text.Trim());
            MailText = MailText.Replace("[newServiceDetail]", txtSrvRDetails.Text.Trim());
            MailText = MailText.Replace("[newConsent]", consentConfirm);


            // Session variable to pass email information to confirmation page.
            Session["emailConfirm"] = MailText;

            string subject = "Service Request: " + txtSrvRCompany.Text + " - " + txtSrvRSerialNum.Text;

            var customerEmail = txtSrvREmail.Text.Trim();

            try
            {

                // Create the msg object to be sent
                MailMessage _mailmsg = new MailMessage();

                //Make TRUE because our body text is html
                _mailmsg.IsBodyHtml = true;

                // Configure the address we are sending the mail from
                MailAddress address = new MailAddress("webadmin@haeger.com");

                _mailmsg.From = address;

                // Add your email address to the recipients
                _mailmsg.To.Add("service@haeger.com");
                _mailmsg.To.Add("MRuma@haeger.com");
                _mailmsg.CC.Add(customerEmail);
                _mailmsg.Bcc.Add("regeah811@gmail.com");
                _mailmsg.Bcc.Add("webadmin@haeger.com");

                //Set Subject
                _mailmsg.Subject = subject;

                //Set Body Text of Email
                _mailmsg.Body = MailText;

                // Configure an SmtpClient to send the mail.
                SmtpClient _smtp = new SmtpClient("smtp-mail.outlook.com");
                _smtp.Port = 587;
                _smtp.DeliveryMethod = SmtpDeliveryMethod.Network;
                _smtp.UseDefaultCredentials = false;
                System.Net.NetworkCredential credentials =
                    new System.Net.NetworkCredential("webadmin@haeger.com", "Crazycat75");
                _smtp.EnableSsl = true;
                _smtp.Credentials = credentials;

                //_smtp.DeliveryMethod = SmtpDeliveryMethod.SpecifiedPickupDirectory;
                //_smtp.PickupDirectoryLocation = @targetDirectory;

                // Send the msg.
                _smtp.Send(_mailmsg);

                // Display some feedback to the user to let them know it was sent.
                //lblResult.Text = "Your message was sent!";

                clearControls();

                Response.Redirect("/SubmitSucess.aspx");

            }
            catch
            {
                // If the message failed at some point, let the user know.
                lblResult.Text = "Your message failed to send, please try again.";

            }



        }

        private void clearControls()
        {

            txtSrvRCompany.Text = "";
            txtSrvRFirstName.Text = "";
            txtSrvLastName.Text = "";
            txtSrvRPhone.Text = "";
            txtSrvREmail.Text = "";
            txtSrvRState.Text = "";
            txtSrvRZip.Text = "";
            txtSrvRCountry.Text = "";
            txtSrvRModelNum.Text = "";
            txtSrvRSerialNum.Text = "";
            txtSrvRDetails.Text = "";


        }
    }
}