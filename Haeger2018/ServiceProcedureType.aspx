﻿<%@ Page Title="Procedures" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ServiceProcedureType.aspx.cs" Inherits="Haeger2018.ServiceProcedureType" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div class="container">
        <div class="col-md-12 text-center">
            <h1><strong><asp:Label ID="lblSPMachineType" runat="server" Text="Label"></asp:Label></strong></h1>
            <h5>If you do not have Acrobat Reader, click on the Get Acrobat Reader image below. </h5>
            <div>
                <asp:DropDownList ID="drpServProc2" class="btn btn-default dropdown-toggle notranslate" runat="server" AppendDataBoundItems="true" DataSourceID="serviceProcConnect" DataTextField="fileName" DataValueField="currentFileLocation">
                <asp:ListItem Text="----Make a Selection----" Value="" />
                </asp:DropDownList>
                <asp:SqlDataSource ID="serviceProcConnect" runat="server" 
                                ConnectionString="<%$ ConnectionStrings:ServiceProceduresConnectionString3 %>" 
                                SelectCommand="SELECT [fileName], [currentFileLocation] FROM [serviceBulletins$] WHERE ([machineType] = @machineType)">
                <SelectParameters>
                    <asp:SessionParameter Name="machineType" SessionField="spMachineName" Type="String" />
                </SelectParameters>

                </asp:SqlDataSource>
                <asp:Button ID="btnGetInfo" CssClass="btn btn-default"  runat="server" Text="Get Info" OnClientClick="window.document.forms[0].target='_blank';" OnClick="btnGetInfo_Click" />
                <br />
                <br />
            </div>
            <asp:ImageButton ID="getAdobeReader" runat="server" ImageUrl="/images/GetReader.jpg" Width="160px" OnClick="getAdobeReader_Click"/>
        </div>
            
    </div>
     <br />
    <br />
    <br />
    <br />
    <br />
    <br />
</asp:Content>
