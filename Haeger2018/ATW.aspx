﻿<%@ Page Title="Auto Tooling" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ATW.aspx.cs" Inherits="Haeger2018.ATW" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
    <title></title>
  
    <style type="text/css">
        .ListControl
        {
            display:flex;
            align-items:center;
            justify-content:center;
            margin-left:auto;
            margin-right:auto;

        }
        .ListControl input[type=checkbox], input[type=radio]
        {
            background-color:none;
            display:inline-grid;
            align-items:center;
            width:auto;
            border: 15px solid red;
            margin-left:auto;
            margin-right:auto;
        }
        .ListControl label
        {
            
            width:auto;
            margin-left:10px;
            margin-right:10px;
            padding-left:20px;
            padding-right:10px;
            color:black;
        }
        

        .ValidateControl {
            color:black;
        }

        .machineImage
        {
            display:flex;
            align-items:center;
            justify-content:center;
            width:100%;
            height:100%;
        }

        .resultTop
        {
            color: black;
            font-size:small;

        }

           /* WIZARD */
        .stepNotCompleted
        {
            background-color: rgb(153,153,153);
            width: 15px;
            border: 1px solid rgb(153,153,153);
            margin-right: 5px;
            color: black;
            font-family: Arial;
            font-size: 12px;
            text-align: center;
        }

        .stepCompleted
        {
            background-color: #4d4d4d;
            width: 15px;
            border: 1px solid #4d4d4d;
            color: black;
            font-family: Arial;
            font-size: 12px;
            text-align: center;
        }

        .stepCurrent
        {
            background-color: #e01122;
            width: 15px;
            border: 1px solid #e01122;
            color: Black;
            font-family: Arial;
            font-size: 12px;
            font-weight: normal;
            text-align: center;
        }

        .stepBreak
        {
            width: 3px;
            background-color: Transparent;
        }

        .wizardProgress
        {
            padding-right: 10px;
            font-family: Arial;
            color: #333333;
            font-size: 12px;

        }

        .wizardTitle {
            font-family: Arial;
            font-size: 120%;
            font-weight: normal;
            color: black;
            vertical-align: middle;
            padding-right: 10px;
        }
       
        span1{
            color:#0085ca;
        }
        hr { 
    display: block;
    margin-top: 0.5em;
    margin-bottom: 0.5em;
    margin-left: auto;
    margin-right: auto;
    border-style: inset;
    border-width: 1px;
}
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
   
        
        
        <div class="container-fluid">
            <div class="section-title">
                    <h2 style="margin: 0;">Haeger Auto Tooling Wizard</h2>
                    <h3 style="text-transform:initial; color:black; margin:0;">A <span1> PennEngineering&reg</span1> Company</h3>
                   
               </div>
                
            </div>
            <br />
            <div class="row">
                <div class="col-sm-2">
                    <asp:Image ID="imgMachineSelected" CssClass="machineImage" runat="server" Visible="false" Width="240px" />
                 
                </div>
                <div class="col-sm-3">
                    <asp:Label ID="lblMachineType" CssClass="resultTop" runat="server" Text="Machine Type" Visible="false" ></asp:Label>
                    <br />
                    <asp:Label ID="lblSetType" CssClass="resultTop" runat="server" Text="Set Type" Visible="false"></asp:Label>
                    <br />
                    <asp:Label ID="lblFastener" CssClass="resultTop" runat="server" Text="Fastener" Visible="false"></asp:Label>
                    <br />
                    <asp:Label ID="lblThreadType" CssClass="resultTop" runat="server" Text="Thread Type" Visible="false"></asp:Label>
                    <br />
                    <asp:Label ID="lblDashLength" CssClass="resultTop" runat="server" Text="Dash Length" Visible="false"></asp:Label>
                </div>
          
        <%--Auto Tooling Wizard--%>
       
                <div class="col-sm-6">
                    <asp:Wizard ID="Wizard1" CssClass="ListControl" runat="server" Height="" Width="550px" DisplaySideBar="false" 
                                OnFinishButtonClick="Wizard1_FinishButtonClick" OnNextButtonClick="Wizard1_NextButtonClick" 
                                ActiveStepIndex="0" StepNextButtonStyle-CssClass="hidden" FinishCompleteButtonStyle-CssClass="hidden">
<FinishCompleteButtonStyle CssClass="hidden"></FinishCompleteButtonStyle>
                        <HeaderTemplate>
                            <table style="width: 100%" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td class="wizardTitle">
                                        <%= Wizard1.ActiveStep.Title%>
                                    </td>
                                   
                                </tr>
                            </table>
                        </HeaderTemplate>

<StepNextButtonStyle CssClass="hidden"></StepNextButtonStyle>
                        <SideBarTemplate>
                        </SideBarTemplate>
           
                        <StartNavigationTemplate>
                            <asp:Button ID="StartNextButton" runat="server" CommandName="MoveNext" Text="Next" Visible="false"/>
                        </StartNavigationTemplate>
                        <WizardSteps>
                        <%--Choose a Machine--%>
                            <asp:WizardStep ID="WizardStep1"  runat="server" Title="Choose a Machine" StepType="Start">
                                <asp:RadioButtonList ID="rblMachineSelection" CssClass="notranslate" runat="server" AutoPostBack="true"
                                            DataSourceID="sqlMachineSelection" DataTextField="machineType" DataValueField="imageName" 
                                            RepeatColumns="3" OnSelectedIndexChanged="rblMachineSelection_SelectedIndexChanged">

                                </asp:RadioButtonList>
                                <asp:RequiredFieldValidator ID="rfvMachineSelection" CssClass="ValidateControl" runat="server" 
                                                    ErrorMessage="Please Select a Machine" ControlToValidate="rblMachineSelection">

                                </asp:RequiredFieldValidator>
                                <asp:SqlDataSource ID="sqlMachineSelection" runat="server" ConnectionString="<%$ ConnectionStrings:MobileAppDataConnectionString %>" 
                                            SelectCommand="SELECT DISTINCT [machineType], [imageName]  
                                                            FROM [machineNameSetTypes] 
                                                            WHERE (([setType] = @setType) OR ([setType] = @setType2))">
                                    <SelectParameters>
                                        <asp:Parameter DefaultValue="Auto" Name="setType" />
                                        <asp:Parameter DefaultValue="ABFT" Name="setType2" />
                                    </SelectParameters>
                                </asp:SqlDataSource>
                            </asp:WizardStep>

                     <%--Choose a Set Type--%>
                    <asp:WizardStep ID="WizardStep2" runat="server" Title="Choose a Set Type">
                        <asp:RadioButtonList ID="rblSetType" runat="server" 
                                            DataSourceID="sqlSetType" AutoPostBack="true" RepeatDirection="Horizontal" DataTextField="setType" DataValueField="setType" 
                                            OnSelectedIndexChanged="rblSetType_SelectedIndexChanged">

                        </asp:RadioButtonList>
                        <asp:RequiredFieldValidator ID="rfvSetType" runat="server" CssClass="ValidateControl" 
                                                    ErrorMessage="Please Select a Set Type" ControlToValidate="rblSetType">

                        </asp:RequiredFieldValidator>
                        <asp:SqlDataSource ID="sqlSetType" runat="server" ConnectionString="<%$ ConnectionStrings:MobileAppDataConnectionString %>" 
                                            SelectCommand="SELECT DISTINCT [setType] 
                                                            FROM [machineNameSetTypes] 
                                                            WHERE (([machineType] = @machineType) AND ([setType] NOT LIKE '%' + @setType + '%'))">

                            <SelectParameters>
                                <asp:SessionParameter Name="machineType" SessionField="MachineSelected" Type="String" />
                                <asp:Parameter DefaultValue="MANUAL" Name="setType" Type="String" />
                            </SelectParameters>

                        </asp:SqlDataSource>
                    </asp:WizardStep>
                    
                     <%--Choose a Fastener--%>
                    <asp:WizardStep ID="WizardStep3" runat="server" Title="Choose a Fastener">
                        <asp:RadioButtonList ID="rblFastener" runat="server" RepeatColumns="4" AutoPostBack="true"
                                            DataSourceID="sqlFastener" DataTextField="ftype" DataValueField="ftype" 
                                            OnSelectedIndexChanged="rblFastener_SelectedIndexChanged">

                        </asp:RadioButtonList>
                        <asp:RequiredFieldValidator ID="rfvFastener" runat="server" CssClass="ValidateControl" 
                                                    ErrorMessage="Please Select a Fastener" ControlToValidate="rblFastener">

                        </asp:RequiredFieldValidator>
                        <asp:SqlDataSource ID="sqlFastener" runat="server" 
                                            ConnectionString="<%$ ConnectionStrings:MobileAppDataConnectionString %>" 
                                            SelectCommand="SELECT DISTINCT [ftype] FROM [twMainDB1] 
                                                            WHERE (([machineType] = @machineType) 
                                                                    AND ([setType] = @setType))
                                                            UNION
                                                            SELECT DISTINCT [ftype] FROM [twMainDB2] 
                                                            WHERE (([machineType] = @machineType) 
                                                                    AND ([setType] = @setType))
                                                            UNION
                                                            SELECT DISTINCT [ftype] FROM [twMainDB3] 
                                                            WHERE (([machineType] = @machineType) 
                                                                    AND ([setType] = @setType))
                                                            UNION
                                                            SELECT DISTINCT [ftype] FROM [twMainDB4] 
                                                            WHERE (([machineType] = @machineType) 
                                                                    AND ([setType] = @setType))">
                            <SelectParameters>
                                <asp:SessionParameter Name="machineType" SessionField="MachineSelected" Type="String" />
                                <asp:SessionParameter Name="setType" SessionField="SetTypeSelected" Type="String" />
                            </SelectParameters>

                        </asp:SqlDataSource>

                    </asp:WizardStep>
                     <%--Choose a Thread Type--%>
                    <asp:WizardStep ID="WizardStep4" runat="server" Title="Choose a Thread Type">
                        <asp:RadioButtonList ID="rblThreadType" runat="server" AutoPostBack="true"
                                                DataSourceID="sqlThreadType" DataTextField="fsize" DataValueField="fsize" 
                                                RepeatColumns="5" OnSelectedIndexChanged="rblThreadType_SelectedIndexChanged" >

                        </asp:RadioButtonList>
                        <asp:RequiredFieldValidator ID="rfvThreadType" runat="server" CssClass="ValidateControl" 
                                                    ErrorMessage="Please Select a Thread Type" ControlToValidate="rblThreadType">

                        </asp:RequiredFieldValidator>
                        <asp:SqlDataSource ID="sqlThreadType" runat="server" 
                            ConnectionString="<%$ ConnectionStrings:MobileAppDataConnectionString %>" 
                            SelectCommand="SELECT DISTINCT [fsize] FROM [twMainDB1] 
                                            WHERE (([machineType] = @machineType) 
                                                    AND ([setType] = @setType)
                                                    AND ([ftype] = @ftype))
                                            UNION
                                            SELECT DISTINCT [fsize] FROM [twMainDB2] 
                                            WHERE (([machineType] = @machineType) 
                                                    AND ([setType] = @setType)
                                                    AND ([ftype] = @ftype))
                                            UNION
                                            SELECT DISTINCT [fsize] FROM [twMainDB3] 
                                            WHERE (([machineType] = @machineType) 
                                                    AND ([setType] = @setType)
                                                    AND ([ftype] = @ftype))
                                            UNION
                                            SELECT DISTINCT [fsize] FROM [twMainDB4] 
                                            WHERE (([machineType] = @machineType) 
                                                    AND ([setType] = @setType)
                                                    AND ([ftype] = @ftype))">

                            <SelectParameters>
                                <asp:SessionParameter Name="machineType" SessionField="MachineSelected" Type="String" />
                                <asp:SessionParameter Name="setType" SessionField="SetTypeSelected" Type="String" />
                                <asp:SessionParameter Name="ftype" SessionField="FastenerSelected" Type="String" />
                            </SelectParameters>

                        </asp:SqlDataSource>
                        <asp:SqlDataSource ID="sqlDashLength" runat="server" 
                                            ConnectionString="<%$ ConnectionStrings:MobileAppDataConnectionString %>" 
                                            SelectCommand="SELECT [dashMin], [dashMax] FROM [twMainDB1] 
                                                            WHERE (([machineType] = @machineType) 
                                                                    AND ([setType] = @setType)
                                                                    AND ([ftype] = @ftype)
                                                                    AND ([fsize] = @fsize))
                                                            UNION
                                                            SELECT [dashMin], [dashMax] FROM [twMainDB2] 
                                                            WHERE (([machineType] = @machineType) 
                                                                    AND ([setType] = @setType)
                                                                    AND ([ftype] = @ftype)
                                                                    AND ([fsize] = @fsize))
                                                            UNION
                                                            SELECT [dashMin], [dashMax] FROM [twMainDB3] 
                                                            WHERE (([machineType] = @machineType) 
                                                                    AND ([setType] = @setType)
                                                                    AND ([ftype] = @ftype)
                                                                    AND ([fsize] = @fsize))
                                                            UNION
                                                            SELECT [dashMin], [dashMax] FROM [twMainDB4] 
                                                            WHERE (([machineType] = @machineType) 
                                                                    AND ([setType] = @setType)
                                                                    AND ([ftype] = @ftype)
                                                                    AND ([fsize] = @fsize))">

                            <SelectParameters>
                                <asp:SessionParameter Name="machineType" SessionField="MachineSelected" Type="String" />
                                <asp:SessionParameter Name="setType" SessionField="SetTypeSelected" Type="String" />
                                <asp:SessionParameter Name="ftype" SessionField="FastenerSelected" Type="String" />
                                <asp:SessionParameter Name="fsize" SessionField="ThreadTypeSelected" Type="String" />
                            </SelectParameters>

                        </asp:SqlDataSource>

                    </asp:WizardStep>
                     <%--Choose a Dash Length--%>
                    <asp:WizardStep ID="WizardStep5" runat="server" Title="Choose a Dash Length">
                         <asp:RadioButtonList ID="rblDashLength" runat="server" RepeatDirection="Horizontal" AutoPostBack="true"
                                                RepeatColumns="5" OnSelectedIndexChanged="rblDashLength_SelectedIndexChanged">

                        </asp:RadioButtonList>
                        <asp:RequiredFieldValidator ID="rfvDashLength" runat="server" CssClass="ValidateControl" 
                                                    ErrorMessage="Please Select a Dash Length" ControlToValidate="rblDashLength">

                        </asp:RequiredFieldValidator>
                        

                    </asp:WizardStep>

                    <%--Results--%>
                    <asp:WizardStep ID="WizardStep6" runat="server" StepType="Complete" Title="Results">
                       <%--Naming Convention for Link Buttons: Number corresponds to grdresult--%>
                        <asp:GridView ID="grdResults1" CssClass="table table-striped resultTop text-nowrap" runat="server" DataSourceID="sqlResults1" 
                                      AutoGenerateColumns="false" Width="100%" text-align="center" >
                            <Columns >
                                <asp:TemplateField HeaderText="Tooling Set">
                                    <EditItemTemplate>
                                        <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("toolingSet") %>'></asp:TextBox>
                                    </EditItemTemplate>
                                    <ItemTemplate>
                                        <asp:LinkButton ID="lbtnTS1" CommandName="Select" CommandArgument ='<%# Container.DataItemIndex %>' runat="server" OnClick="lbtnTS1_Click" OnClientClick="window.document.forms[0].target='_blank';" Text='<%# Eval("toolingSet") %>'>LinkButton</asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Flight Tube">
                                    <EditItemTemplate>
                                        <asp:TextBox ID="TextBox2" runat="server" Text='<%# Bind("flightTube") %>'></asp:TextBox>
                                    </EditItemTemplate>
                                    <ItemTemplate>
                                        <asp:LinkButton ID="lbtnFT1" CommandName="Select" CommandArgument ='<%# Container.DataItemIndex %>' runat="server" OnClick="lbtnFT1_Click" OnClientClick="window.document.forms[0].target='_blank';" Text='<%# Eval("flightTube") %>'>LinkButton</asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Multi Module">
                                    <EditItemTemplate>
                                        <asp:TextBox ID="TextBox3" runat="server" Text='<%# Bind("multiModule") %>'></asp:TextBox>
                                    </EditItemTemplate>
                                    <ItemTemplate>
                                        <asp:LinkButton ID="lbtnMM1" CommandName="Select" CommandArgument ='<%# Container.DataItemIndex %>' runat="server" OnClick="lbtnMM1_Click" OnClientClick="window.document.forms[0].target='_blank';" Text='<%# Eval("multiModule") %>'>LinkButton</asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Shuttle">
                                    <EditItemTemplate>
                                        <asp:TextBox ID="TextBox4" runat="server" Text='<%# Bind("Shuttle") %>'></asp:TextBox>
                                    </EditItemTemplate>
                                    <ItemTemplate>
                                        <asp:LinkButton ID="lbtnSH1" CommandName="Select" CommandArgument ='<%# Container.DataItemIndex %>' runat="server" OnClick="lbtnSH1_Click" OnClientClick="window.document.forms[0].target='_blank';" Text='<%# Eval("Shuttle") %>'>LinkButton</asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Vacuum Anvil">
                                    <EditItemTemplate>
                                        <asp:TextBox ID="TextBox5" runat="server" Text='<%# Bind("vacuumAnvil") %>'></asp:TextBox>
                                    </EditItemTemplate>
                                    <ItemTemplate>
                                        <asp:LinkButton ID="lbtnVA1" CommandName="Select" CommandArgument ='<%# Container.DataItemIndex %>' runat="server" OnClick="lbtnVA1_Click" OnClientClick="window.document.forms[0].target='_blank';" Text='<%# Eval("vacuumAnvil") %>'>LinkButton</asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Lower Tool">
                                    <EditItemTemplate>
                                        <asp:TextBox ID="TextBox6" runat="server" Text='<%# Bind("lowerTool") %>'></asp:TextBox>
                                    </EditItemTemplate>
                                    <ItemTemplate>
                                        <asp:LinkButton ID="lbtnLT1" CommandName="Select" CommandArgument ='<%# Container.DataItemIndex %>' runat="server" OnClick="lbtnLT1_Click" OnClientClick="window.document.forms[0].target='_blank';" Text='<%# Eval("lowerTool") %>'>LinkButton</asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                        <asp:SqlDataSource ID="sqlResults1" runat="server"
                                           ConnectionString="<%$ ConnectionStrings:MobileAppDataConnectionString %>"
                                           SelectCommand="SELECT TOP 1 [toolingSet], [flightTube], [multiModule],
                                                                  [Shuttle], [vacuumAnvil], [lowerTool]
                                                           FROM [twMainDB1]
                                                           WHERE (([machineType] = @machineType)
                                                                   AND ([setType] = @setType)
                                                                   AND ([ftype] = @ftype)
                                                                   AND ([fsize] = @fsize)
                                                                   AND ([dashMin] &lt;= @dashMin)
                                                                   AND ([dashMax] &gt;= @dashMax))">

                           <SelectParameters>
                               <asp:SessionParameter Name="machineType" SessionField="MachineSelected" Type="String" />
                               <asp:SessionParameter Name="setType" SessionField="SetTypeSelected" Type="String" />
                               <asp:SessionParameter Name="ftype" SessionField="FastenerSelected" Type="String" />
                               <asp:SessionParameter Name="fsize" SessionField="ThreadTypeSelected" Type="String" />
                               <asp:SessionParameter Name="dashMin" SessionField="DashLengthSelected" Type="Int32" />
                               <asp:SessionParameter Name="dashMax" SessionField="DashLengthSelected" Type="Int32" />
                           </SelectParameters>

                        </asp:SqlDataSource>
                        <asp:GridView ID="grdResults2" CssClass="table table-striped resultTop text-nowrap" runat="server" DataSourceID="sqlResults2" 
                                      AutoGenerateColumns="false" Width="100%" text-align="center" >
                            <Columns >
                                <asp:TemplateField HeaderText="Tooling Set">
                                    <EditItemTemplate>
                                        <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("toolingSet") %>'></asp:TextBox>
                                    </EditItemTemplate>
                                    <ItemTemplate>
                                        <asp:LinkButton ID="lbtnTS2" CommandName="Select" CommandArgument ='<%# Container.DataItemIndex %>' runat="server" OnClick="lbtnTS2_Click" OnClientClick="window.document.forms[0].target='_blank';" Text='<%# Eval("toolingSet") %>'>LinkButton</asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Flight Tube">
                                    <EditItemTemplate>
                                        <asp:TextBox ID="TextBox2" runat="server" Text='<%# Bind("flightTube") %>'></asp:TextBox>
                                    </EditItemTemplate>
                                    <ItemTemplate>
                                        <asp:LinkButton ID="lbtnFT2" CommandName="Select" CommandArgument ='<%# Container.DataItemIndex %>' runat="server" OnClick="lbtnFT2_Click" OnClientClick="window.document.forms[0].target='_blank';" Text='<%# Eval("flightTube") %>'>LinkButton</asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Orientation">
                                    <EditItemTemplate>
                                        <asp:TextBox ID="TextBox3" runat="server" Text='<%# Bind("orientation") %>'></asp:TextBox>
                                    </EditItemTemplate>
                                    <ItemTemplate>
                                        <asp:LinkButton ID="lbtnOR2" CommandName="Select" CommandArgument ='<%# Container.DataItemIndex %>' runat="server" OnClick="lbtnOR2_Click" OnClientClick="window.document.forms[0].target='_blank';" Text='<%# Eval("orientation") %>'>LinkButton</asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Singulation">
                                    <EditItemTemplate>
                                        <asp:TextBox ID="TextBox4" runat="server" Text='<%# Bind("singulation") %>'></asp:TextBox>
                                    </EditItemTemplate>
                                    <ItemTemplate>
                                        <asp:LinkButton ID="lbtnSG2" CommandName="Select" CommandArgument ='<%# Container.DataItemIndex %>' runat="server" OnClick="lbtnSG2_Click" OnClientClick="window.document.forms[0].target='_blank';" Text='<%# Eval("singulation") %>'>LinkButton</asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Shuttle">
                                    <EditItemTemplate>
                                        <asp:TextBox ID="TextBox5" runat="server" Text='<%# Bind("Shuttle") %>'></asp:TextBox>
                                    </EditItemTemplate>
                                    <ItemTemplate>
                                        <asp:LinkButton ID="lbtnSH2" CommandName="Select" CommandArgument ='<%# Container.DataItemIndex %>' runat="server" OnClick="lbtnSH2_Click" OnClientClick="window.document.forms[0].target='_blank';" Text='<%# Eval("Shuttle") %>'>LinkButton</asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Vacuum Anvil">
                                    <EditItemTemplate>
                                        <asp:TextBox ID="TextBox6" runat="server" Text='<%# Bind("vacuumAnvil") %>'></asp:TextBox>
                                    </EditItemTemplate>
                                    <ItemTemplate>
                                        <asp:LinkButton ID="lbtnVA2" CommandName="Select" CommandArgument ='<%# Container.DataItemIndex %>' runat="server" OnClick="lbtnVA2_Click" OnClientClick="window.document.forms[0].target='_blank';" Text='<%# Eval("vacuumAnvil") %>'>LinkButton</asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Lower Tool">
                                    <EditItemTemplate>
                                        <asp:TextBox ID="TextBox7" runat="server" Text='<%# Bind("lowerTool") %>'></asp:TextBox>
                                    </EditItemTemplate>
                                    <ItemTemplate>
                                        <asp:LinkButton ID="lbtnLT2" CommandName="Select" CommandArgument ='<%# Container.DataItemIndex %>' runat="server" OnClick="lbtnLT2_Click" OnClientClick="window.document.forms[0].target='_blank';" Text='<%# Eval("lowerTool") %>'>LinkButton</asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                        <asp:SqlDataSource ID="sqlResults2" runat="server"
                                           ConnectionString="<%$ ConnectionStrings:MobileAppDataConnectionString %>"
                                           SelectCommand="SELECT TOP 1 [toolingSet], [flightTube], [orientation],
                                                                  [singulation], [Shuttle], [vacuumAnvil], [lowerTool]
                                                          FROM [twMainDB2]
                                                           WHERE (([machineType] = @machineType)
                                                                   AND ([setType] = @setType)
                                                                   AND ([ftype] = @ftype)
                                                                   AND ([fsize] = @fsize)
                                                                   AND ([dashMin] &lt;= @dashMin)
                                                                   AND ([dashMax] &gt;= @dashMax))">

                           <SelectParameters>
                               <asp:SessionParameter Name="machineType" SessionField="MachineSelected" Type="String" />
                               <asp:SessionParameter Name="setType" SessionField="SetTypeSelected" Type="String" />
                               <asp:SessionParameter Name="ftype" SessionField="FastenerSelected" Type="String" />
                               <asp:SessionParameter Name="fsize" SessionField="ThreadTypeSelected" Type="String" />
                               <asp:SessionParameter Name="dashMin" SessionField="DashLengthSelected" Type="Int32" />
                               <asp:SessionParameter Name="dashMax" SessionField="DashLengthSelected" Type="Int32" />
                           </SelectParameters>

                        </asp:SqlDataSource>
                        <asp:GridView ID="grdResults3" CssClass="table table-striped resultTop text-nowrap" runat="server" DataSourceID="sqlResults3" 
                                      AutoGenerateColumns="false" Width="100%" text-align="center" >
                            <Columns >
                                <asp:TemplateField HeaderText="Tooling Set">
                                    <EditItemTemplate>
                                        <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("toolingSet") %>'></asp:TextBox>
                                    </EditItemTemplate>
                                    <ItemTemplate>
                                        <asp:LinkButton ID="lbtnTS3" CommandName="Select" CommandArgument ='<%# Container.DataItemIndex %>' runat="server" OnClick="lbtnTS3_Click" OnClientClick="window.document.forms[0].target='_blank';" Text='<%# Eval("toolingSet") %>'>LinkButton</asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Flight Tube">
                                    <EditItemTemplate>
                                        <asp:TextBox ID="TextBox2" runat="server" Text='<%# Bind("flightTube") %>'></asp:TextBox>
                                    </EditItemTemplate>
                                    <ItemTemplate>
                                        <asp:LinkButton ID="lbtnFT3" CommandName="Select" CommandArgument ='<%# Container.DataItemIndex %>' runat="server" OnClick="lbtnFT3_Click" OnClientClick="window.document.forms[0].target='_blank';" Text='<%# Eval("flightTube") %>'>LinkButton</asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Multi Module">
                                    <EditItemTemplate>
                                        <asp:TextBox ID="TextBox3" runat="server" Text='<%# Bind("multiModule") %>'></asp:TextBox>
                                    </EditItemTemplate>
                                    <ItemTemplate>
                                        <asp:LinkButton ID="lbtnMM3" CommandName="Select" CommandArgument ='<%# Container.DataItemIndex %>' runat="server" OnClick="lbtnMM3_Click" OnClientClick="window.document.forms[0].target='_blank';" Text='<%# Eval("multiModule") %>'>LinkButton</asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Lower Tool">
                                    <EditItemTemplate>
                                        <asp:TextBox ID="TextBox4" runat="server" Text='<%# Bind("lowerTool") %>'></asp:TextBox>
                                    </EditItemTemplate>
                                    <ItemTemplate>
                                        <asp:LinkButton ID="lbtnLT3" CommandName="Select" CommandArgument ='<%# Container.DataItemIndex %>' runat="server" OnClick="lbtnLT3_Click" OnClientClick="window.document.forms[0].target='_blank';" Text='<%# Eval("lowerTool") %>'>LinkButton</asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Upper Tool">
                                    <EditItemTemplate>
                                        <asp:TextBox ID="TextBox5" runat="server" Text='<%# Bind("upperTool") %>'></asp:TextBox>
                                    </EditItemTemplate>
                                    <ItemTemplate>
                                        <asp:LinkButton ID="lbtnUT3"  CommandName="Select" CommandArgument ='<%# Container.DataItemIndex %>' runat="server" OnClick="lbtnUT3_Click" OnClientClick="window.document.forms[0].target='_blank';" Text='<%# Eval("upperTool") %>'>LinkButton</asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                
                            </Columns>
                        </asp:GridView>
                        <asp:SqlDataSource ID="sqlResults3" runat="server"
                                           ConnectionString="<%$ ConnectionStrings:MobileAppDataConnectionString %>"
                                           SelectCommand="SELECT TOP 1 [toolingSet], [flightTube], [multiModule],
                                                                  [lowerTool], [upperTool]
                                                          FROM [twMainDB3]
                                                          WHERE (([machineType] = @machineType)
                                                                   AND ([setType] = @setType)
                                                                   AND ([ftype] = @ftype)
                                                                   AND ([fsize] = @fsize)
                                                                   AND ([dashMin] &lt;= @dashMin)
                                                                   AND ([dashMax] &gt;= @dashMax))">

                           <SelectParameters>
                               <asp:SessionParameter Name="machineType" SessionField="MachineSelected" Type="String" />
                               <asp:SessionParameter Name="setType" SessionField="SetTypeSelected" Type="String" />
                               <asp:SessionParameter Name="ftype" SessionField="FastenerSelected" Type="String" />
                               <asp:SessionParameter Name="fsize" SessionField="ThreadTypeSelected" Type="String" />
                               <asp:SessionParameter Name="dashMin" SessionField="DashLengthSelected" Type="Int32" />
                               <asp:SessionParameter Name="dashMax" SessionField="DashLengthSelected" Type="Int32" />
                           </SelectParameters>

                        </asp:SqlDataSource>

                        
                        <asp:GridView ID="grdResults4" CssClass="table table-striped resultTop text-nowrap" runat="server" DataSourceID="sqlResults4" 
                                      AutoGenerateColumns="false" Width="100%" text-align="center" >
                            <Columns >
                                <asp:TemplateField HeaderText="Tooling Set">
                                    <EditItemTemplate>
                                        <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("toolingSet") %>'></asp:TextBox>
                                    </EditItemTemplate>
                                    <ItemTemplate>
                                        <asp:LinkButton ID="lbtnTS4" CommandName="Select" CommandArgument ='<%# Container.DataItemIndex %>' runat="server" OnClick="lbtnTS4_Click" OnClientClick="window.document.forms[0].target='_blank';" Text='<%# Eval("toolingSet") %>'>LinkButton</asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Flight Tube">
                                    <EditItemTemplate>
                                        <asp:TextBox ID="TextBox2" runat="server" Text='<%# Bind("flightTube") %>'></asp:TextBox>
                                    </EditItemTemplate>
                                    <ItemTemplate>
                                        <asp:LinkButton ID="lbtnFT4" CommandName="Select" CommandArgument ='<%# Container.DataItemIndex %>' runat="server" OnClick="lbtnFT4_Click" OnClientClick="window.document.forms[0].target='_blank';" Text='<%# Eval("flightTube") %>'>LinkButton</asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Orientation">
                                    <EditItemTemplate>
                                        <asp:TextBox ID="TextBox3" runat="server" Text='<%# Bind("orientation") %>'></asp:TextBox>
                                    </EditItemTemplate>
                                    <ItemTemplate>
                                        <asp:LinkButton ID="lbtnOR4" CommandName="Select" CommandArgument ='<%# Container.DataItemIndex %>' runat="server" OnClick="lbtnOR4_Click" OnClientClick="window.document.forms[0].target='_blank';" Text='<%# Eval("orientation") %>'>LinkButton</asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Singulation">
                                    <EditItemTemplate>
                                        <asp:TextBox ID="TextBox4" runat="server" Text='<%# Bind("singulation") %>'></asp:TextBox>
                                    </EditItemTemplate>
                                    <ItemTemplate>
                                        <asp:LinkButton ID="lbtnSG4" CommandName="Select" CommandArgument ='<%# Container.DataItemIndex %>' runat="server" OnClick="lbtnSG4_Click" OnClientClick="window.document.forms[0].target='_blank';" Text='<%# Eval("singulation") %>'>LinkButton</asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="ABFT Assembly">
                                    <EditItemTemplate>
                                        <asp:TextBox ID="TextBox5" runat="server" Text='<%# Bind("abftAssembly") %>'></asp:TextBox>
                                    </EditItemTemplate>
                                    <ItemTemplate>
                                        <asp:LinkButton ID="lbtnAA4" CommandName="Select" CommandArgument ='<%# Container.DataItemIndex %>' runat="server" OnClick="lbtnAA4_Click" OnClientClick="window.document.forms[0].target='_blank';" Text='<%# Eval("abftAssembly") %>'>LinkButton</asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                
                            </Columns>
                        </asp:GridView>
                        <asp:SqlDataSource ID="sqlResults4" runat="server"
                                           ConnectionString="<%$ ConnectionStrings:MobileAppDataConnectionString %>"
                                           SelectCommand="SELECT TOP 1 [toolingSet], [flightTube], [orientation],
                                                                  [singulation], [abftAssembly]
                                                          FROM [twMainDB4]
                                                          WHERE (([machineType] = @machineType)
                                                                   AND ([setType] = @setType)
                                                                   AND ([ftype] = @ftype)
                                                                   AND ([fsize] = @fsize)
                                                                   AND ([dashMin] &lt;= @dashMin)
                                                                   AND ([dashMax] &gt;= @dashMax))
">

                           <SelectParameters>
                               <asp:SessionParameter Name="machineType" SessionField="MachineSelected" Type="String" />
                               <asp:SessionParameter Name="setType" SessionField="SetTypeSelected" Type="String" />
                               <asp:SessionParameter Name="ftype" SessionField="FastenerSelected" Type="String" />
                               <asp:SessionParameter Name="fsize" SessionField="ThreadTypeSelected" Type="String" />
                               <asp:SessionParameter Name="dashMin" SessionField="DashLengthSelected" Type="Int32" />
                               <asp:SessionParameter Name="dashMax" SessionField="DashLengthSelected" Type="Int32" />
                           </SelectParameters>

</asp:SqlDataSource>



                        <asp:Button ID="btnReset" runat="server" Text="Reset" OnClick="btnReset_Click" />
                    </asp:WizardStep>

                            
                </WizardSteps>
            </asp:Wizard>
                    
                </div>
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />

            </div>
    </div>
</asp:Content>
