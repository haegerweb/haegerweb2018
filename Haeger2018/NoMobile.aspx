﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="NoMobile.aspx.cs" Inherits="Haeger2018.NoMobile" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">


    <div class="container-fluid">
        <div class ="row">
            <div class="col-xs-12 text-center">

                <h1>We're sorry, but this page cannot be accessed on a mobile browser</h1>


            </div>



        </div>
        <div class ="row">
            <div class="col-xs-3">

            </div>

            <div class="col-xs-6 text-center">

                <asp:Image ID="Image1" runat="server" ImageUrl="images/yet.png" Width="200px" />


            </div>
            <div class="col-xs-3">

            </div>


        </div>



    </div>




</asp:Content>
