﻿<%@ Page Title="Request RMA" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="RequestRMA.aspx.cs" Inherits="Haeger2018.RequestRMA" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
    <style>
     
         .form-group{
             display: inline-block;
             width: 70%;
         }
        .requiredRed
        {
            width:auto;
            text-align:right;
            color:red;
            font-size:14px;
            font-weight:bold;
        }
         .requiredRed2
        {
            width:auto;
            text-align:center;
            color:red;
            font-size:14px;
            font-weight:bold;
        }
        h7
        {
            font:normal;
            color:black;
            font-size:18px;

        }

        .red
        {
            color:red;
        }

        .rbl input[type="radio"]
        {
           margin-left: 20px;
           margin-right: 1px;
           
        }

        .rbl2 input[type=radio]
        {
            margin-left:10px;
            margin-right:1px;
        }
          span1{
            color:#0085ca;
        }
       input.form-control 
        {
          width:100%;
        }
        /* The container */
.container1 {
    display:block;
    position:center;
    padding-left: 35px;
    margin-bottom: 12px;
    cursor: pointer;
    font-size: 22px;
    -webkit-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
    user-select: none;
    font-size: 16px;
}

/* Hide the browser's default checkbox */
.container1 input {
    position: absolute;
    opacity: 0;
    cursor: pointer;
    
}

 .checkbox .btn, .checkbox-inline .btn {
    padding-left: 2em;
    min-width: 8em;
    }
    .checkbox label, .checkbox-inline label {
    text-align: left;
    padding-left: 0.5em;
    }
    .checkbox input[type="checkbox"]{
        float:none;
    }


/* Create a custom checkbox */
.checkmark {
    position: absolute;
    top: 0;
    left: 0;
    height: 25px;
    width: 25px;
    background-color: red;
    
}

.container1:hover {
    background-color:greenyellow;
}

/* On mouse-over, add a grey background color */
.container1:hover input ~ .checkmark {
    background-color: #ccc;
}

/* When the checkbox is checked, add a blue background */
.container1 input:checked ~ .checkmark {
    background-color: #0085ca;
}

/* Create the checkmark/indicator (hidden when not checked) */
.checkmark:after {
    content: "";
    position: absolute;
    display: none;
}

/* Show the checkmark when checked */
.container1 input:checked ~ .checkmark:after {
    display: block;
}

/* Style the checkmark/indicator */
.container1 .checkmark:after {
    left: 9px;
    top: 5px;
    width: 5px;
    height: 10px;
    border: solid white;
    border-width: 0 3px 3px 0;
    -webkit-transform: rotate(45deg);
    -ms-transform: rotate(45deg);
    transform: rotate(45deg);
}
</style>
   
</asp:Content>


<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
     <%--Title--%>
    <div class="section-title">
        <h2 style="margin: 0;">Haeger Request RMA Form</h2>
        <h3 style="text-transform:initial; color:black; margin:0;">A <span1> PennEngineering&reg</span1> Company</h3>
    </div>
    <br />
    <%--Form--%>
    <div class="form-horizontal text-center">
        <h7>Items in <h7 class="red">RED</h7> must be filled in</h7>
        <hr />

        <%--Name, Email, ShipTo, SendTo--%>
        <div class="form-group">
            <%--Name--%>
            <div class="col-md-2 col-lg-2 col-sm-2 col-xs-12">
            <asp:Label runat="server"  AssociatedControlID="txtName" CssClass="control-label requiredRed ">Name:</asp:Label>

            </div>
            <div class="col-md-4 col-lg-4 col-sm-4 col-xs-12">
               
                <asp:TextBox runat="server" ID="txtName" CssClass="form-control" />
                <asp:RequiredFieldValidator runat="server" ControlToValidate="txtName"
                        CssClass="text-danger" ErrorMessage="The name field is required." />
            </div>
            <%--End Name--%>

            <%--Email--%>
            <div class="col-md-2 col-lg-2 col-sm-2 col-xs-12">
            <asp:Label runat="server"  AssociatedControlID="txtEmail" CssClass="control-label requiredRed ">Email:</asp:Label>

            </div>
            <div class="col-md-4 col-lg-4 col-sm-4 col-xs-12">
               
                <asp:TextBox runat="server" ID="txtEmail" CssClass="form-control" />
                <asp:RequiredFieldValidator runat="server" ControlToValidate="txtEmail"
                        CssClass="text-danger" ErrorMessage="The email field is required." />
                 <asp:RegularExpressionValidator ID="revEmail" runat="server"
                                     ErrorMessage="  * Invalid Email Format" CssClass="requiredRed" ControlToValidate="txtEmail"
                                     SetFocusOnError="True"
                                     ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*">
                    </asp:RegularExpressionValidator>
            </div>
            <%--End Email--%>

            <%--SoldTo--%>
            <div class="col-md-2 col-lg-2 col-sm-2 col-xs-12">
            <asp:Label runat="server"  AssociatedControlID="txtSoldTo" CssClass="control-label requiredRed ">Sold To:</asp:Label>

            </div>
            <div class="col-md-4 col-lg-4 col-sm-4 col-xs-12">
               
                <asp:TextBox runat="server" ID="txtSoldTo" CssClass="form-control" />
                <asp:RequiredFieldValidator runat="server" ControlToValidate="txtEmail"
                        CssClass="text-danger" ErrorMessage="The sold to field is required." />
                 
            </div>
            <%--End SoldTo--%>

            <%--ShipTo--%>
            <div class="col-md-2 col-lg-2 col-sm-2 col-xs-12">
            <asp:Label runat="server"  AssociatedControlID="txtShipTo" CssClass="control-label requiredRed ">Ship To:</asp:Label>

            </div>
            <div class="col-md-4 col-lg-4 col-sm-4 col-xs-12">
               
                <asp:TextBox runat="server" ID="txtShipTo" CssClass="form-control" />
                <asp:RequiredFieldValidator runat="server" ControlToValidate="txtShipTo"
                        CssClass="text-danger" ErrorMessage="The ship to field is required." />
                 
            </div>
            <%--End ShipTo--%>

        </div>  <%--End FormGroup--%>


        <%--ShippingAddress, OrderNum, SerialNum, Warranty?--%>
        <div class="form-group">
            <%--ShippingAddress--%>
            <div class="col-xs-12">
            <asp:Label runat="server"  AssociatedControlID="txtShipAddress" CssClass="control-label requiredRed ">Shipping Address:</asp:Label>

            </div>
            <div class="col-xs-12">
               
                <asp:TextBox runat="server" ID="txtShipAddress" CssClass="form-control center-block" />
                <asp:RequiredFieldValidator runat="server" ControlToValidate="txtShipAddress"
                        CssClass="text-danger" ErrorMessage="The quantity field is required." />
            </div>
            <%--End ShippingAddress--%>

            <%--OrderNum--%>
              <div class="col-xs-12">
                <asp:Label runat="server"  AssociatedControlID="txtOrderNum" CssClass="control-label requiredRed2">
                    Must have Order Number, Distributor PO Number, or Invoice Number
                </asp:Label>
            </div>
            <div class="col-xs-12">
                 <asp:TextBox runat="server" ID="txtOrderNum" CssClass="form-control center-block" />
                <asp:RequiredFieldValidator runat="server" ControlToValidate="txtOrderNum"
                        CssClass="text-danger" ErrorMessage="The order number field is required." />
            </div>
            <%--End OrderNum--%>

            <%--SerialNum--%>
            <div class="col-xs-12">
                <asp:Label runat="server"  AssociatedControlID="txtMachineSerial" CssClass="control-label requiredRed">
                    Machine Serial Number
                </asp:Label>
            </div>
            <div class="col-xs-12">
                 <asp:TextBox runat="server" ID="txtMachineSerial" CssClass="form-control center-block" />
                <asp:RequiredFieldValidator runat="server" ControlToValidate="txtMachineSerial"
                        CssClass="text-danger" ErrorMessage="The serial number field is required." />
            </div>
            <%--End SerialNum--%>

            <%--Warranty?--%>
            <div class="col-xs-12">
                <asp:Label runat="server"  AssociatedControlID="rblUnderWarranty" CssClass="control-label requiredRed ">
                     Is this part under warranty?:
                </asp:Label>
            </div>
            <br />
            <div class="col-xs-12">
                <asp:RadioButtonList ID="rblUnderWarranty" runat="server" RepeatDirection="Horizontal" CssClass="rbl" align="center">
                        <asp:ListItem>Yes</asp:ListItem>
                        <asp:ListItem>No</asp:ListItem>
                </asp:RadioButtonList>
                <asp:RequiredFieldValidator ID="rfvUnderWarranty" runat="server" CssClass="text-danger" 
                        ErrorMessage="The under warranty field is required." ControlToValidate="rblUnderWarranty">
                </asp:RequiredFieldValidator>
            </div>
            <%--End Warranty?--%>

        </div>  <%--End FormGroup--%>

         <%--Reason for Return--%>
        <div class="form-group">
             <div class="col-xs-12">
                <asp:Label runat="server"  AssociatedControlID="drpReasonReturn" CssClass="control-label requiredRed ">
                    Reason for Return
                </asp:Label>
            </div>
            <div class="col-xs-12">
                <asp:DropDownList ID="drpReasonReturn" class="btn btn-default dropdown-toggle center-block" runat="server" AppendDataBoundItems="true" Width="30%">
                        <asp:ListItem Text="----Make a Selection----" Value="" />
                        <asp:ListItem Text="CUSTOMER ORDERED WRONG PART" Value="CUSTOMER ORDERED WRONG PART" />
                        <asp:ListItem Text="PART DAMAGED IN SHIPMENT" Value="PART DAMAGED IN SHIPMENT" />
                        <asp:ListItem Text="INCORRECT PART SHIPPED" Value="INCORRECT PART SHIPPED" />
                        <asp:ListItem Text="REPAIR" Value="REPAIR" />
                        <asp:ListItem Text="WARRANTY" Value="WARRANTY" />
                        <asp:ListItem Text="DUPLICATED ORDER" Value="DUPLICATED ORDER" />
                        <asp:ListItem Text="REWORK" Value="REWORK" />
                        <asp:ListItem Text="DISTRIBUTOR ORDERED WRONG PART" Value="DISTRIBUTOR ORDERED WRONG PART" />
                        <asp:ListItem Text="PART NOT WORKING" Value="PART NOT WORKING" />
                        <asp:ListItem Text="PART DID NOT SOLVE PROBLEM" Value="PART DID NOT SOLVE PROBLEM" />
                    </asp:DropDownList>
                    <asp:RequiredFieldValidator ID="rfvReasonReturn" runat="server" CssClass="text-danger" ErrorMessage="The reason for return field is required."
                                                       InitialValue ="" ControlToValidate="drpReasonReturn">
                    </asp:RequiredFieldValidator>
            </div>
        </div> <%--End FormGroup--%>



        <%--PartsList--%>
        <div class="form-group center-block">
            <%--Part #1 = Required--%>
            <div class="col-sm-1">
            </div>
            <div class="col-sm-4">
                <asp:Label ID="Label1" CssClass="pull-left requiredRed" runat="server" Text="">Part Number&nbsp&nbsp</asp:Label><br />
                 <asp:RequiredFieldValidator ID="rfvPartNum" CssClass="text-danger pull-left" runat="server" Display="None" ErrorMessage="At least 1 Part Number Required" ControlToValidate="txtPartNumReq" ></asp:RequiredFieldValidator>
                <asp:TextBox ID="txtPartNumReq" runat="server" CssClass="form-control"></asp:TextBox>
               
            </div>
            <div class="col-sm-4">
               <asp:Label ID="Label2" CssClass="pull-left requiredRed" runat="server" Text="">Part Description&nbsp&nbsp</asp:Label><br />
                 <asp:RequiredFieldValidator ID="rfvPartDesc" CssClass="text-danger pull-left" runat="server" Display="None" ErrorMessage="At least 1 Part Description Required" ControlToValidate="txtPartDescReq"></asp:RequiredFieldValidator>
                <asp:TextBox ID="txtPartDescReq" runat="server" CssClass="form-control"></asp:TextBox>
               
            </div>
            <div class="col-sm-1">
                <asp:Label ID="Label3" CssClass="pull-left requiredRed" runat="server" Text="">Quantity&nbsp&nbsp</asp:Label><br />
                <asp:RequiredFieldValidator ID="rfvPartQuan" CssClass="text-danger pull-left" runat="server" Display="None" ErrorMessage="At least 1 Part Quantity Required" ControlToValidate="txtPartQuanReq" ></asp:RequiredFieldValidator>
                <asp:TextBox ID="txtPartQuanReq" runat="server" CssClass="form-control"></asp:TextBox>
               
            </div>
        </div>

        <br class="hidden-lg hidden-md hidden-sm"/>
        <br class="hidden-lg hidden-md hidden-sm"/>
        <br class="hidden-lg hidden-md hidden-sm"/>
         
        <%--Part #2--%>
        <div class="form-group inline">
            <div class="col-sm-1">
            </div>
        <div class="col-sm-4">
            <asp:Label ID="Label4" CssClass="hidden-lg hidden-md hidden-sm pull-left" runat="server" Text="Part Number"></asp:Label>
            <asp:TextBox ID="txtPartNum2" runat="server" CssClass="form-control"></asp:TextBox>
            

        </div>
        <div class="col-sm-4">
            <asp:Label ID="Label5" CssClass="hidden-lg hidden-md hidden-sm pull-left" runat="server" Text="Part Description"></asp:Label>
            <asp:TextBox ID="txtPartDesc2" runat="server" CssClass="form-control"></asp:TextBox>
            
          </div>
          <div class="col-sm-1">
            <asp:Label ID="Label6" CssClass="hidden-lg hidden-md hidden-sm pull-left" runat="server" Text="Quantity"></asp:Label>
            <asp:TextBox ID="txtPartQuan2" runat="server" CssClass="form-control"></asp:TextBox>
          </div>
        </div>

         <%--Part #3--%>
        <div class="form-group inline">
            <div class="col-sm-1">
            </div>
        <div class="col-sm-4">
            <asp:Label ID="Label7" CssClass="hidden-lg hidden-md hidden-sm pull-left" runat="server" Text="Part Number"></asp:Label>
            <asp:TextBox ID="txtPartNum3" runat="server" CssClass="form-control"></asp:TextBox>
        </div>
        <div class="col-sm-4">
            <asp:Label ID="Label8" CssClass="hidden-lg hidden-md hidden-sm pull-left" runat="server" Text="Part Description"></asp:Label>
            <asp:TextBox ID="txtPartDesc3" runat="server" CssClass="form-control"></asp:TextBox>
            
          </div>
          <div class="col-sm-1">
            <asp:Label ID="Label9" CssClass="hidden-lg hidden-md hidden-sm pull-left" runat="server" Text="Quantity"></asp:Label>
            <asp:TextBox ID="txtPartQuan3" runat="server" CssClass="form-control"></asp:TextBox>
          </div>
        </div>

         <%--Part #4--%>
        <div class="form-group inline">
            <div class="col-sm-1">
            </div>
        <div class="col-sm-4">
            <asp:Label ID="Label10" CssClass="hidden-lg hidden-md hidden-sm pull-left" runat="server" Text="Part Number"></asp:Label>
            <asp:TextBox ID="txtPartNum4" runat="server" CssClass="form-control"></asp:TextBox>
        </div>
        <div class="col-sm-4">
            <asp:Label ID="Label11" CssClass="hidden-lg hidden-md hidden-sm pull-left" runat="server" Text="Part Description"></asp:Label>
            <asp:TextBox ID="txtPartDesc4" runat="server" CssClass="form-control"></asp:TextBox>
            
          </div>
          <div class="col-sm-1">
            <asp:Label ID="Label12" CssClass="hidden-lg hidden-md hidden-sm pull-left" runat="server" Text="Quantity"></asp:Label>
            <asp:TextBox ID="txtPartQuan4" runat="server" CssClass="form-control"></asp:TextBox>
          </div>
        </div>

          <%--Part #4--%>
        <div class="form-group inline">
            <div class="col-sm-1">
            </div>
        <div class="col-sm-4">
            <asp:Label ID="Label13" CssClass="hidden-lg hidden-md hidden-sm pull-left" runat="server" Text="Part Number"></asp:Label>
            <asp:TextBox ID="txtPartNum5" runat="server" CssClass="form-control"></asp:TextBox>
        </div>
        <div class="col-sm-4">
            <asp:Label ID="Label14" CssClass="hidden-lg hidden-md hidden-sm pull-left" runat="server" Text="Part Description"></asp:Label>
            <asp:TextBox ID="txtPartDesc5" runat="server" CssClass="form-control"></asp:TextBox>
            
          </div>
          <div class="col-sm-1">
            <asp:Label ID="Label15" CssClass="hidden-lg hidden-md hidden-sm pull-left" runat="server" Text="Quantity"></asp:Label>
            <asp:TextBox ID="txtPartQuan5" runat="server" CssClass="form-control"></asp:TextBox>
          </div>
        </div>

        <%--End PartsList--%>


        <%--Reason for Return Detail--%>
        <div class="form-group">
            <div class="col-xs-12">
                <asp:Label runat="server"  AssociatedControlID="txtReasonDetail" CssClass="control-label ">
                    Reason for Return Detail
                </asp:Label>
            </div>
            <div class="col-xs-12">
                <asp:TextBox ID="txtReasonDetail" CssClass="form-control center-block" runat="server" TextMode="MultiLine" Width="100%" Columns="100" Rows="10"></asp:TextBox>
            </div>    
        </div> <%--End FormGroup--%>

         <br />
            <br />

            <%--GDPR Consent Fail Reminder--%>
            <div class="row">
                 <div class="col-md-1 col-lg-1 col-sm-1 col-xs-12 text-nowrap">
                    
                </div>
                <div class="col-md-8 col-lg-8 col-sm-8 col-xs-12">
                    <span>
                        <asp:Label ID="lblConsentWrn" CssClass="pull-left" runat="server" BorderStyle="Solid" BorderColor="GreenYellow" Text="" Visible="false">
                             <i class="fa fa-arrow-down"></i> Consent must be checked <i class="fa fa-arrow-down"></i>
                        </asp:Label>
                    </span>
                </div>
            </div>
            <%--END GDPR Consent Fail Reminder--%>

            <%--GDPR Consent Checkbox--%>
            <div class="row">
                <div class="col-md-2 col-lg-2 col-sm-2 col-xs-12 text-nowrap">
                    
                </div>
                <div class="col-md-8 col-lg-8 col-sm-8 col-xs-12">
                    <label class="container1">
                         <p>I have read the 
                            <asp:HyperLink ID="hlPrivacy" runat="server" NavigateUrl="/PrivacyPolicy" Target="_blank">Privacy Policy</asp:HyperLink>
                            /
                            <asp:HyperLink ID="hlTOS" runat="server" NavigateUrl="/TOS" Target="_blank">Terms of Use</asp:HyperLink>. 
                            I consent to the collection and use of the personal data submitted on the form above.</p>
                        <input id="gdprConsentBox" runat="server" type="checkbox">
                        <span class="checkmark"></span>
                    </label>
                </div>
            </div>
            <%--END GDPR Consent Checkbox--%>
            <br />
            <br />



        <%--Button, Result Label, Validation Summary--%>
        <div class="form-group">
            <div class="col-xs-12">
                <asp:Button ID="btnRMASubmit" runat="server" Text="Submit RMA" Width="50%" CssClass="btn btn-large btn-block btn-primary center-block" OnClick="btnRMASubmit_Click" />
                <asp:Label ID="lblResult" runat="server" Text=""></asp:Label>
                <asp:ValidationSummary ID="ValidationSummaryRMA" Width="100%" CssClass="text-danger center-block" runat="server" />

            </div>
        </div>  <%--End FormGroup--%>

        

    </div>
    
    

</asp:Content>