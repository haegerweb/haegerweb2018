﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ProductSearchNA.aspx.cs" Inherits="Haeger2018.CustomerNA.ProductSearchNA" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
    <style>
        span1
        {
            color:#0085ca;
        }
        .blink{
		    width:200px;
		    height: 50px;
	        background-color: white;
		    padding: 15px;	
		    text-align: center;
		    line-height: 50px;
	    }
	    span2{	    
		    color: red;
            background-color: lightgray;
		    animation: blink 1s linear infinite;
	    }
        @keyframes blink
        {
            0%{opacity: 0;}
            25%{opacity: 0.25;}
            50%{opacity: 0.5;}
            75%{opacity: 0.75;}
            100%{opacity: 1;}
        }


    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <div class="container-fluid">
        <div class="section-title">
            <h2 style="margin: 0;">Haeger North American Part Information</h2>
            <h3 style="text-transform:initial; color:black; margin:0;">A <span1> PennEngineering&reg</span1> Company</h3>
        </div>

        <div class="row">
            <div class="col-sm-12 text-center">
                <p class="lead text-center">Please enter <b>Part Number</b> or <b>Description</b> and click Enter. If no information is shown, the part number does not exist</p>
                <h3 class="text-center" style="color:red">
                    <b><span2>LEAD TIME</span2> on stock items is <span2>now 3 days</span2> due to short staffing because of the COVID-19 situation. 
                        <br />Hot overnight orders will still ship same day if the order is received by 1:00PM pacific time.  </b>
                    
                </h3> 

                <br />
                <br />    
                <h4>Enter Part Number:</h4>
                
                <asp:Panel ID="Panel1" runat="server" DefaultButton="btnPartNumberNA">
                <asp:UpdatePanel ID="upbtnPart" runat="server">
                    <ContentTemplate>
                        <asp:TextBox ID="txtPartNumberNA" CssClass="form-control input-lg center-block" runat="server"></asp:TextBox>
                        <asp:Button ID="btnPartNumberNA" CssClass="btn btn-large" runat="server" Text="Search" OnClick="btnPartNumberNA_Click" />
                    </ContentTemplate>

                </asp:UpdatePanel>

                
                <br />
                <br />
                



                </asp:Panel>
                <asp:UpdatePanel ID="upPartNumNA" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <asp:GridView ID="grdPartNumNA" CssClass="table table-striped table-bordered table-hover"  runat="server" AutoGenerateColumns="True" Visible="False" ></asp:GridView>

                    </ContentTemplate>

                </asp:UpdatePanel>
                
                
            </div>

        </div>
        <div class="row">
            <div class="col-sm-12 text-center">
                <h4>Enter Description:</h4>
                <asp:Panel ID="panDescripNA" runat="server" DefaultButton="btnDescripNA">
                    <asp:UpdatePanel ID="upDescrip" runat="server">
                    <ContentTemplate>
                        <asp:TextBox ID="txtDescripNA" CssClass="form-control input-lg center-block" runat="server"></asp:TextBox>
                        <asp:Button ID="btnDescripNA" CssClass="btn btn-large" runat="server" Text="Search" OnClick="btnDescripNA_Click" DefaultButton="btnDescripNA" />


                    </ContentTemplate>

                </asp:UpdatePanel>

                </asp:Panel>

                

                <br />
                <br />
                 <asp:UpdatePanel ID="upDescripNA" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <asp:GridView ID="grdDescripNA" CssClass="table table-striped table-bordered table-hover" runat="server" AutoGenerateColumns="True" Visible="False"></asp:GridView>
                    </ContentTemplate>
                 </asp:UpdatePanel>

                
                

            </div>


        </div>
    </div>



</asp:Content>
