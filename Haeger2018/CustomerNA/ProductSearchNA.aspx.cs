﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Data.Linq.SqlClient;
using System.Windows.Forms;

namespace Haeger2018.CustomerNA
{
    public partial class ProductSearchNA : System.Web.UI.Page
    {
        DataTable dtMain = new DataTable();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (IsPostBack)
            {
                dtMain = loadDT();
            }

            //grdPartNumNA.DataSource = dtMain;
            //grdPartNumNA.DataBind();

            //grdPartNumNA.Visible = true;

        }

        private DataTable loadDT()
        {
            DataTable dt = new DataTable();

            string searchQuery = @"SELECT ITEMID as 'Part Number', ITEMNAME AS 'Item Name', '$'+convert(varchar(50), CAST(price as money), -1) AS 'Price', CAST(CAST(qty_onhand as decimal(10,4))  as varchar(40)) AS 'Quantity on Hand', leadtime AS 'Lead Time', cost 
                                    FROM haegerwebitems
                                    WHERE packaginggroupid = 'Show'";

            using (SqlConnection searchConn = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["HaegerSalesConnectionString-Dist-PartInfo-NA"].ConnectionString))
            {
                using (SqlCommand searchCommand = new SqlCommand(searchQuery, searchConn))
                {

                    SqlDataAdapter da = new SqlDataAdapter(searchCommand);
                    da.Fill(dt);

                }

            }

            dt.Select(string.Format("[cost] > {0} AND [cost] < {1}", "0", "1")).ToList<DataRow>().ForEach(r => r["Quantity on Hand"] = "In Stock");



            return dt;
        }

        protected void btnPartNumberNA_Click(object sender, EventArgs e)
        {
            //MessageBox.Show("PN - " + sender.ToString());
            string txtPartNum = txtPartNumberNA.Text;

            DataTable dtDisplayPN = new DataTable();
            
            dtDisplayPN = QueryPartNum(txtPartNum);
            grdPartNumNA.DataSource = dtDisplayPN;
            grdPartNumNA.DataBind();
            grdPartNumNA.Visible = true;
            upPartNumNA.Update();

        }

        private DataTable QueryPartNum(string partNum)
        {
           
            DataTable dtPartNum = new DataTable();
            // var result = collection.Where(o => stringsToCheck.Any(a => o.Name.Contains(a)));
            var results = dtMain.AsEnumerable().Where(r => r.Field<string>("Part Number").ToLower().Contains(partNum.ToLower())).Select(r => r);
            dtPartNum = (results.Any() && !String.IsNullOrEmpty(partNum)) ? results.CopyToDataTable() : dtMain.Clone();
            dtPartNum.Columns.Remove("cost");

            return dtPartNum;
        }

        private DataTable QueryDescrip(string itemName)
        {
            DataTable dtDescrip = new DataTable();

            var results = dtMain.AsEnumerable().Where(r => r.Field<string>("Item Name").ToLower().Contains(itemName.ToLower())).Select(r => r);
            dtDescrip = (results.Any() && !String.IsNullOrEmpty(itemName)) ? results.CopyToDataTable() : dtMain.Clone();
            dtDescrip.Columns.Remove("cost");
            return dtDescrip;
        }

        protected void btnDescripNA_Click(object sender, EventArgs e)
        {
            string txtDescrip = txtDescripNA.Text;

            DataTable dtDisplayDS = new DataTable();
            
            dtDisplayDS = QueryDescrip(txtDescrip);
            grdDescripNA.DataSource = dtDisplayDS;
            grdDescripNA.DataBind();
            grdDescripNA.Visible = true;
            upDescripNA.Update();

        }

       

    }
}