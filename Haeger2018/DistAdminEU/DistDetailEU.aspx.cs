﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Haeger2018.DistAdminEU
{
    public partial class DistDetailEU : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnBackMain_Click(object sender, EventArgs e)
        {
            Response.Redirect("/DistAdminEU/DistAdminEUPage");
        }

        protected void btnBackAll_Click(object sender, EventArgs e)
        {
            Response.Redirect("/DistAdminEU/DistAllEU");
        }

    }

}