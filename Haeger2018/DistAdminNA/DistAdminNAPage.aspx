﻿<%@ Page Title="Dist Admin NA" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="DistAdminNAPage.aspx.cs" Inherits="Haeger2018.DistAdminNA.DistAdminNAPage" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
    <style>
        span1{
            color:#0085ca;
        }

    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
     <div class="container-fluid">    
        <div class="section-title">
            <h2 style="margin: 0;">Haeger North American Distributor Administration Page</h2>
            <h3 style="text-transform:initial; color:black; margin:0;">A <span1> PennEngineering&reg</span1> Company</h3>
        </div>
        <br />
        <asp:Button ID="btnAllDist" CssClass="btn btn-primary" runat="server" Text="All Distributors" OnClick="btnAllDist_Click" />
        <hr />
        <div class="row">
            <div class="col-sm-4">
                <h3>Distributors to be Approved:</h3>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <asp:GridView ID="grdDistAdminNA"  CssClass= "table table-striped table-bordered table-condensed" AutoGenerateEditButton="True" 
                    runat="server" AutoGenerateColumns="False" EmptyDataText="Currently No Distributors to Approve" DataKeyNames="Id" DataSourceID="sqlDistAdminNA">
                    <Columns>
                        <asp:BoundField DataField="Id" HeaderText="Id" ReadOnly="True" SortExpression="Id" Visible="false"/>
                        <asp:BoundField DataField="CompanyName" HeaderText="Company Name" SortExpression="CompanyName" />
                        <asp:BoundField DataField="ContactName" HeaderText="Contact Name" SortExpression="ContactName" />
                        <asp:BoundField DataField="ContactPhone" HeaderText="Contact Phone" SortExpression="ContactPhone" />
                        <asp:CheckBoxField DataField="LockoutEnabled" HeaderText="Approved" SortExpression="LockoutEnabled"></asp:CheckBoxField>
                        <asp:CheckBoxField DataField="EmailConfirmed" HeaderText="Email Confirmed" SortExpression="EmailConfirmed" />
                    </Columns>
                </asp:GridView>
                <asp:SqlDataSource ID="sqlDistAdminNA" runat="server" ConnectionString="<%$ ConnectionStrings:aspnet-Haeger2018-MembershipConnectionString-dist-admin-NA %>" 
                    DeleteCommand="DELETE FROM [AspNetUsers] WHERE [Id] = @original_Id" 
                    InsertCommand="INSERT INTO [AspNetUsers] ([Id], [CompanyName], [ContactName], [ContactPhone], [LockoutEnabled], [EmailConfirmed]) VALUES (@Id, @CompanyName, @ContactName, @ContactPhone, @LockoutEnabled, @EmailConfirmed)" 
                    OldValuesParameterFormatString="original_{0}" 
                     SelectCommand="SELECT AspNetUsers.Id, CompanyName, ContactName, ContactPhone, LockoutEnabled,EmailConfirmed
                                    FROM AspNetUsers
                                    LEFT JOIN AspNetUserRoles ON AspNetUsers.Id = UserId
                                    LEFT JOIN AspNetRoles ON RoleId = AspNetRoles.Id
                                    WHERE AspNetRoles.Name = 'DistributorNA'
                                    AND LockoutEnabled = 'False'"      
                    UpdateCommand="UPDATE [AspNetUsers] SET [CompanyName] = @CompanyName, [ContactName] = @ContactName, [ContactPhone] = @ContactPhone, [LockoutEnabled] = @LockoutEnabled, [EmailConfirmed] = @EmailConfirmed WHERE [Id] = @original_Id">
                    <DeleteParameters>
                        <asp:Parameter Name="original_Id" Type="String" />
                    </DeleteParameters>
                    <InsertParameters>
                        <asp:Parameter Name="Id" Type="String" />
                        <asp:Parameter Name="CompanyName" Type="String" />
                        <asp:Parameter Name="ContactName" Type="String" />
                        <asp:Parameter Name="ContactPhone" Type="String" />
                        <asp:Parameter Name="LockoutEnabled" Type="Boolean" />
                        <asp:Parameter Name="EmailConfirmed" Type="Boolean" />
                    </InsertParameters>
                    <UpdateParameters>
                        <asp:Parameter Name="CompanyName" Type="String" />
                        <asp:Parameter Name="ContactName" Type="String" />
                        <asp:Parameter Name="ContactPhone" Type="String" />
                        <asp:Parameter Name="LockoutEnabled" Type="Boolean" />
                        <asp:Parameter Name="EmailConfirmed" Type="Boolean" />
                        <asp:Parameter Name="original_Id" Type="String" />
                    </UpdateParameters>
                </asp:SqlDataSource>


            </div>

        </div>
    
    
    
    
    
    </div>

</asp:Content>
