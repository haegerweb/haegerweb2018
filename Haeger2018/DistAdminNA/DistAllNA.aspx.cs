﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Haeger2018.DistAdminNA
{
    public partial class DistAllNA : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnDistApp_Click(object sender, EventArgs e)
        {
            Response.Redirect("/DistAdminNA/DistAdminNAPage");
        }

        protected void grdAllDistNA_SelectedIndexChanged(object sender, EventArgs e)
        {
            Session["selectedItem"] = (String)grdAllDistNA.SelectedDataKey.Values["Id"];
            Response.Redirect("/DistAdminNA/DistDetailNA");
        }
    }
}