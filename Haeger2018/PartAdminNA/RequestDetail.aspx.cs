﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Net.Mail;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Haeger2018.PartAdminNA
{
    public partial class RequestDetail : System.Web.UI.Page
    {
        // Database connection used to store or retrieve time sheets, also to find Partner scheduled hours.
        SqlConnection haegerTimeDB = new SqlConnection(ConfigurationManager.ConnectionStrings["HaegerTimeConnectionString"].ConnectionString);

        // Current date
        DateTime localDate = DateTime.Now;

        // ******************** Test date - DELETE FOR PRODUCTION ********************
        //DateTime localDate = new DateTime(2018, 9, 13);

        int currentYear;

        // Basic info
        string OakdaleID;
        int TrackNum;
        string currentID;
        string fName;
        string lName;
        string partnerEmail;
        int weekNumReq;
        int yearNumReq;
        DateTime dateCreated;
        string teamLead;
        string showNoShow;
        string reqYear;
        string status;

        // Monday
        Decimal regHoursMon;
        string commentMon;

        // Tuesday
        Decimal regHoursTue;
        string commentTue;

        // Wednesday
        Decimal regHoursWed;
        string commentWed;

        // Thursday
        Decimal regHoursThu;
        string commentThu;

        // Friday
        Decimal regHoursFri;
        string commentFri;

        // Saturday
        Decimal regHoursSat;
        string commentSat;

        // Sunday
        Decimal regHoursSun;
        string commentSun;

        // Vacation Hours
        Decimal vacMon;
        Decimal vacTue;
        Decimal vacWed;
        Decimal vacThu;
        Decimal vacFri;
        Decimal vacSat;
        Decimal vacSun;

        // ARP Hours
        Decimal arpMon;
        Decimal arpTue;
        Decimal arpWed;
        Decimal arpThu;
        Decimal arpFri;
        Decimal arpSat;
        Decimal arpSun;

        // Holiday Hours
        Decimal holMon;
        Decimal holTue;
        Decimal holWed;
        Decimal holThu;
        Decimal holFri;
        Decimal holSat;
        Decimal holSun;

        // Bereavement Hours
        Decimal berMon;
        Decimal berTue;
        Decimal berWed;
        Decimal berThu;
        Decimal berFri;
        Decimal berSat;
        Decimal berSun;

        // Jury Duty Hours
        Decimal jurMon;
        Decimal jurTue;
        Decimal jurWed;
        Decimal jurThu;
        Decimal jurFri;
        Decimal jurSat;
        Decimal jurSun;

        // Define variables for Reg hours and OT
        // Regular Hours
        Decimal regMon;
        Decimal regTue;
        Decimal regWed;
        Decimal regThu;
        Decimal regFri;
        Decimal regSat;
        Decimal regSun;

        // OT Hours
        Decimal otMon;
        Decimal otTue;
        Decimal otWed;
        Decimal otThu;
        Decimal otFri;
        Decimal otSat;
        Decimal otSun;

        // Double OT Hours
        Decimal dotMon;
        Decimal dotTue;
        Decimal dotWed;
        Decimal dotThu;
        Decimal dotFri;
        Decimal dotSat;
        Decimal dotSun;

        Decimal regTotal;
        Decimal vacTotal;
        Decimal arpTotal;
        Decimal holTotal;
        Decimal berTotal;
        Decimal jurTotal;
        Decimal otTotal;
        Decimal dotTotal;

        string teamLeadEmail;


        protected void Page_Load(object sender, EventArgs e)
        {
            // Identify logged in user.
            //currentID = User.Identity.GetUserId();

            

            // Current cultural information.
            CultureInfo ciCurr = CultureInfo.CurrentCulture;
            // Current Week number to verify existing request.
            int weekNumCurrent = ciCurr.Calendar.GetWeekOfYear(localDate, CalendarWeekRule.FirstFourDayWeek, DayOfWeek.Monday);
            currentYear = ciCurr.Calendar.GetYear(localDate);

            haegerTimeDB.Open();

            // OakdaleTime PK associated with selected row
            string partner_OakdaleID = Session["selectedItemTimeOff"].ToString();

            // Query to get selected Partner's UserID
            string userIDQuery = "SELECT UserID, WeekNum, ReqYear FROM OakdaleRequests WHERE _OakdaleRQ_ID = '" + partner_OakdaleID + "'";
            SqlCommand userIDCommand = new SqlCommand(userIDQuery, haegerTimeDB);

            using (SqlDataReader uID = userIDCommand.ExecuteReader())
            {
                while (uID.Read())
                {
                    currentID = uID[0].ToString();
                    weekNumReq = Convert.ToInt32(uID[1]);
                    yearNumReq = Convert.ToInt32(uID[2]);

                }
            }


            // Retrieve first name from PartnerInfo if user is logged in.
            string currentContact = null;
            string contactNameQuery = "Select DISTINCT fName, lname, TeamLead, Email FROM OakdalePartnerInfo WHERE UserID = '" + currentID + "'";
            SqlCommand currentContactCommand = new SqlCommand(contactNameQuery, haegerTimeDB);

            using (SqlDataReader tm = currentContactCommand.ExecuteReader())
            {
                while (tm.Read())
                {
                    fName = tm[0].ToString();
                    lName = tm[1].ToString();
                    teamLead = tm[2].ToString();
                    currentContact = fName + " " + lName;
                    partnerEmail = tm[3].ToString();
                }
            }

            // In case no one is logged in.
            if (currentContact != null)
            {
                lblPartnerName.Text = currentContact;
            }
            else
            {
                lblPartnerName.Text = "Nobody";
            }

            string teamLeadEmailQuery = "Select Email from OakdaleTeamLeadInfo WHERE TeamLead = '" + teamLead + "'";
            SqlCommand teamLeadEmailCommand = new SqlCommand(teamLeadEmailQuery, haegerTimeDB);
            using (SqlDataReader tl = teamLeadEmailCommand.ExecuteReader())
            {
                while (tl.Read())
                {
                    teamLeadEmail = tl[0].ToString();

                }
            }

            // Find first day of week
            DateTime Firstday = FirstDateOfWeekISO8601(yearNumReq, weekNumReq);

            // Monday and Sunday of current week.
            string monday = Firstday.ToLongDateString();
            string sunday = Firstday.AddDays(6).ToLongDateString();

            // Week Number Label
            lblWeekNum.Text = weekNumReq.ToString();
            // Week Range Label
            lblDateRange.Text = monday + " to " + sunday;

            // Populate the dates for current week.
            lblDateMon.Text = Firstday.ToShortDateString().Trim();
            lblDateTue.Text = (Firstday.AddDays(1)).ToShortDateString().Trim();
            lblDateWed.Text = (Firstday.AddDays(2)).ToShortDateString().Trim();
            lblDateThu.Text = (Firstday.AddDays(3)).ToShortDateString().Trim();
            lblDateFri.Text = (Firstday.AddDays(4)).ToShortDateString().Trim();
            lblDateSat.Text = (Firstday.AddDays(5)).ToShortDateString().Trim();
            lblDateSun.Text = (Firstday.AddDays(6)).ToShortDateString().Trim();

            if (!IsPostBack)
            {
                // Query for OakdaleRequests to see if User has request for current week.
                //string requests = "SELECT * FROM OakdaleRequests WHERE UserID = '" + currentID + "' AND WeekNum = " + weekNumReq + " ORDER BY DateCreated DESC";
                string requests = "SELECT * FROM OakdaleRequests WHERE _OakdaleRQ_ID = '" + partner_OakdaleID + "'";

                SqlCommand requestsCommand = new SqlCommand(requests, haegerTimeDB);
                SqlDataReader requestsReader = requestsCommand.ExecuteReader();
                DataTable rqT = new DataTable();
                rqT.Load(requestsReader);
                int rqtCount = rqT.Rows.Count;

                if (rqtCount > 0)
                {
                    lblHrRegMon.Text = rqT.Rows[0][8].ToString();
                    txtHrVacMon.Text = rqT.Rows[0][9].ToString();
                    txtHrARPMon.Text = rqT.Rows[0][10].ToString();
                    txtHrHolidayMon.Text = rqT.Rows[0][11].ToString();
                    txtHrBereaveMon.Text = rqT.Rows[0][12].ToString();
                    txtHrJuryMon.Text = rqT.Rows[0][13].ToString();
                    txtHROTMon.Text = rqT.Rows[0][14].ToString();
                    txtCommMon.Text = rqT.Rows[0][15].ToString();

                    lblHrRegTue.Text = rqT.Rows[0][16].ToString();
                    txtHrVacTue.Text = rqT.Rows[0][17].ToString();
                    txtHrARPTue.Text = rqT.Rows[0][18].ToString();
                    txtHrHolidayTue.Text = rqT.Rows[0][19].ToString();
                    txtHrBereaveTue.Text = rqT.Rows[0][20].ToString();
                    txtHrJuryTue.Text = rqT.Rows[0][21].ToString();
                    txtHROTTue.Text = rqT.Rows[0][22].ToString();
                    txtCommTue.Text = rqT.Rows[0][23].ToString();

                    lblHrRegWed.Text = rqT.Rows[0][24].ToString();
                    txtHrVacWed.Text = rqT.Rows[0][25].ToString();
                    txtHrARPWed.Text = rqT.Rows[0][26].ToString();
                    txtHrHolidayWed.Text = rqT.Rows[0][27].ToString();
                    txtHrBereaveWed.Text = rqT.Rows[0][28].ToString();
                    txtHrJuryWed.Text = rqT.Rows[0][29].ToString();
                    txtHROTWed.Text = rqT.Rows[0][30].ToString();
                    txtCommWed.Text = rqT.Rows[0][31].ToString();

                    lblHrRegThu.Text = rqT.Rows[0][32].ToString();
                    txtHrVacThu.Text = rqT.Rows[0][33].ToString();
                    txtHrARPThu.Text = rqT.Rows[0][34].ToString();
                    txtHrHolidayThu.Text = rqT.Rows[0][35].ToString();
                    txtHrBereaveThu.Text = rqT.Rows[0][36].ToString();
                    txtHrJuryThu.Text = rqT.Rows[0][37].ToString();
                    txtHROTThu.Text = rqT.Rows[0][38].ToString();
                    txtCommThu.Text = rqT.Rows[0][39].ToString();

                    lblHrRegFri.Text = rqT.Rows[0][40].ToString();
                    txtHrVacFri.Text = rqT.Rows[0][41].ToString();
                    txtHrARPFri.Text = rqT.Rows[0][42].ToString();
                    txtHrHolidayFri.Text = rqT.Rows[0][43].ToString();
                    txtHrBereaveFri.Text = rqT.Rows[0][44].ToString();
                    txtHrJuryFri.Text = rqT.Rows[0][45].ToString();
                    txtHROTFri.Text = rqT.Rows[0][46].ToString();
                    txtCommFri.Text = rqT.Rows[0][47].ToString();

                    lblHrRegSat.Text = rqT.Rows[0][48].ToString();
                    txtHrVacSat.Text = rqT.Rows[0][49].ToString();
                    txtHrARPSat.Text = rqT.Rows[0][50].ToString();
                    txtHrHolidaySat.Text = rqT.Rows[0][51].ToString();
                    txtHrBereaveSat.Text = rqT.Rows[0][52].ToString();
                    txtHrJurySat.Text = rqT.Rows[0][53].ToString();
                    txtHROTSat.Text = rqT.Rows[0][54].ToString();
                    txtCommSat.Text = rqT.Rows[0][55].ToString();

                    lblHrRegSun.Text = rqT.Rows[0][56].ToString();
                    txtHrVacSun.Text = rqT.Rows[0][57].ToString();
                    txtHrARPSun.Text = rqT.Rows[0][58].ToString();
                    txtHrHolidaySun.Text = rqT.Rows[0][59].ToString();
                    txtHrBereaveSun.Text = rqT.Rows[0][60].ToString();
                    txtHrJurySun.Text = rqT.Rows[0][61].ToString();
                    txtHROTSun.Text = rqT.Rows[0][62].ToString();
                    txtCommSun.Text = rqT.Rows[0][63].ToString();

                    status = rqT.Rows[0][64].ToString();
                    reqYear = rqT.Rows[0][65].ToString();

                    //lblTotalReg.Text = rqT.Rows[0][66].ToString();
                    //lblTotalVac.Text = rqT.Rows[0][67].ToString();
                    //lblTotalARP.Text = rqT.Rows[0][68].ToString();
                    //lblTotalHoliday.Text = rqT.Rows[0][69].ToString();
                    //lblTotalBereave.Text = rqT.Rows[0][70].ToString();
                    //lblTotalJuryDuty.Text = rqT.Rows[0][71].ToString();
                    //lblTotalOT.Text = rqT.Rows[0][72].ToString();
                    //lblTotalDOT.Text = rqT.Rows[0][81].ToString();


                    txtHRDOTMon.Text = rqT.Rows[0][74].ToString();
                    txtHRDOTTue.Text = rqT.Rows[0][75].ToString();
                    txtHRDOTWed.Text = rqT.Rows[0][76].ToString();
                    txtHRDOTThu.Text = rqT.Rows[0][77].ToString();
                    txtHRDOTFri.Text = rqT.Rows[0][78].ToString();
                    txtHRDOTSat.Text = rqT.Rows[0][79].ToString();
                    txtHRDOTSun.Text = rqT.Rows[0][80].ToString();



                }
                //else
                //{
                //    string getNormalHours = "SELECT * FROM OakdaleTimeDefault";

                //    SqlCommand getNormalHoursCommand = new SqlCommand(getNormalHours, haegerTimeDB);

                //    if (IsPostBack)
                //    {
                //        using (SqlDataReader gNH = getNormalHoursCommand.ExecuteReader())
                //        {
                //            while (gNH.Read())
                //            {

                //                // Monday
                //                regHoursMon = Convert.ToDecimal(gNH[8]);

                //                // Tuesday
                //                regHoursTue = Convert.ToDecimal(gNH[16]);

                //                // Wednesday
                //                regHoursWed = Convert.ToDecimal(gNH[24]);

                //                // Thursday
                //                regHoursThu = Convert.ToDecimal(gNH[32]);

                //                // Friday
                //                regHoursFri = Convert.ToDecimal(gNH[40]);

                //                // Saturday
                //                regHoursSat = 0.00m;

                //                // Sunday
                //                regHoursSun = 0.00m;

                //                lblHrRegMon.Text = regHoursMon.ToString();
                //                lblHrRegTue.Text = regHoursTue.ToString();
                //                lblHrRegWed.Text = regHoursWed.ToString();
                //                lblHrRegThu.Text = regHoursThu.ToString();
                //                lblHrRegFri.Text = regHoursFri.ToString();
                //                lblHrRegSat.Text = regHoursSat.ToString("0.00");
                //                lblHrRegSun.Text = regHoursSun.ToString("0.00");

                //            }
                //        }
                //    }
                //}
            }





            haegerTimeDB.Close();


            //btnCalc_Click(sender, e);

        }

        public static DateTime FirstDateOfWeekISO8601(int year, int weekOfYear)
        {
            DateTime jan1 = new DateTime(year, 1, 1);
            int daysOffset = DayOfWeek.Thursday - jan1.DayOfWeek;

            // Use first Thursday in January to get first week of the year as
            // it will never be in Week 52/53
            DateTime firstThursday = jan1.AddDays(daysOffset);
            var cal = CultureInfo.CurrentCulture.Calendar;
            int firstWeek = cal.GetWeekOfYear(firstThursday, CalendarWeekRule.FirstFourDayWeek, DayOfWeek.Monday);

            var weekNum = weekOfYear;
            // As we're adding days to a date in Week 1,
            // we need to subtract 1 in order to get the right date for week #1
            if (firstWeek == 1)
            {
                weekNum -= 1;
            }

            // Using the first Thursday as starting week ensures that we are starting in the right year
            // then we add number of weeks multiplied with days
            var result = firstThursday.AddDays(weekNum * 7);

            // Subtract 3 days from Thursday to get Monday, which is the first weekday in ISO8601
            return result.AddDays(-3);
        }


        protected void btnCalc_Click(object sender, EventArgs e)
        {
            // Define MID5 textbox varaibles.
            // Vacation Hours
            vacMon = VerifyNull(txtHrVacMon.Text, txtHrVacMon);
            vacTue = VerifyNull(txtHrVacTue.Text, txtHrVacTue);
            vacWed = VerifyNull(txtHrVacWed.Text, txtHrVacWed);
            vacThu = VerifyNull(txtHrVacThu.Text, txtHrVacThu);
            vacFri = VerifyNull(txtHrVacFri.Text, txtHrVacFri);
            vacSat = VerifyNull(txtHrVacSat.Text, txtHrVacSat);
            vacSun = VerifyNull(txtHrVacSun.Text, txtHrVacSun);


            // ARP Hours
            arpMon = VerifyNull(txtHrARPMon.Text, txtHrARPMon);
            arpTue = VerifyNull(txtHrARPTue.Text, txtHrARPTue);
            arpWed = VerifyNull(txtHrARPWed.Text, txtHrARPWed);
            arpThu = VerifyNull(txtHrARPThu.Text, txtHrARPThu);
            arpFri = VerifyNull(txtHrARPFri.Text, txtHrARPFri);
            arpSat = VerifyNull(txtHrARPSat.Text, txtHrARPSat);
            arpSun = VerifyNull(txtHrARPSun.Text, txtHrARPSun);

            // Holiday Hours
            holMon = VerifyNull(txtHrHolidayMon.Text, txtHrHolidayMon);
            holTue = VerifyNull(txtHrHolidayTue.Text, txtHrHolidayTue);
            holWed = VerifyNull(txtHrHolidayWed.Text, txtHrHolidayWed);
            holThu = VerifyNull(txtHrHolidayThu.Text, txtHrHolidayThu);
            holFri = VerifyNull(txtHrHolidayFri.Text, txtHrHolidayFri);
            holSat = VerifyNull(txtHrHolidaySat.Text, txtHrHolidaySat);
            holSun = VerifyNull(txtHrHolidaySun.Text, txtHrHolidaySun);

            // Bereavement Hours
            berMon = VerifyNull(txtHrBereaveMon.Text, txtHrBereaveMon);
            berTue = VerifyNull(txtHrBereaveTue.Text, txtHrBereaveTue);
            berWed = VerifyNull(txtHrBereaveWed.Text, txtHrBereaveWed);
            berThu = VerifyNull(txtHrBereaveThu.Text, txtHrBereaveThu);
            berFri = VerifyNull(txtHrBereaveFri.Text, txtHrBereaveFri);
            berSat = VerifyNull(txtHrBereaveSat.Text, txtHrBereaveSat);
            berSun = VerifyNull(txtHrBereaveSun.Text, txtHrBereaveSun);

            // Jury Duty Hours
            jurMon = VerifyNull(txtHrJuryMon.Text, txtHrJuryMon);
            jurTue = VerifyNull(txtHrJuryTue.Text, txtHrJuryTue);
            jurWed = VerifyNull(txtHrJuryWed.Text, txtHrJuryWed);
            jurThu = VerifyNull(txtHrJuryThu.Text, txtHrJuryThu);
            jurFri = VerifyNull(txtHrJuryFri.Text, txtHrJuryFri);
            jurSat = VerifyNull(txtHrJurySat.Text, txtHrJurySat);
            jurSun = VerifyNull(txtHrJurySun.Text, txtHrJurySun);


            // Define variables for Reg hours and OT
            // Regular Hours
            regMon = VerifyNull(lblHrRegMon.Text);
            regTue = VerifyNull(lblHrRegTue.Text);
            regWed = VerifyNull(lblHrRegWed.Text);
            regThu = VerifyNull(lblHrRegThu.Text);
            regFri = VerifyNull(lblHrRegFri.Text);
            regSat = VerifyNull(lblHrRegSat.Text);
            regSun = VerifyNull(lblHrRegSun.Text);

            // MID5 Hours = Vacation + ARP + UnpaidLv + Bereave + Volunteer <= 8
            // Calculate MID5 hours
            Decimal mid5TotalMon = vacMon + arpMon + holMon + berMon + jurMon;
            Decimal mid5TotalTue = vacTue + arpTue + holTue + berTue + jurTue;
            Decimal mid5TotalWed = vacWed + arpWed + holWed + berWed + jurWed;
            Decimal mid5TotalThu = vacThu + arpThu + holThu + berThu + jurThu;
            Decimal mid5TotalFri = vacFri + arpFri + holFri + berFri + jurFri;
            Decimal mid5TotalSat = vacSat + arpSat + holSat + berSat + jurSat;
            Decimal mid5TotalSun = vacSun + arpSun + holSun + berSun + jurSun;

            // Array for days of the week
            string[] days = new string[] { "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday" };

            // Array for MID5 totals
            Decimal[] mid5Totals = new Decimal[] { mid5TotalMon, mid5TotalTue, mid5TotalWed, mid5TotalThu, mid5TotalFri };

            // Array for regular hour label names.
            Label[] regHours = new Label[] { lblHrRegMon, lblHrRegTue, lblHrRegWed, lblHrRegThu, lblHrRegFri };


            bool exit = false;

            // Loop to iterate through regular hour labels to enter 8 minus MID5 total.
            for (int i = 0; i <= mid5Totals.Length - 1; i++)
            {
                if (mid5Totals[i] > 8)
                {
                    string msg = "The row for " + days[i] + " adds to more than 8 hours. Please make the correction";
                    Response.Write("<script>alert('" + msg + "')</script>");

                    exit = true;
                    break;
                }
                else
                {
                    regHours[i].Text = (8 - mid5Totals[i]).ToString("0.00");

                }


            }

            if (exit == true)
            {
                btnApprove.Enabled = false;
                btnDeny.Enabled = false;

                return;
            }

            // Define variables for Reg hours and OT
            // Regular Hours
            regMon = VerifyNull(lblHrRegMon.Text);
            regTue = VerifyNull(lblHrRegTue.Text);
            regWed = VerifyNull(lblHrRegWed.Text);
            regThu = VerifyNull(lblHrRegThu.Text);
            regFri = VerifyNull(lblHrRegFri.Text);
            regSat = VerifyNull(lblHrRegSat.Text);
            regSun = VerifyNull(lblHrRegSun.Text);
            // Overtime Hours
            otMon = VerifyNull(txtHROTMon.Text, txtHROTMon);
            otTue = VerifyNull(txtHROTTue.Text, txtHROTTue);
            otWed = VerifyNull(txtHROTWed.Text, txtHROTWed);
            otThu = VerifyNull(txtHROTThu.Text, txtHROTThu);
            otFri = VerifyNull(txtHROTFri.Text, txtHROTFri);
            otSat = VerifyNull(txtHROTSat.Text, txtHROTSat);
            otSun = VerifyNull(txtHROTSun.Text, txtHROTSun);



            // Array for column values to total rows.
            Decimal[] colTotalREG = new decimal[] { regMon, regTue, regWed, regThu, regFri };

            Decimal[] colTotalVAC = new decimal[] { vacMon, vacTue, vacWed, vacThu, vacFri };

            Decimal[] colTotalARP = new decimal[] { arpMon, arpTue, arpWed, arpThu, arpFri };

            Decimal[] colTotalUNL = new decimal[] { holMon, holTue, holWed, holThu, holFri };

            Decimal[] colTotalBER = new decimal[] { berMon, berTue, berWed, berThu, berFri };

            Decimal[] colTotalVOL = new decimal[] { jurMon, jurTue, jurWed, jurThu, jurFri };

            Decimal[] colTotalOT = new decimal[] { otMon, otTue, otWed, otThu, otFri };

            Decimal[] colTotalDOT = new decimal[] { dotMon, dotTue, dotWed, dotThu, dotFri };

            Decimal[][] allColumns = new decimal[][] { colTotalREG, colTotalVAC, colTotalARP, colTotalUNL, colTotalBER, colTotalVOL, colTotalOT, colTotalDOT };

            Label[] colLblTotals = new Label[] { lblTotalReg, lblTotalVac, lblTotalARP, lblTotalHoliday, lblTotalBereave, lblTotalJuryDuty, lblTotalOT, lblTotalDOT };

            // Variable for columnTotal.
            Decimal columnTotal = 0;


            // Loop to sum columns and assign to label totals
            for (int j = 0; j <= allColumns.Length - 1; j++)
            {
                for (int k = 0; k <= allColumns[j].Length - 1; k++)
                {
                    columnTotal = columnTotal + allColumns[j][k];

                }

                colLblTotals[j].Text = columnTotal.ToString("0.00");
                columnTotal = 0;
            }

            btnApprove.Enabled = true;
            btnDeny.Enabled = true;

        }

        public Decimal VerifyNull(string textBox, TextBox txtName)
        {
            Decimal value;

            if (textBox == null)
            {
                value = 0.00m;
            }
            else
            {
                value = Convert.ToDecimal(textBox);
                txtName.Font.Bold = true;
            }

            return value;
        }

        public Decimal VerifyNull(string textBox)
        {
            Decimal value;

            if (textBox == "")
            {
                value = 0;
            }
            else
            {
                value = Convert.ToDecimal(textBox);
            }

            return value;
        }

        protected void btnApprove_Click(object sender, EventArgs e)
        {
            //btnCalc_Click(sender, e);
            // Vacation Hours
            vacMon = VerifyNull(txtHrVacMon.Text, txtHrVacMon);
            vacTue = VerifyNull(txtHrVacTue.Text, txtHrVacTue);
            vacWed = VerifyNull(txtHrVacWed.Text, txtHrVacWed);
            vacThu = VerifyNull(txtHrVacThu.Text, txtHrVacThu);
            vacFri = VerifyNull(txtHrVacFri.Text, txtHrVacFri);
            vacSat = VerifyNull(txtHrVacSat.Text, txtHrVacSat);
            vacSun = VerifyNull(txtHrVacSun.Text, txtHrVacSun);


            // ARP Hours
            arpMon = VerifyNull(txtHrARPMon.Text, txtHrARPMon);
            arpTue = VerifyNull(txtHrARPTue.Text, txtHrARPTue);
            arpWed = VerifyNull(txtHrARPWed.Text, txtHrARPWed);
            arpThu = VerifyNull(txtHrARPThu.Text, txtHrARPThu);
            arpFri = VerifyNull(txtHrARPFri.Text, txtHrARPFri);
            arpSat = VerifyNull(txtHrARPSat.Text, txtHrARPSat);
            arpSun = VerifyNull(txtHrARPSun.Text, txtHrARPSun);

            // Holiday Hours
            holMon = VerifyNull(txtHrHolidayMon.Text, txtHrHolidayMon);
            holTue = VerifyNull(txtHrHolidayTue.Text, txtHrHolidayTue);
            holWed = VerifyNull(txtHrHolidayWed.Text, txtHrHolidayWed);
            holThu = VerifyNull(txtHrHolidayThu.Text, txtHrHolidayThu);
            holFri = VerifyNull(txtHrHolidayFri.Text, txtHrHolidayFri);
            holSat = VerifyNull(txtHrHolidaySat.Text, txtHrHolidaySat);
            holSun = VerifyNull(txtHrHolidaySun.Text, txtHrHolidaySun);

            // Bereavement Hours
            berMon = VerifyNull(txtHrBereaveMon.Text, txtHrBereaveMon);
            berTue = VerifyNull(txtHrBereaveTue.Text, txtHrBereaveTue);
            berWed = VerifyNull(txtHrBereaveWed.Text, txtHrBereaveWed);
            berThu = VerifyNull(txtHrBereaveThu.Text, txtHrBereaveThu);
            berFri = VerifyNull(txtHrBereaveFri.Text, txtHrBereaveFri);
            berSat = VerifyNull(txtHrBereaveSat.Text, txtHrBereaveSat);
            berSun = VerifyNull(txtHrBereaveSun.Text, txtHrBereaveSun);

            // Jury Duty Hours
            jurMon = VerifyNull(txtHrJuryMon.Text, txtHrJuryMon);
            jurTue = VerifyNull(txtHrJuryTue.Text, txtHrJuryTue);
            jurWed = VerifyNull(txtHrJuryWed.Text, txtHrJuryWed);
            jurThu = VerifyNull(txtHrJuryThu.Text, txtHrJuryThu);
            jurFri = VerifyNull(txtHrJuryFri.Text, txtHrJuryFri);
            jurSat = VerifyNull(txtHrJurySat.Text, txtHrJurySat);
            jurSun = VerifyNull(txtHrJurySun.Text, txtHrJurySun);


            // Define variables for Reg hours and OT
            // Regular Hours
            regMon = VerifyNull(lblHrRegMon.Text);
            regTue = VerifyNull(lblHrRegTue.Text);
            regWed = VerifyNull(lblHrRegWed.Text);
            regThu = VerifyNull(lblHrRegThu.Text);
            regFri = VerifyNull(lblHrRegFri.Text);
            regSat = VerifyNull(lblHrRegSat.Text);
            regSun = VerifyNull(lblHrRegSun.Text);


            // Totals
            Decimal regTotal = VerifyNull(lblTotalReg.Text);
            Decimal vacTotal = VerifyNull(lblTotalVac.Text);
            Decimal arpTotal = VerifyNull(lblTotalARP.Text);
            Decimal holTotal = VerifyNull(lblTotalHoliday.Text);
            Decimal berTotal = VerifyNull(lblTotalBereave.Text);
            Decimal jurTotal = VerifyNull(lblTotalJuryDuty.Text);
            Decimal otTotal = VerifyNull(lblTotalOT.Text);
            Decimal dotTotal = VerifyNull(lblTotalDOT.Text);

            string comMon = txtCommMon.Text;
            string comTue = txtCommTue.Text;
            string comWed = txtCommWed.Text;
            string comThu = txtCommThu.Text;
            string comFri = txtCommFri.Text;
            string comSat = txtCommSat.Text;
            string comSun = txtCommSun.Text;

            // Get current selected week
            string weekSelected = lblWeekNum.Text.Trim();

            // Check if value is correct length, if not add leading zero. Should be six (MMYYYY).
            if (weekSelected.Length < 6)
            {
                weekSelected = "0" + weekSelected;
            }

            // Break up value to determine week and year.
            int week = Int32.Parse(weekSelected);
            //int currentYear = Int32.Parse(weekSelected.Substring(2, 4));

            // Identify logged in user, current selected week, and selected week's year.
            //string currentID = User.Identity.GetUserId();
            //string weekNumCurrent = weekSelected.Substring(0, 2);

            // Query and commands to retrieve max tracking number
            string trackNumQuery = "SELECT MAX(TrackNum) FROM OakdaleRequests";

            haegerTimeDB.Open();

            SqlCommand trackNumCommand = new SqlCommand(trackNumQuery, haegerTimeDB);
            string trackNum = "";
            using (SqlDataReader tn = trackNumCommand.ExecuteReader())
            {
                while (tn.Read())
                {
                    trackNum = tn[0].ToString();

                }
            }

            // Add one to max tracking number and creating new _OakdaleID
            int nextIDnum = (Int32.Parse(trackNum)) + 1;
            string strOakdaleIDA = Session["selectedItemTimeOff"].ToString();

            // INSERT INTO QUERY
            string updateQuery = "UPDATE OakdaleRequests " +
                                        "SET UserID = @currentID, UserFName = @userFName , UserLName = @userLName, WeekNum = @weekNumCurrent, " +
                                        " DateCreated = @localDate, TeamLead = @teamLead, " +
                                        "RegHoursMon = @monReg, VacHoursMon = @monVac, ArpHoursMon = @monARP, HolidayHoursMon = @monHol, " +
                                        "BereaveHoursMon = @monBer, JuryDutyHoursMon = @monJur, OverTimeHoursMon = @monOT, DoubleOTHoursMon = @monDOT, CommentMon = @monCom, " +
                                        "RegHoursTue = @tueReg, VacHoursTue = @tueVac, ArpHoursTue = @tueARP, HolidayHoursTue = @tueHol, " +
                                        "BereaveHoursTue = @tueBer, JuryDutyHoursTue = @tueJur, OverTimeHoursTue = @tueOT, DoubleOTHoursTue = @tueDOT, CommentTue = @tueCom, " +
                                        "RegHoursWed = @wedReg, VacHoursWed = @wedVac, ArpHoursWed = @wedARP, HolidayHoursWed = @wedHol, " +
                                        "BereaveHoursWed = @wedBer, JuryDutyHoursWed = @wedJur, OverTimeHoursWed = @wedOT, DoubleOTHoursWed = @wedDOT, CommentWed = @wedCom, " +
                                        "RegHoursThu = @thuReg, VacHoursThu = @thuVac, ArpHoursThu = @thuARP, HolidayHoursThu = @thuHol, " +
                                        "BereaveHoursThu = @thuBer, JuryDutyHoursThu = @thuJur, OverTimeHoursThu = @thuOT, DoubleOTHoursThu = @thuDOT, CommentThu = @thuCom, " +
                                        "RegHoursFri = @friReg, VacHoursFri = @friVac, ArpHoursFri = @friARP, HolidayHoursFri = @friHol, " +
                                        "BereaveHoursFri = @friBer, JuryDutyHoursFri = @friJur, OverTimeHoursFri = @friOT, DoubleOTHoursFri = @friDOT, CommentFri = @friCom, " +
                                        "RegHoursSat = @satReg, VacHoursSat = @satVac, ArpHoursSat = @satARP, HolidayHoursSat = @satHol, " +
                                        "BereaveHoursSat = @satBer, JuryDutyHoursSat = @satJur, OverTimeHoursSat = @satOT, DoubleOTHoursSat = @satDOT, CommentSat = @satCom, " +
                                        "RegHoursSun = @sunReg, VacHoursSun = @sunVac, ArpHoursSun = @sunARP, HolidayHoursSun = @sunHol, " +
                                        "BereaveHoursSun = @sunBer, JuryDutyHoursSun = @sunJur, OverTimeHoursSun = @sunOT, DoubleOTHoursSun = @sunDOT, CommentSun = @sunCom, " +
                                        "Status = @status, ReqYear = @currentYear, RegHoursTotal = @regTotal, VacHoursTotal = @vacTotal, ArpHoursTotal = @arpTotal, " +
                                        "HolidayHoursTotal = @holTotal, BereaveHoursTotal = @berTotal, JuryDutyHoursTotal = @jurTotal, OverTimeHoursTotal = @otTotal, DoubleOTHoursTotal = @dotTotal, ShowNoShow = @showNoShow " +
                                        "WHERE UserID = '" + currentID + "' AND WeekNum = " + week + " AND  _OakdaleRQ_ID = @strOakdaleID";

            SqlCommand updateCommand = new SqlCommand(updateQuery, haegerTimeDB);
            updateCommand.Parameters.AddWithValue("@strOakdaleID", strOakdaleIDA);
            updateCommand.Parameters.AddWithValue("@currentID", currentID);
            updateCommand.Parameters.AddWithValue("@userFName", fName);
            updateCommand.Parameters.AddWithValue("@userLName", lName);
            updateCommand.Parameters.AddWithValue("@weekNumCurrent", week);
            updateCommand.Parameters.AddWithValue("@localDate", localDate);
            updateCommand.Parameters.AddWithValue("@teamLead", teamLead);
            updateCommand.Parameters.AddWithValue("@monReg", regMon);
            updateCommand.Parameters.AddWithValue("@monVac", vacMon);
            updateCommand.Parameters.AddWithValue("@monARP", arpMon);
            updateCommand.Parameters.AddWithValue("@monHol", holMon);
            updateCommand.Parameters.AddWithValue("@monBer", berMon);
            updateCommand.Parameters.AddWithValue("@monJur", jurMon);
            updateCommand.Parameters.AddWithValue("@monOT", otMon);
            updateCommand.Parameters.AddWithValue("@monCom", comMon);
            updateCommand.Parameters.AddWithValue("@tueReg", regTue);
            updateCommand.Parameters.AddWithValue("@tueVac", vacTue);
            updateCommand.Parameters.AddWithValue("@tueARP", arpTue);
            updateCommand.Parameters.AddWithValue("@tueHol", holTue);
            updateCommand.Parameters.AddWithValue("@tueBer", berTue);
            updateCommand.Parameters.AddWithValue("@tueJur", jurTue);
            updateCommand.Parameters.AddWithValue("@tueOT", otTue);
            updateCommand.Parameters.AddWithValue("@tueCom", comTue);
            updateCommand.Parameters.AddWithValue("@wedReg", regWed);
            updateCommand.Parameters.AddWithValue("@wedVac", vacWed);
            updateCommand.Parameters.AddWithValue("@wedARP", arpWed);
            updateCommand.Parameters.AddWithValue("@wedHol", holWed);
            updateCommand.Parameters.AddWithValue("@wedBer", berWed);
            updateCommand.Parameters.AddWithValue("@wedJur", jurWed);
            updateCommand.Parameters.AddWithValue("@wedOT", otWed);
            updateCommand.Parameters.AddWithValue("@wedCom", comWed);
            updateCommand.Parameters.AddWithValue("@thuReg", regThu);
            updateCommand.Parameters.AddWithValue("@thuVac", vacThu);
            updateCommand.Parameters.AddWithValue("@thuARP", arpThu);
            updateCommand.Parameters.AddWithValue("@thuHol", holThu);
            updateCommand.Parameters.AddWithValue("@thuBer", berThu);
            updateCommand.Parameters.AddWithValue("@thuJur", jurThu);
            updateCommand.Parameters.AddWithValue("@thuOT", otThu);
            updateCommand.Parameters.AddWithValue("@thuCom", comThu);
            updateCommand.Parameters.AddWithValue("@friReg", regFri);
            updateCommand.Parameters.AddWithValue("@friVac", vacFri);
            updateCommand.Parameters.AddWithValue("@friARP", arpFri);
            updateCommand.Parameters.AddWithValue("@friHol", holFri);
            updateCommand.Parameters.AddWithValue("@friBer", berFri);
            updateCommand.Parameters.AddWithValue("@friJur", jurFri);
            updateCommand.Parameters.AddWithValue("@friOT", otFri);
            updateCommand.Parameters.AddWithValue("@friCom", comFri);
            updateCommand.Parameters.AddWithValue("@satReg", regSat);
            updateCommand.Parameters.AddWithValue("@satVac", vacSat);
            updateCommand.Parameters.AddWithValue("@satARP", arpSat);
            updateCommand.Parameters.AddWithValue("@satHol", holSat);
            updateCommand.Parameters.AddWithValue("@satBer", berSat);
            updateCommand.Parameters.AddWithValue("@satJur", jurSat);
            updateCommand.Parameters.AddWithValue("@satOT", otSat);
            updateCommand.Parameters.AddWithValue("@satCom", comSat);
            updateCommand.Parameters.AddWithValue("@sunReg", regSun);
            updateCommand.Parameters.AddWithValue("@sunVac", vacSun);
            updateCommand.Parameters.AddWithValue("@sunARP", arpSun);
            updateCommand.Parameters.AddWithValue("@sunHol", holSun);
            updateCommand.Parameters.AddWithValue("@sunBer", berSun);
            updateCommand.Parameters.AddWithValue("@sunJur", jurSun);
            updateCommand.Parameters.AddWithValue("@sunOT", otSun);
            updateCommand.Parameters.AddWithValue("@sunCom", comSun);
            updateCommand.Parameters.AddWithValue("@status", "Approved");
            updateCommand.Parameters.AddWithValue("@currentYear", currentYear);
            updateCommand.Parameters.AddWithValue("@regTotal", regTotal);
            updateCommand.Parameters.AddWithValue("@vacTotal", vacTotal);
            updateCommand.Parameters.AddWithValue("@arpTotal", arpTotal);
            updateCommand.Parameters.AddWithValue("@holTotal", holTotal);
            updateCommand.Parameters.AddWithValue("@berTotal", berTotal);
            updateCommand.Parameters.AddWithValue("@jurTotal", jurTotal);
            updateCommand.Parameters.AddWithValue("@otTotal", otTotal);
            updateCommand.Parameters.AddWithValue("@dotTotal", dotTotal);
            updateCommand.Parameters.AddWithValue("@showNoShow", "Show");
            updateCommand.Parameters.AddWithValue("@monDOT", dotMon);
            updateCommand.Parameters.AddWithValue("@tueDOT", dotTue);
            updateCommand.Parameters.AddWithValue("@wedDOT", dotWed);
            updateCommand.Parameters.AddWithValue("@thuDOT", dotThu);
            updateCommand.Parameters.AddWithValue("@friDOT", dotFri);
            updateCommand.Parameters.AddWithValue("@satDOT", dotSat);
            updateCommand.Parameters.AddWithValue("@sunDOT", dotSun);

            updateCommand.ExecuteNonQuery();
            haegerTimeDB.Close();

           
            string subject = "Request Approved for Week " + week;


            try
            {

                // Create the msg object to be sent
                MailMessage _mailmsg = new MailMessage();

                //Make TRUE because our body text is html
                _mailmsg.IsBodyHtml = true;

                // Configure the address we are sending the mail from
                MailAddress address = new MailAddress("webadmin@haeger.com");

                _mailmsg.From = address;

                // Add your email address to the recipients
                _mailmsg.To.Add(teamLeadEmail);
                _mailmsg.To.Add(partnerEmail);
                _mailmsg.Bcc.Add("webadmin@haeger.com");
                _mailmsg.Bcc.Add("regeah811@gmail.com");

                //Set Subject
                _mailmsg.Subject = subject;

                //Set Body Text of Email
                //_mailmsg.Body = MailText;
                //Attachment attachment;
                //attachment = new Attachment("C:\\ExcelTest\\TestSheet1.xlsx");
                //_mailmsg.Attachments.Add(attachment);


                // Configure an SmtpClient to send the mail.
                SmtpClient _smtp = new SmtpClient("smtp-mail.outlook.com");
                _smtp.Port = 587;
                _smtp.DeliveryMethod = SmtpDeliveryMethod.Network;
                _smtp.UseDefaultCredentials = false;
                System.Net.NetworkCredential credentials =
                    new System.Net.NetworkCredential("webadmin@haeger.com", "Crazycat75");
                _smtp.EnableSsl = true;
                _smtp.Credentials = credentials;

                //_smtp.DeliveryMethod = SmtpDeliveryMethod.SpecifiedPickupDirectory;
                //_smtp.PickupDirectoryLocation = @targetDirectory;

                // Send the msg.
                _smtp.Send(_mailmsg);

                // Display some feedback to the user to let them know it was sent.
                //lblResult.Text = "Your message was sent!";


                Response.Redirect("/PartAdminNA/TeamLeadHome");

            }
            catch (Exception)
            {


            }



            


        }

        protected void btnDeny_Click(object sender, EventArgs e)
        {
            //btnCalc_Click(sender, e);

            // Vacation Hours
            vacMon = VerifyNull(txtHrVacMon.Text, txtHrVacMon);
            vacTue = VerifyNull(txtHrVacTue.Text, txtHrVacTue);
            vacWed = VerifyNull(txtHrVacWed.Text, txtHrVacWed);
            vacThu = VerifyNull(txtHrVacThu.Text, txtHrVacThu);
            vacFri = VerifyNull(txtHrVacFri.Text, txtHrVacFri);
            vacSat = VerifyNull(txtHrVacSat.Text, txtHrVacSat);
            vacSun = VerifyNull(txtHrVacSun.Text, txtHrVacSun);


            // ARP Hours
            arpMon = VerifyNull(txtHrARPMon.Text, txtHrARPMon);
            arpTue = VerifyNull(txtHrARPTue.Text, txtHrARPTue);
            arpWed = VerifyNull(txtHrARPWed.Text, txtHrARPWed);
            arpThu = VerifyNull(txtHrARPThu.Text, txtHrARPThu);
            arpFri = VerifyNull(txtHrARPFri.Text, txtHrARPFri);
            arpSat = VerifyNull(txtHrARPSat.Text, txtHrARPSat);
            arpSun = VerifyNull(txtHrARPSun.Text, txtHrARPSun);

            // Holiday Hours
            holMon = VerifyNull(txtHrHolidayMon.Text, txtHrHolidayMon);
            holTue = VerifyNull(txtHrHolidayTue.Text, txtHrHolidayTue);
            holWed = VerifyNull(txtHrHolidayWed.Text, txtHrHolidayWed);
            holThu = VerifyNull(txtHrHolidayThu.Text, txtHrHolidayThu);
            holFri = VerifyNull(txtHrHolidayFri.Text, txtHrHolidayFri);
            holSat = VerifyNull(txtHrHolidaySat.Text, txtHrHolidaySat);
            holSun = VerifyNull(txtHrHolidaySun.Text, txtHrHolidaySun);

            // Bereavement Hours
            berMon = VerifyNull(txtHrBereaveMon.Text, txtHrBereaveMon);
            berTue = VerifyNull(txtHrBereaveTue.Text, txtHrBereaveTue);
            berWed = VerifyNull(txtHrBereaveWed.Text, txtHrBereaveWed);
            berThu = VerifyNull(txtHrBereaveThu.Text, txtHrBereaveThu);
            berFri = VerifyNull(txtHrBereaveFri.Text, txtHrBereaveFri);
            berSat = VerifyNull(txtHrBereaveSat.Text, txtHrBereaveSat);
            berSun = VerifyNull(txtHrBereaveSun.Text, txtHrBereaveSun);

            // Jury Duty Hours
            jurMon = VerifyNull(txtHrJuryMon.Text, txtHrJuryMon);
            jurTue = VerifyNull(txtHrJuryTue.Text, txtHrJuryTue);
            jurWed = VerifyNull(txtHrJuryWed.Text, txtHrJuryWed);
            jurThu = VerifyNull(txtHrJuryThu.Text, txtHrJuryThu);
            jurFri = VerifyNull(txtHrJuryFri.Text, txtHrJuryFri);
            jurSat = VerifyNull(txtHrJurySat.Text, txtHrJurySat);
            jurSun = VerifyNull(txtHrJurySun.Text, txtHrJurySun);


            // Define variables for Reg hours and OT
            // Regular Hours
            regMon = VerifyNull(lblHrRegMon.Text);
            regTue = VerifyNull(lblHrRegTue.Text);
            regWed = VerifyNull(lblHrRegWed.Text);
            regThu = VerifyNull(lblHrRegThu.Text);
            regFri = VerifyNull(lblHrRegFri.Text);
            regSat = VerifyNull(lblHrRegSat.Text);
            regSun = VerifyNull(lblHrRegSun.Text);

            // Totals
            Decimal regTotal = VerifyNull(lblTotalReg.Text);
            Decimal vacTotal = VerifyNull(lblTotalVac.Text);
            Decimal arpTotal = VerifyNull(lblTotalARP.Text);
            Decimal holTotal = VerifyNull(lblTotalHoliday.Text);
            Decimal berTotal = VerifyNull(lblTotalBereave.Text);
            Decimal jurTotal = VerifyNull(lblTotalJuryDuty.Text);
            Decimal otTotal = VerifyNull(lblTotalOT.Text);
            Decimal dotTotal = VerifyNull(lblTotalDOT.Text);

            string comMon = txtCommMon.Text;
            string comTue = txtCommTue.Text;
            string comWed = txtCommWed.Text;
            string comThu = txtCommThu.Text;
            string comFri = txtCommFri.Text;
            string comSat = txtCommSat.Text;
            string comSun = txtCommSun.Text;

            // Get current selected week
            string weekSelected = lblWeekNum.Text.Trim();

            // Check if value is correct length, if not add leading zero. Should be six (MMYYYY).
            if (weekSelected.Length < 6)
            {
                weekSelected = "0" + weekSelected;
            }

            // Break up value to determine week and year.
            int week = Int32.Parse(weekSelected);
            //int currentYear = Int32.Parse(weekSelected.Substring(2, 4));

            // Identify logged in user, current selected week, and selected week's year.
            //string currentID = User.Identity.GetUserId();
            //string weekNumCurrent = weekSelected.Substring(0, 2);

            // Query and commands to retrieve max tracking number
            string trackNumQuery = "SELECT MAX(TrackNum) FROM OakdaleRequests";

            haegerTimeDB.Open();

            SqlCommand trackNumCommand = new SqlCommand(trackNumQuery, haegerTimeDB);
            string trackNum = "";
            using (SqlDataReader tn = trackNumCommand.ExecuteReader())
            {
                while (tn.Read())
                {
                    trackNum = tn[0].ToString();

                }
            }

            // Add one to max tracking number and creating new _OakdaleID
            int nextIDnum = (Int32.Parse(trackNum)) + 1;
            string strOakdaleIDD = Session["selectedItemTimeOff"].ToString();



            // INSERT INTO QUERY
            string updateQuery = "UPDATE OakdaleRequests " +
                                        "SET _OakdaleRQ_ID = @strOakdaleID, UserID = @currentID, UserFName = @userFName , UserLName = @userLName, WeekNum = @weekNumCurrent, " +
                                        " DateCreated = @localDate, TeamLead = @teamLead, " +
                                        "RegHoursMon = @monReg, VacHoursMon = @monVac, ArpHoursMon = @monARP, HolidayHoursMon = @monHol, " +
                                        "BereaveHoursMon = @monBer, JuryDutyHoursMon = @monJur, OverTimeHoursMon = @monOT, DoubleOTHoursMon = @monDOT, CommentMon = @monCom, " +
                                        "RegHoursTue = @tueReg, VacHoursTue = @tueVac, ArpHoursTue = @tueARP, HolidayHoursTue = @tueHol, " +
                                        "BereaveHoursTue = @tueBer, JuryDutyHoursTue = @tueJur, OverTimeHoursTue = @tueOT, DoubleOTHoursTue = @tueDOT, CommentTue = @tueCom, " +
                                        "RegHoursWed = @wedReg, VacHoursWed = @wedVac, ArpHoursWed = @wedARP, HolidayHoursWed = @wedHol, " +
                                        "BereaveHoursWed = @wedBer, JuryDutyHoursWed = @wedJur, OverTimeHoursWed = @wedOT, DoubleOTHoursWed = @wedDOT, CommentWed = @wedCom, " +
                                        "RegHoursThu = @thuReg, VacHoursThu = @thuVac, ArpHoursThu = @thuARP, HolidayHoursThu = @thuHol, " +
                                        "BereaveHoursThu = @thuBer, JuryDutyHoursThu = @thuJur, OverTimeHoursThu = @thuOT, DoubleOTHoursThu = @thuDOT, CommentThu = @thuCom, " +
                                        "RegHoursFri = @friReg, VacHoursFri = @friVac, ArpHoursFri = @friARP, HolidayHoursFri = @friHol, " +
                                        "BereaveHoursFri = @friBer, JuryDutyHoursFri = @friJur, OverTimeHoursFri = @friOT, DoubleOTHoursFri = @friDOT, CommentFri = @friCom, " +
                                        "RegHoursSat = @satReg, VacHoursSat = @satVac, ArpHoursSat = @satARP, HolidayHoursSat = @satHol, " +
                                        "BereaveHoursSat = @satBer, JuryDutyHoursSat = @satJur, OverTimeHoursSat = @satOT, DoubleOTHoursSat = @satDOT, CommentSat = @satCom, " +
                                        "RegHoursSun = @sunReg, VacHoursSun = @sunVac, ArpHoursSun = @sunARP, HolidayHoursSun = @sunHol, " +
                                        "BereaveHoursSun = @sunBer, JuryDutyHoursSun = @sunJur, OverTimeHoursSun = @sunOT, DoubleOTHoursSun = @sunDOT, CommentSun = @sunCom, " +
                                        "Status = @status, ReqYear = @currentYear, RegHoursTotal = @regTotal, VacHoursTotal = @vacTotal, ArpHoursTotal = @arpTotal, " +
                                        "HolidayHoursTotal = @holTotal, BereaveHoursTotal = @berTotal, JuryDutyHoursTotal = @jurTotal, OverTimeHoursTotal = @otTotal, DoubleOTHoursTotal = @dotTotal, ShowNoShow = @showNoShow " +
                                        "WHERE UserID = '" + currentID + "' AND WeekNum = " + week;

            SqlCommand updateCommand = new SqlCommand(updateQuery, haegerTimeDB);
            updateCommand.Parameters.AddWithValue("@strOakdaleID", strOakdaleIDD);
            updateCommand.Parameters.AddWithValue("@currentID", currentID);
            updateCommand.Parameters.AddWithValue("@userFName", fName);
            updateCommand.Parameters.AddWithValue("@userLName", lName);
            updateCommand.Parameters.AddWithValue("@weekNumCurrent", week);
            updateCommand.Parameters.AddWithValue("@localDate", localDate);
            updateCommand.Parameters.AddWithValue("@teamLead", teamLead);
            updateCommand.Parameters.AddWithValue("@monReg", regMon);
            updateCommand.Parameters.AddWithValue("@monVac", vacMon);
            updateCommand.Parameters.AddWithValue("@monARP", arpMon);
            updateCommand.Parameters.AddWithValue("@monHol", holMon);
            updateCommand.Parameters.AddWithValue("@monBer", berMon);
            updateCommand.Parameters.AddWithValue("@monJur", jurMon);
            updateCommand.Parameters.AddWithValue("@monOT", otMon);
            updateCommand.Parameters.AddWithValue("@monCom", comMon);
            updateCommand.Parameters.AddWithValue("@tueReg", regTue);
            updateCommand.Parameters.AddWithValue("@tueVac", vacTue);
            updateCommand.Parameters.AddWithValue("@tueARP", arpTue);
            updateCommand.Parameters.AddWithValue("@tueHol", holTue);
            updateCommand.Parameters.AddWithValue("@tueBer", berTue);
            updateCommand.Parameters.AddWithValue("@tueJur", jurTue);
            updateCommand.Parameters.AddWithValue("@tueOT", otTue);
            updateCommand.Parameters.AddWithValue("@tueCom", comTue);
            updateCommand.Parameters.AddWithValue("@wedReg", regWed);
            updateCommand.Parameters.AddWithValue("@wedVac", vacWed);
            updateCommand.Parameters.AddWithValue("@wedARP", arpWed);
            updateCommand.Parameters.AddWithValue("@wedHol", holWed);
            updateCommand.Parameters.AddWithValue("@wedBer", berWed);
            updateCommand.Parameters.AddWithValue("@wedJur", jurWed);
            updateCommand.Parameters.AddWithValue("@wedOT", otWed);
            updateCommand.Parameters.AddWithValue("@wedCom", comWed);
            updateCommand.Parameters.AddWithValue("@thuReg", regThu);
            updateCommand.Parameters.AddWithValue("@thuVac", vacThu);
            updateCommand.Parameters.AddWithValue("@thuARP", arpThu);
            updateCommand.Parameters.AddWithValue("@thuHol", holThu);
            updateCommand.Parameters.AddWithValue("@thuBer", berThu);
            updateCommand.Parameters.AddWithValue("@thuJur", jurThu);
            updateCommand.Parameters.AddWithValue("@thuOT", otThu);
            updateCommand.Parameters.AddWithValue("@thuCom", comThu);
            updateCommand.Parameters.AddWithValue("@friReg", regFri);
            updateCommand.Parameters.AddWithValue("@friVac", vacFri);
            updateCommand.Parameters.AddWithValue("@friARP", arpFri);
            updateCommand.Parameters.AddWithValue("@friHol", holFri);
            updateCommand.Parameters.AddWithValue("@friBer", berFri);
            updateCommand.Parameters.AddWithValue("@friJur", jurFri);
            updateCommand.Parameters.AddWithValue("@friOT", otFri);
            updateCommand.Parameters.AddWithValue("@friCom", comFri);
            updateCommand.Parameters.AddWithValue("@satReg", regSat);
            updateCommand.Parameters.AddWithValue("@satVac", vacSat);
            updateCommand.Parameters.AddWithValue("@satARP", arpSat);
            updateCommand.Parameters.AddWithValue("@satHol", holSat);
            updateCommand.Parameters.AddWithValue("@satBer", berSat);
            updateCommand.Parameters.AddWithValue("@satJur", jurSat);
            updateCommand.Parameters.AddWithValue("@satOT", otSat);
            updateCommand.Parameters.AddWithValue("@satCom", comSat);
            updateCommand.Parameters.AddWithValue("@sunReg", regSun);
            updateCommand.Parameters.AddWithValue("@sunVac", vacSun);
            updateCommand.Parameters.AddWithValue("@sunARP", arpSun);
            updateCommand.Parameters.AddWithValue("@sunHol", holSun);
            updateCommand.Parameters.AddWithValue("@sunBer", berSun);
            updateCommand.Parameters.AddWithValue("@sunJur", jurSun);
            updateCommand.Parameters.AddWithValue("@sunOT", otSun);
            updateCommand.Parameters.AddWithValue("@sunCom", comSun);
            updateCommand.Parameters.AddWithValue("@status", "Denied");
            updateCommand.Parameters.AddWithValue("@currentYear", currentYear);
            updateCommand.Parameters.AddWithValue("@regTotal", regTotal);
            updateCommand.Parameters.AddWithValue("@vacTotal", vacTotal);
            updateCommand.Parameters.AddWithValue("@arpTotal", arpTotal);
            updateCommand.Parameters.AddWithValue("@holTotal", holTotal);
            updateCommand.Parameters.AddWithValue("@berTotal", berTotal);
            updateCommand.Parameters.AddWithValue("@jurTotal", jurTotal);
            updateCommand.Parameters.AddWithValue("@otTotal", otTotal);
            updateCommand.Parameters.AddWithValue("@dotTotal", dotTotal);
            updateCommand.Parameters.AddWithValue("@showNoShow", "Show");
            updateCommand.Parameters.AddWithValue("@monDOT", dotMon);
            updateCommand.Parameters.AddWithValue("@tueDOT", dotTue);
            updateCommand.Parameters.AddWithValue("@wedDOT", dotWed);
            updateCommand.Parameters.AddWithValue("@thuDOT", dotThu);
            updateCommand.Parameters.AddWithValue("@friDOT", dotFri);
            updateCommand.Parameters.AddWithValue("@satDOT", dotSat);
            updateCommand.Parameters.AddWithValue("@sunDOT", dotSun);

            updateCommand.ExecuteNonQuery();


           


            haegerTimeDB.Close();


            string subject = "Request Denied for Week " + week;


            try
            {

                // Create the msg object to be sent
                MailMessage _mailmsg = new MailMessage();

                //Make TRUE because our body text is html
                _mailmsg.IsBodyHtml = true;

                // Configure the address we are sending the mail from
                MailAddress address = new MailAddress("webadmin@haeger.com");

                _mailmsg.From = address;

                // Add your email address to the recipients
                _mailmsg.To.Add(teamLeadEmail);
                _mailmsg.To.Add(partnerEmail);
                _mailmsg.Bcc.Add("webadmin@haeger.com");
                _mailmsg.Bcc.Add("regeah811@gmail.com");

                //Set Subject
                _mailmsg.Subject = subject;

                //Set Body Text of Email
                //_mailmsg.Body = MailText;
                //Attachment attachment;
                //attachment = new Attachment("C:\\ExcelTest\\TestSheet1.xlsx");
                //_mailmsg.Attachments.Add(attachment);


                // Configure an SmtpClient to send the mail.
                SmtpClient _smtp = new SmtpClient("smtp-mail.outlook.com");
                _smtp.Port = 587;
                _smtp.DeliveryMethod = SmtpDeliveryMethod.Network;
                _smtp.UseDefaultCredentials = false;
                System.Net.NetworkCredential credentials =
                    new System.Net.NetworkCredential("webadmin@haeger.com", "Crazycat75");
                _smtp.EnableSsl = true;
                _smtp.Credentials = credentials;

                //_smtp.DeliveryMethod = SmtpDeliveryMethod.SpecifiedPickupDirectory;
                //_smtp.PickupDirectoryLocation = @targetDirectory;

                // Send the msg.
                _smtp.Send(_mailmsg);

                // Display some feedback to the user to let them know it was sent.
                //lblResult.Text = "Your message was sent!";


                Response.Redirect("/PartAdminNA/TeamLeadHome");

            }
            catch (Exception)
            {


            }



           

        }

        protected void btnBack_Click(object sender, EventArgs e)
        {
            Response.Redirect("/PartAdminNA/TeamLeadHome");
        }
    }
}