﻿using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Net.Mail;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Haeger2018.PartAdminNA
{
    public partial class TeamLeadCurrentWeek : System.Web.UI.Page
    {
        // Database connection used to store or retrieve time sheets, also to find Partner scheduled hours.
        SqlConnection haegerTimeDB = new SqlConnection(ConfigurationManager.ConnectionStrings["HaegerTimeConnectionString"].ConnectionString);

        // Current date
        DateTime localDate = DateTime.Now;

        int weekNumCurrent = 0;
        string currentID = string.Empty;

        // ******************** Test date - DELETE FOR PRODUCTION ********************
        //DateTime localDate = new DateTime(2018, 9, 13);


        string teamLeadEmail;

        protected void Page_Load(object sender, EventArgs e)
        {

            currentID = User.Identity.GetUserId();

            // Find current week.
            // Current cultural information.
            CultureInfo ciCurr = CultureInfo.CurrentCulture;
            // Current Week number to verify existing request.
            weekNumCurrent = ciCurr.Calendar.GetWeekOfYear(localDate, CalendarWeekRule.FirstFourDayWeek, DayOfWeek.Monday);
            Session["ssWeekNum"] = weekNumCurrent.ToString();
            lblDate.Text = weekNumCurrent.ToString();

            // Check database if rows with week exist
            haegerTimeDB.Open();

            string checkHours = "SELECT * FROM OakdaleTime WHERE WeekNum = " + weekNumCurrent;
            SqlCommand checkHoursCommand = new SqlCommand(checkHours, haegerTimeDB);
            SqlDataReader checkHoursReader = checkHoursCommand.ExecuteReader();
            DataTable chT = new DataTable();
            chT.Load(checkHoursReader);
            int chtCount = chT.Rows.Count;

            // based on team lead
            string partnerList = "SELECT UserID, Fname, Lname, TeamLead FROM OakdalePartnerInfo ORDER BY Lname";
            SqlCommand partnerListCommand = new SqlCommand(partnerList, haegerTimeDB);
            SqlDataReader partnerListReader = partnerListCommand.ExecuteReader();
            DataTable plT = new DataTable();
            plT.Load(partnerListReader);
            int pltCount = plT.Rows.Count;

            // Get default hours
            string defaultHours = "SELECT * FROM OakdaleTimeDefault";
            SqlCommand defaultHoursCommand = new SqlCommand(defaultHours, haegerTimeDB);
            SqlDataReader defaultHoursReader = defaultHoursCommand.ExecuteReader();
            DataTable dhT = new DataTable();
            dhT.Load(defaultHoursReader);

            string teamLeadEmailQuery = "Select Email from OakdaleTeamLeadInfo WHERE UserID = '" + currentID + "'";
            SqlCommand teamLeadEmailCommand = new SqlCommand(teamLeadEmailQuery, haegerTimeDB);
            using (SqlDataReader tl = teamLeadEmailCommand.ExecuteReader())
            {
                while (tl.Read())
                {
                    teamLeadEmail = tl[0].ToString();

                }
            }

            // Current Week table
            DataTable cwT = new DataTable();

            if (chtCount == 0)
            {
               
                for (int i = 0; i <= pltCount - 1; i++)
                {
                    cwT.Merge(dhT);
                    // Query and commands to retrieve max tracking number
                    string trackNumQuery = "SELECT MAX(TrackNum) FROM OakdaleTime";

                    SqlCommand trackNumCommand = new SqlCommand(trackNumQuery, haegerTimeDB);
                    string trackNum = "";
                    using (SqlDataReader tn = trackNumCommand.ExecuteReader())
                    {
                        while (tn.Read())
                        {
                            trackNum = tn[0].ToString();

                        }
                    }

                    // Add one to max tracking number and creating new _OakdaleID
                    int nextIDnum = (Int32.Parse(trackNum)) + 1;
                    string strOakdaleID = "OAK" + nextIDnum.ToString().Trim();

                    // PK
                    cwT.Rows[i][0] = strOakdaleID;
                    // UserID
                    cwT.Rows[i][2] = plT.Rows[i][0];
                    // UserFName
                    cwT.Rows[i][3] = plT.Rows[i][1];
                    // UserLName
                    cwT.Rows[i][4] = plT.Rows[i][2];
                    // WeekNum
                    cwT.Rows[i][5] = weekNumCurrent;

                    // DateCreated
                    cwT.Rows[i][6] = localDate;


                    // TeamLead
                    cwT.Rows[i][7] = plT.Rows[i][3];
                    // ShowNoShow
                    cwT.Rows[i][73] = "Show";
                    // Status
                    cwT.Rows[i][64] = "Approved";
                    // Request Year
                    cwT.Rows[i][65] = localDate.Year;

                    // INSERT INTO QUERY
                    string insertRequestQuery = "INSERT INTO OakdaleTime " +
                                                "(_OakdaleID, UserID, UserFName, UserLName, WeekNum, DateCreated, TeamLead, " +
                                                "RegHoursMon, VacHoursMon, ArpHoursMon, HolidayHoursMon, BereaveHoursMon, JuryDutyHoursMon, OverTimeHoursMon, DoubleOTHoursMon, CommentMon, " +
                                                "RegHoursTue, VacHoursTue, ArpHoursTue, HolidayHoursTue, BereaveHoursTue, JuryDutyHoursTue, OverTimeHoursTue, DoubleOTHoursTue, CommentTue, " +
                                                "RegHoursWed, VacHoursWed, ArpHoursWed, HolidayHoursWed, BereaveHoursWed, JuryDutyHoursWed, OverTimeHoursWed, DoubleOTHoursWed, CommentWed, " +
                                                "RegHoursThu, VacHoursThu, ArpHoursThu, HolidayHoursThu, BereaveHoursThu, JuryDutyHoursThu, OverTimeHoursThu, DoubleOTHoursThu, CommentThu, " +
                                                "RegHoursFri, VacHoursFri, ArpHoursFri, HolidayHoursFri, BereaveHoursFri, JuryDutyHoursFri, OverTimeHoursFri, DoubleOTHoursFri, CommentFri, " +
                                                "RegHoursSat, VacHoursSat, ArpHoursSat, HolidayHoursSat, BereaveHoursSat, JuryDutyHoursSat, OverTimeHoursSat, DoubleOTHoursSat, CommentSat, " +
                                                "RegHoursSun, VacHoursSun, ArpHoursSun, HolidayHoursSun, BereaveHoursSun, JuryDutyHoursSun, OverTimeHoursSun, DoubleOTHoursSun, CommentSun, " +
                                                "Status, ReqYear, RegHoursTotal, VacHoursTotal, ArpHoursTotal, HolidayHoursTotal, BereaveHoursTotal, JuryDutyHoursTotal, OverTimeHoursTotal, DoubleOTHoursTotal, ShowNoShow)" +
                                                "VALUES (@strOakdaleID, @currentID, @userFName, @userLName, @weekNumCurrent, @localDate, @teamLead, " +
                                                 "@monReg, @monVac, @monARP, @monHol, @monBer, @monJur, @monOT, @monDOT, @monCom, " +
                                                "@tueReg, @tueVac, @tueARP, @tueHol, @tueBer, @tueJur, @tueOT,  @tueDOT, @tueCom, " +
                                                "@wedReg, @wedVac, @wedARP, @wedHol, @wedBer, @wedJur, @wedOT,  @wedDOT, @wedCom, " +
                                                "@thuReg, @thuVac, @thuARP, @thuHol, @thuBer, @thuJur, @thuOT,  @thuDOT, @thuCom, " +
                                                "@friReg, @friVac, @friARP, @friHol, @friBer, @friJur, @friOT,  @friDOT, @friCom, " +
                                                "@satReg, @satVac, @satARP, @satHol, @satBer, @satJur, @satOT,  @satDOT, @satCom, " +
                                                "@sunReg, @sunVac, @sunARP, @sunHol, @sunBer, @sunJur, @sunOT,  @sunDOT, @sunCom, " +
                                                "@status, @currentYear, @regTotal, @vacTotal, @arpTotal, @holTotal, @berTotal, @jurTotal, @otTotal, @dotTotal, @showNoShow)";

                    SqlCommand insertRequestCommand = new SqlCommand(insertRequestQuery, haegerTimeDB);
                    insertRequestCommand.Parameters.AddWithValue("@strOakdaleID", cwT.Rows[i][0]);
                    insertRequestCommand.Parameters.AddWithValue("@currentID", cwT.Rows[i][2]);
                    insertRequestCommand.Parameters.AddWithValue("@userFName", cwT.Rows[i][3]);
                    insertRequestCommand.Parameters.AddWithValue("@userLName", cwT.Rows[i][4]);
                    insertRequestCommand.Parameters.AddWithValue("@weekNumCurrent", cwT.Rows[i][5]);
                    insertRequestCommand.Parameters.AddWithValue("@localDate", localDate);
                    insertRequestCommand.Parameters.AddWithValue("@teamLead", cwT.Rows[i][7]);
                    insertRequestCommand.Parameters.AddWithValue("@monReg", cwT.Rows[i][8]);
                    insertRequestCommand.Parameters.AddWithValue("@monVac", cwT.Rows[i][9]);
                    insertRequestCommand.Parameters.AddWithValue("@monARP", cwT.Rows[i][10]);
                    insertRequestCommand.Parameters.AddWithValue("@monHol", cwT.Rows[i][11]);
                    insertRequestCommand.Parameters.AddWithValue("@monBer", cwT.Rows[i][12]);
                    insertRequestCommand.Parameters.AddWithValue("@monJur", cwT.Rows[i][13]);
                    insertRequestCommand.Parameters.AddWithValue("@monOT", cwT.Rows[i][14]);
                    insertRequestCommand.Parameters.AddWithValue("@monCom", cwT.Rows[i][15]);
                    insertRequestCommand.Parameters.AddWithValue("@tueReg", cwT.Rows[i][16]);
                    insertRequestCommand.Parameters.AddWithValue("@tueVac", cwT.Rows[i][17]);
                    insertRequestCommand.Parameters.AddWithValue("@tueARP", cwT.Rows[i][18]);
                    insertRequestCommand.Parameters.AddWithValue("@tueHol", cwT.Rows[i][19]);
                    insertRequestCommand.Parameters.AddWithValue("@tueBer", cwT.Rows[i][20]);
                    insertRequestCommand.Parameters.AddWithValue("@tueJur", cwT.Rows[i][21]);
                    insertRequestCommand.Parameters.AddWithValue("@tueOT", cwT.Rows[i][22]);
                    insertRequestCommand.Parameters.AddWithValue("@tueCom", cwT.Rows[i][23]);
                    insertRequestCommand.Parameters.AddWithValue("@wedReg", cwT.Rows[i][24]);
                    insertRequestCommand.Parameters.AddWithValue("@wedVac", cwT.Rows[i][25]);
                    insertRequestCommand.Parameters.AddWithValue("@wedARP", cwT.Rows[i][26]);
                    insertRequestCommand.Parameters.AddWithValue("@wedHol", cwT.Rows[i][27]);
                    insertRequestCommand.Parameters.AddWithValue("@wedBer", cwT.Rows[i][28]);
                    insertRequestCommand.Parameters.AddWithValue("@wedJur", cwT.Rows[i][29]);
                    insertRequestCommand.Parameters.AddWithValue("@wedOT", cwT.Rows[i][30]);
                    insertRequestCommand.Parameters.AddWithValue("@wedCom", cwT.Rows[i][31]);
                    insertRequestCommand.Parameters.AddWithValue("@thuReg", cwT.Rows[i][32]);
                    insertRequestCommand.Parameters.AddWithValue("@thuVac", cwT.Rows[i][33]);
                    insertRequestCommand.Parameters.AddWithValue("@thuARP", cwT.Rows[i][34]);
                    insertRequestCommand.Parameters.AddWithValue("@thuHol", cwT.Rows[i][35]);
                    insertRequestCommand.Parameters.AddWithValue("@thuBer", cwT.Rows[i][36]);
                    insertRequestCommand.Parameters.AddWithValue("@thuJur", cwT.Rows[i][37]);
                    insertRequestCommand.Parameters.AddWithValue("@thuOT", cwT.Rows[i][38]);
                    insertRequestCommand.Parameters.AddWithValue("@thuCom", cwT.Rows[i][39]);
                    insertRequestCommand.Parameters.AddWithValue("@friReg", cwT.Rows[i][40]);
                    insertRequestCommand.Parameters.AddWithValue("@friVac", cwT.Rows[i][41]);
                    insertRequestCommand.Parameters.AddWithValue("@friARP", cwT.Rows[i][42]);
                    insertRequestCommand.Parameters.AddWithValue("@friHol", cwT.Rows[i][43]);
                    insertRequestCommand.Parameters.AddWithValue("@friBer", cwT.Rows[i][44]);
                    insertRequestCommand.Parameters.AddWithValue("@friJur", cwT.Rows[i][45]);
                    insertRequestCommand.Parameters.AddWithValue("@friOT", cwT.Rows[i][46]);
                    insertRequestCommand.Parameters.AddWithValue("@friCom", cwT.Rows[i][47]);
                    insertRequestCommand.Parameters.AddWithValue("@satReg", cwT.Rows[i][48]);
                    insertRequestCommand.Parameters.AddWithValue("@satVac", cwT.Rows[i][49]);
                    insertRequestCommand.Parameters.AddWithValue("@satARP", cwT.Rows[i][50]);
                    insertRequestCommand.Parameters.AddWithValue("@satHol", cwT.Rows[i][51]);
                    insertRequestCommand.Parameters.AddWithValue("@satBer", cwT.Rows[i][52]);
                    insertRequestCommand.Parameters.AddWithValue("@satJur", cwT.Rows[i][53]);
                    insertRequestCommand.Parameters.AddWithValue("@satOT", cwT.Rows[i][54]);
                    insertRequestCommand.Parameters.AddWithValue("@satCom", cwT.Rows[i][55]);
                    insertRequestCommand.Parameters.AddWithValue("@sunReg", cwT.Rows[i][56]);
                    insertRequestCommand.Parameters.AddWithValue("@sunVac", cwT.Rows[i][57]);
                    insertRequestCommand.Parameters.AddWithValue("@sunARP", cwT.Rows[i][58]);
                    insertRequestCommand.Parameters.AddWithValue("@sunHol", cwT.Rows[i][59]);
                    insertRequestCommand.Parameters.AddWithValue("@sunBer", cwT.Rows[i][60]);
                    insertRequestCommand.Parameters.AddWithValue("@sunJur", cwT.Rows[i][61]);
                    insertRequestCommand.Parameters.AddWithValue("@sunOT", cwT.Rows[i][62]);
                    insertRequestCommand.Parameters.AddWithValue("@sunCom", cwT.Rows[i][63]);
                    insertRequestCommand.Parameters.AddWithValue("@status", cwT.Rows[i][64]);
                    insertRequestCommand.Parameters.AddWithValue("@currentYear", cwT.Rows[i][65]);
                    insertRequestCommand.Parameters.AddWithValue("@regTotal", cwT.Rows[i][66]);
                    insertRequestCommand.Parameters.AddWithValue("@vacTotal", cwT.Rows[i][67]);
                    insertRequestCommand.Parameters.AddWithValue("@arpTotal", cwT.Rows[i][68]);
                    insertRequestCommand.Parameters.AddWithValue("@holTotal", cwT.Rows[i][69]);
                    insertRequestCommand.Parameters.AddWithValue("@berTotal", cwT.Rows[i][70]);
                    insertRequestCommand.Parameters.AddWithValue("@jurTotal", cwT.Rows[i][71]);
                    insertRequestCommand.Parameters.AddWithValue("@otTotal", cwT.Rows[i][72]);
                    insertRequestCommand.Parameters.AddWithValue("@showNoShow", cwT.Rows[i][73]);
                    insertRequestCommand.Parameters.AddWithValue("@monDOT", cwT.Rows[i][74]);
                    insertRequestCommand.Parameters.AddWithValue("@tueDOT", cwT.Rows[i][75]);
                    insertRequestCommand.Parameters.AddWithValue("@wedDOT", cwT.Rows[i][76]);
                    insertRequestCommand.Parameters.AddWithValue("@thuDOT", cwT.Rows[i][77]);
                    insertRequestCommand.Parameters.AddWithValue("@friDOT", cwT.Rows[i][78]);
                    insertRequestCommand.Parameters.AddWithValue("@satDOT", cwT.Rows[i][79]);
                    insertRequestCommand.Parameters.AddWithValue("@sunDOT", cwT.Rows[i][80]);
                    insertRequestCommand.Parameters.AddWithValue("@dotTotal", cwT.Rows[i][81]);

                    insertRequestCommand.ExecuteNonQuery();

                }


            }



            // Check request off table for current week for each partner.


            for (int j = 0; j <= pltCount - 1; j++)
            {

                string userID = plT.Rows[j][0].ToString();



                string requests = "SELECT * FROM OakdaleRequests WHERE UserID = '" + userID + "' AND WeekNum = " + weekNumCurrent + " AND Status = 'Approved' ORDER BY DateCreated DESC";
                SqlCommand requestsCommand = new SqlCommand(requests, haegerTimeDB);
                SqlDataReader requestsReader = requestsCommand.ExecuteReader();
                DataTable rqT = new DataTable();
                rqT.Load(requestsReader);
                int rqtCount = rqT.Rows.Count;

                if (rqT.Rows.Count > 0)
                {
                    string getOakID = "SELECT _OakdaleID FROM OakdaleTime WHERE UserID = '" + userID + "' AND WeekNum = " + weekNumCurrent;
                    SqlCommand getOakIDCommand = new SqlCommand(getOakID, haegerTimeDB);
                    SqlDataReader getOakIDReader = getOakIDCommand.ExecuteReader();
                    DataTable oidT = new DataTable();
                    oidT.Load(getOakIDReader);
                    string strOakdaleID = oidT.Rows[0][0].ToString();

                    // INSERT INTO QUERY
                    string updateQuery = "UPDATE OakdaleTime " +
                                                "SET _OakdaleID = @strOakdaleID, UserID = @currentID, UserFName = @userFName , UserLName = @userLName, WeekNum = @weekNumCurrent, " +
                                                " DateCreated = @localDate, TeamLead = @teamLead, " +
                                                "RegHoursMon = @monReg, VacHoursMon = @monVac, ArpHoursMon = @monARP, HolidayHoursMon = @monHol, " +
                                                "BereaveHoursMon = @monBer, JuryDutyHoursMon = @monJur, OverTimeHoursMon = @monOT, DoubleOTHoursMon = @monDOT, CommentMon = @monCom, " +
                                                "RegHoursTue = @tueReg, VacHoursTue = @tueVac, ArpHoursTue = @tueARP, HolidayHoursTue = @tueHol, " +
                                                "BereaveHoursTue = @tueBer, JuryDutyHoursTue = @tueJur, OverTimeHoursTue = @tueOT, DoubleOTHoursTue = @tueDOT, CommentTue = @tueCom, " +
                                                "RegHoursWed = @wedReg, VacHoursWed = @wedVac, ArpHoursWed = @wedARP, HolidayHoursWed = @wedHol, " +
                                                "BereaveHoursWed = @wedBer, JuryDutyHoursWed = @wedJur, OverTimeHoursWed = @wedOT, DoubleOTHoursWed = @wedDOT, CommentWed = @wedCom, " +
                                                "RegHoursThu = @thuReg, VacHoursThu = @thuVac, ArpHoursThu = @thuARP, HolidayHoursThu = @thuHol, " +
                                                "BereaveHoursThu = @thuBer, JuryDutyHoursThu = @thuJur, OverTimeHoursThu = @thuOT, DoubleOTHoursThu = @thuDOT, CommentThu = @thuCom, " +
                                                "RegHoursFri = @friReg, VacHoursFri = @friVac, ArpHoursFri = @friARP, HolidayHoursFri = @friHol, " +
                                                "BereaveHoursFri = @friBer, JuryDutyHoursFri = @friJur, OverTimeHoursFri = @friOT, DoubleOTHoursFri = @friDOT, CommentFri = @friCom, " +
                                                "RegHoursSat = @satReg, VacHoursSat = @satVac, ArpHoursSat = @satARP, HolidayHoursSat = @satHol, " +
                                                "BereaveHoursSat = @satBer, JuryDutyHoursSat = @satJur, OverTimeHoursSat = @satOT, DoubleOTHoursSat = @satDOT, CommentSat = @satCom, " +
                                                "RegHoursSun = @sunReg, VacHoursSun = @sunVac, ArpHoursSun = @sunARP, HolidayHoursSun = @sunHol, " +
                                                "BereaveHoursSun = @sunBer, JuryDutyHoursSun = @sunJur, OverTimeHoursSun = @sunOT, DoubleOTHoursSun = @sunDOT, CommentSun = @sunCom, " +
                                                "Status = @status, ReqYear = @currentYear, RegHoursTotal = @regTotal, VacHoursTotal = @vacTotal, ArpHoursTotal = @arpTotal, " +
                                                "HolidayHoursTotal = @holTotal, BereaveHoursTotal = @berTotal, JuryDutyHoursTotal = @jurTotal, OverTimeHoursTotal = @otTotal, DoubleOTHoursTotal = @dotTotal, ShowNoShow = @showNoShow " +
                                                "WHERE UserID = '" + userID + "' AND WeekNum = " + weekNumCurrent;

                    SqlCommand updateCommand = new SqlCommand(updateQuery, haegerTimeDB);
                    updateCommand.Parameters.AddWithValue("@strOakdaleID", strOakdaleID);
                    updateCommand.Parameters.AddWithValue("@currentID", rqT.Rows[0][2]);
                    updateCommand.Parameters.AddWithValue("@userFName", rqT.Rows[0][3]);
                    updateCommand.Parameters.AddWithValue("@userLName", rqT.Rows[0][4]);
                    updateCommand.Parameters.AddWithValue("@weekNumCurrent", rqT.Rows[0][5]);
                    updateCommand.Parameters.AddWithValue("@localDate", localDate);
                    updateCommand.Parameters.AddWithValue("@teamLead", rqT.Rows[0][7]);
                    updateCommand.Parameters.AddWithValue("@monReg", rqT.Rows[0][8]);
                    updateCommand.Parameters.AddWithValue("@monVac", rqT.Rows[0][9]);
                    updateCommand.Parameters.AddWithValue("@monARP", rqT.Rows[0][10]);
                    updateCommand.Parameters.AddWithValue("@monHol", rqT.Rows[0][11]);
                    updateCommand.Parameters.AddWithValue("@monBer", rqT.Rows[0][12]);
                    updateCommand.Parameters.AddWithValue("@monJur", rqT.Rows[0][13]);
                    updateCommand.Parameters.AddWithValue("@monOT", rqT.Rows[0][14]);
                    updateCommand.Parameters.AddWithValue("@monCom", rqT.Rows[0][15]);
                    updateCommand.Parameters.AddWithValue("@tueReg", rqT.Rows[0][16]);
                    updateCommand.Parameters.AddWithValue("@tueVac", rqT.Rows[0][17]);
                    updateCommand.Parameters.AddWithValue("@tueARP", rqT.Rows[0][18]);
                    updateCommand.Parameters.AddWithValue("@tueHol", rqT.Rows[0][19]);
                    updateCommand.Parameters.AddWithValue("@tueBer", rqT.Rows[0][20]);
                    updateCommand.Parameters.AddWithValue("@tueJur", rqT.Rows[0][21]);
                    updateCommand.Parameters.AddWithValue("@tueOT", rqT.Rows[0][22]);
                    updateCommand.Parameters.AddWithValue("@tueCom", rqT.Rows[0][23]);
                    updateCommand.Parameters.AddWithValue("@wedReg", rqT.Rows[0][24]);
                    updateCommand.Parameters.AddWithValue("@wedVac", rqT.Rows[0][25]);
                    updateCommand.Parameters.AddWithValue("@wedARP", rqT.Rows[0][26]);
                    updateCommand.Parameters.AddWithValue("@wedHol", rqT.Rows[0][27]);
                    updateCommand.Parameters.AddWithValue("@wedBer", rqT.Rows[0][28]);
                    updateCommand.Parameters.AddWithValue("@wedJur", rqT.Rows[0][29]);
                    updateCommand.Parameters.AddWithValue("@wedOT", rqT.Rows[0][30]);
                    updateCommand.Parameters.AddWithValue("@wedCom", rqT.Rows[0][31]);
                    updateCommand.Parameters.AddWithValue("@thuReg", rqT.Rows[0][32]);
                    updateCommand.Parameters.AddWithValue("@thuVac", rqT.Rows[0][33]);
                    updateCommand.Parameters.AddWithValue("@thuARP", rqT.Rows[0][34]);
                    updateCommand.Parameters.AddWithValue("@thuHol", rqT.Rows[0][35]);
                    updateCommand.Parameters.AddWithValue("@thuBer", rqT.Rows[0][36]);
                    updateCommand.Parameters.AddWithValue("@thuJur", rqT.Rows[0][37]);
                    updateCommand.Parameters.AddWithValue("@thuOT", rqT.Rows[0][38]);
                    updateCommand.Parameters.AddWithValue("@thuCom", rqT.Rows[0][39]);
                    updateCommand.Parameters.AddWithValue("@friReg", rqT.Rows[0][40]);
                    updateCommand.Parameters.AddWithValue("@friVac", rqT.Rows[0][41]);
                    updateCommand.Parameters.AddWithValue("@friARP", rqT.Rows[0][42]);
                    updateCommand.Parameters.AddWithValue("@friHol", rqT.Rows[0][43]);
                    updateCommand.Parameters.AddWithValue("@friBer", rqT.Rows[0][44]);
                    updateCommand.Parameters.AddWithValue("@friJur", rqT.Rows[0][45]);
                    updateCommand.Parameters.AddWithValue("@friOT", rqT.Rows[0][46]);
                    updateCommand.Parameters.AddWithValue("@friCom", rqT.Rows[0][47]);
                    updateCommand.Parameters.AddWithValue("@satReg", rqT.Rows[0][48]);
                    updateCommand.Parameters.AddWithValue("@satVac", rqT.Rows[0][49]);
                    updateCommand.Parameters.AddWithValue("@satARP", rqT.Rows[0][50]);
                    updateCommand.Parameters.AddWithValue("@satHol", rqT.Rows[0][51]);
                    updateCommand.Parameters.AddWithValue("@satBer", rqT.Rows[0][52]);
                    updateCommand.Parameters.AddWithValue("@satJur", rqT.Rows[0][53]);
                    updateCommand.Parameters.AddWithValue("@satOT", rqT.Rows[0][54]);
                    updateCommand.Parameters.AddWithValue("@satCom", rqT.Rows[0][55]);
                    updateCommand.Parameters.AddWithValue("@sunReg", rqT.Rows[0][56]);
                    updateCommand.Parameters.AddWithValue("@sunVac", rqT.Rows[0][57]);
                    updateCommand.Parameters.AddWithValue("@sunARP", rqT.Rows[0][58]);
                    updateCommand.Parameters.AddWithValue("@sunHol", rqT.Rows[0][59]);
                    updateCommand.Parameters.AddWithValue("@sunBer", rqT.Rows[0][60]);
                    updateCommand.Parameters.AddWithValue("@sunJur", rqT.Rows[0][61]);
                    updateCommand.Parameters.AddWithValue("@sunOT", rqT.Rows[0][62]);
                    updateCommand.Parameters.AddWithValue("@sunCom", rqT.Rows[0][63]);
                    updateCommand.Parameters.AddWithValue("@status", rqT.Rows[0][64]);
                    updateCommand.Parameters.AddWithValue("@currentYear", rqT.Rows[0][65]);
                    updateCommand.Parameters.AddWithValue("@regTotal", rqT.Rows[0][66]);
                    updateCommand.Parameters.AddWithValue("@vacTotal", rqT.Rows[0][67]);
                    updateCommand.Parameters.AddWithValue("@arpTotal", rqT.Rows[0][68]);
                    updateCommand.Parameters.AddWithValue("@holTotal", rqT.Rows[0][69]);
                    updateCommand.Parameters.AddWithValue("@berTotal", rqT.Rows[0][70]);
                    updateCommand.Parameters.AddWithValue("@jurTotal", rqT.Rows[0][71]);
                    updateCommand.Parameters.AddWithValue("@otTotal", rqT.Rows[0][72]);
                    updateCommand.Parameters.AddWithValue("@showNoShow", rqT.Rows[0][73]);
                    updateCommand.Parameters.AddWithValue("@monDOT", rqT.Rows[0][74]);
                    updateCommand.Parameters.AddWithValue("@tueDOT", rqT.Rows[0][75]);
                    updateCommand.Parameters.AddWithValue("@wedDOT", rqT.Rows[0][76]);
                    updateCommand.Parameters.AddWithValue("@thuDOT", rqT.Rows[0][77]);
                    updateCommand.Parameters.AddWithValue("@friDOT", rqT.Rows[0][78]);
                    updateCommand.Parameters.AddWithValue("@satDOT", rqT.Rows[0][79]);
                    updateCommand.Parameters.AddWithValue("@sunDOT", rqT.Rows[0][80]);
                    updateCommand.Parameters.AddWithValue("@dotTotal", rqT.Rows[0][81]);

                    updateCommand.ExecuteNonQuery();

                }

            }



            haegerTimeDB.Close();


        }

        protected void grdTLCurrentWeek_SelectedIndexChanged(object sender, EventArgs e)
        {
            Session["selectedItemTimeOff"] = grdTLCurrentWeek.SelectedDataKey.Values["column1"].ToString();
            Response.Redirect("/PartAdminNA/CurrentDetail");
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {

            string weekNum = Session["ssWeekNum"].ToString();

            haegerTimeDB.Open();
            string currentID = User.Identity.GetUserId();
            string currentTeamLead = String.Empty;
            string teamLeadNameQuery = "SELECT Fname, LName from OakdaleTeamLeadInfo WHERE UserID = '" + currentID + "'";
            SqlCommand teamLeadNameCommand = new SqlCommand(teamLeadNameQuery, haegerTimeDB);





            using (SqlDataReader tLR = teamLeadNameCommand.ExecuteReader())
            {
                while (tLR.Read())
                {
                    currentTeamLead = tLR[0].ToString().Trim() + " " + tLR[1].ToString().Trim();
                }
            }

            string partnerNumQuery = "SELECT PartnerNum From OakdalePartnerInfo WHERE TeamLead = '" + currentTeamLead +
                                        "' ORDER BY Lname";

            string currentWeekQuery = "SELECT UserFName, UserLName, RegHoursTotal, VacHoursTotal, " +
                                        "ArpHoursTotal, HolidayHoursTotal, BereaveHoursTotal, JuryDutyHoursTotal, OverTimeHoursTotal, " +
                                        "DoubleOTHoursTotal, TeamLead " +
                                    "FROM OakdaleTime " +
                                    "WHERE WeekNum = " + weekNum + " AND TeamLead = '" + currentTeamLead +
                                    "' ORDER BY UserLName";

            SqlCommand partnerNumCommand = new SqlCommand(partnerNumQuery, haegerTimeDB);
            SqlDataReader partnerNumReader = partnerNumCommand.ExecuteReader();
            DataTable pnT = new DataTable();
            pnT.Load(partnerNumReader);
            int pntCount = pnT.Rows.Count;

            SqlCommand currentWeekCommand = new SqlCommand(currentWeekQuery, haegerTimeDB);
            SqlDataReader currentWeekReader = currentWeekCommand.ExecuteReader();
            DataTable cwT = new DataTable();
            cwT.Load(currentWeekReader);
            int cwtCount = cwT.Rows.Count;

            DataTable masterTable = new DataTable();

            // Add Partner ID to where column1 was
            cwT.Columns.Add("Emp #").SetOrdinal(0);
            // Loop to populate first column
            for (int i = 0; i < pnT.Rows.Count; i++)
            {
                cwT.Rows[i]["Emp #"] = pnT.Rows[i]["PartnerNum"];
                
            }

            cwT.Columns["UserFName"].ColumnName = "First";
            cwT.Columns["UserLName"].ColumnName = "Last";
            cwT.Columns["RegHoursTotal"].ColumnName = "Regular Hrs";
            cwT.Columns["VacHoursTotal"].ColumnName = "Vacation Hrs";
            cwT.Columns["ArpHoursTotal"].ColumnName = "ARP(Sick) Hrs";
            cwT.Columns["HolidayHoursTotal"].ColumnName = "Holiday Hrs";
            cwT.Columns["BereaveHoursTotal"].ColumnName = "Bereavement Hrs";
            cwT.Columns["JuryDutyHoursTotal"].ColumnName = "Jury Duty Hrs";
            cwT.Columns["OverTimeHoursTotal"].ColumnName = "Overtime Hrs";
            cwT.Columns["DoubleOTHoursTotal"].ColumnName = "Double OT Hrs";
            cwT.Columns["TeamLead"].ColumnName = "Team Lead";


            ClosedXML.Excel.XLWorkbook wbook = new ClosedXML.Excel.XLWorkbook();
            var ws = wbook.Worksheets.Add(cwT, "test1");



            for (int i = 1; i <= 12; i++) // this will aply it form col 1 to 10
            {
                var col1 = ws.Column(i);
                col1.Width = 12;
                for (int k = 1; k <= 12; k++)
                {

                    ws.Cell(i, k).Style.Font.Bold = true;
                    ws.Cell(i, k).Style.Font.FontName = "Arial";
                    ws.Cell(i, k).Style.Font.FontSize = 14;
                }

            }


            wbook.SaveAs("D:\\Emails\\TestSheet1.xlsx");

            string subject = "Week Summary" + localDate.ToString();

            try
            {

                // Create the msg object to be sent
                MailMessage _mailmsg = new MailMessage();

                //Make TRUE because our body text is html
                _mailmsg.IsBodyHtml = true;

                // Configure the address we are sending the mail from
                MailAddress address = new MailAddress("webadmin@haeger.com");

                _mailmsg.From = address;

                // Add your email address to the recipients
                _mailmsg.To.Add(teamLeadEmail);
                _mailmsg.Bcc.Add("webadmin@haeger.com");
                _mailmsg.Bcc.Add("regeah811@gmail.com");

                //Set Subject
                _mailmsg.Subject = subject;

                //Set Body Text of Email
                //_mailmsg.Body = MailText;
                Attachment attachment;
                attachment = new Attachment("D:\\Emails\\TestSheet1.xlsx");
                _mailmsg.Attachments.Add(attachment);


                // Configure an SmtpClient to send the mail.
                SmtpClient _smtp = new SmtpClient("smtp-mail.outlook.com");
                _smtp.Port = 587;
                _smtp.DeliveryMethod = SmtpDeliveryMethod.Network;
                _smtp.UseDefaultCredentials = false;
                System.Net.NetworkCredential credentials =
                    new System.Net.NetworkCredential("webadmin@haeger.com", "Crazycat75");
                _smtp.EnableSsl = true;
                _smtp.Credentials = credentials;

                //_smtp.DeliveryMethod = SmtpDeliveryMethod.SpecifiedPickupDirectory;
                //_smtp.PickupDirectoryLocation = @targetDirectory;

                // Send the msg.
                _smtp.Send(_mailmsg);

                // Display some feedback to the user to let them know it was sent.
                //lblResult.Text = "Your message was sent!";


                Response.Redirect("/PartAdminNA/TeamLeadHome");

            }
            catch (Exception)
            {


            }


            haegerTimeDB.Close();




        }

        protected void btnBack_Click(object sender, EventArgs e)
        {
            Response.Redirect("/PartAdminNA/TeamLeadHome");
        }
    }
}