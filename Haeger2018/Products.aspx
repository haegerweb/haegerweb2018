﻿<%@ Page Title="Products" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Products.aspx.cs" Inherits="Haeger2018.Products" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
    
        <link href="/content/style.css" rel="stylesheet"/>
        <link href="/content/color-default.css" rel="stylesheet"/>
        <link href="/content/font-awesome.css" rel="stylesheet"/>
        <link href="https://fortawesome.github.io/Font-Awesome/assets/font-awesome/css/font-awesome.css" rel="stylesheet">
        <link rel="stylesheet" href="https://fortawesome.github.io/Font-Awesome/3.2.1/assets/font-awesome/css/font-awesome.css">

    <style>
        iframe  {height:100%; width:100%;}

         p1
        {
            color:darkred
        }
        * {
            box-sizing: border-box;
        }

        body {
            background-color: #ffffff;
            padding: 0px;
            font-family: Arial;
            height:100%;
        }

        /* Center website */
        .main {
            max-width: 100%;
            margin: 100%;
        }

        h1 {
            font-size: 50px;
            word-break: break-all;
        }

        .row {
            margin: 10px -16px;
        }

        /* Add padding BETWEEN each column */
        .row,
        .row > .column {
            padding: 10px;
        }

        /* Create three equal columns that floats next to each other */
        .column {
            float: left;
            width: 33.33%;
            display: table; /* Hide all elements by default */
        }

        /* Clear floats after rows */ 
        .row:after {
            content: "";
            display: table;
            clear: both;
        }

        /* Content */

        /* The "show" class is added to the filtered elements */
        .show {
          display: block;
        }
    </style>

    <script type="text/javascript">
        window.setInterval(function(){
            var lang = $(".goog-te-menu-value span:first").text();
            //OT4
            var enOT4div = document.getElementById('<%=EnOT4.ClientID%>');
            var frOT4div = document.getElementById('<%=FrOT4.ClientID%>');
            var geOT4div = document.getElementById('<%=GeOT4.ClientID%>');
            var itOT4div = document.getElementById('<%=ItOT4.ClientID%>');
            var spOT4div = document.getElementById('<%=SpOT4.ClientID%>');
            // OTL
            var enOTLdiv = document.getElementById('<%=EnOTL.ClientID%>');
            var frOTLdiv = document.getElementById('<%=FrOTL.ClientID%>');
            var geOTLdiv = document.getElementById('<%=GeOTL.ClientID%>');
            var itOTLdiv = document.getElementById('<%=ItOTL.ClientID%>');
            var spOTLdiv = document.getElementById('<%=SpOTL.ClientID%>');
            // WT4
            var enWT4div = document.getElementById('<%=EnWT4.ClientID%>');
            var frWT4div = document.getElementById('<%=FrWT4.ClientID%>');
            var geWT4div = document.getElementById('<%=GeWT4.ClientID%>');
            var itWT4div = document.getElementById('<%=ItWT4.ClientID%>');
            var spWT4div = document.getElementById('<%=SpWT4.ClientID%>');
            // 824 MSPe
            var en8MSPediv = document.getElementById('<%=En8MSPe.ClientID%>');
            var fr8MSPediv = document.getElementById('<%=Fr8MSPe.ClientID%>');
            var ge8MSPediv = document.getElementById('<%=Ge8MSPe.ClientID%>');
            var it8MSPediv = document.getElementById('<%=It8MSPe.ClientID%>');
            var sp8MSPediv = document.getElementById('<%=Sp8MSPe.ClientID%>');
            // 618 MSPe
            var en6MSPediv = document.getElementById('<%=En6MSPe.ClientID%>');
            var fr6MSPediv = document.getElementById('<%=Fr6MSPe.ClientID%>');
            var ge6MSPediv = document.getElementById('<%=Ge6MSPe.ClientID%>');
            var it6MSPediv = document.getElementById('<%=It6MSPe.ClientID%>');
            var sp6MSPediv = document.getElementById('<%=Sp6MSPe.ClientID%>');
            // XYZ
            var enXYZdiv = document.getElementById('<%=EnXYZ.ClientID%>');
            var frXYZdiv = document.getElementById('<%=FrXYZ.ClientID%>');
            var geXYZdiv = document.getElementById('<%=GeXYZ.ClientID%>');
            var itXYZdiv = document.getElementById('<%=ItXYZ.ClientID%>');
            var spXYZdiv = document.getElementById('<%=SpXYZ.ClientID%>');
                        
            if (lang === "English") {
                // OT4
                enOT4div.style.display = "";
                frOT4div.style.display = "none";
                geOT4div.style.display = "none";
                itOT4div.style.display = "none";
                spOT4div.style.display = "none";
                // OTL
                enOTLdiv.style.display = "";
                frOTLdiv.style.display = "none";
                geOTLdiv.style.display = "none";
                itOTLdiv.style.display = "none";
                spOTLdiv.style.display = "none";
                // WT4
                enWT4div.style.display = "";
                frWT4div.style.display = "none";
                geWT4div.style.display = "none";
                itWT4div.style.display = "none";
                spWT4div.style.display = "none";
                // 824 MSPe
                en8MSPediv.style.display = "";
                fr8MSPediv.style.display = "none";
                ge8MSPediv.style.display = "none";
                it8MSPediv.style.display = "none";
                sp8MSPediv.style.display = "none";
                // 618 MSPe
                en6MSPediv.style.display = "";
                fr6MSPediv.style.display = "none";
                ge6MSPediv.style.display = "none";
                it6MSPediv.style.display = "none";
                sp6MSPediv.style.display = "none";
                // XYZ
                enXYZdiv.style.display = "";
                frXYZdiv.style.display = "none";
                geXYZdiv.style.display = "none";
                itXYZdiv.style.display = "none";
                spXYZdiv.style.display = "none";
            } else if (lang === "French") {
                // OT4
                enOT4div.style.display = "none";
                frOT4div.style.display = "";
                geOT4div.style.display = "none";
                itOT4div.style.display = "none";
                spOT4div.style.display = "none";
                // OTL
                enOTLdiv.style.display = "none";
                frOTLdiv.style.display = "";
                geOTLdiv.style.display = "none";
                itOTLdiv.style.display = "none";
                spOTLdiv.style.display = "none";
                // WT4
                enWT4div.style.display = "none";
                frWT4div.style.display = "";
                geWT4div.style.display = "none";
                itWT4div.style.display = "none";
                spWT4div.style.display = "none";
                // 824 MSPe
                en8MSPediv.style.display = "none";
                fr8MSPediv.style.display = "";
                ge8MSPediv.style.display = "none";
                it8MSPediv.style.display = "none";
                sp8MSPediv.style.display = "none";
                // 618 MSPe
                en6MSPediv.style.display = "none";
                fr6MSPediv.style.display = "";
                ge6MSPediv.style.display = "none";
                it6MSPediv.style.display = "none";
                sp6MSPediv.style.display = "none";
                // XYZ
                enXYZdiv.style.display = "none";
                frXYZdiv.style.display = "";
                geXYZdiv.style.display = "none";
                itXYZdiv.style.display = "none";
                spXYZdiv.style.display = "none";
            } else if (lang === "German") {
                // OT4
                enOT4div.style.display = "none";
                frOT4div.style.display = "none";
                geOT4div.style.display = "";
                itOT4div.style.display = "none";
                spOT4div.style.display = "none";
                // OTL
                enOTLdiv.style.display = "none";
                frOTLdiv.style.display = "none";
                geOTLdiv.style.display = "";
                itOTLdiv.style.display = "none";
                spOTLdiv.style.display = "none";
                // WT4
                enWT4div.style.display = "none";
                frWT4div.style.display = "none";
                geWT4div.style.display = "";
                itWT4div.style.display = "none";
                spWT4div.style.display = "none";
                // 824 MSPe
                en8MSPediv.style.display = "none";
                fr8MSPediv.style.display = "none";
                ge8MSPediv.style.display = "";
                it8MSPediv.style.display = "none";
                sp8MSPediv.style.display = "none";
                // 618 MSPe
                en6MSPediv.style.display = "none";
                fr6MSPediv.style.display = "none";
                ge6MSPediv.style.display = "";
                it6MSPediv.style.display = "none";
                sp6MSPediv.style.display = "none";
                // XYZ
                enXYZdiv.style.display = "none";
                frXYZdiv.style.display = "none";
                geXYZdiv.style.display = "";
                itXYZdiv.style.display = "none";
                spXYZdiv.style.display = "none";
            } else if (lang === "Italian") {
                // OT4
                enOT4div.style.display = "none";
                frOT4div.style.display = "none";
                geOT4div.style.display = "none";
                itOT4div.style.display = "";
                spOT4div.style.display = "none";
                // OTL
                enOTLdiv.style.display = "none";
                frOTLdiv.style.display = "none";
                geOTLdiv.style.display = "none";
                itOTLdiv.style.display = "";
                spOTLdiv.style.display = "none";
                // WT4
                enWT4div.style.display = "none";
                frWT4div.style.display = "none";
                geWT4div.style.display = "none";
                itWT4div.style.display = "";
                spWT4div.style.display = "none";
                // 824 MSPe
                en8MSPediv.style.display = "none";
                fr8MSPediv.style.display = "none";
                ge8MSPediv.style.display = "none";
                it8MSPediv.style.display = "";
                sp8MSPediv.style.display = "none";
                // 618 MSPe
                en6MSPediv.style.display = "none";
                fr6MSPediv.style.display = "none";
                ge6MSPediv.style.display = "none";
                it6MSPediv.style.display = "";
                sp6MSPediv.style.display = "none";
                // XYZ
                enXYZdiv.style.display = "none";
                frXYZdiv.style.display = "none";
                geXYZdiv.style.display = "none";
                itXYZdiv.style.display = "";
                spXYZdiv.style.display = "none";
            } else if (lang === "Spanish") {
                // OT4
                enOT4div.style.display = "none";
                frOT4div.style.display = "none";
                geOT4div.style.display = "none";
                itOT4div.style.display = "none";
                spOT4div.style.display = "";
                // OTL
                enOTLdiv.style.display = "none";
                frOTLdiv.style.display = "none";
                geOTLdiv.style.display = "none";
                itOTLdiv.style.display = "none";
                spOTLdiv.style.display = "";
                // WT4
                enWT4div.style.display = "none";
                frWT4div.style.display = "none";
                geWT4div.style.display = "none";
                itWT4div.style.display = "none";
                spWT4div.style.display = "";
                // 824 MSPe
                en8MSPediv.style.display = "none";
                fr8MSPediv.style.display = "none";
                ge8MSPediv.style.display = "none";
                it8MSPediv.style.display = "none";
                sp8MSPediv.style.display = "";
                // 618 MSPe
                en6MSPediv.style.display = "none";
                fr6MSPediv.style.display = "none";
                ge6MSPediv.style.display = "none";
                it6MSPediv.style.display = "none";
                sp6MSPediv.style.display = "";
                // XYZ
                enXYZdiv.style.display = "none";
                frXYZdiv.style.display = "none";
                geXYZdiv.style.display = "none";
                itXYZdiv.style.display = "none";
                spXYZdiv.style.display = "";
            } else {
                // OT4
                enOT4div.style.display = "";
                frOT4div.style.display = "none";
                geOT4div.style.display = "none";
                itOT4div.style.display = "none";
                spOT4div.style.display = "none";
                // OTL
                enOTLdiv.style.display = "";
                frOTLdiv.style.display = "none";
                geOTLdiv.style.display = "none";
                itOTLdiv.style.display = "none";
                spOTLdiv.style.display = "none";
                // WT4
                enWT4div.style.display = "";
                frWT4div.style.display = "none";
                geWT4div.style.display = "none";
                itWT4div.style.display = "none";
                spWT4div.style.display = "none";
                // 824 MSPe
                en8MSPediv.style.display = "";
                fr8MSPediv.style.display = "none";
                ge8MSPediv.style.display = "none";
                it8MSPediv.style.display = "none";
                sp8MSPediv.style.display = "none";
                // 618 MSPe
                en6MSPediv.style.display = "";
                fr6MSPediv.style.display = "none";
                ge6MSPediv.style.display = "none";
                it6MSPediv.style.display = "none";
                sp6MSPediv.style.display = "none";
                // XYZ
                enXYZdiv.style.display = "";
                frXYZdiv.style.display = "none";
                geXYZdiv.style.display = "none";
                itXYZdiv.style.display = "none";
                spXYZdiv.style.display = "none";
            }
           
        },500);
        



    </script>




</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">


    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-8 col-xs-12">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="summernote"><h1><strong>Products</strong></h1>
                            <p>From our standard product offering insertion machines, to our build-to-order products and custom engineered insertion solutions, 
                                Haeger has the ability to provide you with a totally unified solution for virtually any of your insertion challenges. 
                                Whether your shop is new to hardware insertion, or inserts millions of fasteners each year, Haeger’s solution oriented approach 
                                will ensure that you get the correct system for your needs.</p>
                            <p>Downloadable pdf documents provide you with product details, guidance on choosing the right tools and part overviews. Need assistance? 
                                Contact us today! USA Headquarters: +1 (209) 848-4000, Europe Headquarters: +31 541 530 230</p>
                        </div>
        <div class="row">
            <%--********************** ONE TOUCH **********************--%>
            <div class="haeger col-lg-4 col-md-4 col-sm-6 col-xs-12 mix cat_1" style="display: block;">
                <div class="img-caption">
                    <div class="row">
                        <div class="col-md-12 text-center">
                            <img src="images/824.ot.4e.png" alt="Lights" />
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 text-center notranslate">
                            <p1>824 OneTouch-4e</p1>
                        </div>
                    </div>
                    <div class="caption">
                        <div class="caption-content">
                            <div class="row">
                                <div class="col-md-8 col-md-offset-2">
                                    <a href="https://www.youtube.com/embed/OQqC9b9Jt9A?autohide=1&amp;fs=1&amp;rel=0&amp;hd=1&amp;wmode=opaque&amp;enablejsapi=1" data-action="video" class="btn-block animated fadeInDown mlbox_cat_1" data-type="iframe" data-options="thumbnail:'https://i.ytimg.com/vi/OQqC9b9Jt9A/mqdefault.jpg', icon: 'video', width: 1920, height: 1200, keepAspectRatio: true" target="_blank"><i class="fab fa-youtube" aria-hidden="true"></i>&nbsp Video</a>                                            
                                </div>
                            </div>
                            <div class="row">
                                <div id="EnOT4" runat="server" class="col-md-8 col-md-offset-2">
                                    <a href="/pdf/Machine Brochures LowResolution/824 OT4 - English - LR.pdf" target="_blank" data-action="pdf" class="btn-block animated fadeInDown">
                                        <i class="fa fa-file-pdf" aria-hidden="true"></i>&nbsp Brochure</a>
                                </div>
                                <div id="FrOT4" runat="server" class="col-md-8 col-md-offset-2">
                                    <a href="/pdf/Machine Brochures LowResolution/824 OT4 - French - LR.pdf" target="_blank" data-action="pdf" class="btn-block animated fadeInDown">
                                        <i class="fa fa-file-pdf" aria-hidden="true"></i>&nbsp Brochure</a>
                                </div>
                                <div id="GeOT4" runat="server" class="col-md-8 col-md-offset-2">
                                    <a href="/pdf/Machine Brochures LowResolution/824 OT4 - German - LR.pdf" target="_blank" data-action="pdf" class="btn-block animated fadeInDown">
                                        <i class="fa fa-file-pdf" aria-hidden="true"></i>&nbsp Brochure</a>
                                </div>
                                <div id="ItOT4" runat="server" class="col-md-8 col-md-offset-2">
                                    <a href="/pdf/Machine Brochures LowResolution/824 OT4 - Italian - LR.pdf" target="_blank" data-action="pdf" class="btn-block animated fadeInDown">
                                        <i class="fa fa-file-pdf" aria-hidden="true"></i>&nbsp Brochure</a>
                                </div>
                                <div id="SpOT4" runat="server" class="col-md-8 col-md-offset-2">
                                    <a href="/pdf/Machine Brochures LowResolution/824 OT4 - Spanish - LR.pdf" target="_blank" data-action="pdf" class="btn-block animated fadeInDown">
                                        <i class="fa fa-file-pdf" aria-hidden="true"></i>&nbsp Brochure</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <%--********************** ONE TOUCH Lite **********************--%>
            <div class="haeger col-lg-4 col-md-4 col-sm-6 col-xs-12 mix cat_1" style="display: block;">
                <div class="img-caption">
                    <div class="row">
                        <div class="col-md-12 text-center">
                            <img src="images/824.ot.4e.lite.png" alt="Lights" />
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 text-center notranslate">
                            <p1>824 OneTouch-4e Lite</p1>
                        </div>
                    </div>
                    <div class="caption">
                        <div class="caption-content">
                            <div class="row">
                                <div class="col-md-8 col-md-offset-2">
                                    <a href="https://www.youtube.com/embed/u1yts1-xcKw?autohide=1&amp;fs=1&amp;rel=0&amp;hd=1&amp;wmode=opaque&amp;enablejsapi=1" data-action="video" class="btn-block animated fadeInDown mlbox_cat_1" data-type="iframe" data-options="thumbnail:'https://i.ytimg.com/vi/u1yts1-xcKw/mqdefault.jpg', icon: 'video', width: 1920, height: 1200, keepAspectRatio: true" target="_blank"><i class="fab fa-youtube" aria-hidden="true"></i>&nbsp Video</a>                                           
                                </div>
                            </div>
                            <div class="row">
                                <div id="EnOTL" runat="server" class="col-md-8 col-md-offset-2">
                                    <a href="/pdf/Machine Brochures LowResolution/824 OT-Lite - English - LR.pdf" target="_blank" data-action="pdf" class="btn-block animated fadeInDown">
                                        <i class="fa fa-file-pdf"></i>&nbsp Brochure</a>
                                </div>
                                <div id="FrOTL" runat="server" class="col-md-8 col-md-offset-2">
                                    <a href="/pdf/Machine Brochures LowResolution/824 OT-Lite - French - LR.pdf" target="_blank" data-action="pdf" class="btn-block animated fadeInDown">
                                        <i class="fa fa-file-pdf"></i>&nbsp Brochure</a>
                                </div>
                                <div id="GeOTL" runat="server" class="col-md-8 col-md-offset-2">
                                    <a href="/pdf/Machine Brochures LowResolution/824 OT-Lite - German - LR.pdf" target="_blank" data-action="pdf" class="btn-block animated fadeInDown">
                                        <i class="fa fa-file-pdf"></i>&nbsp Brochure</a>
                                </div>
                                <div id="ItOTL" runat="server" class="col-md-8 col-md-offset-2">
                                    <a href="/pdf/Machine Brochures LowResolution/824 OT-Lite - Italian - LR.pdf" target="_blank" data-action="pdf" class="btn-block animated fadeInDown">
                                        <i class="fa fa-file-pdf"></i>&nbsp Brochure</a>
                                </div>
                                <div id="SpOTL" runat="server" class="col-md-8 col-md-offset-2">
                                    <a href="/pdf/Machine Brochures LowResolution/824 OT-Lite - Spanish - LR.pdf" target="_blank" data-action="pdf" class="btn-block animated fadeInDown">
                                        <i class="fa fa-file-pdf"></i>&nbsp Brochure</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <%--********************** WINDOW TOUCH **********************--%>
            <div class="haeger col-lg-4 col-md-4 col-sm-6 col-xs-12 mix cat_1" style="display: block;">
                <div class="img-caption">
                    <div class="row">
                        <div class="col-md-12 text-center">
                            <img src="images/824.wt-4e.png" alt="Lights" />
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 text-center notranslate">
                            <p1>824 WindowTouch-4e</p1>
                        </div>
                    </div>
                    <div class="caption">
                        <div class="caption-content">
                            <div class="row">
                                <div class="col-md-8 col-md-offset-2">
                                    <a href="https://www.youtube.com/embed/--j0MHSc2dg?autohide=1&amp;fs=1&amp;rel=0&amp;hd=1&amp;wmode=opaque&amp;enablejsapi=1" data-action="video" class="btn-block animated fadeInDown mlbox_cat_1" data-type="iframe" data-options="thumbnail:'https://i.ytimg.com/vi/--j0MHSc2dg/mqdefault.jpg', icon: 'video', width: 1920, height: 1200, keepAspectRatio: true" target="_blank"><i class="fab fa-youtube" aria-hidden="true"></i>&nbsp Video</a>                                            
                                </div>
                            </div>
                            <div class="row">
                                <div id="EnWT4" runat="server" class="col-md-8 col-md-offset-2">
                                    <a href="/pdf/Machine Brochures LowResolution/824 WT4 - English - LR.pdf" target="_blank" data-action="pdf" class="btn-block animated fadeInDown">
                                        <i class="fa fa-file-pdf"></i>&nbsp Brochure</a>
                                </div>
                                 <div id="FrWT4" runat="server" class="col-md-8 col-md-offset-2">
                                    <a href="/pdf/Machine Brochures LowResolution/824 WT4 - French - LR.pdf" target="_blank" data-action="pdf" class="btn-block animated fadeInDown">
                                        <i class="fa fa-file-pdf"></i>&nbsp Brochure</a>
                                </div>
                                 <div id="GeWT4" runat="server" class="col-md-8 col-md-offset-2">
                                    <a href="/pdf/Machine Brochures LowResolution/824 WT4 - German - LR.pdf" target="_blank" data-action="pdf" class="btn-block animated fadeInDown">
                                        <i class="fa fa-file-pdf"></i>&nbsp Brochure</a>
                                </div>
                                 <div id="ItWT4" runat="server" class="col-md-8 col-md-offset-2">
                                    <a href="/pdf/Machine Brochures LowResolution/824 WT4 - Italian - LR.pdf" target="_blank" data-action="pdf" class="btn-block animated fadeInDown">
                                        <i class="fa fa-file-pdf"></i>&nbsp Brochure</a>
                                </div>
                                 <div id="SpWT4" runat="server" class="col-md-8 col-md-offset-2">
                                    <a href="/pdf/Machine Brochures LowResolution/824 WT4 - Spanish - LR.pdf" target="_blank" data-action="pdf" class="btn-block animated fadeInDown">
                                        <i class="fa fa-file-pdf"></i>&nbsp Brochure</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <%--********************** 824 MSPe **********************--%>
            <div class="haeger col-lg-4 col-md-4 col-sm-6 col-xs-12 mix cat_1" style="display: block;">
                <div class="img-caption">
                    <div class="row">
                        <div class="col-md-12 text-center">
                            <img src="images/824-MSPe.jpg" alt="Lights" />
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 text-center notranslate">
                            <p1>824 MSPe</p1>
                        </div>
                    </div>
                    <div class="caption">
                        <div class="caption-content">
                            <div class="row">
                                <div class="col-md-8 col-md-offset-2">
                                    <a href="https://www.youtube.com/embed/aqnS5kqPQCQ?autohide=1&amp;fs=1&amp;rel=0&amp;hd=1&amp;wmode=opaque&amp;enablejsapi=1" data-action="video" class="btn-block animated fadeInDown mlbox_cat_1" data-type="iframe" data-options="thumbnail:'https://i.ytimg.com/vi/aqnS5kqPQCQ/mqdefault.jpg', icon: 'video', width: 1920, height: 1200, keepAspectRatio: true" target="_blank"><i class="fab fa-youtube" aria-hidden="true"></i>&nbsp Video</a>                                           
                                </div>
                            </div>
                            <div class="row">
                                <div id="En8MSPe" runat="server" class="col-md-8 col-md-offset-2">
                                    <a href="/pdf/Machine Brochures LowResolution/824 MSPe - English - LR.pdf" target="_blank" data-action="pdf" class="btn-block animated fadeInDown">
                                        <i class="fa fa-file-pdf"></i>&nbsp Brochure</a>
                                </div>
                                <div id="Fr8MSPe" runat="server" class="col-md-8 col-md-offset-2">
                                    <a href="/pdf/Machine Brochures LowResolution/824 MSPe - French - LR.pdf" target="_blank" data-action="pdf" class="btn-block animated fadeInDown">
                                        <i class="fa fa-file-pdf"></i>&nbsp Brochure</a>
                                </div>
                                <div id="Ge8MSPe" runat="server" class="col-md-8 col-md-offset-2">
                                    <a href="/pdf/Machine Brochures LowResolution/824 MSPe - German - LR.pdf" target="_blank" data-action="pdf" class="btn-block animated fadeInDown">
                                        <i class="fa fa-file-pdf"></i>&nbsp Brochure</a>
                                </div>
                                <div id="It8MSPe" runat="server" class="col-md-8 col-md-offset-2">
                                    <a href="/pdf/Machine Brochures LowResolution/824 MSPe - Italian - LR.pdf" target="_blank" data-action="pdf" class="btn-block animated fadeInDown">
                                        <i class="fa fa-file-pdf"></i>&nbsp Brochure</a>
                                </div>
                                <div id="Sp8MSPe" runat="server" class="col-md-8 col-md-offset-2">
                                    <a href="/pdf/Machine Brochures LowResolution/824 MSPe - Spanish - LR.pdf" target="_blank" data-action="pdf" class="btn-block animated fadeInDown">
                                        <i class="fa fa-file-pdf"></i>&nbsp Brochure</a>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <%--********************** 618 MSPe **********************--%>
            <div class="haeger col-lg-4 col-md-4 col-sm-6 col-xs-12 mix cat_1" style="display: block;">
                <div class="img-caption">
                    <div class="row">
                        <div class="col-md-12 text-center">
                            <img src="images/618-MSPe.jpg" alt="Lights" />
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 text-center notranslate">
                            <p1>618 MSPe</p1>
                        </div>
                    </div>
                    <div class="caption">
                        <div class="caption-content">
                            <div class="row">
                                <div class="col-md-8 col-md-offset-2">
                                    <a href="https://www.youtube.com/embed/aqnS5kqPQCQ?autohide=1&amp;fs=1&amp;rel=0&amp;hd=1&amp;wmode=opaque&amp;enablejsapi=1" data-action="video" class="btn-block animated fadeInDown mlbox_cat_1" data-type="iframe" data-options="thumbnail:'https://i.ytimg.com/vi/aqnS5kqPQCQ/mqdefault.jpg', icon: 'video', width: 1920, height: 1200, keepAspectRatio: true" target="_blank"><i class="fab fa-youtube" aria-hidden="true"></i>&nbsp Video</a>                                             
                                </div>
                            </div>
                            <div class="row">
                                 <div id="En6MSPe" runat="server" class="col-md-8 col-md-offset-2">
                                    <a href="/pdf/Machine Brochures LowResolution/618 MSPe - English - LR.pdf" target="_blank" data-action="pdf" class="btn-block animated fadeInDown">
                                        <i class="fa fa-file-pdf"></i>&nbsp Brochure</a>
                                </div>
                                <div id="Fr6MSPe" runat="server" class="col-md-8 col-md-offset-2">
                                    <a href="/pdf/Machine Brochures LowResolution/618 MSPe - French - LR.pdf" target="_blank" data-action="pdf" class="btn-block animated fadeInDown">
                                        <i class="fa fa-file-pdf"></i>&nbsp Brochure</a>
                                </div>
                                <div id="Ge6MSPe" runat="server" class="col-md-8 col-md-offset-2">
                                    <a href="/pdf/Machine Brochures LowResolution/618 MSPe - German - LR.pdf" target="_blank" data-action="pdf" class="btn-block animated fadeInDown">
                                        <i class="fa fa-file-pdf"></i>&nbsp Brochure</a>
                                </div>
                                <div id="It6MSPe" runat="server" class="col-md-8 col-md-offset-2">
                                    <a href="/pdf/Machine Brochures LowResolution/618 MSPe - Italian - LR.pdf" target="_blank" data-action="pdf" class="btn-block animated fadeInDown">
                                        <i class="fa fa-file-pdf"></i>&nbsp Brochure</a>
                                </div>
                                <div id="Sp6MSPe" runat="server" class="col-md-8 col-md-offset-2">
                                    <a href="/pdf/Machine Brochures LowResolution/618 MSPe - Spanish - LR.pdf" target="_blank" data-action="pdf" class="btn-block animated fadeInDown">
                                        <i class="fa fa-file-pdf"></i>&nbsp Brochure</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="haeger col-lg-4 col-md-4 col-sm-6 col-xs-12 mix cat_1" style="display: block;">
                <div class="img-caption">
                    <div class="row">
                        <div class="col-md-12 text-center">
                            <img src="images/824.OT-4e.xyz.r.png" alt="Lights" />
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 text-center notranslate">
                            <p1>824 OneTouch-4e XYZR</p1>
                        </div>
                    </div>
                    <div class="caption">
                        <div class="caption-content">
                            <div class="row">
                                <div class="col-md-6">
                                    <a href="https://www.youtube.com/embed/H_yVuY2quz8?autohide=1&amp;fs=1&amp;rel=0&amp;hd=1&amp;wmode=opaque&amp;enablejsapi=1" data-action="video" class="btn-block animated fadeInDown mlbox_cat_1" data-type="iframe" data-options="thumbnail:'https://i.ytimg.com/vi/H_yVuY2quz8/mqdefault.jpg', icon: 'video', width: 1920, height: 1200, keepAspectRatio: true" target="_blank"><i class="fab fa-youtube"></i><small>&nbsp Video 1</small></a>                                                
                                </div>
                                <div class="col-md-6">
                                    <a href="https://www.youtube.com/embed/bkbP3If4JpU?autohide=1&amp;fs=1&amp;rel=0&amp;hd=1&amp;wmode=opaque&amp;enablejsapi=1" data-action="video" 
                                        class="btn-block animated fadeInDown mlbox_cat_1" data-type="iframe" 
                                        data-options="thumbnail:'https://i.ytimg.com/vi/bkbP3If4JpU/mqdefault.jpg', icon: 'video', width: 1920, height: 1200, keepAspectRatio: true" target="_blank"><i class="fab fa-youtube"></i><small>&nbsp Video 2</small></a>
                                </div>
                            </div>
                            <div class="row">
                                <div id="EnXYZ" runat="server" class="col-md-8 col-md-offset-2">
                                    <a href="/pdf/Machine Brochures LowResolution/824 XYZ - English - LR.pdf" target="_blank" class="btn-block animated fadeInDown">
                                       <i class="fa fa-file-pdf"></i>&nbsp Brochure </a>
                                </div>
                                <div id="FrXYZ" runat="server" class="col-md-8 col-md-offset-2">
                                    <a href="/pdf/Machine Brochures LowResolution/824 XYZ - French - LR.pdf" target="_blank" class="btn-block animated fadeInDown">
                                       <i class="fa fa-file-pdf"></i>&nbsp Brochure </a>
                                </div>
                                <div id="GeXYZ" runat="server" class="col-md-8 col-md-offset-2">
                                    <a href="/pdf/Machine Brochures LowResolution/824 XYZ - German - LR.pdf" target="_blank" class="btn-block animated fadeInDown">
                                       <i class="fa fa-file-pdf"></i>&nbsp Brochure </a>
                                </div>
                                <div id="ItXYZ" runat="server" class="col-md-8 col-md-offset-2">
                                    <a href="/pdf/Machine Brochures LowResolution/824 XYZ - Italian - LR.pdf" target="_blank" class="btn-block animated fadeInDown">
                                       <i class="fa fa-file-pdf"></i>&nbsp Brochure </a>
                                </div>
                                <div id="SpXYZ" runat="server" class="col-md-8 col-md-offset-2">
                                    <a href="/pdf/Machine Brochures LowResolution/824 XYZ - Spanish - LR.pdf" target="_blank" class="btn-block animated fadeInDown">
                                       <i class="fa fa-file-pdf"></i>&nbsp Brochure </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
                    </div>

                </div>




            </div> <%--END col-xs-8--%>



            <div class="col-sm-4 col-xs-12">
                 <div class="row">
                        <div class="col-md-6">
                            <a class="tileLink"  href="ServiceRequest.aspx" > 
                                <div class="tile nosize dark-blue animated zoomIn animation-delay-5 small" style="margin:0"> 
                                    <h2><i class="fa fa-wrench"></i>Service Request</h2> 
                                    <p>Service visit request</p> 
                                </div> 
                            </a> 
                        </div>
                        <div class="col-md-6">
                            <a class="tileLink" href="SalesRequest.aspx"> 
                                <div class="tile nosize blue animated zoomIn animation-delay-5 small"> 
                                    <h2><i class="fa fa-arrow-right"></i>&nbsp;&nbsp;Sales Request</h2> 
                                    <p>Sales visit request</p> 
                                </div> 
                            </a> 
                        </div>
                    </div>

                <div class="row">
                        <div class="col-md-12">
                            <a class="tileLink" data-action="service" href="RequestRMA.aspx"> 
                                <div class="tile nosize dark-red animated zoomIn animation-delay-7 small"> 
                                    <h2><i class="fa fa-undo"></i>&nbsp;&nbsp;RMA Request</h2> 
                                    <p>Return Authorization Form</p> 
                                </div> 
                            </a> 
                        </div>
                    </div>
                <div class="row">    
                    <a class="btn btn-black btn-block animated bounceInRight animation-delay-9" href="https://itunes.apple.com/us/app/haeger-wizard/id625478700?mt=8&amp;uo=4" target="_itunes">
                        <div class="row row-vertical-align row-no-pad">
                            <div class="col-md-6">
                                <span style="font-size: 18px;">Haeger Wizard</span>
                            </div>
                            <div class="col-md-5">
                                <img src="/images/appstore.png" />
                            </div>
                            <div class="col-md-1">
                            </div>
                        </div>
                    </a>

                    </div>
                <br />
                <div class="row">    
                <a class="btn btn-black btn-block animated bounceInRight animation-delay-11" href="https://play.google.com/store/apps/details?id=edu.haeger.haegerwizard&hl=en" target="_itunes">
                        <div class="row row-vertical-align row-no-pad">
                            <div class="col-md-6">
                                <span style="font-size: 18px;">Haeger Wizard</span>
                            </div>
                            <div class="col-md-5">
                                <img src="/images/googleplay.png" />
                            </div>
                            <div class="col-md-1">
                            </div>
                        </div>
                    </a>
                </div>
                <br />


                <%--YouTube Videos--%>
                <div class="panel" style="background-color:lightgrey;">
                     <div class="row text-center">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <a href="https://www.youtube.com/user/HaegerUSA?feature=watch" target="_blank" style="width:auto" >
                                <asp:Image ID="Image1" runat="server" ImageUrl="/images/watchOnYouTube.jpg" Height="100" Width="200" />
                            </a>
                        </div>
                    </div>




                <%--Row 1--%>
                <div class="row"  >
                    <div class="col-xs-1">

                    </div>
                    <div class="col-xs-5 text-center" style="margin:1px;height:120px">
                        <iframe 
                            src="https://www.youtube.com/embed/k6af4qIE8Yk?autohide=1&amp;fs=1&amp;rel=0&amp;hd=1&amp;wmode=opaque&amp;enablejsapi=1" 
                            allowfullscreen>
                        </iframe>                                   
                    </div>
                    <div class="col-xs-5 text-center" style="margin:1px">
                          <iframe 
                            src="https://www.youtube.com/embed/Pdg4UjWRnP4?autohide=1&amp;fs=1&amp;rel=0&amp;hd=1&amp;wmode=opaque&amp;enablejsapi=1" 
                            allowfullscreen>
                         </iframe>
                    </div>
                    <div class="col-xs-1">

                    </div>
                </div> <%--End Row--%>

                 <%--Row 2--%>
                <div class="row" >
                    <div class="col-xs-1">

                    </div>
                    <div class="col-xs-5 text-center" style="margin:1px;height:120px">
                        <iframe 
                            src="https://www.youtube.com/embed/x-U4BYUJeKI?autohide=1&amp;fs=1&amp;rel=0&amp;hd=1&amp;wmode=opaque&amp;enablejsapi=1" 
                            allowfullscreen>
                        </iframe>                                   
                    </div>
                    <div class="col-xs-5 text-center" style="margin:1px">
                          <iframe 
                            src="https://www.youtube.com/embed/7SkoPAxTjqY?autohide=1&amp;fs=1&amp;rel=0&amp;hd=1&amp;wmode=opaque&amp;enablejsapi=1" 
                            allowfullscreen>
                         </iframe>
                    </div>
                    <div class="col-xs-1">

                    </div>
                </div> <%--End Row--%>

                 <%--Row 3--%>
                <div class="row" >
                    <div class="col-xs-1">

                    </div>
                    <div class="col-xs-5 text-center" style="margin:1px;height:120px">
                        <iframe 
                            src="https://www.youtube.com/embed/1svi6J70tvM?autohide=1&amp;fs=1&amp;rel=0&amp;hd=1&amp;wmode=opaque&amp;enablejsapi=1" 
                            allowfullscreen>
                        </iframe>                                   
                    </div>
                    <div class="col-xs-5 text-center" style="margin:1px">
                          <iframe 
                            src="https://www.youtube.com/embed/_8l4YLC_Go0?autohide=1&amp;fs=1&amp;rel=0&amp;hd=1&amp;wmode=opaque&amp;enablejsapi=1" 
                            allowfullscreen>
                         </iframe>
                    </div>
                    <div class="col-xs-1">

                    </div>
                </div> <%--End Row--%>

                 <%--Row 4--%>
                <div class="row" >
                    <div class="col-xs-1">

                    </div>
                    <div class="col-xs-5 text-center" style="margin:1px;height:120px">
                        <iframe 
                            src="https://www.youtube.com/embed/n5El1t4LaJc?autohide=1&amp;fs=1&amp;rel=0&amp;hd=1&amp;wmode=opaque&amp;enablejsapi=1" 
                            allowfullscreen>
                        </iframe>                                   
                    </div>
                    <div class="col-xs-5 text-center" style="margin:1px">
                          <iframe 
                            src="https://www.youtube.com/embed/aX0YtdWy5gM?autohide=1&amp;fs=1&amp;rel=0&amp;hd=1&amp;wmode=opaque&amp;enablejsapi=1" 
                            allowfullscreen>
                         </iframe>
                    </div>
                    <div class="col-xs-1">

                    </div>
                </div> <%--End Row--%>

                <%--Row 5--%>
                <div class="row" >
                    <div class="col-xs-1">

                    </div>
                    <div class="col-xs-5 text-center" style="margin:1px;height:120px">
                        <iframe 
                            src="https://www.youtube.com/embed/IsgSkcdVqEI?autohide=1&amp;fs=1&amp;rel=0&amp;hd=1&amp;wmode=opaque&amp;enablejsapi=1" 
                            allowfullscreen>
                        </iframe>                                   
                    </div>
                    <div class="col-xs-5 text-center" style="margin:1px">
                          <iframe 
                            src="https://www.youtube.com/embed/jvkO6lxFDME?autohide=1&amp;fs=1&amp;rel=0&amp;hd=1&amp;wmode=opaque&amp;enablejsapi=1" 
                            allowfullscreen>
                         </iframe>
                    </div>
                    <div class="col-xs-1">

                    </div>
                </div> <%--End Row--%>







            </div>
        </div> <%--END col-xs-4--%>

        </div> <%--END row--%>

    </div> <%--END container-fluid--%>






</asp:Content>
