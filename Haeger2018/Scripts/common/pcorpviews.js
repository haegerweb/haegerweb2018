(function ($) {
    $.fn.exists = function () {
        return this.length !== 0;
    }

    $.extend({

        pcorp: {

            addNewPartner: function (item, options) {
                var params = {
                    id: item.UniqueId
                };

                var url = "/api/CorpPerformance/FindPartner?" + $.param(params);

                var defaults = {
                    onSuccess: function (result, data) {
                        if (options && $.isFunction(options.onSuccess))
                            options.onSuccess(result, data, options);
                    },
                    apiUrl: url,
                };
                var opts = $.extend(true, {}, defaults, options);
                return $("#commonAddPartner").dialogAddPartner(opts);
            },

            addPartnerDialogSkin: function () {
                this.data = null;
                this.getOptionOverrides = function (options) {
                    var skin = this;
                    //var isNew = (this.data.pkId <= 0 ? "Add" : "Modify") + " CMS Entry";

                    return {
                        title: "Add Partner Record",
                        description: "Please use the form below to create a Partner entry.",
                        HeaderTemplate: "<div class='dialogTitle row'>" +
                                                "<div class='col-md-1'><img src='/apple-touch-icon-60x60.png' /></div>" +
                                                "<div class='col-md-11'><h3>{title}: <span class='text-info' id='{containerId}Name'></span></h3><h4>{description}<h4></div>" +
                                            "</div>",
                        contentTemplate:
                                        "<div id='{containerId}ContactContainer' class='tab-v1 margin-bottom-20'>" +
                                        "<div id='{containerId}Area' name='{containerId}Area'>" +

                                        "<div id='{containerId}SuccessMsg' class='alert alert-success margin-bottom-10' role='alert' style='display:none;'></div>" +
                                        "<div id='{containerId}ErrorMsg' class='alert alert-danger margin-bottom-10' role='alert' style='display:none;'></div>" +
                                        "<div id='{containerId}fkContactId' name='{containerId}fkContactId' data-bind='true' data-field=id style='display:none;'></div>" +

                                        "<div data-area='tabs'>" +
                                            "<ul class='nav nav-tabs' id='{containerId}Tabs'>" +
                                                "<li class='active'><a data-toggle='tab' href='#{containerId}General'><img src='/images/contact/info.png'/>General</a></li>" +
                                                "<li ><a data-toggle='tab' href='#{containerId}Team'><img src='/images/contact/mail.png'/>Team</a></li>" +
                                            "</ul>" +

                                            "<div class='tab-content' style='padding-top:10px;overflow:visible;'>" +

                                                //General Tab
                                                    "<div class='tab-pane active' id='{containerId}General' >" +

                                                        "<div class='row'>" +
                                                            "<div class='col-md-6'>" +
                                                                "<div class='form-group'>" +
                                                                        "<label for='{containerId}Name'>Name</label>" +
                                                                        "<input id='{containerId}Name' name='{containerId}Name' class='form-control type='text' data-bind='true' data-field=name disabled = 'true'></select>" +
                                                                "</div>" +
                                                            "</div>" +
                                                            "<div class='col-md-6'>" +
                                                                "<div class='form-group'>" +
                                                                        "<label for='{containerId}fkDivisionId'>Division</label>" +
                                                                        "<select id='{containerId}fkDivisionId' name='{containerId}fkDivisionId' class='form-control selectpicker show-tick' data-style='btn-primary' data-bind='true' data-field='fkDivisionId'></select>" +
                                                                "</div>" +
                                                            "</div>" +
                                                            "<div class='col-md-6'>" +
																"<div class='form-group'>" +
                                                                        "<label for='{containerId}DivisionLocation'>Division Location</label>" +
                                                                        "<input id='{containerId}DivisionLocation' name='{containerId}DivisionLocation' class='form-control' type='text'  data-bind='true' data-field='DivisionLocation' >" +
                                                                "</div>" +
                                                            "</div>" +
                                                            "<div class='col-md-6'>" +
															"<div class='form-group'>" +
                                                                    "<label for='fkDepartmentId'>Department</label><br />" +
                                                                    "<select id='{containerId}fkDepartmentId' name='{containerId}fkDepartmentId' class='form-control selectpicker show-tick' data-style='btn-primary' data-bind='true' data-field='fkDepartmentId'></select>" +
                                                                "</div>" +
                                                            "</div>" +
                                                        "</div>" +

                                                        "<div class='row'>" +
                                                            "<div class='col-md-6'>" +
																"<div class='form-group'>" +
                                                                        "<label for='{containerId}PerformanceCenter'>Performance Center</label>" +
                                                                        "<input id='{containerId}PerformanceCenter' name='{containerId}PerformanceCenter' class='form-control' type='text'  data-bind='true' data-field='PerformanceCenter' >" +
                                                                "</div>" +
                                                            "</div>" +
                                                            "<div class='col-md-6'>"+
																"<div class='form-group'>" +
                                                                   " <label for='fkRoleId'>Role</label><br />" +
                                                                    "<select id = '{containerId}fkRoleId' name='{containerId}fkRoleId' class='form-control selectpicker show-tick' data-style='btn-primary' data-bind='true' data-field='fkRoleId'></select>" +
                                                                "</div>" +
                                                            "</div>" +
                                                        "</div>"+

                                                        "<div class='row'>" +
                                                            "<div class='col-md-6'>" +
                                                                "<div class='form-group'>" +
                                                                    "<div class='form-group'>" +
                                                                        "<label for='{containerId}IsBrandAmbassador'>Brand Ambassador?</label>" +
                                                                        "<br />" +
                                                                        "<div id='{containerId}IsBrandAmbassador' class='btn-group' data-toggle='buttons' data-bind='true' data-field='IsBrandAmbassador' data-control='true'>" +
                                                                            "<label class='btn btn-toggle'>" +
                                                                                "<input type='radio' name='{containerId}BaYes' id='{containerId}BaYes' data-value='true' data-type='boolean' />Yes" +
                                                                            "</label>" +
                                                                            "<label class='btn btn-toggle btn-no active'>" +
                                                                                "<input type='radio' name='{containerId}BaNo' id='{containerId}BaNo' data-value='false' data-type='boolean'/>No" +
                                                                            "</label>" +
                                                                        "</div>" +
                                                                    "</div>" +
                                                                "</div>" +
                                                            "</div>" +

                                                            "<div class='col-md-6'>" +
                                                                "<div class='form-group'>" +
                                                                    "<div class='form-group'>" +
                                                                        "<label for='{containerId}PublishCdd'>Publish latest CDD for this Partner and their Team Leader</label>" +
                                                                        "<br />" +
                                                                        "<div id='{containerId}PublishCdd' class='btn-group' data-toggle='buttons' data-bind='true' data-field='PublishCdd' data-control='true'>" +
                                                                            "<label id='cYes' class='btn btn-toggle'>" +
                                                                                "<input type='radio' name='{containerId}CddYes' id='{containerId}CddYes' data-value='true' data-type='boolean'/>Yes" +
                                                                            "</label>" +
                                                                            "<label id ='cNo' class='btn btn-toggle btn-no'>" +
                                                                                "<input type='radio' name='{containerId}CddNo' id='{containerId}CddNo' data-value='false' data-type='boolean' />No" +
                                                                            "</label>" +
                                                                        "</div>" +
                                                                    "</div>" +
                                                                "</div>" +
                                                            "</div>" +
                                                        "</div>" +

                                                    "</div>" +//End General tab
                                                    //Team Tab
                                                    "<div class='tab-pane' id='{containerId}Team' >" +
                                                        "<div class='row'>" +
                                                            "<div class='col-md-12'>" +
                                                                   "<div id='Teams' data-teamid='-1'></div>" +
                                                            "</div>" +
                                                        "</div>" +
                                                    "</div>" +  //End Team Tab
                                                "</div>" + //Tab Content
                                            "</div>" //Tab Area
                                        ,
                        footerTemplate: "<div>" +
                                            "<button id='{containerId}Ok' class='btn btn-primary' aria-hidden='true'>Create Partner</button>" +
                                            "<button id='{containerId}Cancel' class='btn' aria-hidden='true'>{cancelText}</button>" +
                                        "</div>",
                    showOkButton: true,
                    okText: "<i class='fa fa-save'></i>&nbsp;&nbsp;&nbsp;Save",
                    cancelText: appStrings.buttons.cancel,
                    validate: true,
                    validateAsTabs: true,
                    validationRules: {
                        commonAddPartnerDepartment: {
                                requiredSelect: true
                            },

                        commonAddPartnerRole: {
                                requiredSelect: true
                        },

                    }
                };
            }
                this.onPreShow = function (success, record, controller, dialogSelector, errorMessage) {
                    var skin = this;
                    this.data = record != null ? record : null;
                    this.isForNew = this.data == null || this.data.pkId == null || this.data.pkId <= 0;

                    if (this.data == null) {
                        this.data = {
                            pkId: -1
                        };
                    }
                    this.$area = $("#" + controller.containerId + "Area");
                    this.$wait = this.$area.find("#wait");
                    this.$error = controller.findUniqueControl("ErrorMsg");
                    this.$success = controller.findUniqueControl("SuccessMsg");
                    this.$ok = $("#" + controller.containerId + "Ok");
                    this.$content = controller.findUniqueControl("Content");
                    this.$name = controller.findUniqueControl("Name");
                    var $publishYes = $("#cYes");
                    $publishYes.addClass("active");
                    var $publishNo = $("#cNo");
                    var $grid = $("#Teams");

                    //Find tabs
                    this.$allTabs = this.$area.find(".tab-pane");
                    this.$generalTab = controller.findUniqueTab("General");
                    this.$teamsTab = controller.findUniqueTab("Teams");
                    this.$tabs = controller.findUniqueControl("Tabs");
                    this.$filesTabLink = this.$area.find("a[href='#" + controller.containerId + "TabFiles']").closest("li");

                    if (!success) {
                        var $controls = this.$area.find("[data-area='tabs']");
                        $controls.hide();
                        this.dialog.cancelBtn.html(appStrings.buttons.close);
                        this.dialog.cancelBtn.show();
                        this.$ok.hide();
                        return;
                    }

                    //account settings will retrieve the data from the controller
                    this.divisionList = (controller != null && controller.options != null && controller.options.divisions == null && record != null && record.divisionList != null) ? record.divisionList : null;
                    var $divisionPicker = controller.findUniqueControl("fkDivisionId");

                    $divisionPicker.corpDivisionSelectList({
                        data: (controller.options.divisions == null) ? this.divisionList : controller.options.divisions,
                        width: "100%",
                        allowUnassigned: true,
                        preselect: record.Contact.division,
                        onChange: function ($sel, $option, item) {
                            record.Contact.division = item.pkId;
                            controller.view.reset();
                        }
                    });
                    $divisionPicker.parent().show();

                    var $deptPicker = controller.findUniqueControl("fkDepartmentId");
                    var $rolePicker = controller.findUniqueControl("fkRoleId");
                    $deptPicker.corpDepartmentSelectList({
                        $buddy: $rolePicker,
                        buddyData: controller.options.roles,
                        data: controller.options.departments,
                        width: "100%",
                        preselect: record.Contact.fkDepartmentId,
                    });
                    $deptPicker.parent().show();

                    $rolePicker.corpRoleSelectList({
                        data: controller.options.roles,
                        width: "100%",
                        preselect: null,
                        onChange: function ($sel, $option, item) {
                        }
                    });
                    $rolePicker.parent().show();

                    $grid.addPartnerTeamsView({
                        queryUrlCallback: function (controller, index, sortFields, toolQueryValues) {
                            var params = $.extend({
                                id: record.Contact.division //this.$fkCorpDivisionId.pkId
                            }, toolQueryValues);
                            return "/api/CorpPerformance/GetTeamListing?" + $.param(params);
                        },
                    });
                    controller.view = $grid.data("multiview");

                  this.$area.bindToUI($.extend(true, {}, this.data.Contact));
                  $publishNo.removeClass("active");


                };

                this.onOk = function (controller) {
                    var skin = this;
                    var fields = this.$area.unbindFromUI();

                    var onSaveError = function (message) {
                        skin.$error.html(message);
                        skin.$error.show();
                    }

                    var $contactId = controller.findUniqueControl("fkContactId");
                    fields.fkContactId = fields.id;
                    fields.SelectedTeam = $("#Teams").attr("data-teamid");

                    var NewPartner = {
                        Partner: fields,
                        PublishCdd: fields.PublishCdd,
                        SelectedTeam: fields.SelectedTeam,
                        PerformanceCenter: fields.PerformanceCenter,
                        DivisionLocation: fields.DivisionLocation
                    }

                    $.ajax(
                    {
                        type: "PUT",
                        url: "/api/CorpPerformance/PutNewPartner",
                        data: JSON.stringify(NewPartner),
                        contentType: "application/json; charset=utf-8",
                        beforeSend: function () {
                            skin.$error.hide();
                            controller.showWaitIndicator(true);
                        },
                        complete: function () {
                            controller.showWaitIndicator(false);
                        },
                        success: function (data) {
                            try {
                                if (data != null && !data.Success)
                                    onSaveError(data.Message);
                                else {
                                    controller.okBtn.hide();
                                    controller.cancelBtn.html("Close");
                                    skin.$area.children().hide();
                                    skin.$success.html($.isNullOrEmpty(data.Message) ? "Partner has been added successfully." : data.Message);
                                    skin.$success.show();
                                }
                            }
                            catch (err) {
                                onSaveError(err);
                            }
                        },
                        error: function (xhr, status, error) {
                            var err = $.getAjaxResponseText(xhr, status, error);
                            var msg = err.Message;
                            if (!$.isNullOrEmpty(err.ExceptionMessage))
                                msg += "<div style='margin-top:15px;'><strong>Exception:</strong><p>" + err.ExceptionMessage + "</p></div>";
                            onSaveError(msg);
                        }
                    });
                    return { delayClose: true };

                };
            },

            showBrandInfo: function (brand, options) {

                var defaults = {
                    record: brand
                };
                var opts = $.extend(true, {}, defaults, options);

                $("#dialogBrandInfo").dialogBrandInfo(opts);
            },

            brandInfoDialogSkin: function () {

                this.data = null;

                this.getOptionOverrides = function () {
                    return {
                        title: "Brand Information",
                        description: "Please pick from the following section.",
                        HeaderTemplate: "<div class='row'>" +
                                                "<div class='col-md-12'>" +
                                                    "<img class='hidden-xs' src='/images/logos/pcorp.logo.med.norm.png' width='auto' height='auto' />" +
                                                    "<img class='hidden-sm hidden-md hidden-lg' src='/images/logos/pcorp.logo.xs.norm.png' width='auto' height='auto' />" +
                                                "</div>" +
                                            "</div>",
                        contentTemplate: "<div id='{containerId}Area' name='{containerId}Area'>" +
                                            "<div class='row'>" +
                                                "<div class='col-md-4' >" +
                                                    "<img src='#' class='img-responsive' style='display:none;' data-bind='true' data-field='imageurl'>" +
                                                "</div>" +
                                                "<div class='col-md-8'>" +
                                                    "<div class='row'>" +
                                                        "<div class='col-md-12'>" +
                                                            "<h2 class='margin-top-0' data-bind='true' data-field='name'></h2>" +
                                                        "</div>" +
                                                    "</div>" +
                                                    "<div class='row'>" +
                                                        "<div class='col-md-12'>" +
                                                            "<table class='table table-condensed'>" +
                                                                "<tr>" +
                                                                    "<td>Manufacturer Url:</td>" +
                                                                    "<td><a class='dashed' target='_ext' data-bind='true' data-field='manufacturerurl'><span data-bind='true' data-field='manufacturerurl'></span></a></td>" +
                                                                "</tr>" +
                                                                "<tr>" +
                                                                    "<td>Categories:</td>" +
                                                                    "<td id='{containerId}Cats'></td>" +
                                                                "</tr>" +
                                                                "<tr>" +
                                                                    "<td>Availability:</td>" +
                                                                    "<td id='{containerId}Avail'></td>" +
                                                                "</tr>" +
                                                                "<tr>" +
                                                                    "<td>Find a Salesman:</td>" +
                                                                    "<td id='{containerId}Sales'></td>" +
                                                                "</tr>" +
                                                                "<tr id='{containerId}Actions'>" +
                                                                    "<td>Actions:</td>" +
                                                                    "<td><a id='{containerId}Quote' class='btn btn-danger' href='#'><i class='fa fa-check-square'></i>&nbsp;&nbsp;&nbsp;Get a Quote...</a></td>" +
                                                                "</tr>" +
                                                            "</table>" +
                                                        "</div>" +
                                                    "</div>" +
                                                "</div>" +
                                            "</div>" +
                                            "<div class='row'>" +
                                                "<div class='col-md-12'>" +
                                                    "<p data-bind='true' data-field='description'></p>" +
                                                "</div>" +
                                            "</div>" +
                                         "</div>",
                        showOkButton: false,
                        cancelText: appStrings.buttons.close
                    };
                }

                // Called before the dialog is ready to show its data.
                this.onPreShow = function (success, record, controller, dialogSelector, errorMessage) {
                    var skin = this;
                    this.data = record;
                    this.$area = $("#" + controller.containerId + "Area");

                    this.$area.bindToUI(this.data);

                    this.$cats = controller.findUniqueControl("Cats");
                    var cats = "";
                    if (this.data.categories == null || this.data.categories.length == 0)
                        cats = "(none)";
                    else {
                        $.each(this.data.categories, function (i, category) {
                            var catname = null;
                            for (var j = 0; j < controller.options.categories.length; j++) {
                                if ($.findIdValue(controller.options.categories[j]) == category) {
                                    catname = $.findNameValue(controller.options.categories[j]);
                                    break;
                                }
                            }
                            if (!$.isNullOrEmpty(catname)) {
                                if (!$.isNullOrEmpty(cats))
                                    cats += "<br>";
                                cats += catname;
                            }
                        });
                    }

                    if ($.isNullOrEmpty(cats))
                        cats = "(none)";
                    this.$cats.html(cats);
                    this.cats = cats;

                    this.$sales = controller.findUniqueControl("Sales");
                    var sales = "";
                    if (this.data.availability == null || this.data.availability.length == 0)
                        sales = "(none)";
                    else {
                        $.each(this.data.availability, function (i, availability) {
                            if (!$.isNullOrEmpty(availability.name)) {
                                var css = "btn-primary";
                                var url = "/pages/public/dist/locate";
                                if (availability.pkId == 100) {
                                    url = "/pages/public/federal/locate";
                                    css = "btn-warning";
                                }
                                if (skin.data.name.toLowerCase().indexOf("haeger") >= 0)
                                    url = "/pages/public/manu/locator";

                                var salesBtn = $.stringFormat("<a href='{0}' class='btn {2}'><i class='fa fa-search'></i>&nbsp;&nbsp;&nbsp;{1}...</a>", url, availability.name, css);
                                sales += salesBtn;
                            }
                        });
                    }

                    if ($.isNullOrEmpty(sales))
                        sales = "(none)";
                    this.$sales.html(sales);

                    this.$avail = controller.findUniqueControl("Avail");
                    var avail = "";
                    if (this.data.divisions == null || this.data.divisions.length == 0)
                        avail = "(none)";
                    else {
                        var getDivision = function (id) {
                            if (controller.options.divisions == null || controller.options.divisions.length == 0)
                                return null;
                            for (var i = 0; i < controller.options.divisions.length; i++) {
                                if (controller.options.divisions[i].pkId == id)
                                    return controller.options.divisions[i];
                            }
                            return null;
                        };
                        $.each(this.data.divisions, function (i, divisionId) {
                            var division = getDivision(divisionId);
                            if (division != null && !$.isNullOrEmpty(division.name)) {
                                if (!$.isNullOrEmpty(avail))
                                    avail += ", ";
                                avail += division.name;
                            }
                        });
                    }

                    if ($.isNullOrEmpty(avail))
                        avail = "(none)";
                    this.$avail.html(avail);


                    var canQuote = true;
                    this.$actions = controller.findUniqueControl("Actions");
                    if (canQuote) {
                        this.$quote = controller.findUniqueControl("Quote");
                        this.$quote.on("click", function (e) {
                            e.preventDefault();
                            skin.onGetQuote(controller);
                        });
                    }
                    else
                        this.$actions.hide();
                };

                this.onGetQuote = function (controller) {
                    var skin = this;
                    controller.onAction(DIALOG_ACTION_HIDE);
                    $.pcorp.getQuote(
                        {
                            countries: controller.options.countries,
                            categories: controller.options.categories,
                            divisions: controller.options.divisions,
                            brands: controller.options.brands,
                            selected:
                            {
                                country: "US",
                                state: null,
                                postal: null,
                                items: [
                                    { brandId: skin.data.pkid, technology: skin.cats, model: null }
                                ]
                            }
                        }
                    );
                };

            },

            getQuote: function (options) {

                var defaults = {
                    countries: null,
                    categories: null,
                    divisions: null,
                    brands: null,
                    selected: {
                        country: null,
                        state: null,
                        postal: null,
                        items: null //{ brandId: brand.pkid, technology: category, model: null }
                    }
                };
                var opts = $.extend(true, {}, defaults, options);
                $(this).pushDialog(function (o) { $("#dialogQuote").dialogQuote(o); }, opts);

            },

            getQuoteDialogSkin: function () {

                this.data = null;
                this.getOptionOverrides = function () {
                    return {
                        title: "Build a Quote",
                        description: "Please use the form below to tell us a little about yourself and the products you are interested. We will collect this data and route it to the proper salesman who will generate your quote.",
                        HeaderTemplate: "<div class='dialogTitle row'>" +
                                                "<div class='col-md-1'><img src='/apple-touch-icon-60x60.png' /></div>" +
                                                "<div class='col-md-11'><h3 id='{containerId}currentDivision' class='margin-top-0'></h3> <h3>{title}</h3><h4>{description}<h4></div>" +

                                            "</div>",
                        contentTemplate: "<div id='{containerId}Area' name='{containerId}Area'>" +

                                            "<div id='{containerId}SuccessMsg' class='alert alert-success margin-bottom-10' role='alert' style='display:none;'></div>" +
                                            "<div id='{containerId}ErrorMsg' class='alert alert-danger margin-bottom-10' role='alert' style='display:none;'></div>" +

                                            "<ul class='nav nav-tabs' id='{containerId}Tabs'>" +
                                                "<li class='active'><a data-toggle='tab' href='#{containerId}Info'>Your Details</a></li>" +
                                                "<li ><a data-toggle='tab' href='#{containerId}Products'>Products To Quote</a></li>" +
                                            "</ul>" +
                                            "<div class='tab-content' style='padding-top:10px;overflow:visible;'>" +
                                                "<div class='tab-pane active' id='{containerId}Info' >" +
                                                    "<h4><strong>Sales/Service Location</strong></h4>" +
                                                    "<div class='row'>" +
                                                        "<div class='col-md-4' >" +
                                                            "<div class='form-group'>" +
                                                                "<label for='{containerId}CountryList'>Country</label>" +
                                                                "<select id='{containerId}CountryList' class='form-control selectpicker show-tick' data-style='btn-primary' data-bind='true' data-field='Country' required></select>" +
                                                            "</div>" +
                                                        "</div>" +
                                                        "<div class='col-md-4'>" +
                                                            "<label for='{containerId}Postal'>Zip/Postal Code</label>" +
                                                            "<div class='input-group'>" +
                                                                "<input  id='{containerId}Postal' name='{containerId}Postal' class='form-control' type='text' placeholder='postal code here' data-bind='true' data-field='Postal' >" +
                                                                "<span class='input-group-btn'><button class='btn btn-default' type='button'><i class='fa fa-search'></i>&nbsp;&nbsp;&nbsp;Search</button></span>" +
                                                            "</div>" +
                                                            "<span id='{containerId}PostalMsg' class='small'></span>" +
                                                        "</div>" +
                                                        "<div class='col-md-4'>" +
                                                            "<div class='form-group'>" +
                                                                "<label for='{containerId}StateList'>State/Province</label>" +
                                                                "<select id='{containerId}StateList' class='form-control selectpicker show-tick' data-style='btn-primary' data-bind='true' data-field='State'></select>" +
                                                                "<input  id='{containerId}State' name='{containerId}State' class='form-control' style='display:none;' type='text' placeholder='state/province' data-bind='true' data-field='StateName' >" +
                                                            "</div>" +
                                                        "</div>" +
                                                    "</div>" +
                                                    "<div class='row'>" +
                                                        "<div class='col-md-12'>" +
                                                            "<h4><strong>Contact Details</strong></h4>" +
                                                            "<div class='row'>" +
                                                                "<div class='col-md-4'>" +
                                                                    "<div class='form-group'>" +
                                                                        "<label for='{containerId}Company'>Company</label>" +
                                                                        "<input  id='{containerId}Company' name='{containerId}Company' class='form-control' type='text' placeholder='company name' data-bind='true' data-field='Company' >" +
                                                                    "</div>" +
                                                                "</div>" +
                                                                "<div class='col-md-4'>" +
                                                                    "<div class='form-group'>" +
                                                                        "<label for='{containerId}NameFirst'>First Name</label>" +
                                                                        "<input  id='{containerId}NameFirst' name='{containerId}NameFirst' class='form-control' type='text' placeholder='first name' data-bind='true' data-field='NameFirst' >" +
                                                                    "</div>" +
                                                                "</div>" +
                                                                "<div class='col-md-4'>" +
                                                                    "<div class='form-group'>" +
                                                                        "<label for='{containerId}NameLast'>Last Name</label>" +
                                                                        "<input  id='{containerId}NameLast' name='{containerId}NameLast' class='form-control' type='text' placeholder='last name' data-bind='true' data-field='NameLast' >" +
                                                                    "</div>" +
                                                                "</div>" +
                                                            "</div>" +
                                                            "<div class='row'>" +
                                                                "<div class='col-md-4'>" +
                                                                    "<div class='form-group'>" +
                                                                        "<label for='{containerId}Phone'>Phone</label>" +
                                                                        "<input  id='{containerId}Phone' name='{containerId}Phone' class='form-control' type='text' placeholder='phone' data-bind='true' data-field='Phone' >" +
                                                                    "</div>" +
                                                                "</div>" +
                                                                "<div class='col-md-8'>" +
                                                                    "<div class='form-group'>" +
                                                                        "<label for='{containerId}Email'>Email</label>" +
                                                                        "<input  id='{containerId}Email' name='{containerId}Email' class='form-control' type='text' placeholder='someone@somewhere.com' data-bind='true' data-field='Email'>" +
                                                                    "</div>" +
                                                                "</div>" +
                                                            "</div>" +
                                                            "<div class='row'>" +
                                                                "<div class='col-md-4'>" +
                                                                    "<div class='form-group'>" +
                                                                        "<label for='{containerId}ContactTypes'>Contact Me</label><br>" +
                                                                        "<select id='{containerId}ContactTypes' class='form-control selectpicker show-tick' data-style='btn-primary' data-bind='true' data-field='ContactMethods' multiple></select>" +
                                                                    "</div>" +
                                                                "</div>" +
                                                                "<div class='col-md-4'>" +
                                                                    "<div class='form-group'>" +
                                                                        "<label for='{containerId}ContactTime'>Best Time to Contact Me</label><br>" +
                                                                        "<select id='{containerId}ContactTime' class='form-control selectpicker show-tick' data-style='btn-primary' data-bind='true' data-field='ContactTime'></select>" +
                                                                    "</div>" +
                                                                "</div>" +
                                                                "<div class='col-md-4'>" +
                                                                "</div>" +
                                                            "</div>" +
                                                        "</div>" +
                                                    "</div>" +
                                                "</div>" +
                                                // Products
                                                 "<div class='tab-pane' id='{containerId}Products' >" +

                                                    "<div class='form-group'>" +
                                                        "<label for='{containerId}SalesType'>Sales Type</label><br>" +
                                                        "<select id='{containerId}SalesType' name='{containerId}SalesType' class='form-control selectpicker show-tick' data-style='btn-primary' data-bind='true' data-field='DivisionType'></select>" +
                                                    "</div>" +

                                                    "<div class='table-search-v1 panel panel-grey'>" +
                                                        "<div class='panel-heading clearfix' style='padding-right: 5px;'>" +
                                                            "<h3 class='panel-title pull-left'><i class='fa fa-tasks fa-lg'></i>&nbsp;I am interested in the following products...</h3>" +
                                                            "<div class='pull-right'>" +
                                                                "<button id='{containerId}NewMachine' class='btn btn-sm btn-success pull-right' style='display:none;' type='button'><i class='fa fa-plus-circle fa-lg'></i>&nbsp;New Task</button>" +
                                                            "</div>" +
                                                        "</div>" +
                                                        "<div class='table-responsive' style='max-height: 400px;overflow-y:auto;'>" +
                                                            "<table class='table table-bordered table-striped dataFormatted'>" +
                                                                "<thead>" +
                                                                    "<tr>" +
                                                                        "<th></th>" +
                                                                        "<th>Manufacturer</th>" +
                                                                        "<th>Technology</th>" +
                                                                        "<th>Machine Model</th>" +
                                                                        "<th>Serial Number</th>" +
                                                                    "</tr>" +
                                                                "</thead>" +
                                                                "<tbody id='{containerId}ProductBody'>" +
                                                                    "<tr><td><button class='btn btn-block btn-success' data-action='new' type='button'><i class='fa fa-plus-circle'></i>&nbsp;&nbsp;add</button></td><td colspan='3' style='width:100%;'></td></tr>" +
                                                                "</tbody>" +
                                                                "<tbody id='{containerId}ItemReadOnlyBody'>" +
                                                                     "<tbody id='{containerId}ReadOnlyTable'></tbody>" +
                                                                "</tbody>" +
                                                            "</table>" +
                                                        "</div>" +
                                                    "</div>" +


                                                "</div>" +
                                           "</div>" +
                                         "</div>",
                        showOkButton: true,
                        okText: "<i class='fa fa-send'></i>&nbsp;&nbsp;&nbsp;Send My Quote Request!",
                        cancelText: appStrings.buttons.cancel,
                        validate: true,
                        validateAsTabs: true,
                        validationRules: {
                            dialogQuoteCountry: {
                                requiredSelect: true
                            },
                            dialogQuoteCompany: {
                                required: true,
                                maxlength: 500
                            },
                            dialogQuoteNameFirst: {
                                required: true,
                                maxlength: 255
                            },
                            dialogQuoteNameLast: {
                                required: true,
                                maxlength: 255
                            },
                            dialogQuotePhone: {
                                required: true,
                                maxlength: 255
                            },
                            dialogQuoteEmail: {
                                required: true,
                                maxlength: 1000,
                                email: true
                            },
                            dialogQuoteStateList: {
                                required: true
                            },
                            dialogQuoteSalesType: {
                                requiredSelect: true
                            }
                        },
                    };
                }
                this.getDynamicValidationMessages = function (controller) {
                    return {
                    };
                }

                // Called before the dialog is ready to show its data.
                this.onPreShow = function (success, record, controller, dialogSelector, errorMessage) {
                    var skin = this;

                    //if the record is null then this being called from web request, so this will be pre-populated with readonly data
                    var readonly = (controller.options != null && controller.options.data && controller.options.data.IsReadonly && controller.options.data.IsReadonly == true) ? true : false;
                    if (record == null) {
                        if (controller.options) {
                            if (controller.options.data) {
                                record = controller.options.data;
                                controller.options.isReadOnly = true;
                            }
                        }
                    }

                    this.data = record;
                    this.$area = $("#" + controller.containerId + "Area");
                    this.$wait = this.$area.find("#wait");
                    this.$error = controller.findUniqueControl("ErrorMsg");
                    this.$success = controller.findUniqueControl("SuccessMsg");
                    this.$currentDivision = controller.findUniqueControl("currentDivision");
                    controller.okBtn.removeClass("btn-primary").addClass("btn-success");

                    this.brands = controller.options.brands;
                    this.categories = controller.options.categories;


                    this.$countries = controller.findUniqueControl("CountryList");
                    this.$territories = controller.findUniqueControl("StateList");
                    this.$territoryName = controller.findUniqueControl("State");
                    this.$postal = controller.findUniqueControl("Postal");
                    this.$postalSearch = this.$postal.next().find("button");
                    this.$postalMsg = controller.findUniqueControl("PostalMsg");

                    if (controller.options.selected.currentDivisionId != null) {
                        this.$currentDivision.text(controller.options.divisions[controller.options.selected.currentDivisionId].name);
                    }
                    if (controller.options.selected) {
                        if (controller.options.selected.postal == null) {
                            controller.options.selected.postal = (this.data != null && this.data.Postal != null) ? this.data.Postal : "";
                        }
                        this.$postal.val(controller.options.selected.postal);
                    }

                    this.$postal.off("keypress").on("keypress", function (e) {
                        if (e.keyCode == 13) {
                            skin.onSearchZip();
                            return false;
                        }
                    });
                    this.$postalSearch.on("click", function (e) {
                        e.preventDefault();
                        skin.onSearchZip();
                    });

                    //may be pre-selected
                    if (controller.options.selected) {
                        if (controller.options.selected.state == null) {
                            controller.options.selected.state = (this.data != null && this.data.State != null) ? this.data.State : null;
                        }
                    }

                    skin.$territories.territoryList({
                        preselect: controller.options.selected.state,
                        data: [],
                    });

                    this.countries = controller.options.countries;
                    this.$countries.countryList({
                        preselect: controller.options.selected.country,
                        data: this.countries,
                        onChange: function ($sel, $option, item) {
                            setTimeout(function () {
                                $.when(skin.loadTerritories(item.Code)).always(function (data) {
                                    skin.$territoryName.val("");
                                    skin.territories = data.Territories;
                                    skin.territories.splice(0, 0, { Code: null, pkId: null, Name: "Please select...", fkCountryCode: item.Code });
                                    if (data == null || data.Success == false || data.Territories == null || data.Territories.length == 0) {
                                        skin.$territories.selectpicker("hide");
                                        skin.$territoryName.show();

                                    }
                                    else {
                                        skin.$territoryName.hide();

                                        skin.$territories.territoryList({
                                            preselect: controller.options.selected.state,
                                            data: skin.territories,
                                            onChange: function ($sel, $option, item) {
                                            }
                                        });
                                        skin.$territories.selectpicker("show");
                                        skin.$territories.selectpicker("refresh");
                                        skin.firstCountryChange = false;
                                    }
                                });
                            }, 100);
                        }
                    });


                    this.$salesType = controller.findUniqueControl("SalesType");
                    this.$salesType.divisionSalesTypeList({
                        preselect: controller.options.selected.divisionSalesType,
                    });
                    //may be pre-selected. This is not returning the hard-coded value
                    this.$salesType = controller.findUniqueControl("SalesType");
                    //this.$salesType.returnSalesTypeList();
                    if (this.data != null && this.data.SalesType && this.data.SalesType.name != null) {
                        var salesTypeValue = this.$salesType.find($.stringFormat("option:contains('{0}')", this.data.SalesType.name)).val();
                        this.$salesType.selectpicker("val", salesTypeValue);
                    }


                    //This drop-down does not take multiple values so take first.
                    var vals = (this.data != null && this.data.ContactMethods != null) ? this.data.ContactMethods.split(",") : null;
                    var contactMethods = vals!=null && vals.length > 1 ? vals[0] : $.isNullOrEmpty(vals) ? "-1" : vals;
                    if (controller.options.selected) {
                        if (controller.options.selected.contactMe == null) {
                            controller.options.selected.contactMe = (this.data != null && this.data.ContactMethods != null) ? this.data.ContactMethods : [-1];
                        }
                    }

                    this.$contactMethod = controller.findUniqueControl("ContactTypes");
                    this.$contactMethod.contactMeMethodList({
                        preselect: controller.options.selected.contactMe,
                    });

                    this.$contactTime = controller.findUniqueControl("ContactTime");
                    this.$contactTime.contactMeTimeList({
                        //preselect: [],
                    });

                    if (readonly && readonly == true) {
                        this.dialog.okBtn.remove();
                        this.dialog.cancelBtn.html(appStrings.buttons.close);
                    }

                    this.$area.bindToUI(this.data);
                    this.firstCountryChange = true;
                    this.$countries.trigger("change");

                    this.$newMachine = controller.findUniqueControl("NewMachine");
                    this.$machineBody = controller.findUniqueControl("ProductBody");
                    this.$itemReadonlyBody = controller.findUniqueControl("ItemReadOnlyBody");



                    this.$newMachine = this.$machineBody.find("[data-action='new']");
                    if (readonly == false) {
                        this.$itemReadonlyBody.remove();
                        this.$newMachine.on("click", function (e) {
                            e.preventDefault();
                            skin.onAddMachine(skin.$machineBody);
                        });

                        if (controller.options.selected == null || controller.options.selected.items == null || controller.options.selected.items.length == 0)
                            this.onAddMachine(this.$machineBody);
                        else {
                            $.each(controller.options.selected.items, function (i, item) {
                                skin.onAddMachine(skin.$machineBody, {
                                    manufacturer: item.brandId,
                                    technology: item.technology,
                                    model: item.model
                                });
                            });
                        }

                        this.$itemReadonlyBody.remove();
                    }
                    else {
                        this.$machineBody.remove();
                        this.$itemReadonlyBody = controller.findUniqueControl("ItemReadOnlyBody");
                        this.$itemReadonlyBodyQuantity = controller.findUniqueControl("ReadOnlyTable");

                        var myHtml = "";
                        if (this.data && this.data.Products) {
                            $.each(this.data.Products, function (i, product) {
                                myHtml += $.stringFormat("<tr><td>&nbsp;</td><td>{0}</td><td>{1}</td><td>{2}</td><td>{3}</td></tr>", product.Manufacturer, product.Model, product.Technology, product.SerialNumber);
                            });
                        }
                        else
                            var myHtml = "<tr><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>";

                        this.$itemReadonlyBodyQuantity.html(myHtml);
                    }
                };

                this.onShown = function (controller) {
                    var isReadOnly = controller.options.isReadOnly == true;
                    // update for readonly
                    if (isReadOnly)
                        this.$area.disableElements();
                }

                this.onUpdateButtons = function ($cont) {
                    var $rows = $cont.find("tr");
                    var $newRow = this.$newMachine.closest("tr");
                    if ($rows.length >= 7) {
                        $newRow.hide();
                    }
                    else {
                        $newRow.show();
                    }
                };

                this.onAddMachine = function ($cont, options) {
                    var skin = this;

                    var defaults = {
                        manufacturer: null,
                        technology: "",
                        model: "",
                        serial: "",
                        template: "<tr>" +
                                        "<td class='animated fadeIn'><button class='btn btn-block btn-danger' data-action='delete' type='button'><i class='fa fa-remove'></i>&nbsp;&nbsp;remove</button></td>" +
                                        "<td class='animated fadeIn' style='width:25%;'><a href='#' data-pk='{index}' id='manufacturer' data-type='select' data-title='Enter Manufacturer Name' data-bind='true' data-field='Manufacturer'></a></td>" +
                                        "<td class='animated fadeIn' style='width:25%;'><a href='#' data-pk='{index}' id='technology' data-type='text' data-title='Enter Technology Name' data-bind='true' data-field='Technology'>{technology}</a></td>" +
                                        "<td class='animated fadeIn' style='width:25%;'><a href='#' data-pk='{index}' id='model' data-type='text' data-title='Enter Model Name' data-bind='true' data-field='Model'>{model}</a></td>" +
                                        "<td class='animated fadeIn' style='width:25%;'><a href='#' data-pk='{index}' id='serial' data-type='text' data-title='Enter Serial Number' data-bind='true' data-field='SerialNumber'>{serial}</a></td>" +
                                    "</tr>"
                    };
                    var opts = $.extend(true, {}, defaults, options);

                    if (this.manuChoices == null) {
                        this.manuChoices = [];
                        $.each(this.brands, function (i, brand) {
                            skin.manuChoices.push({
                                value: brand.pkid,
                                text: brand.name
                            });
                        });
                    }

                    var $allrows = $cont.find("tr");

                    var $row = $($.expandTemplateFromObject(opts.template, $.extend({}, opts, {
                        index: $allrows.length,
                    })));

                    var $del = $row.find("[data-action='delete']");
                    $del.on("click", function (e) {
                        e.preventDefault();
                        var $delrow = $(this).closest("tr");
                        $delrow.remove();
                        skin.onUpdateButtons($cont);
                    });
                    var $last = $cont.children().last("tr");
                    if (!$last.exists())
                        $cont.append($row);
                    else
                        $last.before($row);



                    try {
                        if ($row.find("#manufacturer").editable != null) {
                            $row.find("#manufacturer").editable({
                                value: opts.manufacturer,
                                emptytext: "click to select",
                                mode: "inline",
                                source: this.manuChoices,
                                onblur: "submit",
                                validate: function (value) {
                                    if ($.trim(value) == 0)
                                        return "The manufacturer is required";
                                }
                            });
                        }

                        if ($row.find("#technology").editable != null) {
                            $row.find("#technology").editable({
                                emptytext: "click to edit",
                                mode: "inline",
                                onblur: "submit"
                            });
                        }

                        if ($row.find("#model").editable != null) {
                            $row.find("#model").editable({
                                emptytext: "click to edit",
                                mode: "inline",
                                onblur: "submit"
                            });
                        }

                        if ($row.find("#serial").editable != null) {
                            $row.find("#serial").editable({
                                emptytext: "click to edit",
                                mode: "inline",
                                onblur: "submit"
                            });
                        }
                    } catch (exception) {
                        //there was an error
                    }

                    skin.onUpdateButtons($cont);
                };

                this.onSearchZip = function () {
                    var skin = this;

                    $.when(this.doPostalSearch(skin.$postal.val())).always(function (data) {
                        if (data == null || data.Success == false || data.Matches == null || data.Matches.length <= 0) {
                            skin.$postalMsg.addClass("error");
                            skin.$postalMsg.html("<p>Could not match postal code.</p>");
                            skin.$postalMsg.show();
                            return;
                        }

                        skin.postalMatch = data.Matches[0];
                        skin.$territories.selectpicker("val", skin.postalMatch.State);
                        skin.$postalMsg.removeClass("error");
                        skin.$territoryName.val(skin.postalMatch.StateName);
                        skin.$postalMsg.html($.stringFormat("<strong>Match:</strong> {0}, {1}", skin.postalMatch.City, skin.postalMatch.StateName));
                        skin.$postalMsg.show();

                    });
                };

                this.doPostalSearch = function (postal, options) {
                    var skin = this;
                    if ($.isNullOrEmpty(postal)) {
                        skin.$postalMsg.html("<p>Please enter a postal code.</p>");
                        skin.$postalMsg.addClass("error");
                        skin.$postalMsg.show();
                    }

                    var defaults = {
                        Query: postal,
                        Country: skin.$countries.val(),
                    };

                    var opts = $.extend({}, defaults, options);

                    var apiUrl = '/api/Corp/GetRegionFromQuery'
                    return $.ajax(
                    {
                        type: 'GET',
                        url: apiUrl + '?' + $.param(opts),
                        contentType: 'application/json; charset=utf-8',
                        beforeSend: function () {
                            skin.$area.tempDisable();
                        },
                        complete: function (data) {
                            skin.$area.tempDisable('release');
                        }
                    });

                };

                this.loadTerritories = function (country, options) {

                    var skin = this;

                    var defaults = {
                        Country: country,
                    };
                    var opts = $.extend({}, defaults, options);
                    var $btn = skin.$territories.next().find("button.selectpicker");

                    var apiUrl = '/api/Public/GetTerritories'
                    return $.ajax(
                    {
                        type: 'GET',
                        url: apiUrl + '?' + $.param(opts),
                        contentType: 'application/json; charset=utf-8',
                        beforeSend: function () {
                            skin.$area.tempDisable();
                        },
                        complete: function (data) {
                            skin.$area.tempDisable('release');
                        }
                    });
                };

                this.onOk = function (controller) {
                    var skin = this;

                    var products = skin.$machineBody.unbindFromUI("xeditable");

                    var data = $.extend({}, {
                        City: (skin.postalMatch != null ? skin.postalMatch.City : null),
                        Products: products,
                        divisionId: controller.options.selected.currentDivisionId

                    }, this.$area.unbindFromUI());

                    var onSaveError = function (message) {
                        skin.$error.html(message);
                        skin.$error.show();
                    }

                    var apiUrl = "/api/Corp/PutQuote";
                    $.ajax(
                    {
                        type: "PUT",
                        url: apiUrl,
                        data: JSON.stringify(data),
                        contentType: "application/json; charset=utf-8",
                        beforeSend: function () {
                            skin.$error.hide();
                            controller.showWaitIndicator(true);
                        },
                        complete: function () {
                            controller.showWaitIndicator(false);
                        },
                        success: function (data) {
                            try {
                                if (data != null && !data.Success)
                                    onSaveError(data.Message);
                                else {
                                    controller.okBtn.hide();
                                    controller.cancelBtn.html("Close");
                                    skin.$area.children().hide();
                                    skin.$success.html($.isNullOrEmpty(data.Message) ? "Your request was submitted successfully." : data.Message);
                                    skin.$success.show();
                                }
                            }
                            catch (err) {
                                onSaveError(err);
                            }
                        },
                        error: function (xhr, status, error) {
                            var err = $.getAjaxResponseText(xhr, status, error);
                            var msg = err.Message;
                            if (!$.isNullOrEmpty(err.ExceptionMessage))
                                msg += "<div style='margin-top:15px;'><strong>Exception:</strong><p>" + err.ExceptionMessage + "</p></div>";
                            onSaveError(msg);
                        }
                    });
                    return { delayClose: true };
                }

            },


            serviceRequest: function (options) {

                var isReadOnly = (options != null && options.data != null && options.data.IsReadonly) ? options.data.IsReadonly : false;

                var defaults = {
                    readonly: isReadOnly,
                    countries: null,
                    categories: null,
                    divisions: null,
                    brands: null,
                    selected: {
                        country: null,
                        state: null,
                        postal: null,
                        items: null //{ brandId: brand.pkid, technology: category, model: null }
                    }
                };
                var opts = $.extend(true, {}, defaults, options);
                $(this).pushDialog(function (o) { $("#serviceRequest").dialogServiceRequest(o); }, opts);

            },

            serviceRequestDialogSkin: function () {

                this.data = null;

                this.getOptionOverrides = function () {
                    return {
                        title: "Service/Sales Request Form",
                        description: "Please use the form below to tell us a little about yourself and the reasons for the requested visit.",
                        HeaderTemplate: "<div class='dialogTitle row'>" +
                                                "<div class='col-md-1'><img src='/apple-touch-icon-60x60.png' /></div>" +
                                                "<div class='col-md-11'><h3>{title}</h3><h4>{description}<h4></div>" +
                                            "</div>",
                        contentTemplate: "<div id='{containerId}Area' name='{containerId}Area'>" +
                                            "<div id='{containerId}SuccessMsg' class='alert alert-success margin-bottom-10' role='alert' style='display:none;'></div>" +
                                            "<div id='{containerId}ErrorMsg' class='alert alert-danger margin-bottom-10' role='alert' style='display:none;'></div>" +
                                            "<ul class='nav nav-tabs' id='{containerId}Tabs'>" +
                                                "<li class='active'><a data-toggle='tab' href='#{containerId}Info'>Your Details</a></li>" +
                                                "<li ><a data-toggle='tab' href='#{containerId}Visit'>Visit Details</a></li>" +
                                            "</ul>" +
                                            "<div class='tab-content' style='padding-top:10px;overflow:visible;'>" +
                                                // Info
                                                "<div class='tab-pane active' id='{containerId}Info' >" +
                                                    "<h4><strong>Sales/Service Location</strong></h4>" +
                                                    "<div class='row'>" +
                                                        "<div class='col-md-4' >" +
                                                            "<div class='form-group'>" +
                                                                "<label for='{containerId}CountryList'>Country</label>" +
                                                                "<select id='{containerId}CountryList' class='form-control selectpicker show-tick' data-style='btn-primary' data-bind='true' data-field='Country' required></select>" +
                                                            "</div>" +
                                                        "</div>" +
                                                        "<div class='col-md-4'>" +
                                                            "<label for='{containerId}Postal'>Zip/Postal Code</label>" +
                                                            "<div class='input-group'>" +
                                                                "<input  id='{containerId}Postal' name='{containerId}Postal' class='form-control' type='text' placeholder='postal code here' data-bind='true' data-field='Postal' >" +
                                                                "<span class='input-group-btn'><button class='btn btn-default' type='button'><i class='fa fa-search'></i>&nbsp;&nbsp;&nbsp;Search</button></span>" +
                                                            "</div>" +
                                                            "<span id='{containerId}PostalMsg' class='small'></span>" +
                                                        "</div>" +
                                                        "<div class='col-md-4'>" +
                                                            "<div class='form-group'>" +
                                                                "<label for='{containerId}StateList'>State/Province</label>" +
                                                                "<select id='{containerId}StateList' class='form-control selectpicker show-tick' data-style='btn-primary' data-bind='true' data-field='State'></select>" +
                                                                "<input  id='{containerId}State' name='{containerId}State' class='form-control' style='display:none;' type='text' placeholder='state/province' data-bind='true' data-field='StateName' >" +
                                                            "</div>" +
                                                        "</div>" +
                                                    "</div>" +
                                                    "<div class='row'>" +
                                                        "<div class='col-md-12'>" +
                                                            "<h4><strong>Contact Details</strong></h4>" +
                                                            "<div class='row'>" +
                                                                "<div class='col-md-4'>" +
                                                                    "<div class='form-group'>" +
                                                                        "<label for='{containerId}Company'>Company</label>" +
                                                                        "<input  id='{containerId}Company' name='{containerId}Company' class='form-control' type='text' placeholder='company name' data-bind='true' data-field='Company' >" +
                                                                    "</div>" +
                                                                "</div>" +
                                                                "<div class='col-md-4'>" +
                                                                    "<div class='form-group'>" +
                                                                        "<label for='{containerId}NameFirst'>First Name</label>" +
                                                                        "<input  id='{containerId}NameFirst' name='{containerId}NameFirst' class='form-control' type='text' placeholder='first name' data-bind='true' data-field='NameFirst' >" +
                                                                    "</div>" +
                                                                "</div>" +
                                                                "<div class='col-md-4'>" +
                                                                    "<div class='form-group'>" +
                                                                        "<label for='{containerId}NameLast'>Last Name</label>" +
                                                                        "<input  id='{containerId}NameLast' name='{containerId}NameLast' class='form-control' type='text' placeholder='last name' data-bind='true' data-field='NameLast' >" +
                                                                    "</div>" +
                                                                "</div>" +
                                                            "</div>" +
                                                            "<div class='row'>" +
                                                                "<div class='col-md-4'>" +
                                                                    "<div class='form-group'>" +
                                                                        "<label for='{containerId}Phone'>Phone</label>" +
                                                                        "<input  id='{containerId}Phone' name='{containerId}Phone' class='form-control' type='text' placeholder='phone' data-bind='true' data-field='Phone' >" +
                                                                    "</div>" +
                                                                "</div>" +
                                                                "<div class='col-md-8'>" +
                                                                    "<div class='form-group'>" +
                                                                        "<label for='{containerId}Email'>Email</label>" +
                                                                        "<input  id='{containerId}Email' name='{containerId}Email' class='form-control' type='text' placeholder='someone@somewhere.com' data-bind='true' data-field='Email'>" +
                                                                    "</div>" +
                                                                "</div>" +
                                                            "</div>" +
                                                            "<div class='row'>" +
                                                                "<div class='col-md-4'>" +
                                                                    "<div class='form-group'>" +
                                                                        "<label for='{containerId}ContactTypes'>Contact Me</label><br>" +
                                                                        "<select id='{containerId}ContactTypes' class='form-control selectpicker show-tick' data-style='btn-primary' data-bind='true' data-field='ContactMethods' multiple></select>" +
                                                                    "</div>" +
                                                                "</div>" +
                                                                "<div class='col-md-4'>" +
                                                                    "<div class='form-group'>" +
                                                                        "<label for='{containerId}ContactTime'>Best Time to Contact Me</label><br>" +
                                                                        "<select id='{containerId}ContactTime' class='form-control selectpicker show-tick' data-style='btn-primary' data-bind='true' data-field='ContactTime'></select>" +
                                                                    "</div>" +
                                                                "</div>" +
                                                                "<div class='col-md-4'>" +
                                                                "</div>" +
                                                            "</div>" +
                                                        "</div>" +
                                                    "</div>" +
                                                "</div>" +
                                                // Visit
                                                 "<div class='tab-pane' id='{containerId}Visit' >" +

                                                    "<div class='row'>" +
                                                        "<div class='col-md-12'>" +
                                                            "<div class='form-group'>" +
                                                                "<label for='{containerId}VisitType'>Type of Visit</label><br>" +
                                                                "<select id='{containerId}VisitType' name='{containerId}VisitType' class='form-control selectpicker show-tick' data-style='btn-primary' data-bind='true' data-field='VisitType' multiple></select>" +
                                                            "</div>" +
                                                        "</div>" +
                                                    "</div>" +

                                                    "<div class='row'>" +
                                                        "<div class='col-md-12'>" +
                                                            "<div class='form-group'>" +
                                                                "<label for='{containerId}Comments'>Comments  <small class='text-muted'>Please include dates/times/reasons for the visit.<small></label>" +
                                                                "<textarea  id='{containerId}Comments' name='{containerId}Comments' rows=10 class='form-control' type='text' data-bind='true' data-field='Comments'></textarea>" +
                                                            "</div>" +
                                                        "</div>" +
                                                    "</div>" +

                                                "</div>" +
                                           "</div>" +
                                         "</div>",
                        showOkButton: true,
                        okText: "<i class='fa fa-send'></i>&nbsp;&nbsp;&nbsp;Send My Request!",
                        cancelText: appStrings.buttons.cancel,
                        validate: true,
                        validateAsTabs: true,
                        validationRules: {
                            serviceRequestCompany: {
                                required: true,
                                maxlength: 500
                            },
                            serviceRequestNameFirst: {
                                required: true,
                                maxlength: 255
                            },
                            serviceRequestNameLast: {
                                required: true,
                                maxlength: 255
                            },
                            serviceRequestPhone: {
                                required: true,
                                maxlength: 255
                            },
                            serviceRequestEmail: {
                                required: true,
                                maxlength: 1000,
                                email: true
                            },
                            serviceRequestStateList: {
                                required: true
                            },
                            serviceRequestVisitType: {
                                required: true
                            },
                            serviceRequestComments: {
                                required: true,
                                maxlength: 2000,
                            },
                            serviceRequestCountry: {
                                requiredSelect: true
                            },
                            serviceRequestVisitType: {
                                requiredSelect: true
                            }
                        },
                    };
                }
                this.getDynamicValidationMessages = function (controller) {
                    return {
                    };
                }

                // Called before the dialog is ready to show its data.
                this.onPreShow = function (success, record, controller, dialogSelector, errorMessage) {
                    var skin = this;

                    //if the record is null then this being called from web request, so this will be pre-populated with readonly data
                    var readonly = (controller.options != null && controller.options.data && controller.options.data.IsReadonly && controller.options.data.IsReadonly == true) ? true : false;
                    if (record == null) {
                        if (controller.options) {
                            if (controller.options.data) {
                                record = controller.options.data;
                                controller.options.isReadOnly = true;
                            }
                        }
                    }

                    this.data = record;
                    this.$area = $("#" + controller.containerId + "Area");
                    this.$wait = this.$area.find("#wait");
                    this.$error = controller.findUniqueControl("ErrorMsg");
                    this.$success = controller.findUniqueControl("SuccessMsg");

                    controller.okBtn.removeClass("btn-primary").addClass("btn-success");

                    this.categories = controller.options.categories;

                    this.$countries = controller.findUniqueControl("CountryList");
                    this.$territories = controller.findUniqueControl("StateList");
                    this.$territoryName = controller.findUniqueControl("State");
                    this.$postal = controller.findUniqueControl("Postal");
                    this.$postalSearch = this.$postal.next().find("button");
                    this.$postalMsg = controller.findUniqueControl("PostalMsg");


                    if (controller.options.selected) {
                        if (controller.options.selected.postal == null) {
                            controller.options.selected.postal = (this.data != null && this.data.Postal!=null) ? this.data.Postal : "";
                        }
                        this.$postal.val(controller.options.selected.postal);
                    }

                    this.$postal.off("keypress").on("keypress", function (e) {
                        if (e.keyCode == 13) {
                            skin.onSearchZip();
                            return false;
                        }
                    });
                    this.$postalSearch.on("click", function (e) {
                        e.preventDefault();
                        skin.onSearchZip();
                    });

                    //The visit type may be pre-selected
                    if (controller.options.selected) {
                        if (controller.options.selected.state == null) {
                            controller.options.selected.state = (this.data != null && this.data.State != null) ? this.data.State : null;
                        }
                    }


                    skin.$territories.territoryList({
                        preselect: controller.options.selected.state,
                        data: [],
                    });

                    //The country may be pre-selected
                    if (controller.options.selected) {
                        if (controller.options.selected.country == null) {
                            controller.options.selected.country = (this.data != null && this.data.Country != null) ? this.data.Country : null;
                        }
                    }

                    this.countries = controller.options.countries;
                    this.$countries.countryList({
                        preselect: controller.options.selected.country,
                        data: this.countries,
                        onChange: function ($sel, $option, item) {
                            setTimeout(function () {
                                $.when(skin.loadTerritories(item.Code)).always(function (data) {
                                    skin.$territoryName.val("");
                                    skin.territories = data.Territories;
                                    skin.territories.splice(0, 0, { Code: null, pkId: null, Name: "Please select...", fkCountryCode: item.Code });
                                    if (data == null || data.Success == false || data.Territories == null || data.Territories.length == 0) {
                                        skin.$territories.selectpicker("hide");
                                        skin.$territoryName.show();

                                    }
                                    else {
                                        skin.$territoryName.hide();

                                        skin.$territories.territoryList({
                                            preselect: controller.options.selected.state,
                                            data: skin.territories,
                                            onChange: function ($sel, $option, item) {
                                            }
                                        });
                                        skin.$territories.selectpicker("refresh");
                                        skin.firstCountryChange = false;
                                    }
                                });
                            }, 100);
                        }
                    });


                    //The contact me may be pre-selected
                    if (controller.options.selected) {
                        if (controller.options.selected.contactMe == null) {
                            controller.options.selected.contactMe = (this.data != null && this.data.ContactMethods != null) ? this.data.ContactMethods : [-1];
                        }
                    }

                    this.$contactMethod = controller.findUniqueControl("ContactTypes");
                    this.$contactMethod.contactMeMethodList({
                        preselect: controller.options.selected.contactMe,
                    });

                    this.$contactTime = controller.findUniqueControl("ContactTime");
                    this.$contactTime.contactMeTimeList({
                    });


                    //The visit type may be pre-selected
                    var visitTypeSelected = [-1];
                    if (controller.options.selected) {
                        if (controller.options.selected.visitType == null) {
                            controller.options.selected.visitType = (this.data != null && this.data.VisitType != null) ? this.data.VisitType : [-1];
                        }
                    }

                    this.$visitType = controller.findUniqueControl("VisitType");
                    this.$visitType.visitTypeList({
                        preselect: controller.options.selected.visitType,
                    });

                    if (readonly && readonly == true)
                    {
                        this.dialog.okBtn.remove();
                        this.dialog.cancelBtn.html(appStrings.buttons.close);
                    }

                    this.$area.bindToUI(this.data);
                    this.firstCountryChange = true;
                    this.$countries.trigger("change");

                };

                this.onShown = function (controller) {
                    var isReadOnly = controller.options.isReadOnly == true;
                    // update for readonly
                    if (isReadOnly)
                        this.$area.find(".tab-content").disableElements();
                }

                this.onSearchZip = function () {
                    var skin = this;

                    $.when(this.doPostalSearch(skin.$postal.val())).always(function (data) {
                        if (data == null || data.Success == false || data.Matches == null || data.Matches.length <= 0) {
                            skin.$postalMsg.addClass("error");
                            skin.$postalMsg.html("<p>Could not match postal code.</p>");
                            skin.$postalMsg.show();
                            return;
                        }

                        skin.postalMatch = data.Matches[0];
                        skin.$territories.selectpicker("val", skin.postalMatch.State);
                        skin.$postalMsg.removeClass("error");
                        skin.$territoryName.val(skin.postalMatch.StateName);
                        skin.$postalMsg.html($.stringFormat("<strong>Match:</strong> {0}, {1}", skin.postalMatch.City, skin.postalMatch.StateName));
                        skin.$postalMsg.show();

                    });
                };

                this.doPostalSearch = function (postal, options) {
                    var skin = this;
                    if ($.isNullOrEmpty(postal)) {
                        skin.$postalMsg.html("<p>Please enter a postal code.</p>");
                        skin.$postalMsg.addClass("error");
                        skin.$postalMsg.show();
                    }

                    var defaults = {
                        Query: postal,
                        Country: skin.$countries.val(),
                    };

                    var opts = $.extend({}, defaults, options);

                    var apiUrl = '/api/Corp/GetRegionFromQuery'
                    return $.ajax(
                    {
                        type: 'GET',
                        url: apiUrl + '?' + $.param(opts),
                        contentType: 'application/json; charset=utf-8',
                        beforeSend: function () {
                            skin.$area.tempDisable();
                        },
                        complete: function (data) {
                            skin.$area.tempDisable('release');
                        }
                    });

                };

                this.loadTerritories = function (country, options) {

                    var skin = this;

                    var defaults = {
                        Country: country,
                    };
                    var opts = $.extend({}, defaults, options);
                    var $btn = skin.$territories.next().find("button.selectpicker");

                    var apiUrl = '/api/Public/GetTerritories'
                    return $.ajax(
                    {
                        type: 'GET',
                        url: apiUrl + '?' + $.param(opts),
                        contentType: 'application/json; charset=utf-8',
                        beforeSend: function () {
                            skin.$area.tempDisable();
                        },
                        complete: function (data) {
                            skin.$area.tempDisable('release');
                        }
                    });
                };

                this.onOk = function (controller) {
                    var skin = this;

                    var data = $.extend({}, {
                        City: (skin.postalMatch != null ? skin.postalMatch.City : null)
                    }, this.$area.unbindFromUI());

                    var onSaveError = function (message) {
                        skin.$error.html(message);
                        skin.$error.show();
                    }

                    var apiUrl = "/api/Corp/PutServiceRequest";
                    $.ajax(
                    {
                        type: "PUT",
                        url: apiUrl,
                        data: JSON.stringify(data),
                        contentType: "application/json; charset=utf-8",
                        beforeSend: function () {
                            skin.$error.hide();
                            controller.showWaitIndicator(true);
                        },
                        complete: function () {
                            controller.showWaitIndicator(false);
                        },
                        success: function (data) {
                            try {
                                if (data != null && !data.Success)
                                    onSaveError(data.Message);
                                else {
                                    controller.okBtn.hide();
                                    controller.cancelBtn.html("Close");
                                    skin.$area.children().hide();
                                    skin.$success.html($.isNullOrEmpty(data.Message) ? "Your request was submitted successfully." : data.Message);
                                    skin.$success.show();
                                }
                            }
                            catch (err) {
                                onSaveError(err);
                            }
                        },
                        error: function (xhr, status, error) {
                            var err = $.getAjaxResponseText(xhr, status, error);
                            var msg = err.Message;
                            if (!$.isNullOrEmpty(err.ExceptionMessage))
                                msg += "<div style='margin-top:15px;'><strong>Exception:</strong><p>" + err.ExceptionMessage + "</p></div>";
                            onSaveError(msg);
                        }
                    });
                    return { delayClose: true };
                }

            },

            rmaRequest: function (options) {

                var defaults = {
                    countries: null,
                    categories: null,
                    divisions: null,
                    brands: null,
                    selected: {
                        country: null,
                        state: null,
                        postal: null,
                        items: null //{ brandId: brand.pkid, technology: category, model: null }
                    }
                };
                var opts = $.extend(true, {}, defaults, options);
                $(this).pushDialog(function (o) { $("#dialogRMARequest").dialogRMARequest(o); }, opts);

            },

            rmaRequestDialogSkin: function () {

                this.data = null;

                this.getOptionOverrides = function () {
                    var skin = this;
                    return {
                        title: "Request for RMA",
                        description: "Please use the form below to tell us a little about yourself and the products you wish to return. We will collect this data and contact you with your RMA authorization information.",
                        HeaderTemplate: "<div class='dialogTitle row'>" +
                                                "<div class='col-md-1'><img src='/apple-touch-icon-60x60.png' /></div>" +
                                                "<div class='col-md-11'><h3>{title}</h3><h4>{description}<h4></div>" +
                                            "</div>",
                        contentTemplate: "<div id='{containerId}Area' name='{containerId}Area'>" +

                                            "<div id='{containerId}SuccessMsg' class='alert alert-success margin-bottom-10' role='alert' style='display:none;'></div>" +
                                            "<div id='{containerId}ErrorMsg' class='alert alert-danger margin-bottom-10' role='alert' style='display:none;'></div>" +

                                            "<ul class='nav nav-tabs' id='{containerId}Tabs'>" +
                                                "<li class='active'><a data-toggle='tab' href='#{containerId}Info'>Your Details</a></li>" +
                                                "<li ><a data-toggle='tab' href='#{containerId}Order'>Machine Information</a></li>" +
                                                "<li ><a data-toggle='tab' href='#{containerId}Parts'>Parts to Return</a></li>" +
                                                "<li ><a data-toggle='tab' href='#{containerId}Reasons'>Reasons for Return</a></li>" +
                                            "</ul>" +
                                            "<div class='tab-content' style='padding-top:10px;overflow:visible;'>" +
                                                "<div class='tab-pane active' id='{containerId}Info' >" +
                                                    "<h4><strong>Sales/Service Location</strong></h4>" +
                                                    "<div class='row'>" +
                                                        "<div class='col-md-4' >" +
                                                            "<div class='form-group'>" +
                                                                "<label for='{containerId}CountryList'>Country</label>" +
                                                                "<select id='{containerId}CountryList' class='form-control selectpicker show-tick' data-style='btn-primary' data-bind='true' data-field='Country' required></select>" +
                                                            "</div>" +
                                                        "</div>" +
                                                        "<div class='col-md-4'>" +
                                                            "<label for='{containerId}Postal'>Zip/Postal Code</label>" +
                                                            "<div class='input-group'>" +
                                                                "<input  id='{containerId}Postal' name='{containerId}Postal' class='form-control' type='text' placeholder='postal code here' data-bind='true' data-field='Postal' >" +
                                                                "<span class='input-group-btn'><button class='btn btn-default' type='button'><i class='fa fa-search'></i>&nbsp;&nbsp;&nbsp;Search</button></span>" +
                                                            "</div>" +
                                                            "<span id='{containerId}PostalMsg' class='small'></span>" +
                                                        "</div>" +
                                                        "<div class='col-md-4'>" +
                                                            "<div class='form-group'>" +
                                                                "<label for='{containerId}StateList'>State/Province</label>" +
                                                                "<select id='{containerId}StateList' class='form-control selectpicker show-tick' data-style='btn-primary' data-bind='true' data-field='State'></select>" +
                                                                "<input  id='{containerId}State' name='{containerId}State' class='form-control' style='display:none;' type='text' placeholder='state/province' data-bind='true' data-field='StateName' >" +
                                                            "</div>" +
                                                        "</div>" +
                                                    "</div>" +
                                                    "<div class='row'>" +
                                                        "<div class='col-md-12'>" +
                                                            "<h4><strong>Contact Details</strong></h4>" +
                                                            "<div class='row'>" +
                                                                "<div class='col-md-4'>" +
                                                                    "<div class='form-group'>" +
                                                                        "<label for='{containerId}Company'>Company</label>" +
                                                                        "<input  id='{containerId}Company' name='{containerId}Company' class='form-control' type='text' placeholder='company name' data-bind='true' data-field='Company' >" +
                                                                    "</div>" +
                                                                "</div>" +
                                                                "<div class='col-md-4'>" +
                                                                    "<div class='form-group'>" +
                                                                        "<label for='{containerId}NameFirst'>First Name</label>" +
                                                                        "<input  id='{containerId}NameFirst' name='{containerId}NameFirst' class='form-control' type='text' placeholder='first name' data-bind='true' data-field='NameFirst' >" +
                                                                    "</div>" +
                                                                "</div>" +
                                                                "<div class='col-md-4'>" +
                                                                    "<div class='form-group'>" +
                                                                        "<label for='{containerId}NameLast'>Last Name</label>" +
                                                                        "<input  id='{containerId}NameLast' name='{containerId}NameLast' class='form-control' type='text' placeholder='last name' data-bind='true' data-field='NameLast' >" +
                                                                    "</div>" +
                                                                "</div>" +
                                                            "</div>" +
                                                            "<div class='row'>" +
                                                                "<div class='col-md-4'>" +
                                                                    "<div class='form-group'>" +
                                                                        "<label for='{containerId}Phone'>Phone</label>" +
                                                                        "<input  id='{containerId}Phone' name='{containerId}Phone' class='form-control' type='text' placeholder='phone' data-bind='true' data-field='Phone' >" +
                                                                    "</div>" +
                                                                "</div>" +
                                                                "<div class='col-md-8'>" +
                                                                    "<div class='form-group'>" +
                                                                        "<label for='{containerId}Email'>Email</label>" +
                                                                        "<input  id='{containerId}Email' name='{containerId}Email' class='form-control' type='text' placeholder='someone@somewhere.com' data-bind='true' data-field='Email'>" +
                                                                    "</div>" +
                                                                "</div>" +
                                                            "</div>" +
                                                            "<div class='row'>" +
                                                                "<div class='col-md-4'>" +
                                                                    "<div class='form-group'>" +
                                                                        "<label for='{containerId}ContactTypes'>Contact Me</label><br>" +
                                                                        "<select id='{containerId}ContactTypes' class='form-control selectpicker show-tick' data-style='btn-primary' data-bind='true' data-field='ContactMethods' multiple></select>" +
                                                                    "</div>" +
                                                                "</div>" +
                                                                "<div class='col-md-4'>" +
                                                                    "<div class='form-group'>" +
                                                                        "<label for='{containerId}ContactTime'>Best Time to Contact Me</label><br>" +
                                                                        "<select id='{containerId}ContactTime' class='form-control selectpicker show-tick' data-style='btn-primary' data-bind='true' data-field='ContactTime'></select>" +
                                                                    "</div>" +
                                                                "</div>" +
                                                                "<div class='col-md-4'>" +
                                                                "</div>" +
                                                            "</div>" +
                                                        "</div>" +
                                                    "</div>" +
                                                "</div>" +
                                                // Machine
                                                 "<div class='tab-pane' id='{containerId}Order' >" +

                                                    "<h4><strong>Original Order Details</strong></h4>" +
                                                    "<div class='row'>" +
                                                        "<div class='col-md-6' >" +
                                                            "<div class='form-group'>" +
                                                                "<label for='{containerId}SoldTo'>Sold To</label>" +
                                                                "<input  id='{containerId}SoldTo' name='{containerId}SoldTo' class='form-control' type='text' placeholder='customer or distributor name' data-bind='true' data-field='SoldTo'>" +
                                                            "</div>" +
                                                        "</div>" +
                                                        "<div class='col-md-6'>" +
                                                            "<label for='{containerId}ShippedTo'>Shipped To</label>" +
                                                            "<div class='form-group'>" +
                                                                "<input  id='{containerId}ShippedTo' name='{containerId}ShippedTo' class='form-control' type='text' placeholder='location' data-bind='true' data-field='ShippedTo' >" +
                                                            "</div>" +
                                                        "</div>" +
                                                    "</div>" +

                                                    "<div class='row'>" +
                                                        "<div class='col-md-4' >" +
                                                            "<div class='form-group'>" +
                                                                "<label for='{containerId}OrderNum'>Order Number</label>" +
                                                                "<input  id='{containerId}OrderNum' name='{containerId}OrderNum' class='form-control' type='text' placeholder='order number' data-bind='true' data-field='OrderNum'>" +
                                                            "</div>" +
                                                        "</div>" +
                                                        "<div class='col-md-4'>" +
                                                            "<label for='{containerId}DistPO'>Distributor PO#</label>" +
                                                            "<div class='form-group'>" +
                                                                "<input  id='{containerId}DistPO' name='{containerId}DistPO' class='form-control' type='text' placeholder='purchase order number' data-bind='true' data-field='DistributorPO' >" +
                                                            "</div>" +
                                                        "</div>" +
                                                        "<div class='col-md-4'>" +
                                                            "<label for='{containerId}InvoiceNum'>Invoice Number</label>" +
                                                            "<div class='form-group'>" +
                                                                "<input  id='{containerId}InvoiceNum' name='{containerId}InvoiceNum' class='form-control' type='text' placeholder='invoice number' data-bind='true' data-field='InvoiceNum' >" +
                                                            "</div>" +
                                                        "</div>" +
                                                    "</div>" +

                                                    "<h4><strong>Warranty</strong></h4>" +
                                                    "<div class='row margin-bottom-10'>" +
                                                        "<div class='col-md-12 text-info' >" +
                                                            "<strong>Note: </strong>One Machine per RMA. If sending multiple items from other machines, you must complete a new RMA form." +
                                                        "</div>" +
                                                    "</div>" +
                                                    "<div class='row'>" +
                                                        "<div class='col-md-6' >" +
                                                            "<div class='form-group'>" +
                                                                "<label for='{containerId}Warranty'>Machine Serial Number</label>" +
                                                                "<div class='checkbox checkbox-slider-md checkbox-slider--a yesno'><label><input id='{containerId}Warranty' name='{containerId}Warranty' type='checkbox' data-bind='true' data-field='Warranty'><span>Is this machine under warranty?</span></label></div>" +
                                                            "</div>" +
                                                        "</div>" +
                                                        "<div class='col-md-6'>" +
                                                            "<div class='form-group'>" +
                                                                "<label for='{containerId}SerialNum'>Machine Serial Number</label>" +
                                                                "<input  id='{containerId}SerialNum' name='{containerId}SerialNum' class='form-control' type='text' placeholder='serial number' data-bind='true' data-field='Serial' >" +
                                                            "</div>" +
                                                        "</div>" +
                                                    "</div>" +


                                                "</div>" +
                                                // Parts
                                                 "<div class='tab-pane' id='{containerId}Parts' >" +

                                                    "<div class='table-search-v1 panel panel-grey'>" +
                                                        "<div class='panel-heading clearfix' style='padding-right: 5px;'>" +
                                                            "<h3 class='panel-title pull-left'><i class='fa fa-tasks fa-lg'></i>&nbsp;Parts to be returned</h3>" +
                                                        "</div>" +
                                                        "<div class='table-responsive' style='max-height: 400px;overflow-y:auto;'>" +
                                                            "<table class='table table-bordered table-striped dataFormatted'>" +
                                                                "<thead>" +
                                                                    "<tr>" +
                                                                        "<th></th>" +
                                                                        "<th>Quantity</th>" +
                                                                        "<th>Description</th>" +
                                                                    "</tr>" +
                                                                "</thead>" +
                                                                "<tbody id='{containerId}ItemBody'>" +
                                                                    "<tr><td><button class='btn btn-block btn-success' data-action='new' type='button'><i class='fa fa-plus-circle'></i>&nbsp;&nbsp;add</button></td><td colspan='3' style='width:100%;'></td></tr>" +
                                                                "</tbody>" +
                                                                "<tbody id='{containerId}ItemReadOnlyBody'>" +
                                                                     "<tbody id='{containerId}ReadOnlyTable'></tbody>" +
                                                                "</tbody>" +
                                                            "</table>" +
                                                        "</div>" +
                                                    "</div>" +


                                                "</div>" +
                                                // Reasons
                                                 "<div class='tab-pane' id='{containerId}Reasons' >" +

                                                    "<div class='row'>" +
                                                        "<div class='col-md-6' >" +
                                                            "<div class='form-group'>" +
                                                                "<label for='{containerId}Reason'>Return Reason</label>" +
                                                                "<select id='{containerId}Reason' name='{containerId}Reason' class='form-control selectpicker show-tick' data-style='btn-primary' data-bind='true' data-field='Reason' data-unbind='name' required></select>" +
                                                            "</div>" +
                                                        "</div>" +
                                                        "<div class='col-md-6'>" +
                                                        "</div>" +
                                                    "</div>" +

                                                    "<div class='row'>" +
                                                        "<div class='col-md-12' >" +
                                                            "<label for='{containerId}ReasonDetail'>Reason Details</label>" +
                                                            "<div class='form-group'>" +
                                                                "<textarea  id='{containerId}ReasonDetail' name='{containerId}ReasonDetail' rows='10' class='form-control' type='text' placeholder='Details must be completed for parts returned' data-bind='true' data-field='ReasonDetail' ></textarea>" +
                                                            "</div>" +
                                                        "</div>" +
                                                    "</div>" +

                                                "</div>" +
                                           "</div>" +
                                         "</div>",
                        showOkButton: true,
                        okText: "<i class='fa fa-send'></i>&nbsp;&nbsp;&nbsp;Submit my RMA Request!",
                        cancelText: appStrings.buttons.cancel,
                        validate: true,
                        validateAsTabs: true,
                        validationRules: {
                            dialogRMARequestCountry: {
                                requiredSelect: true
                            },
                            dialogRMARequestCompany: {
                                required: true,
                                maxlength: 500
                            },
                            dialogRMARequestNameFirst: {
                                required: true,
                                maxlength: 255
                            },
                            dialogRMARequestNameLast: {
                                required: true,
                                maxlength: 255
                            },
                            dialogRMARequestPhone: {
                                required: true,
                                maxlength: 255
                            },
                            dialogRMARequestEmail: {
                                required: true,
                                maxlength: 1000,
                                email: true
                            },
                            dialogRMARequestSoldTo: {
                                required: true,
                                maxlength: 255,
                            },
                            dialogRMARequestShippedTo: {
                                required: true,
                                maxlength: 255,
                            },
                            dialogRMARequestSerialNum: {
                                required: function (element) {
                                    return skin.$area.find("#dialogRMARequestWarranty").is(":checked");
                                },
                                maxlength: 255,
                            },
                            dialogRMARequestOrderNum: {
                                required: function (element) {
                                    return $.isNullOrEmpty(skin.$area.find("#dialogRMARequestDistPO").val()) && $.isNullOrEmpty(skin.$area.find("#dialogRMARequestInvoiceNum").val());
                                },
                                maxlength: 255,
                            },
                            dialogRMARequestDistPO: {
                                required: function (element) {
                                    return $.isNullOrEmpty(skin.$area.find("#dialogRMARequestOrderNum").val()) && $.isNullOrEmpty(skin.$area.find("#dialogRMARequestInvoiceNum").val());
                                },
                                maxlength: 255,
                            },
                            dialogRMARequestInvoiceNum: {
                                required: function (element) {
                                    return $.isNullOrEmpty(skin.$area.find("#dialogRMARequestOrderNum").val()) && $.isNullOrEmpty(skin.$area.find("#dialogRMARequestDistPO").val());
                                },
                                maxlength: 255,
                            },
                            dialogRMARequestReasonDetail: {
                                required: true,
                                maxlength: 1000,
                            }
                        },
                    };
                }
                this.getDynamicValidationMessages = function (controller) {
                    return {
                        dialogRMARequestOrderNum: "Please supply at least one field above.",
                        dialogRMARequestDistPO: "",
                        dialogRMARequestInvoiceNum: "",
                        dialogRMARequestSerialNum: "Serial number is required for machines under warranty."
                    };
                }

                // Called before the dialog is ready to show its data.
                this.onPreShow = function (success, record, controller, dialogSelector, errorMessage) {
                    var skin = this;

                    //if the record is null then this being called from web request, so this will be pre-populated with readonly data
                    var readonly = (controller.options != null && controller.options.data && controller.options.data.IsReadonly && controller.options.data.IsReadonly == true) ? true : false;
                    if (record == null) {
                        if (controller.options) {
                            if (controller.options.data) {
                                record = controller.options.data;
                                controller.options.isReadOnly = true;
                            }
                        }
                    }

                    this.data = record;
                    this.$area = $("#" + controller.containerId + "Area");
                    this.$wait = this.$area.find("#wait");
                    this.$error = controller.findUniqueControl("ErrorMsg");
                    this.$success = controller.findUniqueControl("SuccessMsg");

                    controller.okBtn.removeClass("btn-primary").addClass("btn-success");

                    this.brands = controller.options.brands;
                    this.categories = controller.options.categories;

                    this.$countries = controller.findUniqueControl("CountryList");
                    this.$territories = controller.findUniqueControl("StateList");
                    this.$territoryName = controller.findUniqueControl("State");
                    this.$postal = controller.findUniqueControl("Postal");
                    this.$postalSearch = this.$postal.next().find("button");
                    this.$postalMsg = controller.findUniqueControl("PostalMsg");


                    if (controller.options.selected) {
                        if (controller.options.selected.postal == null) {
                            controller.options.selected.postal = (this.data != null && this.data.Postal != null) ? this.data.Postal : "";
                        }
                        this.$postal.val(controller.options.selected.postal);
                    }

                    this.$postal.off("keypress").on("keypress", function (e) {
                        if (e.keyCode == 13) {
                            skin.onSearchZip();
                            return false;
                        }
                    });
                    this.$postalSearch.on("click", function (e) {
                        e.preventDefault();
                        skin.onSearchZip();
                    });


                    if (controller.options.selected) {
                        if (controller.options.selected.state == null) {
                            controller.options.selected.state = (this.data != null && this.data.State != null) ? this.data.State : null;
                        }
                    }

                    skin.$territories.territoryList({
                        preselect: controller.options.selected.state,
                        data: [],
                    });

                    this.countries = controller.options.countries;
                    this.$countries.countryList({
                        preselect: controller.options.selected.country,
                        data: this.countries,
                        onChange: function ($sel, $option, item) {
                            setTimeout(function () {
                                $.when(skin.loadTerritories(item.Code)).always(function (data) {
                                    skin.$territoryName.val("");
                                    skin.territories = data.Territories;
                                    skin.territories.splice(0, 0, { Code: null, pkId: null, Name: "Please select...", fkCountryCode: item.Code });
                                    if (data == null || data.Success == false || data.Territories == null || data.Territories.length == 0) {
                                        skin.$territories.selectpicker("hide");
                                        skin.$territoryName.show();

                                    }
                                    else {
                                        skin.$territoryName.hide();

                                        skin.$territories.territoryList({
                                            preselect: controller.options.selected.state,
                                            data: skin.territories,
                                            onChange: function ($sel, $option, item) {
                                            }
                                        });
                                        skin.$territories.selectpicker("refresh");
                                        skin.firstCountryChange = false;
                                    }
                                });
                            }, 100);
                        }
                    });

                    //This drop-down does not take multiple values so take first.
                    var vals = (this.data != null && this.data.ContactMethods != null) ? this.data.ContactMethods.split(",") : null;
                    var contactMethods = vals != null && vals.length > 1 ? vals[0] : $.isNullOrEmpty(vals) ? "-1" : vals;
                    if (controller.options.selected) {
                        if (controller.options.selected.contactMe == null) {

                            controller.options.selected.contactMe = (this.data != null && this.data.ContactMethods != null && vals != null) ? this.data.ContactMethods : [-1];
                        }
                    }

                    this.$contactMethod = controller.findUniqueControl("ContactTypes");
                    this.$contactMethod.contactMeMethodList({
                        preselect: controller.options.selected.contactMe,
                    });


                    this.$contactTime = controller.findUniqueControl("ContactTime");
                    this.$contactTime.contactMeTimeList({
                        //preselect: [],
                    });

                    this.$area.bindToUI(this.data);
                    this.firstCountryChange = true;
                    this.$countries.trigger("change");

                    this.$itemBody = controller.findUniqueControl("ItemBody");
                    this.$itemReadonlyBody = controller.findUniqueControl("ItemReadOnlyBody");

                    this.$newItem = this.$itemBody.find("[data-action='new']");
                    if (readonly == false) {
                        this.$itemReadonlyBody.remove();
                        this.$newItem.on("click", function (e) {
                            e.preventDefault();
                            skin.onAddItem(skin.$itemBody);
                        });

                        this.onAddItem(this.$itemBody);
                        this.$itemReadonlyBody.remove();
                    }
                    else
                    {
                        this.$itemBody.remove();
                        this.$itemReadonlyBody = controller.findUniqueControl("ItemReadOnlyBody");
                        this.$itemReadonlyBodyQuantity = controller.findUniqueControl("ReadOnlyTable");

                        var myHtml = "";
                        if (this.data && this.data.Parts) {
                            $.each(this.data.Parts, function (i, part) {
                                myHtml += $.stringFormat("<tr><td>&nbsp;</td><td>{0}</td><td>{1}</td></tr>", part.Quantity, part.Description);
                            });
                        }
                        else
                            var myHtml = "<tr><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>";

                        this.$itemReadonlyBodyQuantity.html(myHtml);
                    }

                    if (controller.options.selected) {
                        if (controller.options.selected.reason == null) {
                            controller.options.selected.reason = (this.data != null && this.data.ReasonIdForHardCodedDropDown != null) ? this.data.ReasonIdForHardCodedDropDown : null;
                        }
                    }

                    this.$reason = controller.findUniqueControl("Reason");
                    this.$reason.returnReasonList();
                    if (this.data != null) {
                        var reasonValue = this.$reason.find($.stringFormat("option:contains('{0}')", this.data.Reason)).val();
                        this.$reason.selectpicker("val", reasonValue);
                    }

                    if (readonly && readonly == true) {
                        this.dialog.okBtn.remove();
                        this.dialog.cancelBtn.html(appStrings.buttons.close);
                    }

                };

                this.onShown = function (controller) {
                    var isReadOnly = controller.options.isReadOnly == true;
                    // update for readonly
                    if (isReadOnly)
                        this.$area.disableElements();
                }

                this.onSearchZip = function () {
                    var skin = this;

                    $.when(this.doPostalSearch(skin.$postal.val())).always(function (data) {
                        if (data == null || data.Success == false || data.Matches == null || data.Matches.length <= 0) {
                            skin.$postalMsg.addClass("error");
                            skin.$postalMsg.html("<p>Could not match postal code.</p>");
                            skin.$postalMsg.show();
                            return;
                        }

                        skin.postalMatch = data.Matches[0];
                        skin.$territories.selectpicker("val", skin.postalMatch.State);
                        skin.$postalMsg.removeClass("error");
                        skin.$territoryName.val(skin.postalMatch.StateName);
                        skin.$postalMsg.html($.stringFormat("<strong>Match:</strong> {0}, {1}", skin.postalMatch.City, skin.postalMatch.StateName));
                        skin.$postalMsg.show();

                    });
                };

                this.doPostalSearch = function (postal, options) {
                    var skin = this;
                    if ($.isNullOrEmpty(postal)) {
                        skin.$postalMsg.html("<p>Please enter a postal code.</p>");
                        skin.$postalMsg.addClass("error");
                        skin.$postalMsg.show();
                    }

                    var defaults = {
                        Query: postal,
                        Country: skin.$countries.val(),
                    };

                    var opts = $.extend({}, defaults, options);

                    var apiUrl = '/api/Corp/GetRegionFromQuery'
                    return $.ajax(
                    {
                        type: 'GET',
                        url: apiUrl + '?' + $.param(opts),
                        contentType: 'application/json; charset=utf-8',
                        beforeSend: function () {
                            skin.$area.tempDisable();
                        },
                        complete: function (data) {
                            skin.$area.tempDisable('release');
                        }
                    });

                };

                this.loadTerritories = function (country, options) {

                    var skin = this;

                    var defaults = {
                        Country: country,
                    };
                    var opts = $.extend({}, defaults, options);
                    var $btn = skin.$territories.next().find("button.selectpicker");

                    var apiUrl = '/api/Public/GetTerritories'
                    return $.ajax(
                    {
                        type: 'GET',
                        url: apiUrl + '?' + $.param(opts),
                        contentType: 'application/json; charset=utf-8',
                        beforeSend: function () {
                            skin.$area.tempDisable();
                        },
                        complete: function (data) {
                            skin.$area.tempDisable('release');
                        }
                    });
                };

                this.onOk = function (controller) {
                    var skin = this;

                    var items = skin.$itemBody.unbindFromUI("xeditable");

                    var data = $.extend({}, {
                        City: (skin.postalMatch != null ? skin.postalMatch.City : null),
                        Parts: items
                    }, this.$area.unbindFromUI());



                    var onSaveError = function (message) {
                        skin.$error.html(message);
                        skin.$error.show();
                    }

                    var apiUrl = "/api/Corp/PutRMA";
                    $.ajax(
                    {
                        type: "PUT",
                        url: apiUrl,
                        data: JSON.stringify(data),
                        contentType: "application/json; charset=utf-8",
                        beforeSend: function () {
                            skin.$error.hide();
                            controller.showWaitIndicator(true);
                        },
                        complete: function () {
                            controller.showWaitIndicator(false);
                        },
                        success: function (data) {
                            try {
                                if (data != null && !data.Success)
                                    onSaveError(data.Message);
                                else {
                                    controller.okBtn.hide();
                                    controller.cancelBtn.html("Close");
                                    skin.$area.children().hide();
                                    skin.$success.html($.isNullOrEmpty(data.Message) ? "Your request was submitted successfully." : data.Message);
                                    skin.$success.show();
                                }
                            }
                            catch (err) {
                                onSaveError(err);
                            }
                        },
                        error: function (xhr, status, error) {
                            var err = $.getAjaxResponseText(xhr, status, error);
                            var msg = err.Message;
                            if (!$.isNullOrEmpty(err.ExceptionMessage))
                                msg += "<div style='margin-top:15px;'><strong>Exception:</strong><p>" + err.ExceptionMessage + "</p></div>";
                            onSaveError(msg);
                        }
                    });
                    return { delayClose: true };
                }

                this.onUpdateButtons = function ($cont) {
                    var $rows = $cont.find("tr");
                    var $newRow = this.$newItem.closest("tr");
                    if ($rows.length >= 7) {
                        $newRow.hide();
                    }
                    else {
                        $newRow.show();
                    }
                };

                this.onAddItem = function ($cont, options) {
                    var skin = this;

                    var defaults = {
                        quantity: null,
                        description: "",
                        template: "<tr>" +
                                         "<td class='animated fadeIn'><button class='btn btn-block btn-danger' data-action='delete' type='button'><i class='fa fa-remove'></i>&nbsp;&nbsp;remove</button></td>" +
                                        "<td class='animated fadeIn'><a href='#' data-pk='{index}' id='quantity' data-type='number' data-title='Quantity' data-bind='true' data-field='Quantity'></a></td>" +
                                        "<td class='animated fadeIn' style='width:100%;'><a href='#' data-pk='{index}' id='description' data-type='textarea' data-title='Part Description' data-bind='true' data-field='Description'></a></td>" +
                                    "</tr>"
                    };
                    var opts = $.extend(true, {}, defaults, options);

                    var $allrows = $cont.find("tr");

                    var $row = $($.expandTemplateFromObject(opts.template, $.extend({}, opts, {
                        index: $allrows.length,
                    })));

                    var $del = $row.find("[data-action='delete']");
                    $del.on("click", function (e) {
                        e.preventDefault();
                        var $delrow = $(this).closest("tr");
                        $delrow.remove();
                        skin.onUpdateButtons($cont);
                    });
                    var $last = $cont.children().last("tr");
                    if (!$last.exists())
                        $cont.append($row);
                    else
                        $last.before($row);

                    try {
                        if ($row.find("#quantity").editable != null) {
                            $row.find("#quantity").editable({
                                value: opts.manufacturer,
                                emptytext: "<span style='white-space: nowrap;'>click to edit</span>",
                                mode: "inline",
                                onblur: "submit",
                                validate: function (value) {
                                    if ($.trim(value) == 0)
                                        return "A valid quantity is required";
                                }
                            });
                        }

                        if ($row.find("#description").editable != null) {
                            $row.find("#description").editable({
                                emptytext: "click to edit",
                                mode: "inline",
                                onblur: "submit",
                                validate: function (value) {
                                    if ($.trim(value) == 0)
                                        return "The description is required";
                                }
                            });
                        }
                    } catch (exception) {
                        //Exception
                    }

                    skin.onUpdateButtons($cont);
                };

            },


            myHaegerLauncher: function (options) {
                var defaults = {
                    urlBase: "http://solutions.haeger.com",
                };
                var opts = $.extend(true, {}, defaults, options);

                $("#myHaegerLauncher").dialogMyHaegerLauncher($.extend(true, {}, {}, opts));

            },

            myHaegerLauncherDialogSkin: function () {

                this.data = null;

                this.getOptionOverrides = function () {
                    return {
                        title: "My Haeger",
                        description: "Please pick from the following section.",
                        HeaderTemplate: "<div class='row'>" +
                                                "<div class='col-md-12'>" +
                                                    "<img class='hidden-xs' src='/images/misc/haeger/haeger.logo.med.norm.png' width='auto' height='auto' />" +
                                                    "<img class='hidden-sm hidden-md hidden-lg' src='/images/logos/pcorp.logo.xs.norm.png' width='auto' height='auto' />" +
                                                "</div>" +
                                            "</div>",
                        contentTemplate: "<div id='{containerId}Area' name='{containerId}Area'>" +
                                             "<div class='row equal-height'>" +
                                                "<div class='col-md-4'>" +
                                                    "<a class='tileLink' href='/direct_sales/direct-sales-signon.aspx' target='_haeger'>" +
                                                        "<div class='tile nosize dark-blue animated zoomIn animation-delay-5'>" +
                                                            "<h2><i class='fa fa-sign-in'></i>&nbsp;Customer Login</h2>" +
                                                            "<p>Customer Login Page</p>" +
                                                        "</div>" +
                                                    "</a>" +
                                                "</div>" +
                                                "<div class='col-md-4'>" +
                                                    "<a class='tileLink' href='/Sales/partnersignon.aspx' target='_haeger'>" +
                                                        "<div class='tile nosize blue animated zoomIn animation-delay-5'>" +
                                                            "<h2><i class='fa fa-sign-in'></i>&nbsp;Partner Login</h2>" +
                                                            "<p>Partner Login Page</p>" +
                                                        "</div>" +
                                                    "</a>" +
                                                "</div>" +
                                                "<div class='col-md-4'>" +
                                                    "<a class='tileLink' href='/CustomerFeedBack.aspx' target='_haeger'>" +
                                                        "<div class='tile nosize light-blue animated zoomIn animation-delay-5'>" +
                                                            "<h2><i class='fa fa-comments'></i>&nbsp;Feedback</h2>" +
                                                            "<p>Feedback Page</p>" +
                                                        "</div>" +
                                                    "</a>" +
                                                "</div>" +
                                             "</div>" +
                                             "<div class='row equal-height'>" +
                                                "<div class='col-md-6'>" +
                                                    "<a class='tileLink' href='/Distributors/distributorsignon.aspx' target='_haeger'>" +
                                                        "<div class='tile nosize dark-green animated zoomIn animation-delay-5'>" +
                                                            "<h2><i class='fa fa-globe'></i>&nbsp;Distributor Central</h2>" +
                                                            "<p>Distributor Login Page</p>" +
                                                        "</div>" +
                                                    "</a>" +
                                                "</div>" +
                                                "<div class='col-md-6'>" +
                                                    "<a class='tileLink' href='/servicedocs-signon.aspx' target='_haeger'>" +
                                                        "<div class='tile nosize dark-orange animated zoomIn animation-delay-5'>" +
                                                            "<h2><i class='fa fa-wrench'></i>&nbsp;Service Central</h2>" +
                                                            "<p>Direct Service Login Page</p>" +
                                                        "</div>" +
                                                    "</a>" +
                                                "</div>" +
                                             "</div>" +
                                         "</div>",
                        showOkButton: false,
                        cancelText: appStrings.buttons.close
                    };
                }

                // Called before the dialog is ready to show its data.
                this.onPreShow = function (success, record, controller, dialogSelector, errorMessage) {
                    var skin = this;

                    this.data = record;
                    this.$area = $("#" + controller.containerId + "Area");

                    if (!$.isNullOrEmpty(controller.options.urlBase)) {
                        this.$area.find("a.tileLink").each(function () {
                            var $link = $(this);
                            $link.attr("href", $.stringFormat("{0}{1}", controller.options.urlBase, $link.attr("href")));
                        });
                    }
                };

            },

            userSignup: function (classInstance, options) {
                $.pageNotify("info", "Not yet implemented");
                return;
                var defaults = {
                    apiUrl: "/api/CorpTraining/GetClassInstance/" + id,
                    queryServer: id != null
                };
                var opts = $.extend(true, {}, defaults, options);
                $(this).pushDialog(function (o) { $("#dialogClassSchedule").dialogClassSchedule(o); }, opts);

            },

            /////////////
            editJobPost: function (id, options) {
                var fields = {
                    id: id == null ? -1 : id
                };
                var defaults = {
                    apiUrl: "/api/Corp/GetJobPost?" + $.param(fields),
                    queryServer: true
                };
                var opts = $.extend(true, {}, defaults, options);
                $(this).pushDialog(function (o) { $("#dialogJobPost").dialogJobPost(o); }, opts);

            },

            jobPostEditDialogSkin: function () {

                this.data = null;

                this.getOptionOverrides = function (options) {
                    var skin = this;
                    return {
                        title: "Job Posting",
                        description: "Please use the form below to setup/modify the job posting.",
                        HeaderTemplate: "<div class='dialogTitle row'>" +
                                                "<div class='col-md-1'><img src='/apple-touch-icon-60x60.png' /></div>" +
                                                "<div class='col-md-11'><h3>{title}: <span class='text-info' id='{containerId}Name'></span></h3><h4>{description}<h4></div>" +
                                            "</div>",
                        contentTemplate: "<div id='{containerId}Area' name='{containerId}Area'>" +

                                            "<div id='{containerId}SuccessMsg' class='alert alert-success margin-bottom-10' role='alert' style='display:none;'></div>" +
                                            "<div id='{containerId}ErrorMsg' class='alert alert-danger margin-bottom-10' role='alert' style='display:none;'></div>" +
                                            "<ul class='nav nav-tabs' id='{containerId}Tabs'>" +
                                                "<li class='active'><a data-toggle='tab' href='#{containerId}TabInfo'><i class='fa fa-list'></i>&nbsp;&nbsp;&nbsp;General</a></li>" +
                                                "<li ><a data-toggle='tab' href='#{containerId}TabRequirements'><i class='fa fa-comments'></i>&nbsp;&nbsp;&nbsp;Requirements</a></li>" +
                                                "<li ><a data-toggle='tab' href='#{containerId}TabDescription'><i class='fa fa-user'></i>&nbsp;&nbsp;&nbsp;Description</a></li>" +
                                                "<li ><a data-toggle='tab' href='#{containerId}TabBenefits'><i class='fa fa-user'></i>&nbsp;&nbsp;&nbsp;Benefits</a></li>" +
                                            "</ul>" +
                                            "<div class='tab-content' style='padding-top:10px;overflow:visible;'>" +
                                                "<div class='tab-pane active' id='{containerId}TabInfo' >" +
                                                    "<input type='hidden' data-bind='true' data-field='pkId' />" +
                                                    "<div class='row'>" +
                                                        "<div class='col-md-8'>" +
                                                            "<div class='form-group'>" +
                                                                "<label for='{containerId}Status'>Job Post Status&nbsp;&nbsp;&nbsp;&nbsp;</label><span id='{containerId}StatusMsg' class='small text-danger'></span>" +
                                                                "<select id='{containerId}Status' name='{containerId}Status' class='form-control selectpicker show-tick' data-style='btn-primary' data-bind='true' data-field='fkStatusId' required></select>" +
                                                            "</div>" +
                                                        "</div>" +
                                                        "<div class='col-md-4'>" +
                                                            "<div id='{containerId}PostDate' class='form-group'>" +
                                                                "<label for='{containerId}PostedDatePicker'>Post Date <small class='text-muted'></small></label>" +
                                                                "<div id='{containerId}PostedDatePicker' class='input-group date' data-bind='true' data-control='datetimepicker' data-field='Posted' datetype='local'>" +
                                                                    "<input id='{constainerId}Posted' type='text' class='form-control'  data-format='MM/dd/yyyy' />" +
                                                                    "<span class='input-group-addon add-on'>" +
                                                                        "<i data-time-icon='icon-time' data-date-icon='icon-calendar'></i>" +
                                                                    "</span>" +
                                                                "</div>" +
                                                            "</div>" +
                                                       "</div>" +
                                                    "</div>" +
                                                    "<div class='row'>" +
                                                        "<div class='col-md-3'>" +
                                                            "<div class='form-group'>" +
                                                                "<label for='{containerId}Country'>Country</label>" +
                                                                "<select id='{containerId}Country' name='{containerId}Country' class='form-control selectpicker show-tick' data-style='btn-primary' data-bind='true' data-field='Countries'></select>" +
                                                            "</div>" +
                                                        "</div>" +
                                                         "<div class='col-md-3'>" +
                                                            "<div class='form-group'>" +
                                                                "<label for='{containerId}States'>State/Territories</label>" +
                                                                "<select id='{containerId}States' name='{containerId}States' class='form-control selectpicker show-tick' data-style='btn-primary' data-bind='true' data-field='States' multiple></select>" +
                                                            "</div>" +
                                                         "</div>" +
                                                        "<div class='col-md-6' >" +
                                                            "<div class='form-group'>" +
                                                                "<label for='{containerId}Locations'>Location(s)</label><small class='pull-right text-muted'>description of region</small>" +
                                                                "<input id='{containerId}Locations' name='{containerId}Locations' type='text' class='form-control' data-bind='true' data-field='Locations'/>" +
                                                            "</div>" +
                                                        "</div>" +
                                                    "</div>" +
                                                    "<div class='row'>" +
                                                        "<div class='col-md-8' >" +
                                                            "<div class='form-group'>" +
                                                                "<label for='{containerId}Name'>Position</label>" +
                                                                "<input id='{containerId}Name' name='{containerId}Name' type='text' class='form-control' data-bind='true' data-field='Name'/>" +
                                                            "</div>" +
                                                        "</div>" +
                                                        "<div class='col-md-4' >" +
                                                            "<div class='form-group'>" +
                                                                "<label for='{containerId}StartDate'>Start Date</label><small class='pull-right text-muted'>can be descriptive</small>" +
                                                                "<input id='{containerId}StartDate' name='{containerId}StartDate' type='text' class='form-control' data-bind='true' data-field='StartDate'/>" +
                                                            "</div>" +
                                                        "</div>" +
                                                    "</div>" +
                                                    "<div class='row'>" +
                                                        "<div class='col-md-12' >" +
                                                            "<div class='form-group'>" +
                                                                "<label for='{containerId}Name'>Short Summary</label><small class='pull-right text-muted'>displayed in grid listings</small>" +
                                                                "<div id='{containerId}Summary' name='{containerId}Summary' data-control='summernote' editor-buttons='print' editor-height='150' editor-maxHeight='150px'  data-bind='true' data-field='Summary'></div>" +
                                                            "</div>" +
                                                        "</div>" +
                                                    "</div>" +
                                                "</div>" +
                                                // Requirements
                                                 "<div class='tab-pane' id='{containerId}TabRequirements' >" +

                                                    "<div class='row'>" +
                                                        "<div class='col-md-6'>" +
                                                            "<div class='form-group'>" +
                                                                "<label for='{containerId}SalaryRange'>Salary Range</label><small class='pull-right text-muted'>optional</small>" +
                                                                "<input id='{containerId}SalaryRange' name='{containerId}SalaryRange' type='text' class='form-control' data-bind='true' data-field='SalaryRange'/>" +
                                                            "</div>" +
                                                        "</div>" +
                                                        "<div class='col-md-3'>" +
                                                        "</div>" +
                                                        "<div class='col-md-3'>" +
                                                        "</div>" +
                                                    "</div>" +

                                                    "<div class='row'>" +
                                                        "<div class='col-md-3'>" +
                                                            "<div class='form-group'>" +
                                                                "<label for='{containerId}EmploymentType'>Employment Type</label>" +
                                                                "<select id='{containerId}EmploymentType' name='{containerId}EmploymentType' class='form-control selectpicker show-tick' data-style='btn-primary' data-bind='true' data-field='EmploymentType'></select>" +
                                                            "</div>" +
                                                        "</div>" +
                                                        "<div class='col-md-3'>" +
                                                            "<div class='form-group'>" +
                                                                "<label for='{containerId}EmploymentTerm'>Employment Term</label>" +
                                                                "<select id='{containerId}EmploymentTerm' name='{containerId}EmploymentTerm' class='form-control selectpicker show-tick' data-style='btn-primary' data-bind='true' data-field='EmploymentTerm'></select>" +
                                                            "</div>" +
                                                        "</div>" +
                                                        "<div class='col-md-3'>" +
                                                            "<div class='form-group'>" +
                                                                "<label for='{containerId}HoursPerWeek'>Hours/Week</label><small class='pull-right text-muted'>optional</small>" +
                                                                "<input id='{containerId}HoursPerWeek' name='{containerId}HoursPerWeek' type='text' class='form-control' data-bind='true' data-field='HoursPerWeek'/>" +
                                                            "</div>" +
                                                        "</div>" +
                                                        "<div class='col-md-3' >" +
                                                            "<div class='form-group'>" +
                                                                "<label for='{containerId}WorkHours'>Work Hours</label><small class='pull-right text-muted'>optional</small>" +
                                                                "<input id='{containerId}WorkHours' name='{containerId}WorkHours' type='text' class='form-control' data-bind='true' data-field='WorkHours'/>" +
                                                            "</div>" +
                                                        "</div>" +
                                                    "</div>" +
                                                    "<div class='row'>" +
                                                        "<div class='col-md-4'>" +
                                                            "<div class='form-group'>" +
                                                                "<label for='{containerId}RequiredEducation'>Education</label><small class='pull-right text-muted'>optional</small>" +
                                                                "<input id='{containerId}RequiredEducation' name='{containerId}RequiredEducation' type='text' class='form-control' data-bind='true' data-field='RequiredEducation'/>" +
                                                            "</div>" +
                                                        "</div>" +
                                                        "<div class='col-md-4'>" +
                                                            "<div class='form-group'>" +
                                                                "<label for='{containerId}RequiredExperience'>Experience</label><small class='pull-right text-muted'>optional</small>" +
                                                                "<input id='{containerId}RequiredExperience' name='{containerId}RequiredExperience' type='text' class='form-control' data-bind='true' data-field='RequiredExperience'/>" +
                                                            "</div>" +
                                                        "</div>" +
                                                        "<div class='col-md-4'>" +
                                                            "<div class='form-group'>" +
                                                                "<label for='{containerId}RequiredSecurityClearance'>Security Clearance</label><small class='pull-right text-muted'>optional</small>" +
                                                                "<input id='{containerId}RequiredSecurityClearance' name='{containerId}RequiredSecurityClearance' type='text' class='form-control' data-bind='true' data-field='RequiredSecurityClearance'/>" +
                                                            "</div>" +
                                                        "</div>" +
                                                    "</div>" +

                                                    "<div class='row'>" +
                                                        "<div class='col-md-4'>" +
                                                            "<div class='form-group'>" +
                                                                "<label for='{containerId}Travel'>Travel</label><small class='pull-right text-muted'>optional</small>" +
                                                                "<input id='{containerId}Travel' name='{containerId}Travel' type='text' class='form-control' data-bind='true' data-field='Travel'/>" +
                                                            "</div>" +
                                                        "</div>" +
                                                        "<div class='col-md-4'>" +
                                                            "<div class='form-group'>" +
                                                                "<label for='{containerId}LicenseRequired'>Driver's License Required</label>" +
                                                                "<br />" +
                                                                "<div id='{containerId}LicenseRequired' class='btn-group' data-toggle='buttons' data-bind='true' data-field='LicenseRequired' data-control='true'>" +
                                                                    "<label class='btn btn-toggle'>" +
                                                                        "<input type='radio' name='LCEnable' id='LCEnable' data-value='true' data-type='boolean' />Required" +
                                                                    "</label>" +
                                                                    "<label class='btn btn-toggle btn-no'>" +
                                                                        "<input type='radio' name='LCDisable' id='LCDisable' data-value='false' data-type='boolean' />Not Required" +
                                                                    "</label>" +
                                                                "</div>" +
                                                            "</div>" +
                                                        "</div>" +
                                                        "<div class='col-md-4'>" +
                                                            "<div class='form-group hide'>" +
                                                                "<label for='{containerId}TravelRequired'>Travel Required</label>" +
                                                                "<br />" +
                                                                "<div id='{containerId}TravelRequired' class='btn-group' data-toggle='buttons' data-bind='true' data-field='TravelRequired' data-control='true'>" +
                                                                    "<label class='btn btn-toggle'>" +
                                                                        "<input type='radio' name='TRVEnable' id='TRVEnable' data-value='true' data-type='boolean' />Required" +
                                                                    "</label>" +
                                                                    "<label class='btn btn-toggle btn-no'>" +
                                                                        "<input type='radio' name='TRVDisable' id='TRVDisable' data-value='false' data-type='boolean' />Not Required" +
                                                                    "</label>" +
                                                                "</div>" +
                                                            "</div>" +
                                                        "</div>" +
                                                    "</div>" +
                                                "</div>" +
                                                // Description
                                                 "<div id='{containerId}TabDescription' class='tab-pane' style='min-height:425px;'>" +
                                                    "<div class='row'>" +
                                                        "<div class='col-md-12'>" +
                                                            "<div class='form-group'>" +
                                                                "<label>Detailed Description</label><small class='pull-right text-muted'>displayed on detailed job page</small>" +
                                                                "<div id='{containerId}Description' name='{containerId}Description' data-control='summernote' editor-buttons='print' editor-height='400' editor-maxHeight='385px' data-bind='true' data-field='Description'></div>" +
                                                            "</div>" +
                                                        "</div>" +
                                                    "</div>" +
                                                "</div>" +
                                                // Benefits
                                                 "<div id='{containerId}TabBenefits' class='tab-pane' style='min-height:425px;'>" +
                                                    "<div class='row'>" +
                                                        "<div class='col-md-6'>" +
                                                            "<div class='form-group'>" +
                                                                "<label for='{containerId}BenefitsList'>Benefit Package&nbsp;&nbsp;&nbsp;&nbsp;</label>" +
                                                                "<select id='{containerId}BenefitsList' name='{containerId}BenefitsList' class='form-control selectpicker show-tick' data-style='btn-primary' data-bind='true' data-field='fkJobBenefitPackageId'></select>" +
                                                            "</div>" +
                                                        "</div>" +
                                                        "<div class='col-md-6'>" +
                                                        "</div>" +
                                                    "</div>" +
                                                    "<div class='row'>" +
                                                        "<div class='col-md-12'>" +
                                                            "<div class='form-group'>" +
                                                                "<label>Summary of Benefits</label><small class='pull-right text-muted'>please list one-per line</small>" +
                                                                "<div id='{containerId}Benefits' name='{containerId}Benefits'   editor-height='315' editor-maxHeight='355px' data-bind='true' data-field='Benefits' style='max-height:375px;'></div>" +
                                                            "</div>" +
                                                        "</div>" +
                                                    "</div>" +
                                                "</div>" +

                                           "</div>" +
                                         "</div>",
                        showOkButton: true,
                        okText: "<i class='fa fa-save'></i>&nbsp;&nbsp;&nbsp;Save Job Posting",
                        cancelText: appStrings.buttons.cancel,
                        validate: true,
                        validateAsTabs: true,
                        validationRules: {
                            dialogJobPostStatus: {
                                requiredSelect: true
                            },
                            dialogJobPostCountry: {
                                requiredSelect: true
                            },
                            dialogJobPostStates: {
                                requiredSelect: {
                                    required: function (element) {
                                        return skin.TerritoriesValid;
                                    },
                                }
                            },
                            dialogJobPostLocations: {
                                required: function (element) {
                                    return !skin.TerritoriesValid;
                                },
                                maxlength: 2000
                            },
                            dialogJobPostName: {
                                required: true,
                                maxlength: 500
                            },
                            dialogJobPostStartDate: {
                                maxlength: 500
                            },
                            dialogJobPostSalaryRange: {
                                maxlength: 500
                            },
                            dialogJobPostHoursPerWeek: {
                                maxlength: 255
                            },
                            dialogJobPostWorkHours: {
                                maxlength: 500
                            },
                            dialogJobPostRequiredEducation: {
                                maxlength: 2000
                            },
                            dialogJobPostRequiredExperience: {
                                maxlength: 2000
                            },
                            dialogJobPostSecurityClearance: {
                                maxlength: 2000
                            },
                            dialogJobPostTravel: {
                                maxlength: 500
                            },
                            dialogJobPostBenefitsList: {
                                requiredSelect: true
                            },
                        }
                    };
                }
                this.getDynamicValidationMessages = function (controller) {
                    return {
                        dialogJobPostStates: "Select one or more to aid in job searches.",
                        dialogJobPostLocations: "Descriptive location(s) of the expected job area is required."
                    };
                }

                // Called before the dialog is ready to show its data.
                this.onPreShow = function (success, record, controller, dialogSelector, errorMessage) {
                    var skin = this;

                    this.data = record != null ? record.Data : null;
                    this.isForNew = this.data == null || this.data.pkId == null || this.data.pkId <= 0;
                    if (this.data == null) {
                        this.data = {
                            pkId: -1
                        };
                    }
                    this.$area = $("#" + controller.containerId + "Area");
                    this.$wait = this.$area.find("#wait");
                    this.$error = controller.findUniqueControl("ErrorMsg");
                    this.$success = controller.findUniqueControl("SuccessMsg");
                    controller.okBtn.removeClass("btn-primary").addClass("btn-success");

                    this.$statusMsg = controller.findUniqueControl("StatusMsg");
                    this.$statuses = controller.findUniqueControl("Status");
                    skin.$statuses.corpJobPosStatusList({
                        preselect: JOB_POST_STATUS_PENDING,
                        data: controller.options.jobPostStatuses,
                        onChange: function ($sel, $option, item) {
                            skin.$statusMsg.html(item.Description)
                        }
                    });

                    this.$postedPicker = controller.findUniqueControl("PostedDatePicker");
                    var posted = this.data != null && this.data.Posted != null ? $.serverTime().toLocalMoment(this.data.Posted).toDate() : moment();
                    $.loadDateBox(this.$postedPicker, {
                        readOnly: this.isReadOnly,
                        pickDate: true,
                        pickTime: false,
                        allowNull: false,
                    }, posted);


                    this.$emptype = controller.findUniqueControl("EmploymentType");
                    skin.$emptype.corpEmploymentTypeList({});

                    this.$empterm = controller.findUniqueControl("EmploymentTerm");
                    skin.$empterm.corpEmploymentTermList({});


                    this.$area.find("[data-control='summernote']").attr("editor-readOnly", this.isReadOnly);

                    this.$area.bindToUI($.extend(true, {}, this.data, {
                    }));
                    this.$statuses.trigger("change");

                    this.$tabInfo = controller.findUniqueControl("TabInfo");
                    this.$tabRequirements = controller.findUniqueControl("TabRequirements");
                    this.$tabDescription = controller.findUniqueControl("TabDescription");

                    this.$tabs = this.$area.find(".tab-pane");
                    var maxHeight = 475;
                    this.$tabs.height(maxHeight);

                    var currentCountry = skin.data != null && skin.data.Countries.length > 0 ? skin.data.Countries[0] : "US";
                    var countryBenefits = controller.options.jobBenefits == null ? [] : $.filterArray(controller.options.jobBenefits, { filter: { fkCountryCode: currentCountry } });

                    this.$benefitsList = controller.findUniqueControl("BenefitsList");
                    this.$benefits = controller.findUniqueControl("Benefits");
                    this.$benefitsTab = controller.findUniqueTab("TabBenefits");
                    skin.onLoadBenefitsForCountry(controller, currentCountry);


                    this.$location = controller.findUniqueControl("Locations");
                    this.$countries = controller.findUniqueControl("Country");
                    this.$territories = controller.findUniqueControl("States");
                    skin.$territories.territoryList({
                        preselect: null,
                        data: []
                    });

                    this.countries = controller.options.countries;
                    this.$countries.countryList({
                        preselect: skin.data != null ? skin.data.Countries : ["US"],
                        data: skin.countries,
                        onChange: function ($sel, $option, item) {
                            setTimeout(function () {
                                skin.onLoadBenefitsForCountry(controller, item.Code);
                                $.when(skin.loadTerritories(item.Code)).always(function (data) {
                                    skin.territories = data.Territories;
                                    //                                    skin.territories.splice(0, 0, { Code: null, pkId: null, Name: "Please select...", fkCountryCode: item.Code });
                                    if (data == null || data.Success == false || data.Territories == null || data.Territories.length == 0) {
                                    }
                                    else {
                                        skin.$territories.prop("disabled", false);
                                        skin.$territories.territoryList({
                                            preselect: skin.data != null ? skin.data.States : null,
                                            data: skin.territories,
                                            onChange: function ($sel, $option, item) {
                                            }
                                        });
                                        skin.$territories.selectpicker("show");
                                        skin.$territories.selectpicker("refresh");
                                        skin.firstCountryChange = false;
                                    }
                                });
                            }, 100);
                        }
                    });
                    this.$countries.trigger("change");

                };

                this.onLoadBenefitsForCountry = function (controller, country) {
                    var skin = this;
                    var currentCountry = $.isNullOrEmpty(country) ? "US" : country;
                    var countryBenefits = controller.options.jobBenefits == null ? [] : $.filterArray(controller.options.jobBenefits, { filter: { fkCountryCode: currentCountry } });

                    var countryDefaults = $.filterArray(countryBenefits, { filter: { IsCountryDefault: true } });
                    var countryDefault = null;
                    if (countryDefaults != null && countryDefaults.length > 0)
                        countryDefault = countryDefaults[0].pkId;

                    skin.$benefitsList.corpJobBenefitPackageList({
                        preselect: skin.data != null && skin.data.fkJobBenefitPackageId != null ? skin.data.fkJobBenefitPackageId : countryDefault,
                        data: countryBenefits,
                        onChange: function ($sel, $option, item) {

                            var desc = null;
                            if (item == null || $.isNullOrEmpty(item.Description))
                                desc = "No benefits package available";
                            else
                                desc = item.Description;
                            skin.$benefits.html(desc);
                        }
                    });

                    if (countryBenefits != null && countryBenefits.length > 0)
                        skin.$benefitsList.prop("disabled", false);
                    else
                        skin.$benefitsList.prop("disabled", true);
                    skin.$benefitsList.selectpicker("refresh");
                    skin.$benefitsList.trigger("change");
                };

                this.loadTerritories = function (country, options) {

                    var skin = this;

                    var defaults = {
                        Country: country,
                    };
                    var opts = $.extend({}, defaults, options);
                    var $btn = skin.$territories.next().find("button.selectpicker");

                    var apiUrl = '/api/Public/GetTerritories'
                    return $.ajax(
                    {
                        type: 'GET',
                        url: apiUrl + '?' + $.param(opts),
                        contentType: 'application/json; charset=utf-8',
                        beforeSend: function () {
                            skin.$area.tempDisable();
                        },
                        complete: function (data) {
                            skin.$area.tempDisable('release');
                            if (data == null || skin.territories == null || skin.territories.length == 0) {
                                skin.TerritoriesValid = false;
                                skin.showTerritories(false);
                                skin.$territories.prop("disabled", true);
                                skin.$territories.selectpicker("refresh");
                            }
                            else {
                                skin.TerritoriesValid = true;
                                skin.showTerritories(true);
                                skin.$territories.prop("disabled", false);
                                skin.$territories.selectpicker("refresh");
                            }
                        }
                    });
                };

                this.showTerritories = function (show) {
                    var $colTerr = this.$territories.closest(".form-group").parent();
                    var $colLoc = this.$location.closest(".form-group").parent();
                    if (show) {
                        $colTerr.show();
                        $colLoc.removeClass("col-md-9");
                        $colLoc.addClass("col-md-6");
                    }
                    else {
                        $colTerr.hide();
                        $colLoc.removeClass("col-md-6");
                        $colLoc.addClass("col-md-9");
                    }
                }

                this.onShown = function (controller) {
                    if (controller.options.statusReadOnly == true) {
                        this.$statuses.prop("disabled", true);
                        this.$statuses.selectpicker("refresh");
                    }
                };

                this.onOk = function (controller) {
                    var skin = this;

                    var data = $.extend({}, {
                    }, this.$area.unbindFromUI());

                    if (!skin.TerritoriesValid)
                        data.States = null;

                    if (!$.isArray(data.Countries))
                        data.Countries = [data.Countries];


                    var onSaveError = function (message) {
                        skin.$error.html(message);
                        skin.$error.show();
                    }

                    var apiUrl = "/api/Corp/PutJobPost";
                    $.ajax(
                    {
                        type: "PUT",
                        url: apiUrl,
                        data: JSON.stringify(data),
                        contentType: "application/json; charset=utf-8",
                        beforeSend: function () {
                            skin.$error.hide();
                            controller.showWaitIndicator(true);
                        },
                        complete: function () {
                            controller.showWaitIndicator(false);
                        },
                        success: function (data) {
                            try {
                                if (data != null && !data.Success)
                                    onSaveError(data.Message);
                                else {
                                    if ($.isFunction(controller.options.onSuccess))
                                        controller.options.onSuccess(data);
                                    controller.hide();
                                }
                            }
                            catch (err) {
                                onSaveError(err);
                            }
                        },
                        error: function (xhr, status, error) {
                            var err = $.getAjaxResponseText(xhr, status, error);
                            var msg = err.Message;
                            if (!$.isNullOrEmpty(err.ExceptionMessage))
                                msg += "<div style='margin-top:15px;'><strong>Exception:</strong><p>" + err.ExceptionMessage + "</p></div>";
                            onSaveError(msg);
                        }
                    });
                    return { delayClose: true };
                }


            },

            confirmDeleteJobPost: function (pkId, name, options) {
                if (pkId == null)
                    return;

                var defaults = {
                    apiUrl: "/api/Corp/DeleteJobPost",
                    title: "Delete Job Posting",
                    description: "Permanently remove the job posting.",
                    message: "<p>Are you sure you want to delete the job post for:</p><p>{0}</p></p>Deleting a job posting cannot be undone and is a permanent operation.</p>" +
                                            "<div class='alert alert-warning'>" +
                                                "<strong>Warning:</strong>&nbsp;You cannot delete a job posting if there are existing applications associated with it. However, you can assign it a <strong>Closed</strong> status to remove it from being displayed to the public." +
                                            "</div>",
                    okText: appStrings.buttons.deleteRecordYes,
                    cancelText: appStrings.buttons.cancel
                };
                var opts = $.extend(true, {}, defaults, options);

                $.globalConfirm({
                    title: opts.title,
                    description: $.stringFormat(opts.description, name),
                    message: $.stringFormat(opts.message, name),
                    image: DIAG_IMAGE_WARN, // default, info, warn, error, or a custom url
                    customOkText: opts.okText,
                    customCancelText: opts.cancelText,
                    callback: function (action, dialogSel, callbackData) {
                        if (action.id == "ok")
                            $.pcorp.deleteJobPost(pkId, name, opts);
                    }
                });
            },

            deleteJobPost: function (pkId, name, options) {
                var defaults = {
                    apiUrl: "/api/Corp/DeleteJobPost",
                    onSuccess: null,
                    showSuccessMessage: true,
                    showFailMessage: true,
                    successMessage: "Delete of job posting <strong>{0}</strong> was successful.",
                    failedMessage: "Delete of job posting <strong>{0}</strong> failed."
                };
                var opts = $.extend(true, {}, defaults, options);

                var apiUrl = opts.apiUrl + "/" + pkId;
                var data = { id: pkId };
                return $.ajax(
                {
                    type: "DELETE",
                    url: apiUrl,
                    contentType: "application/json; charset=utf-8",
                    data: JSON.stringify(data),
                    beforeSend: function () {
                        $().tempDisable();
                    },
                    complete: function () {
                        $().tempDisable("release");
                    },
                    success: function (data) {
                        try {
                            if ($.isFunction(opts.onSuccess))
                                opts.onSuccess(data, { pkId: pkId, name: name }, options);
                            if (data.Success) {
                                if (opts.showSuccessMessage)
                                    $.pageNotify("success", $.isNullOrEmpty(data.Message) ? $.stringFormat(opts.successMessage, name) : data.Message);
                            }
                            else if (opts.showFailMessage)
                                $.pageNotify("error", $.isNullOrEmpty(data.Message) ? $.stringFormat(opts.failedMessage, name) : data.Message);
                        }
                        catch (err) {
                            if (opts.showFailMessage)
                                $.pageNotify("error", err);
                        }
                    },
                    error: function (xhr, status, error) {
                        if (opts.showFailMessage)
                            $.pageNotifyAjaxError(xhr, status, error);
                    }
                });

            },

            ////////////////
            applyForJob: function (jobid, userId, options) {

                var isLoggedIn = (typeof CurrentUser != "undefined" && CurrentUser.id != null && CurrentUser.id != -1);

                if (isLoggedIn) {
                    var uid = userId != null ? userId : CurrentUser.id;

                    var defaults = {
                        apiUrl: $.stringFormat("/api/Corp/GetJobApplication?id={0}&userId={1}&allowAdd=true", jobid == null ? -1 : jobid, uid),
                        queryServer: true,
                        countries: options.countries,
                        queryServer: true,
                        User: {
                            pkId: uid
                        }

                    };
                    var opts = $.extend(true, {}, defaults, options);
                    $(this).pushDialog(function (o) { $("#dialogJobApply").dialogJobApply(o); }, opts);

                }
                else {

                    var returnUrl = $.isNullOrEmpty(options.returnUrl) ? $.url().attr("relative") : options.returnUrl;

                    var ADButtons = "&nbsp;&nbsp;&nbsp;<a class='btn btn-success' href='#' data-action='signup'><i class='fa fa-plus-square'></i>&nbsp;&nbsp;&nbsp;Create a New Account</a>";

                    var $dlg = $.globalConfirm({
                        title: "Login Required",
                        description: "Job application requires a login account.",
                        message: "<p>You are not currently logged into this web site.</p>" +
                                "<p>In order to apply for a job position, you must have a valid login account. The account allows us to identify you, obtain contact and billing details, and provides you with a mechanism to manage your application.</p>" +
                                    "<div class='margin-top-20 text-center'>" +
                                    "<a class='btn btn-primary' href='/pages/public/login.aspx?ReturnUrl=" + returnUrl + "'><i class='fa fa-sign-in'></i>&nbsp;&nbsp;&nbsp;Login To My Existing Account</a>" +
                                    ADButtons +
                                    "</div>",
                        image: "/images/cloud_secure.png",
                        hideOk: true,
                        customCancelText: "Cancel",
                        callback: function (action, $dialog) {
                            switch (action.id) {
                                case "cancel":
                                    $dialog.modal("hide");
                                    break;
                            }
                        },
                        onShown: function ($dialog) {
                            var $signups = $dialog.find("[data-action='signup']");
                            $signups.off("click").on("click", function (e) {
                                e.preventDefault();
                                $dialog.modal("hide");

                                var user = {
                                    pkId: -1
                                };
                                var dlgOptions = {
                                    usernameIsEmail: true,
                                    autogenPassword: false,
                                    revealPassword: true,
                                    passwordEditable: true,
                                    onSuccess: function (data) {
                                    }
                                };


                                $.system.signupNewUser(user, $.extend(true, {}, { countries: options.countries }, dlgOptions));

                            });
                        }
                    });

                }

            },

            confirmDeleteJobApplication: function (pkId, name, options) {
                if (pkId == null)
                    return;

                var defaults = {
                    apiUrl: "/api/Corp/DeleteJobApplication",
                    title: "Delete Job Application",
                    description: "Permanently remove the job application.",
                    message: "<p>Are you sure you want to delete the job application for:</p><p>{0}</p>" +
                                            "<div class='alert alert-warning'>" +
                                                "<strong>Warning:</strong>&nbsp;Deleting a job application also removes any resume data, attachments, and associated history records. This is a <strong>permanent</strong> operation." +
                                            "</div>",
                    okText: appStrings.buttons.deleteRecordYes,
                    cancelText: appStrings.buttons.cancel
                };
                var opts = $.extend(true, {}, defaults, options);

                $.globalConfirm({
                    title: opts.title,
                    description: $.stringFormat(opts.description, name),
                    message: $.stringFormat(opts.message, name),
                    image: DIAG_IMAGE_WARN, // default, info, warn, error, or a custom url
                    customOkText: opts.okText,
                    customCancelText: opts.cancelText,
                    callback: function (action, dialogSel, callbackData) {
                        if (action.id == "ok")
                            $.pcorp.deleteJobApplication(pkId, name, opts);
                    }
                });
            },

            deleteJobApplication: function (pkId, name, options) {
                var defaults = {
                    apiUrl: "/api/Corp/DeleteJobApplication",
                    onSuccess: null,
                    showSuccessMessage: true,
                    showFailMessage: true,
                    successMessage: "Delete of job application <strong>{0}</strong> was successful.",
                    failedMessage: "Delete of job application <strong>{0}</strong> failed."
                };
                var opts = $.extend(true, {}, defaults, options);

                var apiUrl = opts.apiUrl + "/" + pkId;
                var data = { id: pkId };
                return $.ajax(
                {
                    type: "DELETE",
                    url: apiUrl,
                    contentType: "application/json; charset=utf-8",
                    data: JSON.stringify(data),
                    beforeSend: function () {
                        $().tempDisable();
                    },
                    complete: function () {
                        $().tempDisable("release");
                    },
                    success: function (data) {
                        try {
                            if ($.isFunction(opts.onSuccess))
                                opts.onSuccess(data, { pkId: pkId, name: name }, options);
                            if (data.Success) {
                                if (opts.showSuccessMessage)
                                    $.pageNotify("success", $.isNullOrEmpty(data.Message) ? $.stringFormat(opts.successMessage, name) : data.Message);
                            }
                            else if (opts.showFailMessage)
                                $.pageNotify("error", $.isNullOrEmpty(data.Message) ? $.stringFormat(opts.failedMessage, name) : data.Message);
                        }
                        catch (err) {
                            if (opts.showFailMessage)
                                $.pageNotify("error", err);
                        }
                    },
                    error: function (xhr, status, error) {
                        if (opts.showFailMessage)
                            $.pageNotifyAjaxError(xhr, status, error);
                    }
                });

            },


            jobApplyDialogSkin: function () {

                this.data = null;

                this.getOptionOverrides = function (options) {
                    var skin = this;

                    return {
                        title: "Application For Job Opening",
                        description: "Please use the form below to supply us with information about the job candidate.",
                        HeaderTemplate: "<div class='dialogTitle row'>" +
                                                "<div class='col-md-1'><img src='/apple-touch-icon-60x60.png' /></div>" +
                                                "<div class='col-md-11'><h3>{title}</h3><h4>{description}<h4></div>" +
                                            "</div>",
                        contentTemplate: "<div id='{containerId}Area' name='{containerId}Area'>" +

                                            "<div id='{containerId}SuccessMsg' class='alert alert-success margin-bottom-10' role='alert' style='display:none;'></div>" +
                                            "<div id='{containerId}ErrorMsg' class='alert alert-danger margin-bottom-10' role='alert' style='display:none;'></div>" +
                                            "<ul class='nav nav-tabs' id='{containerId}Tabs'>" +
                                                "<li class='active'><a data-toggle='tab' href='#{containerId}TabInfo'><i class='fa fa-list'></i>&nbsp;&nbsp;&nbsp;General</a></li>" +
                                                "<li ><a data-toggle='tab' href='#{containerId}TabCover'><i class='fa fa-file-text'></i>&nbsp;&nbsp;&nbsp;Cover Letter</a></li>" +
                                                "<li ><a data-toggle='tab' href='#{containerId}TabResume'><i class='fa fa-file-text'></i>&nbsp;&nbsp;&nbsp;Resume</a></li>" +
                                            "</ul>" +
                                            "<div class='tab-content' style='padding-top:10px;overflow:visible;'>" +
                                                // General
                                                "<div class='tab-pane active' id='{containerId}TabInfo' >" +
                                                    "<input type='hidden' data-bind='true' data-field='pkId' />" +
                                                    "<input type='hidden' data-bind='true' data-field='fkJobId' />" +
                                                    "<input type='hidden' data-bind='true' data-field='fkUserId' />" +
                                                    "<div class='row row-equal-height'>" +
                                                        "<div class='col-md-6' id='{containerId}ContactInfo'>" +
                                                            "<label>Applicant Contact Details</label>" +
                                                            "<div class='col-head'>" +
                                                                "<div style='font-weight: bold;white-space:nowrap;' data-bind='true' data-field='contact'></div>" +
                                                            "</div>" +
                                                        "</div>" +
                                                        "<div class='col-md-6' id='{containerId}JobInfo'>" +
                                                            "<label>Position Applied For</label>" +
                                                            "<div class='col-head'>" +
                                                                "<h5 data-bind='true' data-field='jobFields.Name' style='font-weight:bold;'></h5>" +
                                                                "<div style='font-weight: bold;white-space:nowrap;' data-bind='true' data-field='jobFields.countryNames'></div>" +
                                                                "<div><strong data-bind='true' data-field='jobFields.divisionNames'></strong></div>" +
                                                                "<div data-bind='true' data-field='jobFields.Locations'></div>" +
                                                            "</div>" +
                                                        "</div>" +
                                                    "</div>" +
                                                    "<div class='row margin-top-20'>" +
                                                        "<div class='col-md-6'>" +
                                                            "<label>Applied For This Position On</label><small class='pull-right text-muted'></small>" +
                                                            "<div class='text-info' id='{containerId}FirstApplied' name='{containerId}FirstApplied' data-bind='true' data-field='applied'></div>" +
                                                        "</div>" +
                                                        "<div class='col-md-6'>" +
                                                            "<label>Application Status</label><small class='pull-right text-muted'>viewable by applicant</small>" +
                                                            "<input  id='{containerId}Status' name='{containerId}Status' class='form-control' type='text' placeholder='' data-bind='true' data-field='Status' >" +
                                                        "</div>" +
                                                    "</div>" +
                                                    "<div id='{containerId}InternalUse' class='row margin-top-20 internal-use'>" +
                                                        "<div class='col-md-12'>" +
                                                            "<h2>For Internal Use Only (not visible to public)</h2>" +
                                                            "<div class='row'>" +
                                                                "<div class='col-md-8'>" +
                                                                    "<div class='form-group'>" +
                                                                        "<label for='{containerId}StatusList'>Internal Status</label>&nbsp;&nbsp;&nbsp;<span id='{containerId}StatusMsg' class='text-info'></span>" +
                                                                        "<select id='{containerId}StatusList' class='form-control selectpicker show-tick' data-style='btn-primary' data-bind='true' data-field='fkStatusId'></select>" +
                                                                    "</div>" +
                                                                "</div>" +
                                                                "<div class='col-md-2'>" +
                                                                    "<div class='form-group hide'>" +
                                                                        "<label class='pull-right' for='{containerId}Marked'>Marked</label>" +
                                                                        "<br />" +
                                                                        "<div id='{containerId}Marked' class='btn-group' data-toggle='buttons' data-bind='true' data-field='IntMarked' data-control='true'>" +
                                                                            "<label class='btn btn-toggle'>" +
                                                                                "<input type='radio' name='MrkEnable' id='TRVEnable' data-value='true' data-type='boolean' />Yes" +
                                                                            "</label>" +
                                                                            "<label class='btn btn-toggle btn-no'>" +
                                                                                "<input type='radio' name='MrkDisable' id='TRVDisable' data-value='false' data-type='boolean' />No" +
                                                                            "</label>" +
                                                                        "</div>" +
                                                                    "</div>" +
                                                                "</div>" +
                                                                "<div class='col-md-2'>" +
                                                                    "<div class='form-group'>" +
                                                                        "<label class='pull-right' for='{containerId}Followup'>Needs Followup</label>" +
                                                                        "<br />" +
                                                                        "<div id='{containerId}Followup' class='btn-group' data-toggle='buttons' data-bind='true' data-field='IntFollowup' data-control='true'>" +
                                                                            "<label class='btn btn-toggle'>" +
                                                                                "<input type='radio' name='FollowEnable' id='TRVEnable' data-value='true' data-type='boolean' />Yes" +
                                                                            "</label>" +
                                                                            "<label class='btn btn-toggle btn-no'>" +
                                                                                "<input type='radio' name='FollowDisable' id='TRVDisable' data-value='false' data-type='boolean' />No" +
                                                                            "</label>" +
                                                                        "</div>" +
                                                                    "</div>" +
                                                                "</div>" +
                                                            "</div>" +
                                                            "<div class='row'>" +
                                                                "<div class='col-md-12'>" +
                                                                    "<div class='form-group'>" +
                                                                        "<label>Comments</label><small class='pull-right text-muted'>general comments about applicant</small>" +
                                                                        "<div id='{containerId}IntComment' name='{containerId}IntComment' data-control='summernote' editor-buttons='print' editor-height='100' editor-maxHeight='125x' data-bind='true' data-field='IntComment'></div>" +
                                                                    "</div>" +
                                                                "</div>" +
                                                            "</div>" +
                                                        "</div>" +
                                                    "</div>" +
                                                "</div>" +
                                                // Cover Letter
                                                 "<div id='{containerId}TabCover' class='tab-pane' style='min-height:425px;'>" +
                                                    "<div class='row'>" +
                                                        "<div class='col-md-12'>" +
                                                            "<div class='form-group'>" +
                                                                "<label>Cover Letter</label><span class='text-danger' style='display:none;'>&nbsp;&nbsp;&nbsp;A cover letter is required.</span><small class='pull-right text-muted'>use this space to introduce yourself</small>" +
                                                                "<div id='{containerId}CoverLetter' name='{containerId}CoverLetter' data-control='summernote' editor-buttons='print' editor-height='400' editor-maxHeight='385px' data-bind='true' data-field='CoverLetter'></div>" +
                                                            "</div>" +
                                                        "</div>" +
                                                    "</div>" +
                                                "</div>" +
                                                // Description
                                                 "<div id='{containerId}TabResume' class='tab-pane' style='min-height:425px;'>" +
                                                    "<div class='row'>" +
                                                        "<div class='col-md-12'>" +
                                                            "<div class='form-group'>" +
                                                                "<label>Resume</label><span class='text-danger' style='display:none;'>&nbsp;&nbsp;&nbsp;A resume is required.</span><small class='pull-right text-muted'>copy/paste a text version of your resume</small>" +
                                                                "<div id='{containerId}Resume' name='{containerId}Resume' data-control='summernote' editor-buttons='print' editor-height='400' editor-maxHeight='385px' data-bind='true' data-field='Resume'></div>" +
                                                            "</div>" +
                                                        "</div>" +
                                                    "</div>" +
                                                "</div>" +

                                           "</div>" +
                                         "</div>",
                        showOkButton: true,
                        okText: "<i class='fa fa-save'></i>&nbsp;&nbsp;&nbsp;Save Application",
                        cancelText: appStrings.buttons.cancel,
                        validate: true,
                        validateAsTabs: true,
                        validationRules: {
                            dialogJobApplyCoverLetter: {
                                required: true
                            }
                        }
                    };
                }
                this.getDynamicValidationMessages = function (controller) {
                    return {
                        dialogJobPostStates: "Select one or more to aid in job searches.",
                        dialogJobPostLocations: "Descriptive location(s) of the expected job area is required."
                    };
                }

                // Called before the dialog is ready to show its data.
                this.onPreShow = function (success, record, controller, dialogSelector, errorMessage) {
                    var skin = this;

                    this.data = record != null ? record.Data : null;
                    this.isForNew = this.data == null || this.data.pkId == null || this.data.pkId <= 0;
                    if (this.data == null) {
                        this.data = {
                            pkId: -1,
                            job: {
                                pkId: -1
                            }
                        };
                    }
                    this.job = this.data.Job;
                    this.contact = this.data.Contact;

                    this.hasAdminRights = $.hasRight(appStrings.rights.AppsJobsEdit);

                    this.$area = $("#" + controller.containerId + "Area");
                    this.$wait = this.$area.find("#wait");
                    this.$error = controller.findUniqueControl("ErrorMsg");
                    this.$success = controller.findUniqueControl("SuccessMsg");
                    controller.okBtn.removeClass("btn-primary").addClass("btn-success");

                    this.$status = controller.findUniqueControl("Status");

                    this.$internal = controller.findUniqueControl("InternalUse");
                    if (!this.hasAdminRights) {
                        this.$internal.remove();
                        this.$status.attr("tempdis", true);
                        this.$status.parent().find(".text-muted").hide();
                    }
                    else {

                        this.$status.attr("disabled", false);
                        this.$status.parent().find(".text-muted").show();
                        this.$statusMsg = controller.findUniqueControl("StatusMsg");
                        this.$statuses = controller.findUniqueControl("StatusList");
                        skin.$statuses.corpJobApplicationStatusList({
                            preselect: JOB_APP_STATUS_NOTREVIEWED,
                            data: controller.options.appStatus,
                            onChange: function ($sel, $option, item) {
                                skin.$statusMsg.html(item.Description)
                            }
                        });
                    }

                    var $editors = this.$area.find("[data-control='summernote']");
                    $editors.attr("editor-readOnly", this.isReadOnly);
                    var $fullpageEditors = this.$area.find("[data-control='summernote']:not([data-field='IntComment'])");
                    $fullpageEditors.attr("editor-height", fullHeight);
                    $fullpageEditors.attr("editor-maxHeight", maxHeight);


                    var jobFields = $.pcorp.getFormattedJobFields(this.job);
                    this.$jobinfo = controller.findUniqueControl("JobInfo");

                    var contactHtml = $.pcorp.getFormattedContact(this.contact, { nameFormat: "<span class='strong-primary'>{0}</span>" });
                    this.$contact = controller.findUniqueControl("ContactInfo");


                    var fullHeight = this.hasAdminRights ? 550 : 300;
                    var maxHeight = this.hasAdminRights ? 625 : 375;

                    this.$intcomment = controller.findUniqueControl("IntComment");

                    this.$cover = controller.findUniqueControl("CoverLetter");
                    this.$coverError = this.$cover.parent().find("span.text-danger");
                    this.$coverTab = controller.findUniqueTab("TabCover");

                    this.$resume = controller.findUniqueControl("Resume");
                    this.$resumeError = this.$resume.parent().find("span.text-danger");
                    this.$resumeTab = controller.findUniqueTab("TabResume");


                    this.$area.bindToUI($.extend(true, {}, this.data, {
                        applied: this.isForNew ? "(not yet submitted)" : moment(this.data.Created).format("llll"),
                        Status: (!this.hasAdminRights && this.data.Status == null) ? "(none)" : this.data.Status,
                        contact: contactHtml,
                        jobFields: jobFields
                    }));

                    this.$tabInfo = controller.findUniqueControl("TabInfo");
                    this.$tabCover = controller.findUniqueControl("TabCover");
                    this.$tabResume = controller.findUniqueControl("TabResume");

                    this.$tabs = this.$area.find(".tab-pane");
                    this.$tabs.height(maxHeight);


                };

                this.onShown = function (controller) {
                };

                this.onOk = function (controller) {
                    var skin = this;

                    var data = $.extend({}, {
                    }, this.$area.unbindFromUI());


                    if (!skin.hasAdminRights)
                        data.IntComment = null

                    var cancel = false;

                    if ($.isNullOrEmpty(data.CoverLetter)) {
                        this.$coverError.show();
                        this.$coverTab.addClass("text-danger");
                        this.$coverTab.trigger("click");
                        cancel = true;
                    }
                    else {
                        this.$coverError.hide();
                        this.$coverTab.removeClass("text-danger");
                    }

                    if ($.isNullOrEmpty(data.Resume)) {
                        this.$resumeError.show();
                        this.$resumeTab.addClass("text-danger");
                        this.$resumeTab.trigger("click");
                        cancel = true;
                    }
                    else {
                        this.$resumeError.hide();
                        this.$resumeTab.removeClass("text-danger");
                    }

                    if (cancel)
                        return { cancel: true };

                    var onSaveError = function (message) {
                        skin.$error.html(message);
                        skin.$error.show();
                    }

                    var apiUrl = "/api/Corp/PutJobApplication";
                    $.ajax(
                    {
                        type: "PUT",
                        url: apiUrl,
                        data: JSON.stringify(data),
                        contentType: "application/json; charset=utf-8",
                        beforeSend: function () {
                            skin.$error.hide();
                            controller.showWaitIndicator(true);
                        },
                        complete: function () {
                            controller.showWaitIndicator(false);
                        },
                        success: function (data) {
                            try {
                                if (data != null && !data.Success)
                                    onSaveError(data.Message);
                                else {
                                    if ($.isFunction(controller.options.onSuccess))
                                        controller.options.onSuccess(data);
                                    controller.hide();
                                    $.pageNotify("success", "<strong>Thanks!</strong> Your application was successfully saved and submitted.");
                                }
                            }
                            catch (err) {
                                onSaveError(err);
                            }
                        },
                        error: function (xhr, status, error) {
                            var err = $.getAjaxResponseText(xhr, status, error);
                            var msg = err.Message;
                            if (!$.isNullOrEmpty(err.ExceptionMessage))
                                msg += "<div style='margin-top:15px;'><strong>Exception:</strong><p>" + err.ExceptionMessage + "</p></div>";
                            onSaveError(msg);
                        }
                    });
                    return { delayClose: true };
                }
            },

            getFormattedContact: function (contact, options) {

                var defaults = {
                    showName: true,
                    showCompany: true,
                    showEmail: true,
                    showTitle: true,
                    linebreak: "<br>",
                    nameFormat: "<strong>{0}</strong>",
                    companyFormat: "<strong>{0}</strong>",
                    titleFormat: "<i>{0}</i>",
                    emailFormat: "<i class='fa fa-envelope'></i>&nbsp;&nbsp;&nbsp;<a href='mailto:{0}' class='dashed'>{0}</a>",
                    phoneFormat: "<i class='fa fa-phone'></i>&nbsp;&nbsp;&nbsp;{0}",
                    mobileFormat: "<i class='fa fa-mobile-phone'></i>&nbsp;&nbsp;&nbsp;{0}"
                };
                var opts = $.extend(true, {}, defaults, options);

                if (contact == null)
                    return contact;

                var html = "";
                if (opts.showName) {
                    if (!$.isNullOrEmpty(contact.name)) {
                        if (!$.isNullOrEmpty(html))
                            html += opts.linebreak;
                        html += $.stringFormat(opts.nameFormat, contact.name);
                    }
                }

                if (opts.showTitle) {
                    if (!$.isNullOrEmpty(contact.title)) {
                        if (!$.isNullOrEmpty(html))
                            html += opts.linebreak;
                        html += $.stringFormat(opts.titleFormat, contact.title);
                    }
                }

                if (opts.showCompany) {
                    if (!$.isNullOrEmpty(contact.companyName)) {
                        if (!$.isNullOrEmpty(html))
                            html += opts.linebreak;
                        html += $.stringFormat(opts.companyFormat, contact.companyName);
                    }
                }


                if (!$.isNullOrEmpty(contact.address)) {
                    if (!$.isNullOrEmpty(html))
                        html += opts.linebreak;
                    html += contact.address;
                }

                var line = "";

                if (!$.isNullOrEmpty(contact.city))
                    line += contact.city;

                var state = $.isNullOrEmpty(contact.statename) ? contact.state : contact.statename;
                if (!$.isNullOrEmpty(state)) {
                    if (!$.isNullOrEmpty(line))
                        line += ", ";
                    line += state;
                }

                if (!$.isNullOrEmpty(contact.postal)) {
                    if (!$.isNullOrEmpty(line))
                        line += " ";
                    line += contact.postal;
                }

                if (!$.isNullOrEmpty(line)) {
                    html += opts.linebreak;
                    html += line;
                }

                var country = $.isNullOrEmpty(contact.countryName) ? contact.country : contact.countryName;
                if (!$.isNullOrEmpty(country)) {
                    if (!$.isNullOrEmpty(line))
                        line += opts.linebreak;
                    line += country;
                }

                if (opts.showEmail) {
                    if (!$.isNullOrEmpty(contact.email)) {
                        if (!$.isNullOrEmpty(html))
                            html += opts.linebreak;
                        html += $.stringFormat(opts.emailFormat, contact.email);
                    }
                }

                if (!$.isNullOrEmpty(contact.phone)) {
                    if (!$.isNullOrEmpty(html))
                        html += opts.linebreak;
                    html += $.stringFormat(opts.phoneFormat, contact.phone);
                }

                if (!$.isNullOrEmpty(contact.mobile)) {
                    if (!$.isNullOrEmpty(html))
                        html += opts.linebreak;
                    html += $.stringFormat(opts.mobileFormat, contact.mobile);
                }

                return html;
            },

            getFormattedJobFields: function (job) {

                if (job == null || job.Success == false) {
                    var fields = {
                        Message: (job == null || $.isNullOrEmpty(job.Message)) ? "The requested job listing was not found." : job.Message
                    };
                    return fields;
                }

                var countryList = "";
                if (job.CountryList != null) {
                    $.each(job.CountryList, function (i, country) {
                        if (!$.isNullOrEmpty(countryList))
                            countryList += " | ";
                        countryList += country.Name;
                    });
                }

                var divisionList = "";
                if (job.Divisions != null) {
                    $.each(job.Divisions, function (i, division) {
                        if (!$.isNullOrEmpty(divisionList))
                            divisionList += " | ";
                        divisionList += $.findNameValue(division);
                    });
                }
                if ($.isNullOrEmpty(divisionList))
                    divisionList = "Phillips Corporation";

                var postUrl = $.absoluteUrl("/pages/public/job.aspx", { id: job.Token });

                var $table = $("<table class='table table-bordered table-condensed' style='font-size:12px;'><thead><th colspan='2'>Position Information</th></thead><tbody></tbody></table>");
                var $body = $table.find("tbody");
                var rowTemplate = "<tr><td style='font-weight:bold;'>{0}</td><td>{1}</td></tr>";

                if (!$.isNullOrEmpty(job.Category))
                    $body.append($.stringFormat(rowTemplate, "Category", job.Category));
                if (!$.isNullOrEmpty(job.EmploymentType))
                    $body.append($.stringFormat(rowTemplate, "Employment Type", job.EmploymentType));
                if (!$.isNullOrEmpty(job.EmploymentTerm))
                    $body.append($.stringFormat(rowTemplate, "Employment Term", job.EmploymentTerm));
                if (!$.isNullOrEmpty(job.SalaryRange))
                    $body.append($.stringFormat(rowTemplate, "Salary Range", job.SalaryRange));
                if (!$.isNullOrEmpty(job.WorkHours))
                    $body.append($.stringFormat(rowTemplate, "Work Hours", job.WorkHours));
                if (!$.isNullOrEmpty(job.HoursPerWeek))
                    $body.append($.stringFormat(rowTemplate, "Hours/Week", job.HoursPerWeek));

                if (!$.isNullOrEmpty(job.RequiredEducation))
                    $body.append($.stringFormat(rowTemplate, "Required Education", job.RequiredEducation));
                if (!$.isNullOrEmpty(job.RequiredExperience))
                    $body.append($.stringFormat(rowTemplate, "Required Experience", job.RequiredExperience));
                if (!$.isNullOrEmpty(job.RequiredSecurityClearance))
                    $body.append($.stringFormat(rowTemplate, "Required Security Clearance", job.RequiredSecurityClearance));
                $body.append($.stringFormat(rowTemplate, "Travel Required", job.TravelRequired ? "Yes" : "No"));

                var fields = $.extend(true, {}, job, {
                    postUrl: postUrl,
                    countryNames: countryList,
                    divisionNames: divisionList,
                    start: $.isNullOrEmpty(job.StartDate) ? "" : $.stringFormat("Starting Date: {0}", job.StartDate),
                    detailsTable: $table.html()
                });

                return fields;
            },

            editRedirectUrl: function (id, forNew, options) {

                var defaults = {
                    userId: CurrentUser.id,
                    onSuccess: function (result, data) {
                        if (options && $.isFunction(options.onSuccess))
                            options.onSuccess(result, data, options);
                    },
                    apiUrl: "/api/AppSettings/GetRedirectUrls/" + (forNew ? "-1" : id),
                    apiEditUrl: "/api/AppSettings/PutRedirectUrls",
                };
                var opts = $.extend(true, {}, defaults, options);

                $("#commonEditRedirectUrls").dialogEditRedirectUrls(opts);
            },

            redirectUrlsEditDialogSkin: function (options) {

                this.data = null;

                this.getOptionOverrides = function (options) {
                    var skin = this;
                    return {
                        title: "Redirect Urls",
                        description: "Please use the form below to modify/add a url redirect record.",
                        HeaderTemplate: "<div class='dialogTitle row'>" +
                                                "<div class='col-md-1'><img src='/apple-touch-icon-60x60.png' /></div>" +
                                                "<div class='col-md-11'><h3>{title}: <span class='text-info' id='{containerId}Name'></span></h3><h4>{description}<h4></div>" +
                                            "</div>",
                        contentTemplate: "<div style=\"height:125px !Important;\" id='{containerId}Area' name='{containerId}Area'>" +

                                            "<div id='{containerId}SuccessMsg' class='alert alert-success margin-bottom-10' role='alert' style='display:none;'></div>" +
                                            "<div id='{containerId}ErrorMsg' class='alert alert-danger margin-bottom-10' role='alert' style='display:none;'></div>" +

                                            "<div class='row'>" +
                                                "<div class='col-md-12'>" +
                                                    "<p><strong>Note:</strong> You may use the tilda character ~ to denote the root domain name in order to create a redirect relative to the current site.</p>" +
                                                "</div>" +
                                            "</div>" +
                                            "<div class='row'>" +
                                                "<div class='col-md-6'>" +
                                                    "<div class='form-group'>" +
                                                            "<input type='hidden' data-bind='true' data-field='pkId' />" +
                                                            "<label for='{containerId}Key'>Original URL</label>" +
                                                            "<input   id='{containerId}Key' name='{containerId}Key' class='form-control col-md-4' type='text' data-bind='true' data-field='Name' >" +
                                                    "</div>" +
                                                "</div>" +
                                                "<div class='col-md-6'>" +
                                                    "<div class='form-group'>" +
                                                            "<label for='{containerId}Url'>Redirect to this new URL:</label>" +
                                                            "<input  id='{containerId}Url' name='{containerId}Url' class='form-control col-md-6' type='text' data-bind='true' data-field='RedirectUrl' >" +
                                                    "</div>" +
                                                "</div>" +
                                            "</div>" +
                                         "</div>"
                        ,

                        footerTemplate: "<div>" +
                                            "<button id='{containerId}Ok' class='btn btn-primary' aria-hidden='true'>{okText}</button>" +
                                            "<button id='{containerId}Cancel' class='btn' aria-hidden='true'>{cancelText}</button>" +
                                        "</div>",
                        showOkButton: true,
                        okText: "<i class='fa fa-save'></i>&nbsp;&nbsp;&nbsp;Save",
                        cancelText: appStrings.buttons.cancel,
                        validate: true,
                        validateAsTabs: false,
                        validationRules: {
                            commonEditRedirectUrlsUrl: {
                                required: true,
                            },
                            commonEditRedirectUrlsKey: {
                                required: true,
                            }
                        }
                    };
                }
                this.getDynamicValidationMessages = function (controller) {

                    return {
                    };
                }

                // Called before the dialog is ready to show its data.
                this.onPreShow = function (success, record, controller, dialogSelector, errorMessage) {
                    var skin = this;
                    this.data = record != null ? record : null;
                    this.isForNew = this.data == null || this.data.pkId == null || this.data.pkId <= 0;

                    if (this.data == null) {
                        this.data = {
                            pkId: -1
                        };
                    }
                    this.$area = $("#" + controller.containerId + "Area");
                    this.$wait = this.$area.find("#wait");
                    this.$error = controller.findUniqueControl("ErrorMsg");
                    this.$success = controller.findUniqueControl("SuccessMsg");

                    this.$content = controller.findUniqueControl("Content");
                    this.$key = controller.findUniqueControl("Key");
                    this.$url = controller.findUniqueControl("Url");

                    if (skin.dialog.options != null && skin.dialog.options.isReadOnly != null) {
                        if (skin.dialog.options.isReadOnly) {
                            this.isReadOnly = skin.dialog.options.isReadOnly;
                            skin.dialog.okBtn.attr("tempdis", true);
                            skin.dialog.okBtn.prop("disabled", true);
                            skin.dialog.okBtn.remove();


                            this.$url.attr("tempdis", true);
                            this.$url.prop("disabled", true);
                            this.$key.attr("tempdis", true);
                            this.$key.prop("disabled", true);

                            this.$content.attr("tempdis", true);
                            this.$content.attr("disabled", true);

                            this.dialog.cancelBtn.html(appStrings.buttons.close);
                        }
                    }
                    else
                        skin.dialog.options.isReadOnly = false;

                    this.$area.bindToUI($.extend(true, {}, this.data));
                };

                this.onOk = function (controller) {
                    var skin = this;
                    var fields = this.$area.unbindFromUI();

                    var defaults = {
                        url: "/api/AppSettings/PutRedirectUrl",
                        data: fields,
                    };
                    var opts = $.extend(true, {}, defaults);
                    $.when(skin.$area.webApiSave(opts, fields)).done(function (data) {
                        controller.options.onSuccess(skin.isForNew, data, controller, 'update');
                    });
                };


            },

            //////////////////
            showCorpAffirmation: function (affirmation, options) {

                var defaults = {
                    record: affirmation,
                    onSuccess: function (result, data) {
                        if (options && $.isFunction(options.onSuccess))
                            options.onSuccess(result, data, options);
                    },
                    isReadOnly: false,
                };
                var opts = $.extend(true, {}, defaults, options);
                $("#commonCorpAffirmation").dialogCorpAffirmation(opts);
            },

            corpAffirmationDialogSkin: function () {

                this.data = null;

                this.getOptionOverrides = function (options) {
                    var skin = this;
                    return {
                        title: "<span id='{containerId}Title'></span>",
                        description: "<span id='{containerId}Description'></span>",
                        HeaderTemplate: "<div class='dialogTitle row'>" +
                                                "<div class='col-md-1'><img src='/images/performance/resource_human-new.png' /></div>" +
                                                "<div class='col-md-9'><h3>{title} <span class='text-info' id='{containerId}Name'></span></h3><h4>{description}<h4></div>" +
                                                "<div class='col-md-2' >" +
                                                    "<div class='form-group' style='margin-top-10'>" +
                                                        "<select id='{containerId}LanguageList' class='form-control selectpicker show-tick pull-right' data-style='btn-default'></select>" +
                                                    "</div>" +
                                                "</div>" +
                                            "</div>",
                        contentTemplate: "<div style=\"height:500px !Important;\" id='{containerId}Area' name='{containerId}Area'>" +

                                            "<div id='{containerId}SuccessMsg' class='alert alert-success margin-bottom-10' role='alert' style='display:none;'></div>" +
                                            "<div id='{containerId}ErrorMsg' class='alert alert-danger margin-bottom-10' role='alert' style='display:none;'></div>" +

                                            "<div data-area='affirm-form' class='row'>" +
                                                "<div class='col-md-12'>" +
                                                    "<div class='form-group'>" +
                                                        "<div id='{containerId}Content' name='{containerId}Content' style='overflow-y:scroll;max-height:500px;'></div>" +
                                                    "</div>" +
                                                "</div>" +
                                            "</div>"
                        ,

                        footerTemplate: "<div>" +
                                        "</div>" +
                                        "<div>" +
                                            "<button id='{containerId}Ok' class='btn btn-primary' aria-hidden='true'>{okText}</button>" +
                                            "<button id='{containerId}Cancel' class='btn' aria-hidden='true'>{cancelText}</button>" +
                                        "</div>",
                        showOkButton: true,
                        showCancelButton: false,
                        okText: "<i class='fa fa-save'></i>&nbsp;&nbsp;&nbsp;Save and Dismiss",
                        cancelText: appStrings.buttons.cancel,
                        validate: true,
                        validateAsTabs: false,
                        validationRules: {
                        }
                    };
                }
                this.getDynamicValidationMessages = function (controller) {

                    return {
                    };
                }

                // Called before the dialog is ready to show its data.
                this.onPreShow = function (success, record, controller, dialogSelector, errorMessage) {
                    var skin = this;
                    this.data = record;

                    this.$area = $("#" + controller.containerId + "Area");
                    this.$wait = this.$area.find("#wait");
                    this.$error = controller.findUniqueControl("ErrorMsg");
                    this.$success = controller.findUniqueControl("SuccessMsg");
                    this.$content = controller.findUniqueControl("Content");
                    this.$languages = controller.findUniqueControl("LanguageList");
                    this.$form = this.$area.find("[data-area='affirm-form']");
                    this.$title = controller.findUniqueControl("Title");
                    this.$description = controller.findUniqueControl("Description");

                    this.$title.html(this.data.Affirmation.Name);
                    this.$description.html(this.data.Affirmation.Description);

                    var languages = this.data.Content.selectMany(function (c) { return c.Language; });

                    if (languages != null && languages.length > 0) {
                        skin.$languages.cmsContentLanguagesList({
                            data: languages,
                            preselect: [(CurrentLanguage == null || CurrentLanguage.id == null) ? DEFAULT_LANGUAGE_ID : CurrentLanguage.id],
                            onChange: function ($select, $option, item, selections) {
                                skin.onShowNewLanguage(item);
                            }
                        });
                        skin.$languages.parent().show();
                        skin.$languages.trigger("change");
                    }
                    else
                        this.onShowNewLanguage(this.data.Content[0]);

                };

                this.onShowNewLanguage = function (language) {
                    var content = this.data.Content.any({ fkLanguageId: language.pkId });
                    this.$content.html(content.Content);
                    var values = {
                    };
                    if (this.data != null && this.data.Answers != null) {
                        this.data.Answers.forEach(function (a) {
                            values[a.QuestionIdentifier] = a.Answer;
                        });
                    }
                    this.$content.bindToUI(values, {allow3StateToggles: true});

                };

                this.onOk = function (controller) {
                    var skin = this;

                    var onSaveError = function (message) {
                        skin.$error.html(message);
                        skin.$error.show();
                    }

                    // Yes... dynamically create validations for ... dynamic content!
                    var state = {
                        areaErrorCss: "",
                        $form: skin.$form,
                        validations: [
                        ]
                    };
                    var labels = {};

                    skin.$form.find("[data-bind='true']").filter(
                        function () {
                            var val = {
                                field: $(this).attr("id")
                            };
                            var data = $(this).data();
                            for (var property in data) {
                                var pos = property.indexOf("rule");
                                if (pos == 0) {
                                    var prop = property.substr(pos + 4, property.length - pos + 4);
                                    prop = prop.substr(0, 1).toLowerCase() + prop.substr(1);
                                    val[prop] = data[property];
                                }
                            }
                            if (val.message == null)
                                val.message = "Please supply an answer.";
                            labels[val.field] = val.label == null ? skin.$form.find("[for='" + $(this).attr("id") + "']").text() : val.label;
                            if (labels[val.field] != null)
                                labels[val.field] = labels[val.field].trim();

                            state.validations.push(val);
                        }
                    );

                    var valRes = this.$content.unbindAndValidate(state);
                    if (!valRes.isValid) {
                        var numErrors = valRes.errors.length;
                        var $firstError = valRes.errors[0].$elem;
                        onSaveError($.stringFormat("{0} question{1} require{2} an answer before you may continue. Please review all questions before submitting.", numErrors, numErrors == 1 ? "" : "s", numErrors == 1 ? "s" : ""));

                        var offset = $firstError.offset();
                        skin.$content.animate({
                            scrollTop: offset.top
                        });

                        return { cancel: true };
                    }
                    skin.$error.hide();

                    var answers = [];
                    for (var prop in valRes.fields) {
                        var previousId = $.isArray(skin.data.Answers) ? skin.data.Answers.any({ QuestionIdentifier: prop }, -1).pkId : -1;
                        var answer = {
                            pkId: previousId,
                            fkCorpAffirmationPartnerId: skin.data.PartnerState.pkId,
                            QuestionIdentifier: prop,
                            Answer: valRes.fields[prop],
                            Label: labels == null ? null : labels[prop]
                        };
                        answers.push(answer);
                    };

                    var data = {
                        Complete: true,
                        Answers: answers,
                        fkPartnerId: skin.data.PartnerState.fkPartnerId,
                        fkCorpAffirmationId: skin.data.Affirmation.pkId,
                    };

                     var options = {
                         url: "/api/CorpPerformance/PutPartnerAffirmation",
                        rebindOnSuccess: false,
                        notifySuccessMessage: "<strong>Affirmation</strong> was successfully completed.",
                    };

                     skin.$error.hide();

                     $.when(skin.$area.webApiSave(options, data)).fail(function (xhr, status, error) {
                         onSaveError($.getAjaxResponseText(xhr, status, error).Message);
                     }).done(function (data) {
                         if(data!=null && data.Success)
                             controller.hide();
                         else
                             onSaveError(data.Message);
                     });

                    return { delayClose: true };
                }
            }
        }
    });
})(jQuery);

(function ($) {

    //Attatch the new method
    jQuery.fn.extend({
        addPartnerTeamsView: function (options) {
            var page = this;
            var displayDefaults = {
                dateFormat: "llll"
            };
            var selectedTeam = '';
            var highlightGrid = function ($row) {
                if (selectedTeam != '')
                    selectedTeam.removeClass("success");
                $row.addClass("success");
            }

            var actions = options.actions != null ? options.actions : [
                {
                    id: 0,
                    name: appStrings.grid.action.select,
                    short: "select",
                    icon: "<i class='fa fa-edit'></i>&nbsp;",
                    execute: function (view, item, $item, column, opts) {
                        var $teams = $("#Teams");
                        $teams.attr("data-teamid", item.TeamId);
                        var $row = $item.parents("tr");
                        highlightGrid($row);
                        selectedTeam = $row;
                    }
                },
            ];

            if (typeof (options) == 'string') {
                var selector = $(this[0]);
                var controller = selector.data("multiview");
                return controller.onAction.apply(controller, arguments);
            }
            else { // init with options
                var opts = options;
                var defaults = {
                    sortMethod: "server",
                    contentHeight: 500,
                    infiniteScroll: false,
                    queryServer: true,
                    queryUrlCallback: function (controller, index, sortFields) {
                        var params = {
                           id: 0
                        };
                        return "/api/CorpPerformance/GetTeamListing?" + $.param(params);

                    },

                    tools: [
                             {
                                 standard: "refresh",
                                 options: { css: "btn btn-xs pull-right", title: "Refresh Data", icon: "fa-refresh" }
                             }
                    ],
                    columns: [
                        { id: "DivisionId", name: "divid", type: "number", css: "hide" },
                        { id: "TeamId", name: "teamId", type: "text", css: "hide" },
                        { id: "Actions", name: "", type: "text", style: "button", actions: actions, width: 75 },
                        { id: "DivisionName", name: "Division", type: "text" },
                        { id: "TeamName", name: "Team Name", type: "text" },
                        { id: "TeamLeader", name: "Team Leader", type: "text" },
                        { id: "Description", name: "Description", type: "text" },
                    ]
                };

                var options = $.extend(defaults, options);
                return this.each(function () {
                    $(this).multiview(options);
                });
            }
        },

        // view for app settings redirect urls
        pcorpRedirectUrlsView: function (options) {
            var page = this;
            var displayDefaults = {
                dateFormat: "llll"
            };

            var actions = options.actions != null ? options.actions : [
                {
                    id: 0,
                    name: appStrings.grid.action.edit,
                    short: "edit",
                    icon: "<i class='fa fa-edit'></i>&nbsp;",
                    execute: function (view, item, $item, column, opts) {
                        $.pcorp.editRedirectUrl(item.pkId, false,
                            $.extend(true, {}, view.options, {
                                onSuccess: function (isForNew, data, controller, action) {
                                    view.rebindRecord(data.Record);
                                }
                            }));
                    }
                },
                {
                    id: 2,
                    name: appStrings.grid.action.delete,
                    short: "delete",
                    icon: "<i class='fa fa-times'></i>&nbsp;",
                    //rights: $.hasRight(appStrings.rights.AppsCmsDelete),
                    execute: function (view, item, $item, column, opts) {
                        $.confirmDeleteWebApi("/api/AppSettings/DeleteRedirectUrl", item.pkId, "Redirect Url", {
                            onSuccess: function (isForNew, data, controller) {
                                view.removeRecord(data);
                            }
                        });
                    }
                }
            ];

            if (typeof (options) == 'string') {
                var selector = $(this[0]);
                var controller = selector.data("multiview");
                return controller.onAction.apply(controller, arguments);
            }
            else { // init with options
                var opts = options;
                var defaults = {
                    sortMethod: "server",
                    contentHeight: 500,
                    infiniteScroll: true,
                    queryServer: true,
                    queryUrlCallback: function (controller, index, sortFields) {
                        var params = {
                            page: index,
                            rows: 50,
                            SortBy: sortFields
                        };
                        return "/api/AppSettings/GetRedirectUrls?" + $.param(params);

                    },

                    tools: [
                             {
                                 standard: "refresh",
                                 options: { css: "btn btn-xs pull-right", title: "Refresh Data", icon: "fa-refresh" }
                             }
                    ],
                    columns: [
                        { id: "pkId", name: "id", type: "number", css: "hide" },
                        { id: "Actions", name: "", type: "text", style: "dropdown", actions: actions, width: 95 },
                        { id: "Name", name: "Original URL", type: "text" },
                        { id: "RedirectUrl", name: "Redirects to this URL", type: "text" }
                    ]
                };

                var options = $.extend(defaults, options);
                return this.each(function () {
                    $(this).multiview(options);
                });
            }
        },

        // web request view
        pcorpWebRequestView: function (options) {

            if (typeof (options) == 'string') {
                var selector = $(this[0]);
                var controller = selector.data("multiview");
                return controller.onAction.apply(controller, arguments);
            }
            else { // init with options

                var opts = options;

                // View Defaults are set here
                var defaults = {
                    dataFormat: "json",
                    sortMethod: "local",
                    contentHeight: 500,
                    infiniteScroll: true,
                    queryServer: true,
                    queryUrlCallback: function (controller, index, sortFields) {
                        var params = {
                            page: index,
                            rows: 50,
                            SortBy: sortFields
                        };
                        return "/api/Corp/GetWebRequests?" + $.param(params);
                    },
                    tools: [
                         {
                             standard: "scrolltop",
                             options: { css: "btn btn-xs pull-left", title: "Scroll to top", icon: "fa-chevron-circle-up" }
                         },
                         {
                             standard: "scrollbottom",
                             options: { css: "btn btn-xs pull-left", title: "Scroll to bottom", icon: "fa-chevron-circle-down" }
                         },
                         {
                             standard: "refresh",
                             options: { css: "btn btn-xs pull-right", title: "Refresh Data", icon: "fa-refresh" }
                         }
                    ],
                    columns: [
                        { id: "pkId", name: "id", type: "number", css: "hide" },
                        {
                            id: "Created", name: "Created", type: "date",
                            sort: { isDefault: true, enabled: true, direction: "descending" },
                            valueFormat: function (rowdata, $row, $cell, def) {
                                var m = $.serverTime().toLocalMoment(rowdata.Created);
                                if (!m.isValid())
                                    return "(none)";
                                else
                                    return "<span style='white-space:nowrap;'>" + m.format("LT") + "</span><br><small><span style='white-space:nowrap;'>" + m.format("ddd, MMM D") + "</span></small>";
                            }
                        },
                        {
                            id: "RequestStatusName", name: "Status", type: "string", css: null, sort: { enabled: true, direction: "ascending" }
                        },
                        {
                            id: "RequestTypeName", name: "Type", type: "string", css: null, sort: { enabled: true, direction: "ascending" },
                            valueFormat: function (rowdata, $row, $cell, def) {

                                var request = $.parseJSON(rowdata.Data);

                                var desc = rowdata.RequestTypeName;
                                if (request.DivisionType == SALES_TYPE_FEDERAL)
                                    desc += "<br>Federal";
                                else
                                    desc += "<br>Commercial";
                                return desc;
                            }
                        },
                        {
                            id: "NameLast", name: "Name", type: "string",
                            sort: { enabled: true, direction: "descending", ascending: [{ field: "NameLast", direction: "asc" }, { field: "NameFirst", direction: "asc" }] },
                            valueFormat: function (rowdata, $row, $cell, def) {

                                var request = $.parseJSON(rowdata.Data);

                                var comp = "";
                                if (!$.isNullOrEmpty(request.Company))
                                    comp += "<strong>" + request.Company + "</strong><br>";

                                var desc = "";
                                if (!$.isNullOrEmpty(request.Phone))
                                    desc += "<br><small>" + $.cleanPhoneNumber(request.Phone) + "</small>";
                                else if (!$.isNullOrEmpty(request.Mobile))
                                    desc += "<br><small>" + $.cleanPhoneNumber(request.Mobile) + "</small>";

                                var name = $.formatName(request, { defaultValue: "(unknown)" });
                                if (!$.isNullOrEmpty(request.Email) && !$.isNullOrEmpty(name))
                                    name = $.stringFormat("<a href='mailto:{0}'>{1}</a>", request.Email, name);
                                return $.stringFormat("{0}{1}{2}", comp, name, desc);
                            }
                        },
                        {
                            id: "Location", name: "Location", type: "string", css: null, sort: { enabled: false },
                            valueFormat: function (rowdata, $row, $cell, def) {
                                var request = $.parseJSON(rowdata.Data);

                                var loc = "";
                                if (!$.isNullOrEmpty(request.City))
                                    loc = $.appendDelim(loc, request.City);
                                if (!$.isNullOrEmpty(request.StateName))
                                    loc = $.appendDelim(loc, request.StateName, "<br>");
                                else if (!$.isNullOrEmpty(request.State))
                                    loc = $.appendDelim(loc, request.State, "<br>");
                                if (!$.isNullOrEmpty(request.Country))
                                    loc = $.appendDelim(loc, request.Country, ", ");
                                if (!$.isNullOrEmpty(request.Postal))
                                    loc = $.appendDelim(loc, request.Postal, " ");
                                return loc;
                            }

                        },
                        {
                            id: "Contact", name: "Contact", type: "string", css: null, sort: { enabled: false },
                            valueFormat: function (rowdata, $row, $cell, def) {
                                return $.stringFormat("{0}<br>{1}", rowdata.ContactMethods, rowdata.ContactTime);
                            }

                        }
                    ]
                };

                var options = $.extend(defaults, options);

                return this.each(function () {
                    $(this).multiview(options);
                });

            }
        },

        // Job Listing View
        pcorpJobView: function (options) {

            var rowCss = function (rowdata, $row, $cell, def) {
                return "";
            };

            var baseView = this;
            this.isLoggedIn = (typeof CurrentUser != "undefined" && CurrentUser.id != null && CurrentUser.id != -1);

            var createPostUrl = function (item) {
                return item == null || $.isNullOrEmpty(item.Token) ? "#" : $.absoluteUrl("/pages/public/job.aspx", { id: item.Token });
            }

            var actions = options.actions != null ? options.actions : [
                {
                    id: 0,
                    name: appStrings.grid.action.edit,
                    short: "edit",
                    icon: "<i class='fa fa-edit'></i>&nbsp;",
                    rights: $.hasRight(appStrings.rights.AppsJobsEdit),
                    execute: function (view, item, $item, column, opts) {
                        $.pcorp.editJobPost(item.pkId,
                            $.extend(true, {}, view.options, {
                                onSuccess: function (data) {
                                    view.rebindRecord(data.Data);
                                }
                            }));
                    }
                },
                {
                    id: 2,
                    name: appStrings.grid.action.delete,
                    short: "delete",
                    icon: "<i class='fa fa-times'></i>&nbsp;",
                    rights: $.hasRight(appStrings.rights.AppsJobsDelete),
                    execute: function (view, item, $item, column, opts) {
                        var name = $.stringFormat("<span class='text-info'><strong>{0}</strong></span>", item.Name);
                        $.pcorp.confirmDeleteJobPost($.findIdValue(item), name, {
                            onSuccess: function (data)
                            {
                                view.removeRecord(data.Data);
                            }
                        });
                    }
                },
                {
                    divider: true
                },
                {
                    id: 6,
                    name: appStrings.grid.action.apply,
                    short: "apply",
                    icon: "<i class='fa fa-edit'></i>&nbsp;",
                    rights: function (rowdata, def) {
                        return !$.hasRight(appStrings.rights.AppsJobsEdit) && rowdata != null && rowdata.Status != null && rowdata.Status.CanApply == true;
                    },
                    init: function ($defaultBtn, rowdata, def, defaultAction, opts) {
                        if (rowdata == null || rowdata.Status == null || rowdata.Status.CanApply != true)
                            $defaultBtn.attr("disabled", true);
                    },
                    execute: function (view, item, $item, column, opts) {
                        $.pcorp.applyForJob(item.pkId, null, $.extend(true, {}, view.options, { returnUrl: createPostUrl(item) }));
                    }
                },
                {
                    id: 5,
                    name: appStrings.grid.action.details,
                    short: "details",
                    icon: "<i class='fa fa-list'></i>&nbsp;",
                    rights: function (rowdata, def) {
                        return !$.hasRight(appStrings.rights.AppsJobsEdit) && !$.isNullOrEmpty(rowdata.Token);
                    },
                    execute: function (view, item, $item, column, opts) {
                        window.location = createPostUrl(item);
                    }
                }
            ];

            if (typeof (options) == 'string') {
                var selector = $(this[0]);
                var controller = selector.data("multiview");
                return controller.onAction.apply(controller, arguments);
            }
            else { // init with options

                var opts = options;

                // View Defaults are set here
                var defaults = {
                    dataFormat: "json",
                    sortMethod: "local",
                    //contentHeight: 500,
                    infiniteScroll: true,
                    queryServer: true,
                    queryEvents: {
                        onAllItemsLoaded: function (controller) {
                        }
                    },
                    queryUrlCallback: function (controller, index, sortFields) {
                        var params = {
                            page: index,
                            rows: 50,
                            SortBy: sortFields
                        };
                        return "/api/Corp/GetJobs?" + $.param(params);
                    },
                    tools: [
                         {
                             standard: "scrolltop",
                             options: { css: "btn btn-xs pull-left", title: "Scroll to top", icon: "fa-chevron-circle-up" }
                         },
                         {
                             standard: "scrollbottom",
                             options: { css: "btn btn-xs pull-left", title: "Scroll to bottom", icon: "fa-chevron-circle-down" }
                         },
                         {
                             standard: "refresh",
                             options: { css: "btn btn-xs pull-right", title: "Refresh Data", icon: "fa-refresh" }
                         },
                         {
                             standard: "export",
                             options: { css: "btn btn-xs pull-right", title: "Export to CSV", icon: "fa-download" }
                         }
                    ],
                    columns: [
                        { id: "pkId", name: "id", type: "number", css: "hide" },
                        {
                            id: "Actions", name: "", type: "text", style: "dropdown", actions: actions, width: 110,
                            css: rowCss
                        },
                        {
                            id: "Posted", name: "Posted", type: "date", css: rowCss, sort: { isDefault: true, enabled: true, direction: "descending" },
                            valueFormat: function (rowdata, $row, $cell, def) {
                                var date = "";
                                var m = $.serverTime().toLocalMoment(rowdata.Posted);
                                if (!m.isValid())
                                    date = "(none)";
                                else
                                    date = "<span style='white-space:nowrap;'>" + m.format("ll") + "</span>";

                                return $.stringFormat("{0}<br><span class='text-muted'><small>posted: {1}</small></span><br><span class='number-lg {2}'>{3}</span>",
                                    rowdata.StatusName, date, rowdata.ApplicationCount == null || rowdata.ApplicationCount <= 0 ? "text-muted" : "text-success", rowdata.ApplicationCount == null ? "" : rowdata.ApplicationCount);
                            }
                        },
                        {
                            id: "Name", name: "Job Description", type: "string", css: rowCss, sort: { enabled: true, direction: "ascending" },
                            valueFormat: function (rowdata, $row, $cell, def) {
                                var template =
                                    "<div class='row'>" +
                                        "<div class='col-md-8'>" +
                                            "<div class='text-info'><a href='{postUrl}' class='dashed'><strong>{Name}</strong></a></div>" +
                                            "<div>{Locations}</div>" +
                                        "</div>" +
                                        "<div class='col-md-4 text-right'>" +
                                            "<strong><span style='white-space:nowrap;'>{countryNames}</span> <span style='white-space:nowrap;'>- {EmploymentType}</span></strong>" +
                                            "<div><small>{workSched}</small></div>" +
                                        "</div>" +
                                    "</div>" +
                                    "<div class='row'>" +
                                        "<div class='col-md-12'>" +
                                            "<small>{Summary}</small>" +
                                        "</div>" +
                                    "</div>" +
                                    "<div class='share-bar'>" +
                                        "<span class='left'><i>{didApply}</i></span>" +
                                        "<span>share with:</span>" +
                                        "<a data-action='share_email' class='btn btn-sm'><i class='fa fa-envelope'></i>&nbsp;&nbsp;&nbsp;email</a>" +
                                        "<a data-action='share_linkedin' class='btn btn-sm' href='#' target='_social'><i class='fa fa-linkedin-square'></i>&nbsp;&nbsp;&nbsp;linked-in</a>" +
                                        "<a data-action='share_twitter' class='btn btn-sm' href='#' target='_social'><i class='fa fa-twitter'></i>&nbsp;&nbsp;&nbsp;twitter</a>" +
                                        "<a data-action='share_facebook' class='btn btn-sm hide' href='#' target='_social'><i class='fa fa-facebook-square'></i>&nbsp;&nbsp;&nbsp;facebook</a>" +
                                    "</div>"

                                var postUrl = createPostUrl(rowdata);

                                var countryList = "";
                                if (rowdata.CountryList != null) {
                                    $.each(rowdata.CountryList, function (i, country) {
                                        if (!$.isNullOrEmpty(countryList))
                                            countryList += " | ";
                                        countryList += country.Name;
                                    });
                                }

                                var workSched = "";
                                if (!$.isNullOrEmpty(rowdata.HoursPerWeek))
                                    workSched += $.stringFormat("<span style='white-space:nowrap;'>{0} hr/wk</span>", rowdata.HoursPerWeek);

                                if (!$.isNullOrEmpty(rowdata.WorkHours)) {
                                    if (!$.isNullOrEmpty(workSched))
                                        workSched += " - ";
                                    workSched += $.stringFormat("<span style='white-space:nowrap;'>{0}</span>", rowdata.WorkHours);
                                }

                                var applied = "";
                                if (!$.isNullOrEmpty(rowdata.Applied)) {
                                    var m = $.serverTime().toLocalMoment(rowdata.Applied);
                                    if (m.isValid)
                                        applied = $.stringFormat("you applied on {0}", m.format("ll"));
                                }

                                var $html = $($.expandTemplateFromObject(template, $.extend(true, {}, rowdata, {
                                    postUrl: postUrl,
                                    SmallSummary: $.limitTextAtWholeWord(rowdata.Description, 200),
                                    countryNames: countryList,
                                    workSched: workSched,
                                    didApply: applied
                                })));

                                var $email = $html.find("[data-action='share_email']");
                                var $linkedin = $html.find("[data-action='share_linkedin']");
                                var $twitter = $html.find("[data-action='share_twitter']");
                                var $fb = $html.find("[data-action='share_facebook']");

                                var body = $.stringFormat("I thought you might be interested in this job opportunity:\r\n\r\n{0}\r\n{1}", rowdata.Name, postUrl);
                                var subject = $.stringFormat("Careers @ Phillips Corp: {0}", rowdata.Name);
                                var emfields = {
                                    subject: subject,
                                    body: body
                                };
                                $email.attr("href", $.stringFormat("mailto:?{0}", $.param(emfields).replace(/\+/g, " ")));
                                $linkedin.attr("href", $.socialLinkedInUrl(postUrl, subject, rowdata.Summary, "Phillips Corporation"));
                                $twitter.attr("href", $.socialTweetUrl(postUrl, subject));

                                return $html;
                            }

                        }
                    ]
                };

                var options = $.extend(defaults, options);

                return this.each(function () {
                    $(this).multiview(options);
                });

            }
        },

        // Job Application View
        pcorpJobApplicationView: function (options) {

            var rowCss = function (rowdata, $row, $cell, def) {
                return "";
            };

            var baseView = this;
            this.isLoggedIn = (typeof CurrentUser != "undefined" && CurrentUser.id != null && CurrentUser.id != -1);

            var createPostUrl = function (item) {
                return item == null || $.isNullOrEmpty(item.Token) ? "#" : $.absoluteUrl("/pages/public/job.aspx", { id: item.Token });
            }

            var actions = options.actions != null ? options.actions : [
                {
                    id: 0,
                    name: appStrings.grid.action.edit,
                    short: "edit",
                    icon: "<i class='fa fa-edit'></i>&nbsp;",
                    rights: $.hasRight(appStrings.rights.AppsJobsEdit),
                    execute: function (view, item, $item, column, opts) {
                        $.pcorp.applyForJob(item.fkJobId, item.fkUserId,
                            $.extend(true, {}, view.options, {
                                returnUrl: createPostUrl(item),
                                onSuccess: function (data) {
                                    view.rebindRecord(data.Data);
                                }
                            }));
                    }
                },
                {
                    id: 2,
                    name: appStrings.grid.action.delete,
                    short: "delete",
                    icon: "<i class='fa fa-times'></i>&nbsp;",
                    rights: $.hasRight(appStrings.rights.AppsJobsDelete),
                    execute: function (view, item, $item, column, opts) {

                        var countryList = "";
                        if (item.Job.CountryList != null) {
                            $.each(item.Job.CountryList, function (i, country) {
                                if (!$.isNullOrEmpty(countryList))
                                    countryList += " | ";
                                countryList += country.Name;
                            });
                        }


                        var name = $.stringFormat(
                            "<dl class='dl-horizontal'>" +
                                "<dt>Applicant Name</dt><dd>{0}</dd>" +
                                "<dt>Applicant Email</dt><dd>{1}</dd>" +
                                "<dt>Job Opening</dt><dd>{2}</dd>" +
                                "<dt>Country</dt><dd>{3}</dd>" +
                                "<dt>Location</dt><dd>{4}</dd>" +
                            "</dl>",
                            item.Contact.name, item.Contact.email, item.Job.Name, countryList, item.Job.Locations);
                        $.pcorp.confirmDeleteJobApplication($.findIdValue(item), name, {
                            onSuccess: function (data)
                            {
                                view.removeRecord(data.Data);
                            }
                        });
                    }
                }
            ];

            if (typeof (options) == 'string') {
                var selector = $(this[0]);
                var controller = selector.data("multiview");
                return controller.onAction.apply(controller, arguments);
            }
            else { // init with options

                var opts = options;

                // View Defaults are set here
                var defaults = {
                    dataFormat: "json",
                    sortMethod: "local",
                    //contentHeight: 500,
                    infiniteScroll: true,
                    queryServer: true,
                    queryEvents: {
                        onAllItemsLoaded: function (controller) {
                        }
                    },
                    queryUrlCallback: function (controller, index, sortFields) {
                        var params = {
                            page: index,
                            rows: 50,
                            SortBy: sortFields
                        };
                        return "/api/Corp/GetJobs?" + $.param(params);
                    },
                    tools: [
                         {
                             standard: "scrolltop",
                             options: { css: "btn btn-xs pull-left", title: "Scroll to top", icon: "fa-chevron-circle-up" }
                         },
                         {
                             standard: "scrollbottom",
                             options: { css: "btn btn-xs pull-left", title: "Scroll to bottom", icon: "fa-chevron-circle-down" }
                         },
                         {
                             standard: "refresh",
                             options: { css: "btn btn-xs pull-right", title: "Refresh Data", icon: "fa-refresh" }
                         },
                         {
                             standard: "export",
                             options: { css: "btn btn-xs pull-right", title: "Export to CSV", icon: "fa-download" }
                         }
                    ],
                    columns: [
                        { id: "pkId", name: "id", type: "number", css: "hide" },
                        {
                            id: "Actions", name: "", type: "text", style: "dropdown", actions: actions, width: 110,
                            css: rowCss
                        },
                        {
                            id: "Created", name: "Date", type: "date", css: rowCss, sort: { isDefault: true, enabled: true, direction: "descending" },
                            valueFormat: function (rowdata, $row, $cell, def) {
                                var date = "";
                                var m = $.serverTime().toLocalMoment(rowdata.Created);
                                if (!m.isValid())
                                    date = "(none)";
                                else
                                    date = "<span style='white-space:nowrap;'>" + m.format("ll") + "</span>";

                                return date;
                            }
                        },
                        {
                            id: "IntFollowup", name: "Needs Followup", type: "bool", css: rowCss, sort: { enabled: true, direction: "ascending" },
                            valueFormat: function (rowdata, $row, $cell, def) {
                                return rowdata.IntFollowup ? "<div class='number-md text-center'><i class='fa fa-check'></i></div>" : "";
                            }
                        },
                        {
                            id: "fkStatusId", name: "Internal Status", type: "string", css: rowCss, sort: { enabled: true, direction: "ascending" },
                            valueFormat: function (rowdata, $row, $cell, def) {
                                return rowdata.IntStatus.Name;
                            }
                        },
                        {
                            id: "fkUserId", name: "Name/Position", type: "string", css: rowCss, sort: { enabled: true, direction: "ascending" },
                            valueFormat: function (rowdata, $row, $cell, def) {
                                var template =
                                    "<div class='row'>" +
                                        "<div class='col-md-12'>" +
                                            "<div class='text-info'><strong>{UserName}</strong></div>" +
                                            "<div><a href='mailto:{Email}' class='dashed'>{Email}</a></div>" +
                                        "</div>" +
                                        "<div class='col-md-12'>" +
                                            "<div><a href='{postUrl}' class='dashed'>{Name}</a></div>" +
                                            "<div>{Locations}</div>" +
                                        "</div>" +
                                    "</div>";

                                var postUrl = createPostUrl(rowdata);

                                var countryList = "";
                                if (rowdata.Job.CountryList != null) {
                                    $.each(rowdata.Job.CountryList, function (i, country) {
                                        if (!$.isNullOrEmpty(countryList))
                                            countryList += " | ";
                                        countryList += country.Name;
                                    });
                                }

                                var $html = $($.expandTemplateFromObject(template, $.extend(true, {}, rowdata, {
                                    UserName: rowdata.Contact.name,
                                    Email: rowdata.Contact.email,
                                    Name: rowdata.Job.Name,
                                    Locations: rowdata.Job.Locations,
                                    postUrl: postUrl,
                                    countryNames: countryList
                                })));

                                return $html;
                            }

                        }
                    ]
                };

                var options = $.extend(defaults, options);

                return this.each(function () {
                    $(this).multiview(options);
                });

            }
        },


        dialogMyHaegerLauncher: function (options) {

            //Defaults options are set
            var defaults = {
                dialogElemId: this.selector.substring(1),
                dialogTemplate: "<div id='{dialogElemId}'></div>",
                onOK: null,
                onCancel: null,
                masterController: options.masterController == null ? this : options.masterController,
                skin: new $.pcorp.myHaegerLauncherDialogSkin(),
                queryServer: false
            };
            // override the base defualts with the skin defaults (if we have one)
            var options = options;
            var options = $.extend(defaults, options);

            // find/create a single dialog that is reusable
            var dialog = $.ensureElementExists(options.dialogElemId, null, options.dialogTemplate, options);
            dialog.skeletonDialog(options);
            dialog.skeletonDialog(DIALOG_ACTION_SHOW);
        },

        dialogAddPartner: function (options) {
            var defaults = {
                dialogElemId: this.selector.substring(1),
                dialogTemplate: "<div id='{dialogElemId}'></div>",
                onOK: null,
                onCancel: null,
                masterController: options.masterController == null ? this : options.masterController,
                skin: new $.pcorp.addPartnerDialogSkin(),
                queryServer: true
            };
            var options = options;
            var options = $.extend(defaults, options);

            // find/create a single dialog that is reusable
            var dialog = $.ensureElementExists(options.dialogElemId, null, options.dialogTemplate, options);
            dialog.skeletonDialog(options);
            dialog.skeletonDialog(DIALOG_ACTION_SHOW);
        },

        dialogBrandInfo: function (options) {

            //Defaults options are set
            var defaults = {
                dialogElemId: this.selector.substring(1),
                dialogTemplate: "<div id='{dialogElemId}'></div>",
                onOK: null,
                onCancel: null,
                masterController: options.masterController == null ? this : options.masterController,
                skin: new $.pcorp.brandInfoDialogSkin(),
                queryServer: false
            };
            // override the base defualts with the skin defaults (if we have one)
            var options = options;
            var options = $.extend(defaults, options);

            // find/create a single dialog that is reusable
            var dialog = $.ensureElementExists(options.dialogElemId, null, options.dialogTemplate, options);
            dialog.skeletonDialog(options);
            dialog.skeletonDialog(DIALOG_ACTION_SHOW);
        },

        dialogQuote: function (options) {

            //Defaults options are set
            var defaults = {
                dialogElemId: this.selector.substring(1),
                dialogTemplate: "<div id='{dialogElemId}'></div>",
                onOK: null,
                onCancel: null,
                masterController: options.masterController == null ? this : options.masterController,
                skin: new $.pcorp.getQuoteDialogSkin(),
                queryServer: false
            };
            // override the base defualts with the skin defaults (if we have one)
            var options = options;
            var options = $.extend(defaults, options);

            // find/create a single dialog that is reusable
            var dialog = $.ensureElementExists(options.dialogElemId, null, options.dialogTemplate, options);
            dialog.skeletonDialog(options);
            dialog.skeletonDialog(DIALOG_ACTION_SHOW);
        },

        dialogServiceRequest: function (options) {



            //Defaults options are set
            var defaults = {
                dialogElemId: this.selector.substring(1),
                dialogTemplate: "<div id='{dialogElemId}'></div>",
                onOK: null,
                onCancel: null,
                masterController: options.masterController == null ? this : options.masterController,
                skin: new $.pcorp.serviceRequestDialogSkin(options.data),
                queryServer: false
            };
            // override the base defualts with the skin defaults (if we have one)
            var options = options;
            var options = $.extend(defaults, options);

            // find/create a single dialog that is reusable
            var dialog = $.ensureElementExists(options.dialogElemId, null, options.dialogTemplate, options);
            dialog.skeletonDialog(options);
            dialog.skeletonDialog(DIALOG_ACTION_SHOW);
        },

        dialogRMARequest: function (options) {

            //Defaults options are set
            var defaults = {
                dialogElemId: this.selector.substring(1),
                dialogTemplate: "<div id='{dialogElemId}'></div>",
                onOK: null,
                onCancel: null,
                masterController: options.masterController == null ? this : options.masterController,
                skin: new $.pcorp.rmaRequestDialogSkin(),
                queryServer: false
            };
            // override the base defualts with the skin defaults (if we have one)
            var options = options;
            var options = $.extend(defaults, options);

            // find/create a single dialog that is reusable
            var dialog = $.ensureElementExists(options.dialogElemId, null, options.dialogTemplate, options);
            dialog.skeletonDialog(options);
            dialog.skeletonDialog(DIALOG_ACTION_SHOW);
        },

        dialogJobPost: function (options) {

            //Defaults options are set
            var defaults = {
                dialogElemId: this.selector.substring(1),
                dialogTemplate: "<div id='{dialogElemId}'></div>",
                onOK: null,
                onCancel: null,
                masterController: options.masterController == null ? this : options.masterController,
                skin: new $.pcorp.jobPostEditDialogSkin(),
            };
            // override the base defualts with the skin defaults (if we have one)
            var options = options;
            var options = $.extend(defaults, options);

            // find/create a single dialog that is reusable
            var dialog = $.ensureElementExists(options.dialogElemId, null, options.dialogTemplate, options);
            dialog.skeletonDialog(options);
            dialog.skeletonDialog(DIALOG_ACTION_SHOW);
        },

        dialogJobApply: function (options) {

            //Defaults options are set
            var defaults = {
                dialogElemId: this.selector.substring(1),
                dialogTemplate: "<div id='{dialogElemId}'></div>",
                onOK: null,
                onCancel: null,
                masterController: options.masterController == null ? this : options.masterController,
                skin: new $.pcorp.jobApplyDialogSkin(),
            };
            // override the base defualts with the skin defaults (if we have one)
            var options = options;
            var options = $.extend(defaults, options);

            // find/create a single dialog that is reusable
            var dialog = $.ensureElementExists(options.dialogElemId, null, options.dialogTemplate, options);
            dialog.skeletonDialog(options);
            dialog.skeletonDialog(DIALOG_ACTION_SHOW);
        },

        dialogEditRedirectUrls: function (options) {

            //Defaults options are set
            var defaults = {
                dialogElemId: this.selector.substring(1),
                dialogTemplate: "<div id='{dialogElemId}'></div>",
                onOK: null,
                onCancel: null,
                masterController: options.masterController == null ? this : options.masterController,
                skin: new $.pcorp.redirectUrlsEditDialogSkin(options),
                queryServer: true
            };
            // override the base defualts with the skin defaults (if we have one)
            var options = options;
            var options = $.extend(defaults, options);

            // find/create a single dialog that is reusable
            var dialog = $.ensureElementExists(options.dialogElemId, null, options.dialogTemplate, options);
            dialog.skeletonDialog(options);
            dialog.skeletonDialog(DIALOG_ACTION_SHOW);
        },


        dialogCorpAffirmation: function (options) {

            //Defaults options are set
            var defaults = {
                dialogElemId: this.selector.substring(1),
                dialogTemplate: "<div id='{dialogElemId}'></div>",
                onOK: null,
                onCancel: null,
                masterController: options.masterController == null ? this : options.masterController,
                skin: new $.pcorp.corpAffirmationDialogSkin(),
                queryServer: false
            };
            // override the base defualts with the skin defaults (if we have one)
            var options = options;
            var options = $.extend(defaults, options);

            // find/create a single dialog that is reusable
            var dialog = $.ensureElementExists(options.dialogElemId, null, options.dialogTemplate, options);
            dialog.skeletonDialog(options);
            dialog.skeletonDialog(DIALOG_ACTION_SHOW);
        }

    });
})(jQuery);
