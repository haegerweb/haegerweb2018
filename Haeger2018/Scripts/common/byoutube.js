//////////////////////////////////////////////////////////
// byoutube
// A jquery plugin that displays a list of youtube
// videos for a given youtube user account
// ...and it is built for bootstrap 3+
//////////////////////////////////////////////////////////

(function ($) {

    //Attatch the new method
    jQuery.fn.extend({

        //Plugin Name
        byoutube: function (options) {

            function byoutubeController(containerId, options) {
                this.containerId = containerId;
                this.options = options;
                var uploadFeedId = "UUNSLLbmRfAFrFXB0qfV5NDA";
                var channelId = "UCNSLLbmRfAFrFXB0qfV5NDA";

                this.onAction = function (action) {
                    var args = Array.prototype.slice.call(arguments, 1);
                    switch (action) {
                        case "default":
                            return this.onAction(this.options.defaultAction);
                        case "isBusy":
                            return this.isBusy();
                        default:
                            this.notifyEvent("error", "Action <strong>" + action + "</strong> not implemented.");
                            break;
                    }
                };

                this.notifyEvent = function (eventType, message, data) {
                    this.$container.trigger(this.options.eventPrefix + eventType, [message, data]);
                };

                this.loadProfile = function () {
                    var controller = this;
                    return $.when($.ajax({
                        url: "https://www.googleapis.com/youtube/v3/channels?part=snippet,statistics&forUsername=" + this.youtubeUser + "&key=AIzaSyBAdNbLUw-X4kKNiiV2sHWm6GdwkEqyKls",
                        type: "GET",
                        async: true,
                        cache: controller.options.allowCache,
                        dataType: 'jsonp',
                    })).fail(function (data) {
                        this.notifyEvent("error", "An error occurred retrieving the YouTube user profile", data);
                    }).done(function (data) {

                        controller.profile = $.extend({}, {
                            channelDesc: data.items[0].snippet.description,
                            channelName: data.items[0].snippet.title,
                            channelPic: data.items[0].snippet.thumbnails.default.url,
                            channelSubscribers: data.items[0].statistics.subscriberCount,
                            channelViews: data.items[0].statistics.viewCount,

                        }, controller.options.profileOverrides);

                    });
                };

                this.loadPlaylists = function () {
                    var controller = this;

                    return $.when($.ajax({
                        url: "https://www.googleapis.com/youtube/v3/playlists?part=snippet&channelId=" + channelId + "&key=AIzaSyBAdNbLUw-X4kKNiiV2sHWm6GdwkEqyKls",
                        type: "GET",
                        async: true,
                        cache: controller.options.allowCache,
                        dataType: 'jsonp',
                    })).fail(function (data) {
                        controller.notifyEvent("error", "An error occurred retrieving the YouTube playlists", data);
                    }).done(function (data) {

                        controller.playlists = [];
                        controller.featuredPlaylist = null;

                        if (data != null && data.items != null && data.items.length > 0) {
                            controller.playlists = data.items;
                            var setDefaultFeature= function () {
                                controller.featuredPlaylist = controller.playlists[0];
                                controller.options.featuredPlaylistId = controller.featuredPlaylist.id;
                            };
                            if (controller.options.featuredPlaylistId == null)
                                setDefaultFeature();
                            else {
                                var feature = null;
                                for (var i = 0; i < controller.playlists.length; i++) {
                                    var list = controller.playlists[i];
                                    if (list.id == controller.options.featuredPlaylistId) {
                                        controller.featuredPlaylist = list;
                                        break;
                                    }
                                }
                                if (feature == null)
                                    setDefaultFeature();
                            }
                        }
                    });

                };

                this.getPlaylistVideos = function (playlistId) {
                    var controller = this;

                    return $.when($.ajax({
                        url: "https://www.googleapis.com/youtube/v3/playlistItems?part=snippet&playlistId=" + playlistId + "&maxResults=50&key=AIzaSyBAdNbLUw-X4kKNiiV2sHWm6GdwkEqyKls",
                        type: "GET",
                        async: true,
                        cache: controller.options.allowCache,
                        dataType: 'jsonp',
                    })).fail(function (data) {
                        controller.notifyEvent("error", "An error occurred retrieving the YouTube playlist videos for id=" + playlistId, data);
                    }).done(function (data) {
                        if (data.error != null)
                            controller.notifyEvent("error", data.error.message, data);
                        else
                            controller.currentPlaylist = data;
                    });

                };

                this.getUploadFeed = function () {
                    var controller = this;

                    return $.when($.ajax({
                        url: "https://www.googleapis.com/youtube/v3/playlistItems?part=snippet&maxResults=50&playlistId=" + uploadFeedId + "&key=AIzaSyBAdNbLUw-X4kKNiiV2sHWm6GdwkEqyKls",
                        type: "GET",
                        async: true,
                        cache: controller.options.allowCache,
                        dataType: 'jsonp',
                    })).fail(function (data) {
                        controller.notifyEvent("error", "An error occurred retrieving the YouTube upload feed for user=" + this.youtubeUser, data);
                    }).done(function (data) {
                        controller.uploadFeed = data;

                    });

                };


                this.buildUI = function () {
                    var controller = this;
                    this.$container.empty();

                    $.when(this.loadPlaylists()).always(function () {


                        var $all = $($.expandTemplateFromObject(controller.options.containerTemplate, $.extend({}, controller.profile, {
                            css: controller.options.containerCss,
                            channelViews: controller.profile == null || controller.profile.channelViews == null ? "" : $.number(controller.profile.channelViews,0),
                            channelSubscribers: controller.profile == null || controller.profile.channelSubscribers == null ? "" : $.number(controller.profile.channelSubscribers,0),
                        })));
                        var $subscribe = $all.find("[data-action='subscribe']");
                        $subscribe.on("click", function (e) {
                            e.preventDefault();
                            window.location = $.stringFormat(controller.options.subscribeUrl, controller.profile.channelName);
                        });
                        var $header = $all.find(".header");
                        if (controller.options.showheader)
                            $header.show();
                        else
                            $header.hide();

                        var $categoryContainer = $all.find(".categories");
                        var $cats = $($.expandTemplateFromObject(controller.options.categoriesTemplate, controller.options));
                        var $catLinks = $cats.find(".catlinks");
                        var $catContent = $cats.find(".catcontent");

                        var hideSections = controller.options.hideSingleSection && controller.options.sections.length == 1;
                        if (!hideSections) {
                            controller.$videocount = $($.expandTemplateFromObject(controller.options.categoryCountTemplate, controller.options));
                            $catLinks.append(controller.$videocount);
                        }

                        if (controller.options.sections != null && controller.options.sections.length > 0) {
                            $.each(controller.options.sections, function (i, section) {
                                if (section != null) {
                                    var isDefault = section.isDefault || controller.options.sections.length == 0;
                                    var $link = $($.expandTemplateFromObject(controller.options.categoryLinkTemplate, $.extend({}, section, { css: (isDefault ? "active" : "") + (hideSections ? " hide" : ""), })));
                                    var $actions = $link.find("button,a");
                                    $actions.data("record", { controller: controller, section: section, loaded: false });

                                    var updateCounts = function ($area) {
                                        if (controller.$videocount != null)
                                            controller.$videocount.html($.stringFormat("{0} videos", $area.find(".video").length));
                                    };

                                    $actions.on("click", function (e) {
                                        e.preventDefault();
                                        var data = $(this).data("record");
                                        var $area = controller.$container.find("#" + data.section.categoryId);
                                        if (data.section != null && data.section.loaded != true)
                                            $.when(controller.loadSection(data.section, $area)).always(function () { updateCounts($area); });
                                        else
                                            updateCounts($area);
                                    });
                                    $catLinks.append($link);
                                    var $content = $($.expandTemplateFromObject(controller.options.categoryContentTemplate, $.extend({}, section, { css: isDefault ? "active" : "", })));
                                    $catContent.append($content);
                                    if (isDefault)
                                        $.when(controller.loadSection(section, $content)).always(function () { updateCounts($content); });
                                }
                            });
                        }

                        $categoryContainer.append($cats);
                        controller.$container.append($all);
                    });
                };

                this.loadSection = function (section, $area) {
                    var controller = this;
                    var resolved = $.Deferred().resolve().promise();
                    if (section != null) {
                        switch (section.action) {
                            case "featured":
                                return $.when(this.showFeaturedPlayList($area)).done(function (data) {
                                    if (data.error == null)
                                        section.loaded = true;
                                });
                            case "playlists":
                                break;
                            default:
                                return $.when(this.showUploadFeed($area)).done(function (data) {
                                    if (data.error == null)
                                        section.loaded = true;
                                });
                        }
                    }
                    return resolved;
                };



                this.showUploadFeed = function ($area) {
                    var controller = this;

                    return $.when(controller.getUploadFeed()).always(function (data) {
                        controller.showPlaylist($area, controller.uploadFeed, "uploads");
                    });
                };

                this.showFeaturedPlayList = function ($area) {
                    var controller = this;

                    return $.when(controller.getPlaylistVideos(controller.options.featuredPlaylistId)).always(function (data) {
                        if(data.error==null)
                            controller.showPlaylist($area, data, "featured");
                    });
                };

                this.createVideo = function (item) {
                    if (item == null)
                        return null;

                    var video = item.video != null ? item.video : item;


                    if (!$.isNullOrEmpty(video.thumbnail) && video.thumbnail.hqDefault != null) {
                        video.thumbnail = video.thumbnail.hqDefault;
                        video.thumbnail = video.thumbnail.replace("hqdefault", "mqdefault");

                    }
                    return video;
                };

                this.getColCss = function () {
                    if ($.isNumeric(this.options.colSpan))
                        return $.stringFormat("col-md-{0}", this.options.colSpan);
                    else if ($.isArray(this.options.colSpan)) {
                        var css = "";
                        $.each(this.options.colSpan, function (i, span) {
                            if (!$.isNullOrEmpty(css))
                                css += " ";
                            if ($.isPlainObject(span))
                                css += $.stringFormat("col-{0}-{1}", span.size, span.columns);
                            else
                                css += span;
                        });
                        return css;
                    }
                    else
                        return this.options.colSpan;
                },

                this.showPlaylist = function($cont, playlist, identifier) {
                    var controller = this;
                    $cont.empty();
                    if (playlist == null || playlist.items == null || playlist.items.length == 0)
                        return;

                    var videos = playlist.items;

                    videos.sort(function (a, b) {
                        var adate = a.video != null ? a.video.snippet.uploaded : a.snippet.uploaded;
                        var bdate = b.video != null ? b.video.snippet.uploaded : b.snippet.uploaded;
                        return new Date(bdate) - new Date(adate);
                    });

                    var $row = null;
                    var template = this.options.videoTemplate;

                    var videoIdChain = '';
                    var currentTab = identifier;
                    for (var x = 0; x < videos.length; x++) {
                        if (x != videos.length - 1) {
                            videoIdChain += videos[x].snippet.resourceId.videoId + ',';
                        }
                        else {
                            videoIdChain += videos[x].snippet.resourceId.videoId;
                        }

                    }
                    GetViewCountAndDuration(videoIdChain, currentTab);

                    for (var i = 0; i < videos.length; i++) {
                        var video = this.createVideo(videos[i]);

                        var videoId = video.snippet.resourceId.videoId;

                        if (video != null) {

                            var css = this.getColCss();

                            if ($row == null) {
                                $row = $("<div class='row'></div>");
                                $cont.append($row);
                            }


                            var viewPage = this.options.pageSize == null || this.options.pageSize <= 0 ? 0 : Math.floor(i / this.options.pageSize);
                            var uploaded = moment(video.snippet.publishedAt);
                            var isNew = this.options.newVideoDurationInDays > 0 && moment().diff(uploaded, "days") <= this.options.newVideoDurationInDays;
                            var playerLinkUrl = this.options.allowVideoEmbed ? $.stringFormat("http://www.youtube.com/embed/{0}?autohide=1&fs=1&rel=0&hd=1&wmode=opaque&enablejsapi=1", videoId) : video.player["default"];
                            var playerGroupCss = controller.options.imgPlayerLinkCss + " lbox_" + identifier + viewPage;

                            var $col = $($.expandTemplateFromObject(this.options.videoTemplate, $.extend({}, video, {
                                css: css + ($.isNullOrEmpty(this.options.imgCss) ? "" : " " + this.options.imgCss) + (viewPage == 0 ? "" : " hide"),
                                uploaded: moment.duration(moment().diff(uploaded, "days"), "days").format("y[y] M[m] d[d]"),
                                playerUrl: playerLinkUrl,
                                duration: moment.duration(video.duration, "seconds").format("m:ss", { trim: false }),
                                viewCount: $.number(video.viewCount, 0),
                                viewPage: viewPage,
                                newCss: (isNew ? controller.options.newVideoCss : "hide"),
                                imgPlayerLinkCss: playerGroupCss,
                                thumbnail: video.snippet.thumbnails.medium.url,
                                title: video.snippet.title,
                                statsId: currentTab + i,
                                durId: currentTab + i,



                            })));
                            var $viewer = $col.find("a.viewer");
                            $viewer.data("record", videos[i]);
                            $viewer.on("click", function (e) {
                                var $link = $(this);
                                var ret = {
                                    handled: true
                                };
                                if ($.isFunction(controller.options.onPreviewClick)) {
                                    var r = controller.options.onPreviewClick(controller, $link, $link.data("record"));
                                    ret = $.extend({}, ret, r);
                                }
                                if (!ret.handled)
                                    e.preventDefault();
                            });
                            $row.append($col);
                            controller.$container.trigger(this.options.eventPrefix + "video.shown", [videos[i], $col, $viewer, $row, identifier]);
                        }
                    }

                    if (this.options.pageSize >= 0 && this.options.pageSize < videos.length) {
                        var $pager = $($.expandTemplateFromObject(this.options.pagerTemplate, this.options));
                        var $prev = $pager.find(".previous");
                        var $next = $pager.find(".next");
                        var $pagerrow = $($("<div class='row'></div>"));
                        $pagerrow.append($pager);
                        $cont.append($pagerrow);
                        var pagerInfo = { page: 0, pageSize: this.options.pageSize, totalRecords: videos.length, $prev: $prev, $next: $next, $cont: $cont };
                        $pagerrow.data("record", pagerInfo);

                        var updatePrevNext = function (info, numToAdd) {
                            var lastPage = Math.floor(info.totalRecords / info.pageSize);
                            var newPage = info.page + numToAdd;
                            if (newPage < 0)
                                newPage = 0;
                            if (newPage > lastPage)
                                newPage = lastPage;

                            if (newPage == 0)
                                info.$prev.addClass("disabled");
                            else
                                info.$prev.removeClass("disabled");

                            if (newPage >= lastPage)
                                info.$next.addClass("disabled");
                            else
                                info.$next.removeClass("disabled");

                            info.$cont.find("[data-page='" + info.page + "']").addClass("hide");
                            info.$cont.find("[data-page='" + newPage + "']").removeClass("hide");

                            info.page = newPage;
                        };

                        $prev.find("a,button").on("click", function (e) {
                            e.preventDefault();
                            var info = $pagerrow.data("record");
                            if (info != null && !info.$prev.hasClass("disabled")) {
                                updatePrevNext(info, -1);
                            }
                        });
                        $next.find("a,button").on("click", function (e) {
                            e.preventDefault();
                            var info = $pagerrow.data("record");
                            if (info != null && !info.$next.hasClass("disabled")) {
                                updatePrevNext(info, 1);
                            }
                        });
                        updatePrevNext(pagerInfo, 0);
                    }

                    var totalPages = this.options.pageSize == null || this.options.pageSize <= 0 ? 1 : Math.floor(i / this.options.pageSize);
                    controller.$container.trigger(this.options.eventPrefix + "videos.shown", [videos, totalPages, $cont, identifier]);
                };

                this.update = function () {
                    var controller = this;
                    this.$container = $("#" + this.containerId);
                    this.youtubeUser = null;

                    var colspanString = this.options.colspans;
                    if (!$.isNullOrEmpty(colspanString)) {
                        this.options.colSpan = [];
                        var spans = colspanString.split("|");
                        var sizes = ["lg", "md", "sm", "xs"];
                        $.each(spans, function (i, span) {
                            var colVal = parseInt(span, 10);
                            if (!isNaN(colVal))
                                controller.options.colSpan.push({ size: sizes[i], columns: colVal });
                        });
                    }

                    if ($.isNullOrEmpty(this.options.channelUrl)) {
                        this.notifyEvent("error", "Please specify a valid YouTube channel");
                        return;
                    }
                    else {
                        var channel = this.options.channelUrl.toLowerCase();
                        var ustr = "youtube.com/user/";
                        var ulen = ustr.length;
                        var upos = channel.indexOf(ustr);
                        if (upos >= 0) {
                            var fpos = channel.indexOf("?feature");
                            if (fpos >= 0)
                                this.youtubeUser = channel.substring(upos + ulen, fpos);
                            else
                                this.youtubeUser = channel.substring(upos + ulen);
                        }
                    }
                    if ($.isNullOrEmpty(this.youtubeUser)) {
                        this.notifyEvent("error", "Please specify a valid YouTube user in your url");
                        return;
                    }

                    $.when(this.loadProfile()).fail(function (data) {
                        controller.notifyEvent("error", "There was an error loading the YouTube profile for user=" + this.youtubeUser);
                    }).done(function (data) {
                        controller.buildUI();
                    });
                };
            }

            if (typeof (options) == 'string') {
                var selector = $(this[0]);
                var controller = selector.data("byoutube");
                return controller.onAction.apply(controller, arguments);
            }
            else { // init with options

                var defaults = {
                    eventPrefix: "youtube.",
                    channelUrl: null,
                    featuredPlaylistId: null,
                    sections: [
                        { name: "All Videos", isDefault: true, action: "uploads", categoryId: "uploads", visible: true },
                        { name: "Featured", isDefault: false, action: "featured", categoryId: "featured", visible: true },
                        //{ name: "Playlists", isDefault: false, action: "playlists", categoryId: "playlists", visible: true },
                    ],
                    hideSingleSection: true,
                    subscribeUrl: "http://www.youtube.com/subscription_center?add_user={0}",
                    allowCache: false,
                    allowSubscribe: true,
                    allowVideoEmbed: true,
                    pageSize: 12,
                    maxResults: 50,
                    showheader: true,
                    newVideoDurationInDays: 30,
                    newVideoCss: "new jiggle",
                    profileOverrides: {
                        channelName: undefined,
                        channelPic: undefined,
                        channelSubscribers: undefined,
                        channelViews: undefined,
                        channelDesc: undefined,
                    },
                    totalCols: 12,
                    colSpan: [
                        { size: "lg", columns: 3 }, // for large/medium layout, each video spans 3 columns
                        { size: "sm", columns: 4 }, // for small layout, each video spans 4 columns
                        { size: "xs", columns: 6 }, // for xtra small layout, each video spans 6 columns
                    ],
                    containerCss: "clearfix",
                    categoriesCss: "nav nav-pills",
                    imgCss: "video",
                    imgPlayerLinkCss: "viewer thumbnail ilightbox",
                    onPreviewClick: null, // function(video){}
                    videoTemplate: '\
                                    <div class="{css}" data-page="{viewPage}">\
                                        <div class="preview">\
                                            <a id="ilightbox" href="{playerUrl}" class="{imgPlayerLinkCss}" data-type="iframe" data-options="thumbnail: \'{thumbnail}\', icon: \'video\', width: 1920, height: 1200, keepAspectRatio: true" data-title="{title}" data-description="{description}"><img src="{thumbnail}"\></a> \
                                            <span class="duration" id=duration{durId}>  {duration}</span>\
                                            <span class="{newCss}"><i class="fa fa-check"></i>  New!</span>\
                                        </div>\
                                        <div class="details">\
                                            <div class="title"><a href="{playerUrl}" class="dashed">{title}</a></div>\
                                            <div class="stats" id="stats{statsId}">{viewCount} views | uploaded {uploaded} ago</div>\
                                        </div>\
                                    </div>\
                                    ',
                    categoryLinkTemplate: '<li class="{css}" data-action="{action}"><a href="#{categoryId}" role="tab" data-toggle="tab"><i class="fa fa-video-camera"></i>&nbsp;&nbsp;&nbsp;{name}</a></li>',
                    categoryCountTemplate: '<li class="pull-right small text-muted" data-action="videocount"></li>',
                    categoryContentTemplate: '<div class="tab-pane videos {css}" id="{categoryId}"></div>',
                    categoriesTemplate:
                        '\
                                    <div class="categories row">\
                                        <ul class="catlinks {categoriesCss}" role="tablist">\
                                        </ul>\
                                        <div class="catcontent tab-content">\
                                        </div>\
                                    </div>\
                                    ',
                    pagerTemplate:
                        '\
                                    <ul class="pager">\
                                        <li class="previous btn-xs"><a href="#"><i class="fa fa-chevron-left"></i>&nbsp;&nbsp;&nbsp;newer</a></li>\
                                        <li class="next btn-xs"><a href="#">older&nbsp;&nbsp;&nbsp;<i class="fa fa-chevron-right"></i></a></li>\
                                    </ul>\
                                    ',
                    containerTemplate:
                        '\
                                    <div class="youtube {css}">\
                                        <div class="header row">\
                                            <div class="col-md-9 col-sm-6">\
                                                <a href="#" class="viewer"><img src="{channelPic}"\></a> \
                                                <h2 class="title">{channelName}</h2>\
                                                <p class="description">{channelDesc}</p>\
                                            </div>\
                                            <div class="stats col-md-3 col-sm-6 clearfix">\
                                                <ul class="nav margin-bottom-10">\
                                                    <li>\
                                                        <span class="badge pull-right"><i class="fa fa-eye"></i>&nbsp;&nbsp;&nbsp;{channelViews} total views</span>\
                                                    </li>\
                                                    <li>\
                                                        <span class="badge pull-right"><i class="fa fa-users"></i>&nbsp;&nbsp;&nbsp;{channelSubscribers} subscribers</span>\
                                                    </li>\
                                                </ul>\
                                                <button data-action="subscribe" type="button" class="btn btn-default pull-right"><i class="fa fa-youtube-play"></i>&nbsp;&nbsp;&nbsp;subscribe now!</button>\
                                            </div>\
                                        </div>\
                                        <div class="categories"></div>\
                                        <div class="videos"></div>\
                                    </div>\
                                    ',
                };

                return this.each(function () {
                    ///////////////////////////
                    // Initialization
                    ///////////////////////////
                    var containerId = $(this).attr("id");
                    var inlineOptions = $(this).data();
                    var opts = $.extend(true, {}, defaults, inlineOptions, options);
                    var controller = new byoutubeController(containerId,opts);
                    $(this).data("byoutube", controller);

                    // build the scaffolding HTML
                    controller.update.apply(controller);
                });

            }
        }

    });
    function GetViewCountAndDuration(videoId, tab) {
        $.ajax({
            url: "https://www.googleapis.com/youtube/v3/videos?part=contentDetails,statistics&maxResults=50&id=" + videoId + "&key=AIzaSyBAdNbLUw-X4kKNiiV2sHWm6GdwkEqyKls",
            type: "GET",
            dataType: 'jsonp',
            success: (function (data) {
                $.each(data.items, function (index, videos) {

                    var viewCount = document.getElementById("stats" + tab + index).innerHTML;
                    viewCount = viewCount.replace('0 views', videos.statistics.viewCount + ' views');
                    document.getElementById("stats" + tab + index).innerHTML = viewCount;

                    var thetime = moment.duration(videos.contentDetails.duration, "seconds").format("m:ss", { trim: false })
                    var duration = document.getElementById("duration" + tab + index).innerHTML;
                    var clock = '<i class="fa fa-clock-o"></i>';
                    duration = duration.replace('0:00', thetime);
                    document.getElementById("duration" + tab + index).innerHTML = clock + ' ' + duration;

                });
            }),
            error: (function (data) {
                controller.notifyEvent("error", "An error occurred retrieving the YouTube upload feed for user=" + this.youtubeUser, data);
            })
        });

    }
})(jQuery);
