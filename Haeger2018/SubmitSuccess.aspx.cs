﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Haeger2018
{
    public partial class SubmitSuccess : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            lblMessage.Text = Session["SuccessMessage"].ToString();
            string confirmation = Session["emailConfirm"].ToString();
            ltrConfirmation.Text = confirmation;

        }

        protected void btnSuccessOK_Click(object sender, EventArgs e)
        {
            Response.Redirect("Default.aspx");
        }
    }
}