﻿<%@ Page Title="Documents" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Documents.aspx.cs" Inherits="Haeger2018.Documents" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
    <style>
         span1{
            color:#0085ca;
        }
    </style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div class="container-fluid">
        <div class="row equal-height">            
             <div class="section-title">
                    <h2 style="margin: 0;">Haeger Documents</h2>
                    <h3 style="text-transform:initial; color:black; margin:0;">A <span1> PennEngineering&reg</span1> Company</h3>
                   
               </div>
            <br />
        </div>
        <div class="row">
            <div class="col-sm-4 col-sm-push-2">
                <a class="tileLink" href="MachineManuals.aspx" target="_haeger1">
                    <div class="tile nosize dark-blue animated zoomIn animation-delay-3">
                        <h2>Machine Manuals</h2>
                    </div>
                </a>
            </div>
            <div class="col-sm-4 col-sm-push-2">
                <a class="tileLink" href="ServiceProcedures.aspx" target="_haeger2">
                    <div class="tile nosize dark-red animated zoomIn animation-delay-3">
                        <h2>Service Procedures</h2>
                    </div>
                </a>
            </div>
          
        </div>
        <br />
        <br />
        <br />

</asp:Content>
