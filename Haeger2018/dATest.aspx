﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="dATest.aspx.cs" Inherits="Haeger2018.dATest" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">


    <div class="container-fluid">
        <div class="row">
                <div class="col-md-2"></div>
                <div class="col-md-8 animated bounceInLeft animation-delay-5">
                    <h2 class="section-title margin-bottom-0" style="border-bottom: none; text-align: center;">Haeger IoT Project</h2>
                        <h3 style="text-transform:initial; color:black; margin:0;"></h3>
                </div>
                    <br />
                    <br />
                    <br />

        </div>

        <div class="row">
            <div class="col-xs-3">

            </div>
            
            <div class="col-xs-6">
                <asp:GridView ID="grdDATest" CssClass= "table table-striped table-bordered table-condensed" runat="server" AutoGenerateColumns="False" DataSourceID="sqlDATest">
                    <Columns>
                        <asp:BoundField DataField="Customer" HeaderText="Customer" SortExpression="Customer" />
                        <asp:BoundField DataField="UniqueMachineID" HeaderText="UniqueMachineID" SortExpression="UniqueMachineID" />
                        <asp:BoundField DataField="Date" HeaderText="Date" SortExpression="Date" />
                        <asp:BoundField DataField="TotalStrokeCount" HeaderText="TotalStrokeCount" SortExpression="TotalStrokeCount" />
                        <asp:BoundField DataField="TotalHours" HeaderText="TotalHours" SortExpression="TotalHours" DataFormatString="{0:F0}" />
                    </Columns>
                </asp:GridView>
                <asp:SqlDataSource ID="sqlDATest" runat="server" ConnectionString="<%$ ConnectionStrings:dAdBConnectionString %>" 
                    SelectCommand="SELECT DISTINCT * FROM [dATest] Order By Date">
                    <%--WHERE ([Customer] = @Customer)--%>
                    <SelectParameters>
                        <asp:Parameter DefaultValue="Haeger" Name="Customer" Type="String" />
                    </SelectParameters>
                </asp:SqlDataSource>
            </div>

             <div class="col-xs-3">

            </div>
        </div>
        <br />
                <br />

                <br />

                <br />

                <br />

                <br />

                <br />

                <br />


    </div>




</asp:Content>
