namespace Haeger2018.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Initial : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.AspNetUsers", "CompanyName", c => c.String());
            AddColumn("dbo.AspNetUsers", "StreetAddress", c => c.String());
            AddColumn("dbo.AspNetUsers", "CityUSA", c => c.String());
            AddColumn("dbo.AspNetUsers", "StateUSA", c => c.String());
            AddColumn("dbo.AspNetUsers", "StateProvince", c => c.String());
            AddColumn("dbo.AspNetUsers", "ZipCode", c => c.String());
            AddColumn("dbo.AspNetUsers", "Country", c => c.String());
            AddColumn("dbo.AspNetUsers", "FaxNumber", c => c.Int(nullable: false));
            AddColumn("dbo.AspNetUsers", "ContactName", c => c.String());
            AddColumn("dbo.AspNetUsers", "ContactPhone", c => c.String());
            AddColumn("dbo.AspNetUsers", "ContactPosition", c => c.String());
            AddColumn("dbo.AspNetUsers", "HearAboutUs", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.AspNetUsers", "HearAboutUs");
            DropColumn("dbo.AspNetUsers", "ContactPosition");
            DropColumn("dbo.AspNetUsers", "ContactPhone");
            DropColumn("dbo.AspNetUsers", "ContactName");
            DropColumn("dbo.AspNetUsers", "FaxNumber");
            DropColumn("dbo.AspNetUsers", "Country");
            DropColumn("dbo.AspNetUsers", "ZipCode");
            DropColumn("dbo.AspNetUsers", "StateProvince");
            DropColumn("dbo.AspNetUsers", "StateUSA");
            DropColumn("dbo.AspNetUsers", "CityUSA");
            DropColumn("dbo.AspNetUsers", "StreetAddress");
            DropColumn("dbo.AspNetUsers", "CompanyName");
        }
    }
}
