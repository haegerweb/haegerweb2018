﻿<%@ Page Title="Create a new account." Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Register.aspx.cs" Inherits="Haeger2018.Account.Register" MaintainScrollPositionOnPostback="true" %>

<asp:Content runat="server" ID="Content1" ContentPlaceHolderID="Head">
    <style>
        .requiredRed {
            color: red;
        }

        .red
        {
            color:red;
        }

        .pemBlue
        {
            color:#0085ca
        }
         /* The container */
        .container1 {
            display:block;
            position:center;
            padding-left: 35px;
            margin-bottom: 12px;
            cursor: pointer;
            font-size: 22px;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
            font-size: 16px;
        }

        /* Hide the browser's default checkbox */
        .container1 input {
            position: absolute;
            opacity: 0;
            cursor: pointer;
    
        }

         .checkbox .btn, .checkbox-inline .btn {
            padding-left: 2em;
            min-width: 8em;
            }
            .checkbox label, .checkbox-inline label {
            text-align: left;
            padding-left: 0.5em;
            }
            .checkbox input[type="checkbox"]{
                float:none;
            }


        /* Create a custom checkbox */
        .checkmark {
            position: absolute;
            top: 0;
            left: 0;
            height: 25px;
            width: 25px;
            background-color: red;
    
        }

        .container1:hover {
            background-color:greenyellow;
        }

        /* On mouse-over, add a grey background color */
        .container1:hover input ~ .checkmark {
            background-color: #ccc;
        }

        /* When the checkbox is checked, add a blue background */
        .container1 input:checked ~ .checkmark {
            background-color: #0085ca;
        }

        /* Create the checkmark/indicator (hidden when not checked) */
        .checkmark:after {
            content: "";
            position: absolute;
            display: none;
        }

        /* Show the checkmark when checked */
        .container1 input:checked ~ .checkmark:after {
            display: block;
        }

        /* Style the checkmark/indicator */
        .container1 .checkmark:after {
            left: 9px;
            top: 5px;
            width: 5px;
            height: 10px;
            border: solid white;
            border-width: 0 3px 3px 0;
            -webkit-transform: rotate(45deg);
            -ms-transform: rotate(45deg);
            transform: rotate(45deg);
        }
    </style>


</asp:Content>


<asp:Content runat="server" ID="BodyContent" ContentPlaceHolderID="MainContent">
    
     <%--Title--%>
    <div class="section-title">
        <h2 style="margin: 0;"><%: Title %></h2>
        
    </div>
    <br />
    <p class="text-danger">
        <asp:Literal runat="server" ID="ErrorMessage" />
    </p>

    <div class="form-horizontal">
        <div class="form-inline">
             <h4>Items in&nbsp</h4><h4 class="red">RED&nbsp</h4><h4> must be filled in.</h4><br />
        </div>
        <div class="form-inline">
            <h4>If you sign up as a&nbsp</h4><h4 class="pemBlue">Customer</h4><h4>, you will receive a confirmation email.</h4>
        </div>
        <div class="form-inline">
            <h4>If you sign up as a&nbsp</h4><h4 class="pemBlue">Distributor</h4><h4>, there is a&nbsp<h4 class="red">two step approval process</h4>.</h4>
        </div>
         <div class="form-inline">
             <ol>
                 <li><h4>You will receive a confirmation email. Click the link in the email to verify your email address.</h4></li>
                 <li><h4>Finally, your access will have to be approved by Haeger.</h4></li>
             </ol>
             
         </div>
        <div class="form-inline center-block">
            <h3>Once both these steps are completed, you will gain access.</h3>
        </div>
        <hr />
        <asp:ValidationSummary runat="server" CssClass="text-danger" />
        <div class="form-group">
            <asp:Label runat="server" AssociatedControlID="UserRegion" CssClass="col-md-2 control-label requiredRed">Global Region</asp:Label>
            <div class="col-md-10">
                <asp:DropDownList ID="UserRegion" runat="server" class="btn btn-primary dropdown-toggle" >
                    <asp:ListItem Text="--Select One--" Value="0" ></asp:ListItem>
                    <asp:ListItem Text="Asia" Value="Asia"></asp:ListItem>
                     <asp:ListItem Text="Europe" Value="Europe"></asp:ListItem>
                    <asp:ListItem Text="North America" Value="NorthAmerica"></asp:ListItem>

                </asp:DropDownList>
                <asp:RequiredFieldValidator runat="server" ControlToValidate="UserRegion" InitialValue="0"
                    CssClass="text-danger" Display="Dynamic" ErrorMessage="The region field is required." />
            </div>
        </div>
         <div class="form-group">
            <asp:Label runat="server" AssociatedControlID="UserType" CssClass="col-md-2 control-label requiredRed">I am a:</asp:Label>
            <div class="col-md-10">
                <asp:DropDownList ID="UserType" runat="server" class="btn btn-primary dropdown-toggle" >
                    <asp:ListItem Text="--Select One--" Value="0" ></asp:ListItem>
                     <asp:ListItem Text="Customer" Value="Customer"></asp:ListItem>
                     <asp:ListItem Text="Distributor" Value="Distributor"></asp:ListItem>
                    <asp:ListItem Text="Service Community" Value="Service"></asp:ListItem>
                    
                </asp:DropDownList>
                <asp:RequiredFieldValidator runat="server" ControlToValidate="UserType" InitialValue="0"
                    CssClass="text-danger" Display="Dynamic" ErrorMessage="The user type field is required." />
            </div>
        </div>
         <div class="form-group">
            <asp:Label runat="server" AssociatedControlID="CompanyName" CssClass="col-md-2 control-label requiredRed">Company Name</asp:Label>
            <div class="col-md-10">
                <asp:TextBox runat="server" ID="CompanyName" CssClass="form-control" />
                <asp:RequiredFieldValidator runat="server" ControlToValidate="CompanyName"
                    CssClass="text-danger" ErrorMessage="The company name field is required." />
            </div>
        </div>
         <div class="form-group">
            <asp:Label runat="server" AssociatedControlID="ContactName" CssClass="col-md-2 control-label requiredRed">Contact Name</asp:Label>
            <div class="col-md-10">
                <asp:TextBox runat="server" ID="ContactName" CssClass="form-control" />
                <asp:RequiredFieldValidator runat="server" ControlToValidate="ContactName"
                    CssClass="text-danger" ErrorMessage="The contact name field is required." />
            </div>
        </div>

        <div class="form-group">
            <asp:Label runat="server" AssociatedControlID="Email" CssClass="col-md-2 control-label requiredRed">Email</asp:Label>
            <div class="col-md-10">
                <asp:TextBox runat="server" ID="Email" CssClass="form-control" TextMode="Email" />
                <asp:RequiredFieldValidator runat="server" ControlToValidate="Email"
                    CssClass="text-danger" ErrorMessage="The email field is required." />
            </div>
        </div>
        <div class="form-group">
            <asp:Label runat="server" AssociatedControlID="Password" CssClass="col-md-2 control-label requiredRed">Password</asp:Label>
            <div class="col-md-10">
                <asp:TextBox runat="server" ID="Password" TextMode="Password" CssClass="form-control" />
                <asp:RequiredFieldValidator runat="server" ControlToValidate="Password"
                    CssClass="text-danger" ErrorMessage="The password field is required." />
            </div>
        </div>
        <div class="form-group">
            <asp:Label runat="server" AssociatedControlID="ConfirmPassword" CssClass="col-md-2 control-label requiredRed">Confirm password</asp:Label>
            <div class="col-md-10">
                <asp:TextBox runat="server" ID="ConfirmPassword" TextMode="Password" CssClass="form-control" />
                <asp:RequiredFieldValidator runat="server" ControlToValidate="ConfirmPassword"
                    CssClass="text-danger" Display="Dynamic" ErrorMessage="The confirm password field is required." />
                <asp:CompareValidator runat="server" ControlToCompare="Password" ControlToValidate="ConfirmPassword"
                    CssClass="text-danger" Display="Dynamic" ErrorMessage="The password and confirmation password do not match." />
            </div>
        </div>
        <br />
        <div class="form-group">
            <asp:Label runat="server" AssociatedControlID="ContactPosition" CssClass="col-md-2 control-label requiredRed">Contact Position</asp:Label>
            <div class="col-md-10">
                <asp:TextBox runat="server" ID="ContactPosition" CssClass="form-control" />
                <asp:RequiredFieldValidator runat="server" ControlToValidate="ContactPosition"
                    CssClass="text-danger" ErrorMessage="The contact position field is required." />
            </div>
        </div>
         <div class="form-group">
            <asp:Label runat="server" AssociatedControlID="ContactPhone" CssClass="col-md-2 control-label requiredRed">Contact Phone</asp:Label>
            <div class="col-md-10">
                <asp:TextBox runat="server" ID="ContactPhone" CssClass="form-control" />
                <asp:RequiredFieldValidator runat="server" ControlToValidate="ContactPhone"
                    CssClass="text-danger" ErrorMessage="The contact phone field is required." />
            </div>
        </div>

          <br />
            <br />

            <%--GDPR Consent Fail Reminder--%>
            <div class="row">
                 <div class="col-md-1 col-lg-1 col-sm-1 col-xs-12 text-nowrap">
                    
                </div>
                <div class="col-md-8 col-lg-8 col-sm-8 col-xs-12">
                    <span>
                        <asp:Label ID="lblConsentWrn" CssClass="pull-left" runat="server" BorderStyle="Solid" BorderColor="GreenYellow" Text="" Visible="false">
                             <i class="fa fa-arrow-down"></i> Consent must be checked <i class="fa fa-arrow-down"></i>
                        </asp:Label>
                    </span>
                </div>
            </div>
            <%--END GDPR Consent Fail Reminder--%>

            <%--GDPR Consent Checkbox--%>
            <div class="row">
                <div class="col-md-2 col-lg-2 col-sm-2 col-xs-12 text-nowrap">
                    
                </div>
                <div class="col-md-8 col-lg-8 col-sm-8 col-xs-12">
                    <label class="container1">
                        <p>I have read the 
                            <asp:HyperLink ID="hlPrivacy" runat="server" NavigateUrl="/PrivacyPolicy" Target="_blank">Privacy Policy</asp:HyperLink>
                            /
                            <asp:HyperLink ID="hlTOS" runat="server" NavigateUrl="/TOS" Target="_blank">Terms of Use</asp:HyperLink>. 
                            I consent to the collection and use of the personal data submitted on the form above.</p>
                        <input id="gdprConsentBox" runat="server" type="checkbox">
                        <span class="checkmark"></span>
                    </label>
                </div>
            </div>
            <%--END GDPR Consent Checkbox--%>
            <br />
            <br />


        <div class="form-group">
            <div class="col-md-offset-2 col-md-10">
                <asp:Button runat="server" OnClick="CreateUser_Click" Text="Create New Account" CssClass="btn btn-primary" />
            </div>
        </div>
    </div>
</asp:Content>
