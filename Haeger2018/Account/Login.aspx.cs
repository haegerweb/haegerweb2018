﻿using System;
using System.Web;
using System.Web.UI;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Owin;
using Haeger2018.Models;
using System.Data.SqlClient;

namespace Haeger2018.Account
{
    public partial class Login : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            Session["DummySession"] = "DummySession";
            RegisterHyperLink.NavigateUrl = "Register";
            // Enable this once you have account confirmation enabled for password reset functionality
            ForgotPasswordHyperLink.NavigateUrl = "Forgot";
            //OpenAuthLogin.ReturnUrl = Request.QueryString["ReturnUrl"];
            var returnUrl = HttpUtility.UrlEncode(Request.QueryString["ReturnUrl"]);
            if (!String.IsNullOrEmpty(returnUrl))
            {
                RegisterHyperLink.NavigateUrl += "?ReturnUrl=" + returnUrl;
            }
        }

        protected void LogIn(object sender, EventArgs e)
        {
            if (IsValid)
            {
                // Validate the user password
                var manager = Context.GetOwinContext().GetUserManager<ApplicationUserManager>();
                var signinManager = Context.GetOwinContext().GetUserManager<ApplicationSignInManager>();

                // Require the user to have a confirmed email before they can log on.
                var user = manager.FindByName(Email.Text);
                if (user != null)
                {
                    if (!user.LockoutEnabled)
                    {
                        FailureText.Text = "Invalid login attempt. You must have a your account approved.";
                        ErrorMessage.Visible = true;
                    }
                    else if (!user.EmailConfirmed)
                    {
                        FailureText.Text = "Invalid login attempt. You must have a confirmed email account.";
                        ErrorMessage.Visible = true;
                        ResendConfirm.Visible = true;
                    }
                    else
                    {

                        // This doen't count login failures towards account lockout
                        // To enable password failures to trigger lockout, change to shouldLockout: true
                        var result = signinManager.PasswordSignIn(Email.Text, Password.Text, RememberMe.Checked, shouldLockout: false);

                        switch (result)
                        {
                            case SignInStatus.Success:
                                try
                                {
                                    string connectionString = "Data Source=HAEGERRACKWEB\\HAEGERRACKWEB;Initial Catalog=aspnet-Haeger2018-Membership;Integrated Security=False;User ID=sa;Password=JzuXnDqJgEfYMp5btuzZ";
                                    using (SqlConnection conn = new SqlConnection(connectionString))
                                    {
                                        conn.Open();
                                        using (SqlCommand cmd = new SqlCommand("UPDATE AspNetUsers SET LoginCount = LoginCount + 1 WHERE Id = @userID", conn))
                                        {

                                            cmd.Parameters.AddWithValue("@userID", user.Id);
                                            int rows = cmd.ExecuteNonQuery();

                                        };

                                    };
                                }
                                catch (Exception)
                                {

                                    throw;
                                }
                                if (manager.IsInRole(user.Id, "DistributorAS"))
                                {
                                    Response.Redirect("/DistributorAS/DistributorASPage");
                                }
                                if (manager.IsInRole(user.Id, "DistributorNA"))
                                {
                                    Response.Redirect("/DistributorNA/DistributorNAPage");
                                }
                                if (manager.IsInRole(user.Id, "DistributorEU"))
                                {
                                    Response.Redirect("/DistributorEU/DistributorEUPage");
                                }
                                if (manager.IsInRole(user.Id, "CustomerEU"))
                                {
                                    Response.Redirect("/CustomerEU/CustomerEUPage");
                                }
                                if (manager.IsInRole(user.Id, "CustomerNA"))
                                {
                                    Response.Redirect("/CustomerNA/CustomerNAPage");
                                }
                                if (manager.IsInRole(user.Id, "PartnerNA"))
                                {
                                    Response.Redirect("/PartnerNA/PartnerNAHome");
                                }
                                if (manager.IsInRole(user.Id, "ServiceAS"))
                                {
                                    Response.Redirect("/Service/ServiceCommunity");
                                }
                                if (manager.IsInRole(user.Id, "ServiceNA"))
                                {
                                    Response.Redirect("/Service/ServiceCommunity");
                                }
                                if (manager.IsInRole(user.Id, "ServiceEU"))
                                {
                                    Response.Redirect("/Service/ServiceCommunity");
                                }
                                if (manager.IsInRole(user.Id, "ServiceNACoordinator"))
                                {
                                    Response.Redirect("/ServiceNACoordinator/ServiceCoordinatorHome");
                                }
                                else
                                {
                                    IdentityHelper.RedirectToReturnUrl(Request.QueryString["ReturnUrl"], Response);
                                }


                                break;
                            case SignInStatus.LockedOut:
                                Response.Redirect("/Account/Lockout");
                                break;
                            case SignInStatus.RequiresVerification:
                                Response.Redirect(String.Format("/Account/TwoFactorAuthenticationSignIn?ReturnUrl={0}&RememberMe={1}",
                                                                Request.QueryString["ReturnUrl"],
                                                                RememberMe.Checked),
                                                  true);
                                break;
                            case SignInStatus.Failure:
                            default:
                                FailureText.Text = "Invalid login attempt";
                                ErrorMessage.Visible = true;
                                break;
                        }
                    }
                }
            }
        }

        protected void SendEmailConfirmationToken(object sender, EventArgs e)
        {
            var manager = Context.GetOwinContext().GetUserManager<ApplicationUserManager>();
            var user = manager.FindByName(Email.Text);
            if (user != null)
            {
                if (!user.EmailConfirmed)
                {
                    string code = manager.GenerateEmailConfirmationToken(user.Id);
                    string callbackUrl = IdentityHelper.GetUserConfirmationRedirectUrl(code, user.Id, Request);
                    manager.SendEmail(user.Id, "Confirm your account", "Please confirm your account by clicking <a href=\"" + callbackUrl + "\">here</a>.");

                    FailureText.Text = "Confirmation email sent. Please view the email and confirm your account.";
                    ErrorMessage.Visible = true;
                    ResendConfirm.Visible = false;
                }
            }
        }


    }
}