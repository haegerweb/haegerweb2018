﻿ using System;
using System.Linq;
using System.Web;
using System.Web.UI;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Owin;
using Microsoft.AspNet.Identity.EntityFramework;
using Haeger2018.Models;
using System.Net.Mail;
using System.IO;

namespace Haeger2018.Account
{
    public partial class Register : Page
    {
        DateTime localDate = DateTime.Now;

        protected void CreateUser_Click(object sender, EventArgs e)
        {
            var msg = "Without your consent we cannot process your request.";

            // First check to warn user 
            if (!gdprConsentBox.Checked)
            {
                lblConsentWrn.Visible = true;

                Response.Write("<script>alert('" + msg + "')</script>");

                return;
            }

            var manager = Context.GetOwinContext().GetUserManager<ApplicationUserManager>();
            var signInManager = Context.GetOwinContext().Get<ApplicationSignInManager>();
            var user = new ApplicationUser()
            {
                UserName = Email.Text,
                Email = Email.Text,
                CompanyName = CompanyName.Text,
                //StreetAddress = StreetAddress.Text,
                //CityUSA = City.Text,
                //StateUSA = State.Text,
                //ZipCode = ZipCode.Text,
                //Country = Country.Text,
                ContactName = ContactName.Text,
                ContactPhone = ContactPhone.Text,
                ContactPosition = ContactPosition.Text,
                gdprConsent = true,
                dateCreated = localDate



            };

            var regionSelected = UserRegion.SelectedItem.Value;
            var userTypeSelected = UserType.SelectedItem.Value;
            string userTypeRegion = userTypeSelected + regionSelected;
            Models.ApplicationDbContext context = new ApplicationDbContext();
            var userMgr = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(context));
            IdentityResult result = manager.Create(user, Password.Text);


            if (result.Succeeded)
            {
                string userName = user.UserName;

                switch (userTypeRegion)
                {
                    case "CustomerEurope":

                        // For more information on how to enable account confirmation and password reset please visit https://go.microsoft.com/fwlink/?LinkID=320771
                        manager.AddToRole(user.Id, "CustomerEU");
                        string code = manager.GenerateEmailConfirmationToken(user.Id);
                        string callbackUrl = IdentityHelper.GetUserConfirmationRedirectUrl(code, user.Id, Request);
                        manager.SendEmail(user.Id, "Confirm your myHaeger account", "Thank you for registering with Haeger.com.  Please confirm your account by clicking <a href=\"" + callbackUrl + "\">here</a>.");
                        break;
                    case "CustomerNorthAmerica":

                        // For more information on how to enable account confirmation and password reset please visit https://go.microsoft.com/fwlink/?LinkID=320771
                        manager.AddToRole(user.Id, "CustomerNA");
                        string code1 = manager.GenerateEmailConfirmationToken(user.Id);
                        string callbackUrl1 = IdentityHelper.GetUserConfirmationRedirectUrl(code1, user.Id, Request);
                        manager.SendEmail(user.Id, "Confirm your myHaeger account", "Thank you for registering with Haeger.com. Please confirm your account by clicking <a href=\"" + callbackUrl1 + "\">here</a>.");
                        break;
                    case "CustomerNorthAsia":

                        // For more information on how to enable account confirmation and password reset please visit https://go.microsoft.com/fwlink/?LinkID=320771
                        manager.AddToRole(user.Id, "CustomerAS");
                        string code2 = manager.GenerateEmailConfirmationToken(user.Id);
                        string callbackUrl2 = IdentityHelper.GetUserConfirmationRedirectUrl(code2, user.Id, Request);
                        manager.SendEmail(user.Id, "Confirm your myHaeger account", "Thank you for registering with Haeger.com. Please confirm your account by clicking <a href=\"" + callbackUrl2 + "\">here</a>.");
                        break;
                    case "DistributorEurope":

                        manager.AddToRole(user.Id, "DistributorEU");
                        string code3 = manager.GenerateEmailConfirmationToken(user.Id);
                        string callbackUrl3 = IdentityHelper.GetUserConfirmationRedirectUrl(code3, user.Id, Request);
                        manager.SendEmail(user.Id, "Confirm your myHaeger account", "Thank you for registering with Haeger.com. Please confirm your account by clicking <a href=\"" + callbackUrl3 + "\">here</a>.");
                        manager.SetLockoutEnabled(user.Id, false);
                        sendEmail(userName, "aasbroek@haeger.com");
                        sendEmail(userName, "FRouwenhorst@haeger.com");
                        break;
                          
                    case "DistributorNorthAmerica":

                        manager.AddToRole(user.Id, "DistributorNA");
                        string code4 = manager.GenerateEmailConfirmationToken(user.Id);
                        string callbackUrl4 = IdentityHelper.GetUserConfirmationRedirectUrl(code4, user.Id, Request);
                        manager.SendEmail(user.Id, "Confirm your myHaeger account", "Thank you for registering with Haeger.com. Please confirm your account by clicking <a href=\"" + callbackUrl4 + "\">here</a>.");
                        manager.SetLockoutEnabled(user.Id, false);
                        sendEmail(userName, "RBoggs@haeger.com");
                        break;

                    case "DistributorAsia":

                        manager.AddToRole(user.Id, "DistributorNA");
                        string code5 = manager.GenerateEmailConfirmationToken(user.Id);
                        string callbackUrl5 = IdentityHelper.GetUserConfirmationRedirectUrl(code5, user.Id, Request);
                        manager.SendEmail(user.Id, "Confirm your myHaeger account", "Thank you for registering with Haeger.com. Please confirm your account by clicking <a href=\"" + callbackUrl5 + "\">here</a>.");
                        manager.SetLockoutEnabled(user.Id, false);
                        break;
                    case "ServiceEurope":

                        manager.AddToRole(user.Id, "ServiceEU");
                        string code6 = manager.GenerateEmailConfirmationToken(user.Id);
                        string callbackUrl6 = IdentityHelper.GetUserConfirmationRedirectUrl(code6, user.Id, Request);
                        manager.SendEmail(user.Id, "Confirm your myHaeger account", "Thank you for joining the Service Community with Haeger.com. Please confirm your account by clicking <a href=\"" + callbackUrl6 + "\">here</a>.");
                        manager.SetLockoutEnabled(user.Id, false);
                        sendEmail(userName, "hmeyer@haeger.com");
                        break;

                    case "ServiceNorthAmerica":

                        manager.AddToRole(user.Id, "ServiceNA");
                        string code7 = manager.GenerateEmailConfirmationToken(user.Id);
                        string callbackUrl7 = IdentityHelper.GetUserConfirmationRedirectUrl(code7, user.Id, Request);
                        manager.SendEmail(user.Id, "Confirm your myHaeger account", "Thank you for joining the Service Community with Haeger.com. Please confirm your account by clicking <a href=\"" + callbackUrl7 + "\">here</a>.");
                        manager.SetLockoutEnabled(user.Id, false);
                        sendEmail(userName, "hmeyer@haeger.com");
                        break;

                    case "ServiceAsia":

                        manager.AddToRole(user.Id, "ServiceAS");
                        string code8 = manager.GenerateEmailConfirmationToken(user.Id);
                        string callbackUrl8 = IdentityHelper.GetUserConfirmationRedirectUrl(code8, user.Id, Request);
                        manager.SendEmail(user.Id, "Confirm your myHaeger account", "Thank you for joining the Service Community with Haeger.com. Please confirm your account by clicking <a href=\"" + callbackUrl8 + "\">here</a>.");
                        manager.SetLockoutEnabled(user.Id, false);
                        break;
                    default:
                        break;
                }

                if (user.EmailConfirmed && user.LockoutEnabled)
                {
                    signInManager.SignIn(user, isPersistent: false, rememberBrowser: false);
                    IdentityHelper.RedirectToReturnUrl(Request.QueryString["ReturnUrl"], Response);
                }
                else if (!user.EmailConfirmed)
                {
                    ErrorMessage.Text = "An email has been sent to your account. Please view the email and confirm your account to complete the registration process.";

                }

                else if (user.LockoutEnabled && !user.EmailConfirmed)
                {
                    signInManager.SignIn(user, isPersistent: false, rememberBrowser: false);
                    IdentityHelper.RedirectToReturnUrl(Request.QueryString["ReturnUrl"], Response);
                }
                else if (!user.LockoutEnabled)
                {
                    ErrorMessage.Text = "Your distributor account must be approved to complete the registration process.";
                }
            }
            else 
            {
                ErrorMessage.Text = result.Errors.FirstOrDefault();
            }
        }

        private void sendEmail(string userToBeApproved, string emailForApproval)
        {
            string userName = userToBeApproved;
            string email = emailForApproval;
            string subject = "APPROVAL REQUIRED: " + userName;

            string FilePath = @"D:/EmailTemplates/distributorToBeApproved.html";
            FileStream sourceStream = new FileStream(FilePath, FileMode.Open, FileAccess.ReadWrite, FileShare.ReadWrite);
            StreamReader str = new StreamReader(sourceStream);
            string MailText = str.ReadToEnd();
            str.Close();
            MailText = MailText.Replace("[distName]", userName);

            try
            {
                // Create the msg object to be sent
                MailMessage _mailmsg = new MailMessage();

                //Make TRUE because our body text is html
                _mailmsg.IsBodyHtml = true;

                // Configure the address we are sending the mail from
                MailAddress address = new MailAddress("webadmin@haeger.com");

                _mailmsg.From = address;

                //Add your email address to the recipients
                _mailmsg.To.Add(emailForApproval);
                _mailmsg.Bcc.Add("regeah811@gmail.com");
                _mailmsg.Bcc.Add("webadmin@haeger.com");

                //Set Subject
                _mailmsg.Subject = subject;

                //Set Body Text of Email
                _mailmsg.Body = MailText;

                // Configure an SmtpClient to send the mail.
                SmtpClient _smtp = new SmtpClient("smtp-mail.outlook.com");
                _smtp.Port = 587;
                _smtp.DeliveryMethod = SmtpDeliveryMethod.Network;
                _smtp.UseDefaultCredentials = false;
                System.Net.NetworkCredential credentials =
                    new System.Net.NetworkCredential("webadmin@haeger.com", "Crazycat75");
                _smtp.EnableSsl = true;
                _smtp.Credentials = credentials;



                //_smtp.DeliveryMethod = SmtpDeliveryMethod.SpecifiedPickupDirectory;
                //_smtp.PickupDirectoryLocation = @targetDirectory;

                // Send the msg.
                _smtp.Send(_mailmsg);

                
            }
            catch
            {
                // If the message failed at some point, let the user know.
                //lblResult.Text = "Your message failed to send, please try again.";

            }


        }


    }
}