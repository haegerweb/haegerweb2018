﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Haeger2018
{
    public partial class MachineManuals : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnGetInfo_Click(object sender, EventArgs e)
        {
            Session["machineName"] = drpManuals.SelectedItem.Value;

            Response.Redirect("MachineManualsType.aspx");
        }
    }
}

