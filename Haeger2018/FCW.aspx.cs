﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Haeger2018
{
    public partial class FCW : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            // Check the User Agent
            var agent = Request.UserAgent.ToLower();

            // Check if iOS, Android, or other
            if (agent.Contains("ios"))
            {
                Response.Redirect("https://itunes.apple.com/us/app/haeger-wizard/id625478700?mt=8&uo=4");
            }
            else if (agent.Contains("android"))
            {
                Response.Redirect("https://play.google.com/store/apps/details?id=edu.haeger.haegerwizard&hl=en");
            }
            else if (Request.Browser.IsMobileDevice)
            {
                Response.Redirect("https://itunes.apple.com/us/app/haeger-wizard/id625478700?mt=8&uo=4");
            }

            FCWizard.PreRender += new EventHandler(wzd_PreRender);
        }

        protected void wzd_PreRender(object sender, EventArgs e)
        {
            
        }

        public string GetClassForWizardStep(object wizardStep)
        {
            WizardStep step = wizardStep as WizardStep;

            if (step == null)
            {
                return "";
            }

            int stepIndex = FCWizard.WizardSteps.IndexOf(step);

            if (stepIndex < FCWizard.ActiveStepIndex)
            {
                return "stepCompleted";
            }
            else if (stepIndex > FCWizard.ActiveStepIndex)
            {
                return "stepNotCompleted";
            }
            else
            {
                return "stepCurrent";
            }
        }

        protected void rblFCWFastener_SelectedIndexChanged(object sender, EventArgs e)
        {
            Session["fcwFastenerSelected"] = rblFCWFastener.SelectedItem.Text;

            lblFastener.Text = "Fastener:   " + rblFCWFastener.SelectedItem.Text;
            lblFastener.Visible = true;
            FCWizard.MoveTo(this.WizardStep2);
        }
        protected void rblFCWMaterial_SelectedIndexChanged(object sender, EventArgs e)
        {
            Session["fcwMaterialSelected"] = rblFCWMaterial.SelectedItem.Text;

            lblMaterial.Text = "Material:   " + rblFCWMaterial.SelectedItem.Text;
            lblMaterial.Visible = true;
            FCWizard.MoveTo(this.WizardStep3);
        }

        protected void rblFCWThreadType_SelectedIndexChanged(object sender, EventArgs e)
        {
            Session["fcwThreadTypeSelected"] = rblFCWThreadType.SelectedItem.Text;

            lblThreadType.Text = "Thread Type:   " + rblFCWThreadType.SelectedItem.Text;
            lblThreadType.Visible = true;
            FCWizard.MoveTo(this.WizardStep4);

        }

        protected void rblFCWDashLength_SelectedIndexChanged(object sender, EventArgs e)
        {
            Session["fcwDashLengthSelected"] = rblFCWDashLength.SelectedItem.Text;

            lblDashLength.Text = "Dash Length:   " + rblFCWDashLength.SelectedItem.Text;
            lblDashLength.Visible = true;

            FCWizard.MoveTo(this.WizardStep5);
        }

        protected void btnReset_Click(object sender, EventArgs e)
        {
            Response.Redirect("FCW.aspx");
        }

        protected void FCWizard_NextButtonClick(object sender, WizardNavigationEventArgs e)
        {


        }

        protected void FCWizard_FinishButtonClick(object sender, WizardNavigationEventArgs e)
        {


        }
    }
}