﻿<%@ Page Title="Wizards" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Wizards.aspx.cs" Inherits="Haeger2018.Wizards" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">

    <style>
        .haegerTile
        {
            height: 100px;
            width: 350px;
            justify-content:center;
           text-align:center

        }
        .h1
        {
            font-size:large;
            color:white;
        }

    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div class="container-fluid">
        <div class="row equal-height">            
            <div class="col-sm-6">
               <div class="summernote" data-contentid="20">
              <h2 class="section-title"><strong>Haeger Wizards</strong><br /></h2>
                   </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-4">
                <a class="tileLink" href="ATW.aspx" target="_blank">
                    <div class="tile nosize dark-blue animated zoomIn animation-delay-3">
                        <h2>Auto Tooling Wizard</h2>
                    </div>
                </a>
            </div>
            <div class="col-sm-4">
                <a class="tileLink" href="MTW.aspx" target="_blank">
                    <div class="tile nosize dark-red animated zoomIn animation-delay-3">
                        <h2>Manual Tooling Wizard</h2>
                    </div>
                </a>
            </div>
            <div class="col-sm-4">
                <a class="tileLink" href="FCW.aspx" target="_blank">
                    <div class="tile nosize dark-green animated zoomIn animation-delay-3">
                        <h2>Force Chart</h2>
                    </div>
                </a>
            </div>
        </div>
        <br />
        <br />
        <br />


        

    </div>

</asp:Content>
