﻿<%@ Page Title="Install Checklists" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="InstallChecklists.aspx.cs" Inherits="Haeger2018.Service.InstallChecklists" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">

    <style>
        span1{
            color:#0085ca;
        }

        .distLinks{
            font-weight:bold;
            font-size:18px;
        }
    </style>


</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    
    <div class="container-fluid">    
        <div class="section-title">
            <h2 style="margin: 0;">Haeger Service Community - Install Checklists</h2>
            <h3 style="text-transform:initial; color:black; margin:0;">A <span1> PennEngineering&reg</span1> Company</h3>
        </div>
        <br />
        
        <br />
        <div class="row">
            <div class="col-sm-4">
                <%-- <h3>Useful Pages</h3>
                <hr />--%>
                
            </div>
            <div class="col-sm-4">
               <%-- <h3>Useful Forms</h3>
                <hr />--%>
               
            </div>
            <div class="col-sm-4">
                <h3>Install Checklists</h3>
                <hr />
                <a href="/Service/pdf/InstallChecklist/618-824MSPe-Warranty-Reg.pdf" target="_blank">618/824 MSPe Install Checklist (pdf)</a>
                <br />
                <a href="/Service/pdf/InstallChecklist/WT-4-Install-Checklist.pdf" target="_blank">824 Window Touch-4e Install Checklist (pdf)</a>
                <br />
                <a href="/Service/pdf/InstallChecklist/824 OT-4-Install-Checklist.pdf" target="_blank">824 One Touch-4e Install Checklist (pdf)</a>
                <br />
                <a href="/Service/pdf/InstallChecklist/OTL-4-Warranty-Reg.pdf" target="_blank">824 One Touch Lite-4e Install Checklist (pdf)</a>
                <br />
                <a href="/Service/pdf/InstallChecklist/PS4 Installation Training Checklist.pdf" target="_blank">PS4 Installation Training Checklist (pdf)</a>
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />


            </div>
        </div>
    </div>





</asp:Content>
