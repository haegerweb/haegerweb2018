﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Haeger2018.Startup))]
namespace Haeger2018
{
    public partial class Startup {
        public void Configuration(IAppBuilder app) {
            ConfigureAuth(app);
        }
    }
}
