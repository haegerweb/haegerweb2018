﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="PartnerRequests.aspx.cs" Inherits="Haeger2018.PartnerNA.PartnerRequests" maintainScrollPositionOnPostBack = "true" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div class="container-fluid text-center">
        <div class="row">       
            <div class="col-xs-6">
                <div class="section-title">
                    <h2>Welcome, <asp:Label ID="lblPartnerName" runat="server" Text="Partner Name"></asp:Label></h2> 
                    <h3>Partner Previous Requests</h3>
                    <%--<h3 style="text-transform:initial; color:black; margin:0;">A <span1> PennEngineering&reg</span1> Company</h3>         --%> 
                    <hr />
                </div>
            </div>
             <div class="col-xs-6">
                 
                
                <br />
                <h4><asp:Label ID="lblDate" runat="server" Text="Current Week"></asp:Label></h4>
            </div>
           
           
            <br />
        </div>
        <div class="row">
            
            <div class="col-sm-12 col-xs-12">
                <asp:GridView ID="grdPartnerReq" CssClass="table table-striped table-bordered table-condensed"  runat="server" AutoGenerateColumns="False" DataKeyNames="column1" DataSourceID="sqlPartnerReq" EnableSortingAndPagingCallbacks="False" AllowSorting="True">
                    <Columns>
                        <asp:BoundField DataField="column1" HeaderText="column1" ReadOnly="True" SortExpression="column1" Visible="false" />
                        <asp:BoundField DataField="WeekNum" HeaderText="WeekNum" SortExpression="WeekNum" />
                        <asp:BoundField DataField="DateCreated" HeaderText="DateCreated" SortExpression="DateCreated" />
                        <asp:BoundField DataField="ReqYear" HeaderText="ReqYear" SortExpression="ReqYear" />
                        <asp:BoundField DataField="Status" HeaderText="Status" SortExpression="Status" />
                        <asp:BoundField DataField="RegHoursTotal" HeaderText="RegHoursTotal" SortExpression="RegHoursTotal" />
                        <asp:BoundField DataField="OverTimeHoursTotal" HeaderText="OverTimeHoursTotal" SortExpression="OverTimeHoursTotal" />
                        <asp:BoundField DataField="DoubleOTHoursTotal" HeaderText="DoubleOTHoursTotal" SortExpression="DoubleOTHoursTotal" />
                        <asp:BoundField DataField="VacHoursTotal" HeaderText="VacHoursTotal" SortExpression="VacHoursTotal" />
                        <asp:BoundField DataField="ArpHoursTotal" HeaderText="ArpHoursTotal" SortExpression="ArpHoursTotal" />
                        <asp:BoundField DataField="HolidayHoursTotal" HeaderText="HolidayHoursTotal" SortExpression="HolidayHoursTotal" />
                        <asp:BoundField DataField="JuryDutyHoursTotal" HeaderText="JuryDutyHoursTotal" SortExpression="JuryDutyHoursTotal" />
                        <asp:BoundField DataField="BereaveHoursTotal" HeaderText="BereaveHoursTotal" SortExpression="BereaveHoursTotal" />
                    </Columns>
                </asp:GridView>
                <asp:SqlDataSource ID="sqlPartnerReq" runat="server" ConnectionString="<%$ ConnectionStrings:HaegerTimeConnectionString %>" 
                                SelectCommand="SELECT [_OakdaleRQ_ID] AS column1, [WeekNum], [DateCreated], [ReqYear], [Status], 
                                                        [RegHoursTotal], [OverTimeHoursTotal], 
                                                        [DoubleOTHoursTotal], [VacHoursTotal], [ArpHoursTotal], [HolidayHoursTotal], 
                                                        [JuryDutyHoursTotal], [BereaveHoursTotal] 
                                                FROM [OakdaleRequests] 
                                                WHERE (([UserID] = @UserID) AND ([WeekNum] &lt;&gt; @WeekNum) AND ([ShowNoShow] = @ShowNoShow))">
                    <SelectParameters>
                        <asp:SessionParameter Name="UserID" SessionField="currentPartner" Type="String" />
                        <asp:Parameter DefaultValue="0" Name="WeekNum" Type="Int32" />
                        <asp:Parameter DefaultValue="Show" Name="ShowNoShow" Type="String" />
                    </SelectParameters> 
                </asp:SqlDataSource>

            </div>
            <div class="col-sm-3">

            </div>
        </div>

</div>






</asp:Content>
