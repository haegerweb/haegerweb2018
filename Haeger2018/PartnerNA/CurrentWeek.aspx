﻿<%@ Page Title="Current Week" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="CurrentWeek.aspx.cs" Inherits="Haeger2018.PartnerNA.CurrentWeek" maintainScrollPositionOnPostBack = "true" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">

    <style>
      
        th,td
        {
            text-align:left;
            vertical-align:central;
            align-content:flex-start;
            height:auto;
            padding-right: 10px;
            padding-left:10px;
           
        }
       
        span1
        {
            color:#0085ca;
        }
   
       
      </style>


</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <div class="container-fluid text-center">
        <div class="row">       
            <div class="col-xs-6">
                <div class="section-title">
                    <h2>Welcome, <asp:Label ID="lblPartnerName" runat="server" Text="Partner Name"></asp:Label></h2>
                    <h3>Time Sheet</h3>
                    <%--<h3 style="text-transform:initial; color:black; margin:0;">A <span1> PennEngineering&reg</span1> Company</h3>         --%>       
                </div>
            </div>
             <div class="col-xs-6">
                 
                
                <br />
                <h4>Week <asp:Label ID="lblWeekNum" runat="server" Text="#"></asp:Label>: <asp:Label ID="lblDateRange" runat="server" Text="01/01/1900 to 01/06/1900"></asp:Label></h4>
            </div>         
           
           
            <br />
        </div>

    <div class="row">
       
        <div class="col-xs-12">
             <table style="width:100%; table-layout:fixed">
            <tr>
                <th style="width:8%;">Date</th>
                <th style="width:8%;">Regular</th>
                <th style="width:8%;">Vacation</th>
                <th style="width:8%;">ARP(Sick)</th>
                <th style="width:8%;">Holiday</th>
                <th style="width:8%;">Bereavement</th>
                <th style="width:8%;">Jury Duty</th>
                <th style="width:8%;">OT</th>
                <th style="width:8%;">Double OT</th>
                <th style="width:20%;">Comments</th>
               
            </tr>
            <tr>
                <td style="width:8%;padding-bottom:15px">
                    <h3><asp:Label CssClass="label label-primary" ID="lblDateMon" runat="server" Text="Mon"></asp:Label></h3>
                    
                </td>
               <td style="width:8%;padding-bottom:15px">
                  <h3><asp:Label CssClass="label label-default" ID="lblHrRegMon" runat="server" Text="0"></asp:Label></h3> 
                   
                </td>
                <td style="width:8%;">
                    <asp:TextBox CssClass="form-control" ID="txtHrVacMon" runat="server"></asp:TextBox>
                    <asp:RegularExpressionValidator ID="revVacMon" runat="server" ErrorMessage="Only Numbers" 
                        ValidationExpression="^\d+(\.\d{1,2})?$" ControlToValidate="txtHrVacMon" Display="Dynamic"></asp:RegularExpressionValidator>
                    <asp:CompareValidator ID="cvVacMon" runat="server"
                              ControlToValidate="txtHrVacMon" ErrorMessage="Must be &lt; 8"
                              Operator="LessThanEqual" Type="Double"
                              ValueToCompare="8" Display="Dynamic" />
                </td>
                 <td style="width:8%;">
                    <asp:TextBox CssClass="form-control" ID="txtHrARPMon" runat="server"></asp:TextBox>
                    <asp:RegularExpressionValidator ID="revARPMon" runat="server" ErrorMessage="Only Numbers" 
                        ValidationExpression="^\d+(\.\d{1,2})?$" ControlToValidate="txtHrARPMon" Display="Dynamic"></asp:RegularExpressionValidator>
                     <asp:CompareValidator ID="cvARPMon" runat="server"
                              ControlToValidate="txtHrARPMon" ErrorMessage="Must be &lt; 8"
                              Operator="LessThanEqual" Type="Double"
                              ValueToCompare="8" Display="Dynamic" />
                </td>
                 <td style="width:8%;">
                    <asp:TextBox CssClass="form-control" ID="txtHrHolidayMon" runat="server"></asp:TextBox>
                    <asp:RegularExpressionValidator ID="revHolMon" runat="server" ErrorMessage="Only Numbers" 
                        ValidationExpression="^\d+(\.\d{1,2})?$" ControlToValidate="txtHrHolidayMon" Display="Dynamic"></asp:RegularExpressionValidator>
                     <asp:CompareValidator ID="cvHolMon" runat="server"
                              ControlToValidate="txtHrHolidayMon" ErrorMessage="Must be &lt; 8"
                              Operator="LessThanEqual" Type="Double"
                              ValueToCompare="8" Display="Dynamic" />

                </td>
                 <td style="width:8%;">
                    <asp:TextBox CssClass="form-control" ID="txtHrBereaveMon" runat="server"></asp:TextBox>
                    <asp:RegularExpressionValidator ID="revBereaveMon" runat="server" ErrorMessage="Only Numbers" 
                        ValidationExpression="^\d+(\.\d{1,2})?$" ControlToValidate="txtHrBereaveMon" Display="Dynamic"></asp:RegularExpressionValidator>
                     <asp:CompareValidator ID="cvBereaveMon" runat="server"
                              ControlToValidate="txtHrBereaveMon" ErrorMessage="Must be &lt; 8"
                              Operator="LessThanEqual" Type="Double"
                              ValueToCompare="8" Display="Dynamic" />
                </td>
                 <td style="width:8%;">
                    <asp:TextBox CssClass="form-control" ID="txtHrJuryMon" runat="server"></asp:TextBox>
                    <asp:RegularExpressionValidator ID="revJurMon" runat="server" ErrorMessage="Only Numbers" 
                        ValidationExpression="^\d+(\.\d{1,2})?$" ControlToValidate="txtHrJuryMon" Display="Dynamic"></asp:RegularExpressionValidator>
                    <asp:CompareValidator ID="cvJurMon" runat="server"
                              ControlToValidate="txtHrJuryMon" ErrorMessage="Must be &lt; 8"
                              Operator="LessThanEqual" Type="Double"
                              ValueToCompare="8" Display="Dynamic" />

                </td>
                 <td style="width:8%;">
                    <asp:TextBox CssClass="form-control" ID="txtHROTMon" runat="server"></asp:TextBox>
                    <asp:RegularExpressionValidator ID="revOTMon" runat="server" ErrorMessage="Only Numbers" 
                        ValidationExpression="^\d+(\.\d{1,2})?$" ControlToValidate="txtHROTMon" Display="Dynamic"></asp:RegularExpressionValidator>
                    <asp:CompareValidator ID="cvOTMon" runat="server"
                              ControlToValidate="txtHROTMon" ErrorMessage="Must be &lt; 12"
                              Operator="LessThanEqual" Type="Double"
                              ValueToCompare="12" Display="Dynamic" />
                </td>

                 <td style="width:8%;">
                    <asp:TextBox CssClass="form-control" ID="txtHRDOTMon" runat="server"></asp:TextBox>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ErrorMessage="Only Numbers" 
                        ValidationExpression="^\d+(\.\d{1,2})?$" ControlToValidate="txtHRDOTMon" Display="Dynamic"></asp:RegularExpressionValidator>
                    <asp:CompareValidator ID="CompareValidator1" runat="server"
                              ControlToValidate="txtHRDOTMon" ErrorMessage="Must be &lt; 12"
                              Operator="LessThanEqual" Type="Double"
                              ValueToCompare="12" Display="Dynamic" />
                </td>
               
                <td style="width:20%;">
                    <asp:TextBox CssClass="form-control" ID="txtCommMon" runat="server"></asp:TextBox>
                    
                </td>
            </tr>
            <%--Tuesday--%>
            <tr>
                <td style="width:8%;padding-bottom:15px"">
                    <h3><asp:Label CssClass="label label-primary" ID="lblDateTue" runat="server" Text="Tue"></asp:Label></h3>
                    
                </td>
               <td style="width:8%;padding-bottom:15px"">
                  <h3><asp:Label CssClass="label label-default" ID="lblHrRegTue" runat="server" Text="0"></asp:Label></h3> 
                   
                </td>
                <td style="width:8%;">
                    <asp:TextBox CssClass="form-control" ID="txtHrVacTue" runat="server"></asp:TextBox>
                    <asp:RegularExpressionValidator ID="revVacTue" runat="server" ErrorMessage="Only Numbers" 
                        ValidationExpression="^\d+(\.\d{1,2})?$" ControlToValidate="txtHrVacTue" Display="Dynamic"></asp:RegularExpressionValidator>
                    <asp:CompareValidator ID="cvVacTue" runat="server"
                              ControlToValidate="txtHrVacTue" ErrorMessage="Must be &lt; 8"
                              Operator="LessThanEqual" Type="Double"
                              ValueToCompare="8" Display="Dynamic" />
                </td>
                 <td style="width:8%;">
                    <asp:TextBox CssClass="form-control" ID="txtHrARPTue" runat="server"></asp:TextBox>
                    <asp:RegularExpressionValidator ID="revARPTue" runat="server" ErrorMessage="Only Numbers" 
                        ValidationExpression="^\d+(\.\d{1,2})?$" ControlToValidate="txtHrARPTue" Display="Dynamic"></asp:RegularExpressionValidator>
                    <asp:CompareValidator ID="cvARPTue" runat="server"
                              ControlToValidate="txtHrARPTue" ErrorMessage="Must be &lt; 8"
                              Operator="LessThanEqual" Type="Double"
                              ValueToCompare="8" Display="Dynamic" />

                </td>
                 <td style="width:8%;">
                    <asp:TextBox CssClass="form-control" ID="txtHrHolidayTue" runat="server"></asp:TextBox>
                    <asp:RegularExpressionValidator ID="revHolTue" runat="server" ErrorMessage="Only Numbers" 
                        ValidationExpression="^\d+(\.\d{1,2})?$" ControlToValidate="txtHrHolidayTue" Display="Dynamic"></asp:RegularExpressionValidator>
                    <asp:CompareValidator ID="cvHolTue" runat="server"
                              ControlToValidate="txtHrHolidayTue" ErrorMessage="Must be &lt; 8"
                              Operator="LessThanEqual" Type="Double"
                              ValueToCompare="8" Display="Dynamic" />

                </td>
                 <td style="width:8%;">
                    <asp:TextBox CssClass="form-control" ID="txtHrBereaveTue" runat="server"></asp:TextBox>
                    <asp:RegularExpressionValidator ID="revBereaveTue" runat="server" ErrorMessage="Only Numbers" 
                        ValidationExpression="^\d+(\.\d{1,2})?$" ControlToValidate="txtHrBereaveTue" Display="Dynamic"></asp:RegularExpressionValidator>
                    <asp:CompareValidator ID="cvBereaveTue" runat="server"
                              ControlToValidate="txtHrBereaveTue" ErrorMessage="Must be &lt; 8"
                              Operator="LessThanEqual" Type="Double"
                              ValueToCompare="8" Display="Dynamic" />
                </td>
                 <td style="width:8%;">
                    <asp:TextBox CssClass="form-control" ID="txtHrJuryTue" runat="server"></asp:TextBox>
                    <asp:RegularExpressionValidator ID="revJurTue" runat="server" ErrorMessage="Only Numbers" 
                        ValidationExpression="^\d+(\.\d{1,2})?$" ControlToValidate="txtHrJuryTue" Display="Dynamic"></asp:RegularExpressionValidator>
                    <asp:CompareValidator ID="cvJurTue" runat="server"
                              ControlToValidate="txtHrJuryTue" ErrorMessage="Must be &lt; 8"
                              Operator="LessThanEqual" Type="Double"
                              ValueToCompare="8" Display="Dynamic" />
                </td>
                 <td style="width:8%;">
                    <asp:TextBox CssClass="form-control" ID="txtHROTTue" runat="server"></asp:TextBox>
                    <asp:RegularExpressionValidator ID="revOTTue" runat="server" ErrorMessage="Only Numbers" 
                        ValidationExpression="^\d+(\.\d{1,2})?$" ControlToValidate="txtHROTTue" Display="Dynamic"></asp:RegularExpressionValidator>
                    <asp:CompareValidator ID="cvOTTue" runat="server"
                              ControlToValidate="txtHROTTue" ErrorMessage="Must be &lt; 12"
                              Operator="LessThanEqual" Type="Double"
                              ValueToCompare="12" Display="Dynamic" />
                </td>

                 <td style="width:8%;">
                    <asp:TextBox CssClass="form-control" ID="txtHRDOTTue" runat="server"></asp:TextBox>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ErrorMessage="Only Numbers" 
                        ValidationExpression="^\d+(\.\d{1,2})?$" ControlToValidate="txtHRDOTTue" Display="Dynamic"></asp:RegularExpressionValidator>
                    <asp:CompareValidator ID="CompareValidator2" runat="server"
                              ControlToValidate="txtHRDOTTue" ErrorMessage="Must be &lt; 12"
                              Operator="LessThanEqual" Type="Double"
                              ValueToCompare="12" Display="Dynamic" />
                </td>
               
                <td style="width:20%;">
                    <asp:TextBox CssClass="form-control" ID="txtCommTue" runat="server"></asp:TextBox>
                    
                </td>
            </tr>
            <%--Wednesday--%>
            <tr>
                <td style="width:8%;padding-bottom:15px"">
                    <h3><asp:Label CssClass="label label-primary" ID="lblDateWed" runat="server" Text="Wed"></asp:Label></h3>
                    
                </td>
               <td style="width:8%;padding-bottom:15px"">
                  <h3><asp:Label CssClass="label label-default" ID="lblHrRegWed" runat="server" Text="0"></asp:Label></h3> 

                </td>
                <td style="width:8%;">
                    <asp:TextBox CssClass="form-control" ID="txtHrVacWed" runat="server"></asp:TextBox>
                    <asp:RegularExpressionValidator ID="revVacWed" runat="server" ErrorMessage="Only Numbers" 
                        ValidationExpression="^\d+(\.\d{1,2})?$" ControlToValidate="txtHrVacWed" Display="Dynamic"></asp:RegularExpressionValidator>
                    <asp:CompareValidator ID="cvVacWed" runat="server"
                              ControlToValidate="txtHrVacWed" ErrorMessage="Must be &lt; 8"
                              Operator="LessThanEqual" Type="Double"
                              ValueToCompare="8" Display="Dynamic" />
                </td>
                 <td style="width:8%;">
                    <asp:TextBox CssClass="form-control" ID="txtHrARPWed" runat="server"></asp:TextBox>
                    <asp:RegularExpressionValidator ID="revARPWed" runat="server" ErrorMessage="Only Numbers" 
                        ValidationExpression="^\d+(\.\d{1,2})?$" ControlToValidate="txtHrARPWed" Display="Dynamic"></asp:RegularExpressionValidator>
                     <asp:CompareValidator ID="cvARPWed" runat="server"
                              ControlToValidate="txtHrARPWed" ErrorMessage="Must be &lt; 8"
                              Operator="LessThanEqual" Type="Double"
                              ValueToCompare="8" Display="Dynamic" />

                </td>
                 <td style="width:8%;">
                    <asp:TextBox CssClass="form-control" ID="txtHrHolidayWed" runat="server"></asp:TextBox>
                    <asp:RegularExpressionValidator ID="revHolWed" runat="server" ErrorMessage="Only Numbers" 
                        ValidationExpression="^\d+(\.\d{1,2})?$" ControlToValidate="txtHrHolidayWed" Display="Dynamic"></asp:RegularExpressionValidator>
                    <asp:CompareValidator ID="cvHolWed" runat="server"
                              ControlToValidate="txtHrHolidayWed" ErrorMessage="Must be &lt; 8"
                              Operator="LessThanEqual" Type="Double"
                              ValueToCompare="8" Display="Dynamic" />
                </td>
                 <td style="width:8%;">
                    <asp:TextBox CssClass="form-control" ID="txtHrBereaveWed" runat="server"></asp:TextBox>
                    <asp:RegularExpressionValidator ID="revBereaveWed" runat="server" ErrorMessage="Only Numbers" 
                        ValidationExpression="^\d+(\.\d{1,2})?$" ControlToValidate="txtHrBereaveWed" Display="Dynamic"></asp:RegularExpressionValidator>
                    <asp:CompareValidator ID="cvBereaveWed" runat="server"
                              ControlToValidate="txtHrBereaveWed" ErrorMessage="Must be &lt; 8"
                              Operator="LessThanEqual" Type="Double"
                              ValueToCompare="8" Display="Dynamic" />
                </td>
                 <td style="width:8%;">
                    <asp:TextBox CssClass="form-control" ID="txtHrJuryWed" runat="server"></asp:TextBox>
                    <asp:RegularExpressionValidator ID="revJurWed" runat="server" ErrorMessage="Only Numbers" 
                        ValidationExpression="^\d+(\.\d{1,2})?$" ControlToValidate="txtHrJuryWed" Display="Dynamic"></asp:RegularExpressionValidator>
                    <asp:CompareValidator ID="cvJurWed" runat="server"
                              ControlToValidate="txtHrJuryWed" ErrorMessage="Must be &lt; 8"
                              Operator="LessThanEqual" Type="Double"
                              ValueToCompare="8" Display="Dynamic" />
                </td>
                 <td style="width:8%;">
                    <asp:TextBox CssClass="form-control" ID="txtHROTWed" runat="server"></asp:TextBox>
                    <asp:RegularExpressionValidator ID="revOTWed" runat="server" ErrorMessage="Only Numbers" 
                        ValidationExpression="^\d+(\.\d{1,2})?$" ControlToValidate="txtHROTWed" Display="Dynamic"></asp:RegularExpressionValidator>
                    <asp:CompareValidator ID="cvOTWed" runat="server"
                              ControlToValidate="txtHROTWed" ErrorMessage="Must be &lt; 12"
                              Operator="LessThanEqual" Type="Double"
                              ValueToCompare="12" Display="Dynamic" />
                </td>

                 <td style="width:8%;">
                    <asp:TextBox CssClass="form-control" ID="txtHRDOTWed" runat="server"></asp:TextBox>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" ErrorMessage="Only Numbers" 
                        ValidationExpression="^\d+(\.\d{1,2})?$" ControlToValidate="txtHRDOTWed" Display="Dynamic"></asp:RegularExpressionValidator>
                    <asp:CompareValidator ID="CompareValidator3" runat="server"
                              ControlToValidate="txtHRDOTWed" ErrorMessage="Must be &lt; 12"
                              Operator="LessThanEqual" Type="Double"
                              ValueToCompare="12" Display="Dynamic" />
                </td>
               
                <td style="width:20%;">
                    <asp:TextBox CssClass="form-control" ID="txtCommWed" runat="server"></asp:TextBox>
                    
                </td>
            </tr>
            <%--Thursday--%>
            <tr>
                <td style="width:8%;padding-bottom:15px"">
                    <h3><asp:Label CssClass="label label-primary" ID="lblDateThu" runat="server" Text="Thu"></asp:Label></h3>
                    
                </td>
               <td style="width:8%;padding-bottom:15px"">
                  <h3><asp:Label CssClass="label label-default" ID="lblHrRegThu" runat="server" Text="0"></asp:Label></h3> 

                </td>
                <td style="width:8%;">
                    <asp:TextBox CssClass="form-control" ID="txtHrVacThu" runat="server"></asp:TextBox>
                    <asp:RegularExpressionValidator ID="revVacThu" runat="server" ErrorMessage="Only Numbers" 
                        ValidationExpression="^\d+(\.\d{1,2})?$" ControlToValidate="txtHrVacThu" Display="Dynamic"></asp:RegularExpressionValidator>
                    <asp:CompareValidator ID="cvVacThu" runat="server"
                              ControlToValidate="txtHrVacThu" ErrorMessage="Must be &lt; 8"
                              Operator="LessThanEqual" Type="Double"
                              ValueToCompare="8" Display="Dynamic" />
                </td>
                 <td style="width:8%;">
                    <asp:TextBox CssClass="form-control" ID="txtHrARPThu" runat="server"></asp:TextBox>
                    <asp:RegularExpressionValidator ID="revARPThu" runat="server" ErrorMessage="Only Numbers" 
                        ValidationExpression="^\d+(\.\d{1,2})?$" ControlToValidate="txtHrARPThu" Display="Dynamic"></asp:RegularExpressionValidator>
                    <asp:CompareValidator ID="cvARPThu" runat="server"
                              ControlToValidate="txtHrARPThu" ErrorMessage="Must be &lt; 8"
                              Operator="LessThanEqual" Type="Double"
                              ValueToCompare="8" Display="Dynamic" />

                </td>
                 <td style="width:8%;">
                    <asp:TextBox CssClass="form-control" ID="txtHrHolidayThu" runat="server"></asp:TextBox>
                    <asp:RegularExpressionValidator ID="revHolThu" runat="server" ErrorMessage="Only Numbers" 
                        ValidationExpression="^\d+(\.\d{1,2})?$" ControlToValidate="txtHrHolidayThu" Display="Dynamic"></asp:RegularExpressionValidator>
                    <asp:CompareValidator ID="cvHolThu" runat="server"
                              ControlToValidate="txtHrHolidayThu" ErrorMessage="Must be &lt; 8"
                              Operator="LessThanEqual" Type="Double"
                              ValueToCompare="8" Display="Dynamic" />
                </td>
                 <td style="width:8%;">
                    <asp:TextBox CssClass="form-control" ID="txtHrBereaveThu" runat="server"></asp:TextBox>
                    <asp:RegularExpressionValidator ID="revBereaveThu" runat="server" ErrorMessage="Only Numbers" 
                        ValidationExpression="^\d+(\.\d{1,2})?$" ControlToValidate="txtHrBereaveThu" Display="Dynamic"></asp:RegularExpressionValidator>
                    <asp:CompareValidator ID="cvBereaveThu" runat="server"
                              ControlToValidate="txtHrBereaveThu" ErrorMessage="Must be &lt; 8"
                              Operator="LessThanEqual" Type="Double"
                              ValueToCompare="8" Display="Dynamic" />
                </td>
                 <td style="width:8%;">
                    <asp:TextBox CssClass="form-control" ID="txtHrJuryThu" runat="server"></asp:TextBox>
                    <asp:RegularExpressionValidator ID="revJurThu" runat="server" ErrorMessage="Only Numbers" 
                        ValidationExpression="^\d+(\.\d{1,2})?$" ControlToValidate="txtHrJuryThu" Display="Dynamic"></asp:RegularExpressionValidator>
                    <asp:CompareValidator ID="cvJurThu" runat="server"
                              ControlToValidate="txtHrJuryThu" ErrorMessage="Must be &lt; 8"
                              Operator="LessThanEqual" Type="Double"
                              ValueToCompare="8" Display="Dynamic" />
                </td>
                 <td style="width:8%;">
                    <asp:TextBox CssClass="form-control" ID="txtHROTThu" runat="server"></asp:TextBox>
                    <asp:RegularExpressionValidator ID="revOTThu" runat="server" ErrorMessage="Only Numbers" 
                        ValidationExpression="^\d+(\.\d{1,2})?$" ControlToValidate="txtHROTThu" Display="Dynamic"></asp:RegularExpressionValidator>
                    <asp:CompareValidator ID="cvOTThu" runat="server"
                              ControlToValidate="txtHROTThu" ErrorMessage="Must be &lt; 12"
                              Operator="LessThanEqual" Type="Double"
                              ValueToCompare="12" Display="Dynamic" />
                </td>

                 <td style="width:8%;">
                    <asp:TextBox CssClass="form-control" ID="txtHRDOTThu" runat="server"></asp:TextBox>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator4" runat="server" ErrorMessage="Only Numbers" 
                        ValidationExpression="^\d+(\.\d{1,2})?$" ControlToValidate="txtHRDOTThu" Display="Dynamic"></asp:RegularExpressionValidator>
                    <asp:CompareValidator ID="CompareValidator4" runat="server"
                              ControlToValidate="txtHRDOTThu" ErrorMessage="Must be &lt; 12"
                              Operator="LessThanEqual" Type="Double"
                              ValueToCompare="12" Display="Dynamic" />
                </td>
               
                <td style="width:20%;">
                    <asp:TextBox CssClass="form-control" ID="txtCommThu" runat="server"></asp:TextBox>
                   
                </td>
            </tr>
            <%--Friday--%>
            <tr>
                <td style="width:8%;padding-bottom:15px"">
                    <h3><asp:Label CssClass="label label-primary" ID="lblDateFri" runat="server" Text="Fri"></asp:Label></h3>
                    
                </td>
               <td style="width:8%;padding-bottom:15px"">
                  <h3><asp:Label CssClass="label label-default" ID="lblHrRegFri" runat="server" Text="0"></asp:Label></h3> 

                </td>
                <td style="width:8%;">
                    <asp:TextBox CssClass="form-control" ID="txtHrVacFri" runat="server"></asp:TextBox>
                    <asp:RegularExpressionValidator ID="revVacFri" runat="server" ErrorMessage="Only Numbers" 
                        ValidationExpression="^\d+(\.\d{1,2})?$" ControlToValidate="txtHrVacFri" Display="Dynamic"></asp:RegularExpressionValidator>
                    <asp:CompareValidator ID="cvVacFri" runat="server"
                              ControlToValidate="txtHrVacFri" ErrorMessage="Must be &lt; 8"
                              Operator="LessThanEqual" Type="Double"
                              ValueToCompare="8" Display="Dynamic" />
                </td>
                 <td style="width:8%;">
                    <asp:TextBox CssClass="form-control" ID="txtHrARPFri" runat="server"></asp:TextBox>
                    <asp:RegularExpressionValidator ID="revARPFri" runat="server" ErrorMessage="Only Numbers" 
                        ValidationExpression="^\d+(\.\d{1,2})?$" ControlToValidate="txtHrARPFri" Display="Dynamic"></asp:RegularExpressionValidator>
                    <asp:CompareValidator ID="cvARPFri" runat="server"
                              ControlToValidate="txtHrARPFri" ErrorMessage="Must be &lt; 8"
                              Operator="LessThanEqual" Type="Double"
                              ValueToCompare="8" Display="Dynamic" />

                </td>
                 <td style="width:8%;">
                    <asp:TextBox CssClass="form-control" ID="txtHrHolidayFri" runat="server"></asp:TextBox>
                    <asp:RegularExpressionValidator ID="revHolFri" runat="server" ErrorMessage="Only Numbers" 
                        ValidationExpression="^\d+(\.\d{1,2})?$" ControlToValidate="txtHrHolidayFri" Display="Dynamic"></asp:RegularExpressionValidator>
                    <asp:CompareValidator ID="cvHolFri" runat="server"
                              ControlToValidate="txtHrHolidayFri" ErrorMessage="Must be &lt; 8"
                              Operator="LessThanEqual" Type="Double"
                              ValueToCompare="8" Display="Dynamic" />
                </td>
                 <td style="width:8%;">
                    <asp:TextBox CssClass="form-control" ID="txtHrBereaveFri" runat="server"></asp:TextBox>
                    <asp:RegularExpressionValidator ID="revBereaveFri" runat="server" ErrorMessage="Only Numbers" 
                        ValidationExpression="^\d+(\.\d{1,2})?$" ControlToValidate="txtHrBereaveFri" Display="Dynamic"></asp:RegularExpressionValidator>
                    <asp:CompareValidator ID="cvBereaveFri" runat="server"
                              ControlToValidate="txtHrBereaveFri" ErrorMessage="Must be &lt; 8"
                              Operator="LessThanEqual" Type="Double"
                              ValueToCompare="8" Display="Dynamic" />
                </td>
                 <td style="width:8%;">
                    <asp:TextBox CssClass="form-control" ID="txtHrJuryFri" runat="server"></asp:TextBox>
                    <asp:RegularExpressionValidator ID="revJurFri" runat="server" ErrorMessage="Only Numbers" 
                        ValidationExpression="^\d+(\.\d{1,2})?$" ControlToValidate="txtHrJuryFri" Display="Dynamic"></asp:RegularExpressionValidator>
                    <asp:CompareValidator ID="cvJurFri" runat="server"
                              ControlToValidate="txtHrJuryFri" ErrorMessage="Must be &lt; 8"
                              Operator="LessThanEqual" Type="Double"
                              ValueToCompare="8" Display="Dynamic" />
                </td>
                 <td style="width:8%;">
                    <asp:TextBox CssClass="form-control" ID="txtHROTFri" runat="server"></asp:TextBox>
                    <asp:RegularExpressionValidator ID="revOTFri" runat="server" ErrorMessage="Only Numbers" 
                        ValidationExpression="^\d+(\.\d{1,2})?$" ControlToValidate="txtHROTFri" Display="Dynamic"></asp:RegularExpressionValidator>
                    <asp:CompareValidator ID="cvOTFri" runat="server"
                              ControlToValidate="txtHROTFri" ErrorMessage="Must be &lt; 12"
                              Operator="LessThanEqual" Type="Double"
                              ValueToCompare="12" Display="Dynamic" />
                </td>

                 <td style="width:8%;">
                    <asp:TextBox CssClass="form-control" ID="txtHRDOTFri" runat="server"></asp:TextBox>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator5" runat="server" ErrorMessage="Only Numbers" 
                        ValidationExpression="^\d+(\.\d{1,2})?$" ControlToValidate="txtHRDOTFri" Display="Dynamic"></asp:RegularExpressionValidator>
                    <asp:CompareValidator ID="CompareValidator5" runat="server"
                              ControlToValidate="txtHRDOTFri" ErrorMessage="Must be &lt; 12"
                              Operator="LessThanEqual" Type="Double"
                              ValueToCompare="12" Display="Dynamic" />
                </td>
               
                <td style="width:20%;">
                    <asp:TextBox CssClass="form-control" ID="txtCommFri" runat="server"></asp:TextBox>
                   
                </td>
            </tr>
    <%--Saturday--%>
            <tr>
                <td style="width:8%;padding-bottom:15px"">
                    <h3><asp:Label CssClass="label label-primary" ID="lblDateSat" runat="server" Text="Sat"></asp:Label></h3>
                    
                </td>
               <td style="width:8%;padding-bottom:15px"">
                  <h3><asp:Label CssClass="label label-default" ID="lblHrRegSat" runat="server" Text="0"></asp:Label></h3> 

                </td>
                <td style="width:8%;">
                    <asp:TextBox CssClass="form-control" ID="txtHrVacSat" runat="server"></asp:TextBox>
                    <asp:RegularExpressionValidator ID="revVacSat" runat="server" ErrorMessage="Only Numbers" 
                        ValidationExpression="^\d+(\.\d{1,2})?$" ControlToValidate="txtHrVacSat" Display="Dynamic"></asp:RegularExpressionValidator>
                     <asp:CompareValidator ID="cvVacSat" runat="server"
                              ControlToValidate="txtHrVacSat" ErrorMessage="Must be &lt; 8"
                              Operator="LessThanEqual" Type="Double"
                              ValueToCompare="8" Display="Dynamic" />
                </td>
                 <td style="width:8%;">
                    <asp:TextBox CssClass="form-control" ID="txtHrARPSat" runat="server"></asp:TextBox>
                    <asp:RegularExpressionValidator ID="revARPSat" runat="server" ErrorMessage="Only Numbers" 
                        ValidationExpression="^\d+(\.\d{1,2})?$" ControlToValidate="txtHrARPSat" Display="Dynamic"></asp:RegularExpressionValidator>
                    <asp:CompareValidator ID="cvARPSat" runat="server"
                              ControlToValidate="txtHrARPSat" ErrorMessage="Must be &lt; 8"
                              Operator="LessThanEqual" Type="Double"
                              ValueToCompare="8" Display="Dynamic" />

                </td>
                 <td style="width:8%;">
                    <asp:TextBox CssClass="form-control" ID="txtHrHolidaySat" runat="server"></asp:TextBox>
                    <asp:RegularExpressionValidator ID="revHolSat" runat="server" ErrorMessage="Only Numbers" 
                        ValidationExpression="^\d+(\.\d{1,2})?$" ControlToValidate="txtHrHolidaySat" Display="Dynamic"></asp:RegularExpressionValidator>
                    <asp:CompareValidator ID="cvHolSat" runat="server"
                              ControlToValidate="txtHrHolidaySat" ErrorMessage="Must be &lt; 8"
                              Operator="LessThanEqual" Type="Double"
                              ValueToCompare="8" Display="Dynamic" />
                </td>
                 <td style="width:8%;">
                    <asp:TextBox CssClass="form-control" ID="txtHrBereaveSat" runat="server"></asp:TextBox>
                    <asp:RegularExpressionValidator ID="revBereaveSat" runat="server" ErrorMessage="Only Numbers" 
                        ValidationExpression="^\d+(\.\d{1,2})?$" ControlToValidate="txtHrBereaveSat" Display="Dynamic"></asp:RegularExpressionValidator>
                    <asp:CompareValidator ID="cvBereaveSat" runat="server"
                              ControlToValidate="txtHrBereaveSat" ErrorMessage="Must be &lt; 8"
                              Operator="LessThanEqual" Type="Double"
                              ValueToCompare="8" Display="Dynamic" />
                </td>
                 <td style="width:8%;">
                    <asp:TextBox CssClass="form-control" ID="txtHrJurySat" runat="server"></asp:TextBox>
                    <asp:RegularExpressionValidator ID="revJurSat" runat="server" ErrorMessage="Only Numbers" 
                        ValidationExpression="^\d+(\.\d{1,2})?$" ControlToValidate="txtHrJurySat" Display="Dynamic"></asp:RegularExpressionValidator>
                    <asp:CompareValidator ID="cvJurSat" runat="server"
                              ControlToValidate="txtHrJurySat" ErrorMessage="Must be &lt; 8"
                              Operator="LessThanEqual" Type="Double"
                              ValueToCompare="8" Display="Dynamic" />
                </td>
                 <td style="width:8%;">
                    <asp:TextBox CssClass="form-control" ID="txtHROTSat" runat="server"></asp:TextBox>
                    <asp:RegularExpressionValidator ID="revOTSat" runat="server" ErrorMessage="Only Numbers" 
                        ValidationExpression="^\d+(\.\d{1,2})?$" ControlToValidate="txtHROTSat" Display="Dynamic"></asp:RegularExpressionValidator>
                    <asp:CompareValidator ID="cvOTSat" runat="server"
                              ControlToValidate="txtHROTSat" ErrorMessage="Must be &lt; 12"
                              Operator="LessThanEqual" Type="Double"
                              ValueToCompare="12" Display="Dynamic" />
                </td>

                <td style="width:8%;">
                    <asp:TextBox CssClass="form-control" ID="txtHRDOTSat" runat="server"></asp:TextBox>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator6" runat="server" ErrorMessage="Only Numbers" 
                        ValidationExpression="^\d+(\.\d{1,2})?$" ControlToValidate="txtHRDOTSat" Display="Dynamic"></asp:RegularExpressionValidator>
                    <asp:CompareValidator ID="CompareValidator6" runat="server"
                              ControlToValidate="txtHRDOTSat" ErrorMessage="Must be &lt; 12"
                              Operator="LessThanEqual" Type="Double"
                              ValueToCompare="12" Display="Dynamic" />
                </td>
               
                <td style="width:20%;">
                    <asp:TextBox CssClass="form-control" ID="txtCommSat" runat="server"></asp:TextBox>
                    
                </td>
            </tr>     
            <%--Sunday--%>
            <tr>
                <td style="width:8%;padding-bottom:15px"">
                    <h3><asp:Label CssClass="label label-primary" ID="lblDateSun" runat="server" Text="Sun"></asp:Label></h3>
                    
                </td>
               <td style="width:8%;padding-bottom:15px"">
                  <h3><asp:Label CssClass="label label-default" ID="lblHrRegSun" runat="server" Text="0"></asp:Label></h3> 

                </td>
                <td style="width:8%;">
                    <asp:TextBox CssClass="form-control" ID="txtHrVacSun" runat="server"></asp:TextBox>
                    <asp:RegularExpressionValidator ID="revVacSun" runat="server" ErrorMessage="Only Numbers" 
                        ValidationExpression="^\d+(\.\d{1,2})?$" ControlToValidate="txtHrVacSun" Display="Dynamic"></asp:RegularExpressionValidator>
                    <asp:CompareValidator ID="cvVacSun" runat="server"
                              ControlToValidate="txtHrVacSun" ErrorMessage="Must be &lt; 8"
                              Operator="LessThanEqual" Type="Double"
                              ValueToCompare="8" Display="Dynamic" />
                </td>
                 <td style="width:8%;">
                    <asp:TextBox CssClass="form-control" ID="txtHrARPSun" runat="server"></asp:TextBox>
                    <asp:RegularExpressionValidator ID="revARPSun" runat="server" ErrorMessage="Only Numbers" 
                        ValidationExpression="^\d+(\.\d{1,2})?$" ControlToValidate="txtHrARPSun" Display="Dynamic"></asp:RegularExpressionValidator>
                    <asp:CompareValidator ID="cvARPSun" runat="server"
                              ControlToValidate="txtHrARPSun" ErrorMessage="Must be &lt; 8"
                              Operator="LessThanEqual" Type="Double"
                              ValueToCompare="8" Display="Dynamic" />

                </td>
                 <td style="width:8%;">
                    <asp:TextBox CssClass="form-control" ID="txtHrHolidaySun" runat="server"></asp:TextBox>
                    <asp:RegularExpressionValidator ID="revHolSun" runat="server" ErrorMessage="Only Numbers" 
                        ValidationExpression="^\d+(\.\d{1,2})?$" ControlToValidate="txtHrHolidaySun" Display="Dynamic"></asp:RegularExpressionValidator>
                    <asp:CompareValidator ID="cvHolSun" runat="server"
                              ControlToValidate="txtHrHolidaySun" ErrorMessage="Must be &lt; 8"
                              Operator="LessThanEqual" Type="Double"
                              ValueToCompare="8" Display="Dynamic" />
                </td>
                 <td style="width:8%;">
                    <asp:TextBox CssClass="form-control" ID="txtHrBereaveSun" runat="server"></asp:TextBox>
                    <asp:RegularExpressionValidator ID="revBereaveSun" runat="server" ErrorMessage="Only Numbers" 
                        ValidationExpression="^\d+(\.\d{1,2})?$" ControlToValidate="txtHrBereaveSun" Display="Dynamic"></asp:RegularExpressionValidator>
                    <asp:CompareValidator ID="cvBereaveSun" runat="server"
                              ControlToValidate="txtHrBereaveSun" ErrorMessage="Must be &lt; 8"
                              Operator="LessThanEqual" Type="Double"
                              ValueToCompare="8" Display="Dynamic" />
                </td>
                 <td style="width:8%;">
                    <asp:TextBox CssClass="form-control" ID="txtHrJurySun" runat="server"></asp:TextBox>
                    <asp:RegularExpressionValidator ID="revJurSun" runat="server" ErrorMessage="Only Numbers" 
                        ValidationExpression="^\d+(\.\d{1,2})?$" ControlToValidate="txtHrJurySun" Display="Dynamic"></asp:RegularExpressionValidator>
                    <asp:CompareValidator ID="cvJurSun" runat="server"
                              ControlToValidate="txtHrJurySun" ErrorMessage="Must be &lt; 8"
                              Operator="LessThanEqual" Type="Double"
                              ValueToCompare="8" Display="Dynamic" />
                </td>
                 <td style="width:8%;">
                    <asp:TextBox CssClass="form-control" ID="txtHROTSun" runat="server"></asp:TextBox>
                    <asp:RegularExpressionValidator ID="revOTSun" runat="server" ErrorMessage="Only Numbers" 
                        ValidationExpression="^\d+(\.\d{1,2})?$" ControlToValidate="txtHROTSun" Display="Dynamic"></asp:RegularExpressionValidator>
                    <asp:CompareValidator ID="cvOTSun" runat="server"
                              ControlToValidate="txtHROTSun" ErrorMessage="Must be &lt; 12"
                              Operator="LessThanEqual" Type="Double"
                              ValueToCompare="12" Display="Dynamic" />
                </td>

                 <td style="width:8%;">
                    <asp:TextBox CssClass="form-control" ID="txtHRDOTSun" runat="server"></asp:TextBox>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator7" runat="server" ErrorMessage="Only Numbers" 
                        ValidationExpression="^\d+(\.\d{1,2})?$" ControlToValidate="txtHRDOTSun" Display="Dynamic"></asp:RegularExpressionValidator>
                    <asp:CompareValidator ID="CompareValidator7" runat="server"
                              ControlToValidate="txtHRDOTSun" ErrorMessage="Must be &lt; 12"
                              Operator="LessThanEqual" Type="Double"
                              ValueToCompare="12" Display="Dynamic" />
                </td>
               
                <td style="width:20%;">
                    <asp:TextBox CssClass="form-control" ID="txtCommSun" runat="server"></asp:TextBox>
                </td>
            </tr>
            <%--Totals Row--%>
             <tr>
                 <td style="width:8%;">
                    
                </td>
                <td style="width:8%;">
                    <h3><asp:Label CssClass="label label-danger" ID="lblTotalReg" runat="server" Text="total"></asp:Label></h3>
                    
                </td>
                <td style="width:8%;">
                    <h3><asp:Label CssClass="label label-danger" ID="lblTotalVac" runat="server" Text="total"></asp:Label></h3>
                    
                </td>
                <td style="width:8%;">
                    <h3><asp:Label CssClass="label label-danger" ID="lblTotalARP" runat="server" Text="total"></asp:Label></h3>
                    
                </td>
                 <td style="width:8%;">
                    <h3><asp:Label CssClass="label label-danger" ID="lblTotalHoliday" runat="server" Text="total"></asp:Label></h3>
                    
                </td>
                <td style="width:8%;">
                    <h3><asp:Label CssClass="label label-danger" ID="lblTotalBereave" runat="server" Text="total"></asp:Label></h3>
                    
                </td>
                <td style="width:8%;">
                    <h3><asp:Label CssClass="label label-danger" ID="lblTotalJuryDuty" runat="server" Text="total"></asp:Label></h3>
                    
                </td>
                <td style="width:8%;">
                    <h3><asp:Label CssClass="label label-danger" ID="lblTotalOT" runat="server" Text="total"></asp:Label></h3>
                    
                </td>
                  <td style="width:8%;">
                    <h3><asp:Label CssClass="label label-danger" ID="lblTotalDOT" runat="server" Text="total"></asp:Label></h3>
                    
                </td>
                <td style="width:20%;">
                    
                </td>
            </tr>

        </table>
    </div>
    </div> <%--END ROW--%>
        <br />
        <br />
        <br />
    <div class="row">
        <div class="col-xs-3">
           <%-- <asp:Button CssClass="btn btn-warning" ID="btnStartOver" runat="server" Text="Start Over" Width="100%" OnClick="btnStartOver_Click" />--%>
        </div>
        <div class="col-xs-3">
            <asp:Button CssClass="btn btn-success" ID="btnCalc" runat="server" Text="Calculate" Width="100%" OnClick="btnCalc_Click" />
        </div>
        <div class="col-xs-3">
            <asp:Button CssClass="btn btn-primary" ID="btnSubmit" runat="server" Text="Submit" Width="100%" OnClick="btnSubmit_Click" Enabled="false"/>
        </div>


    </div>
       


    </div>


</asp:Content>

