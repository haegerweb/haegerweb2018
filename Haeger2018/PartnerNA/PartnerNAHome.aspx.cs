﻿using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Haeger2018.PartnerNA
{
    public partial class PartnerNAHome : System.Web.UI.Page
    {
        // SQL connection to access who the current user is
        SqlConnection aspUsersDB = new SqlConnection(ConfigurationManager.ConnectionStrings["HaegerTimeConnectionString"].ConnectionString);
        DateTime localDate = DateTime.Now;

        protected void Page_Load(object sender, EventArgs e)
        {
            // Display date on welcome page
            lblDate.Text = localDate.ToLongDateString();
            // Find current user's AspNetUsers ID
            string currentID = User.Identity.GetUserId();

            // Access database to identify current user and retrieve contact name.
            // In production version, will be fName from database.
            aspUsersDB.Open();
            string currentContact = null;
            string contactNameQuery = "Select Fname FROM OakdalePartnerInfo WHERE UserID = '" + currentID + "'";
            SqlCommand currentContactCommand = new SqlCommand(contactNameQuery, aspUsersDB);
            using (SqlDataReader tm = currentContactCommand.ExecuteReader())
            {
                while (tm.Read())
                {
                    currentContact = tm[0].ToString().Trim();
                }
            }

            aspUsersDB.Close();

           

            // In case no one is logged in.
            if (currentContact != null)
            {
                
                lblPartnerName.Text = currentContact;
            }
            else
            {
                Response.Redirect("/Default");
            }


        }

        // Buttons to redirect to Time Sheet Pages
        protected void btnCurrentWk_Click(object sender, EventArgs e)
        {
            Response.Redirect("/PartnerNA/CurrentWeek");
        }

        protected void btnRequestOff_Click(object sender, EventArgs e)
        {
            Response.Redirect("/PartnerNA/RequestTimeOff");
        }

        protected void brnPreviousRequests_Click(object sender, EventArgs e)
        {
            Response.Redirect("/PartnerNA/PartnerRequests");
        }
    }
}