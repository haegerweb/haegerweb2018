﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

namespace Haeger2018
{
    public partial class ATW : System.Web.UI.Page
    {
        SqlConnection pdfConn = new SqlConnection(ConfigurationManager.ConnectionStrings["ToolingPDFConnectionString"].ConnectionString);


        protected void Page_Load(object sender, EventArgs e)
        {
            // Check the User Agent
            var agent = Request.UserAgent.ToLower();

            // Check if iOS, Android, or other
            if (agent.Contains("ios"))
            {
                Response.Redirect("https://itunes.apple.com/us/app/haeger-wizard/id625478700?mt=8&uo=4");
            }
            else if (agent.Contains("android"))
            {
                Response.Redirect("https://play.google.com/store/apps/details?id=edu.haeger.haegerwizard&hl=en");
            }
            else if (Request.Browser.IsMobileDevice)
            {
                Response.Redirect("https://itunes.apple.com/us/app/haeger-wizard/id625478700?mt=8&uo=4");
            }

            Wizard1.PreRender += new EventHandler(wzd_PreRender);
        }



        protected void wzd_PreRender(object sender, EventArgs e)
        {
        
        }

        public string GetClassForWizardStep(object wizardStep)
        {
            WizardStep step = wizardStep as WizardStep;

            if (step == null)
            {
                return "";
            }

            int stepIndex = Wizard1.WizardSteps.IndexOf(step);

            if (stepIndex < Wizard1.ActiveStepIndex)
            {
                return "stepCompleted";
            }
            else if (stepIndex > Wizard1.ActiveStepIndex)
            {
                return "stepNotCompleted";
            }
            else
            {
                return "stepCurrent";
            }
        }


        protected void Wizard1_NextButtonClick(object sender, WizardNavigationEventArgs e)
        {
           
        }

        protected void rblMachineSelection_SelectedIndexChanged(object sender, EventArgs e)
        {
            Session["MachineSelected"] = rblMachineSelection.SelectedItem.Text;
            var machineSelectedImage = rblMachineSelection.SelectedItem.Value;

            imgMachineSelected.ImageUrl = "images/pngs/" + machineSelectedImage + ".png";
            lblMachineType.Text = "Machine Type:   " + rblMachineSelection.SelectedItem.Text;
            imgMachineSelected.Visible = true;
            lblMachineType.Visible = true;
            Wizard1.MoveTo(this.WizardStep2);
        }

        protected void rblSetType_SelectedIndexChanged(object sender, EventArgs e)
        {
            Session["SetTypeSelected"] = rblSetType.SelectedItem.Text;

            lblSetType.Text = "Set Type:   " + rblSetType.SelectedItem.Text;
            lblSetType.Visible = true;
            Wizard1.MoveTo(this.WizardStep3);
        }

        protected void rblFastener_SelectedIndexChanged(object sender, EventArgs e)
        {
            Session["FastenerSelected"] = rblFastener.SelectedItem.Text;

            lblFastener.Text = "Fastener:   " + rblFastener.SelectedItem.Text;
            lblFastener.Visible = true;
            Wizard1.MoveTo(this.WizardStep4);
        }

        protected void rblThreadType_SelectedIndexChanged(object sender, EventArgs e)
        {
            Session["ThreadTypeSelected"] = rblThreadType.SelectedItem.Text;

            lblThreadType.Text = "Thread Type:   " + rblThreadType.SelectedItem.Text;
            lblThreadType.Visible = true;

            // Retrieve data from query
            DataView dvDashLength = (DataView)sqlDashLength.Select(DataSourceSelectArguments.Empty);
            // DataView Length
            var dvDashLengthCount = dvDashLength.Count;
            // ArrayList to hold query results
            ArrayList lstDashLengths = new ArrayList();

            // Loop to add query data to array
            for (int i = 0; i <= (dvDashLengthCount - 1); i++)
            {
                for (int j = 0; j <= 1; j++)
                {
                    // Retrieve string from query.
                    var strDash = dvDashLength[i][j].ToString();
                    // Convert query result to integer for sorting.
                    int intDash = Convert.ToInt32(strDash);
                    // Add integer to ArrayList
                    lstDashLengths.Add(intDash);
                }
            }

            // Sort Array List.
            lstDashLengths.Sort();
            // Array list count.
            var lstDashLengthsCount = lstDashLengths.Count - 1;
            int dashLengthMin = Convert.ToInt32(lstDashLengths[0]);
            int dashLengthMax = Convert.ToInt32(lstDashLengths[lstDashLengthsCount]);

            // Clear Radio button list in case user moves to previous before reaching results.
            rblDashLength.Items.Clear();

            // Loop to add dash lengths to radio button list.
            for (int k = dashLengthMin; k <= dashLengthMax; k++)
            {

                rblDashLength.Items.Add(new ListItem(k.ToString(), k.ToString()));

            }

            Wizard1.MoveTo(this.WizardStep5);

        }

        protected void rblDashLength_SelectedIndexChanged(object sender, EventArgs e)
        {
            Session["DashLengthSelected"] = rblDashLength.SelectedItem.Text;

            lblDashLength.Text = "Dash Length:   " + rblDashLength.SelectedItem.Text;
            lblDashLength.Visible = true;

            Wizard1.MoveTo(this.WizardStep6);
        }

        protected void Wizard1_FinishButtonClick(object sender, WizardNavigationEventArgs e)
        {
            // Retrieve data from querys
            DataView dvResults1 = (DataView)sqlResults1.Select(DataSourceSelectArguments.Empty);
            DataView dvResults2 = (DataView)sqlResults2.Select(DataSourceSelectArguments.Empty);
            DataView dvResults3 = (DataView)sqlResults3.Select(DataSourceSelectArguments.Empty);
            DataView dvResults4 = (DataView)sqlResults4.Select(DataSourceSelectArguments.Empty);
            // DataView Lengths
            int dvResultsCount1 = dvResults1.Count;
            int dvResultsCount2 = dvResults2.Count;
            int dvResultsCount3 = dvResults3.Count;
            int dvResultsCount4 = dvResults4.Count;

            if (dvResultsCount1 > 0)
            {
                // Assign Session variables to query results.
                Session["atwToolingSet"] = dvResults1[0][0].ToString();
                Session["atwFlightTube"] = dvResults1[0][1].ToString();
                Session["atwMultiModule"] = dvResults1[0][2].ToString();
                Session["atwShuttle"] = dvResults1[0][3].ToString();
                Session["atwVacuumAnvil"] = dvResults1[0][4].ToString();
                Session["atwLowerTool"] = dvResults1[0][5].ToString();

                // Changing any existing values to blank.
                Session["atwOrientation"] = "";
                Session["atwSingulation"] = "";
                Session["atwUpperTool"] = "";
                Session["atwABFTAssembly"] = "";

            }
            if (dvResultsCount2 > 0)
            {
                // Assign Session variables to query results.
                Session["atwToolingSet"] = dvResults2[0][0].ToString();
                Session["atwFlightTube"] = dvResults2[0][1].ToString();
                Session["atwOrientation"] = dvResults2[0][2].ToString();
                Session["atwSingulation"] = dvResults2[0][3].ToString();
                Session["atwShuttle"] = dvResults2[0][4].ToString();
                Session["atwVacuumAnvil"] = dvResults2[0][5].ToString();
                Session["atwLowerTool"] = dvResults2[0][6].ToString();

                // Changing any existing values to blank.
                Session["atwMultiModule"] = "";
                Session["atwUpperTool"] = "";
                Session["atwABFTAssembly"] = "";

            }
            if (dvResultsCount3 > 0)
            {
                // Assign Session variables to query results.
                Session["atwToolingSet"] = dvResults3[0][0].ToString();
                Session["atwFlightTube"] = dvResults3[0][1].ToString();
                Session["atwMultiModule"] = dvResults3[0][2].ToString();
                Session["atwLowerTool"] = dvResults3[0][3].ToString();
                Session["atwUpperTool"] = dvResults3[0][4].ToString();

                // Changing any existing values to blank.
                Session["atwOrientation"] = "";
                Session["atwSingulation"] = "";
                Session["atwVacuumAnvil"] = "";
                Session["atwABFTAssembly"] = "";
                Session["atwShuttle"] = "";


            }
            if (dvResultsCount4 > 0)
            {
                // Assign Session variables to query results.
                Session["atwToolingSet"] = dvResults4[0][0].ToString();
                Session["atwFlightTube"] = dvResults4[0][1].ToString();
                Session["atwOrientation"] = dvResults4[0][2].ToString();
                Session["atwSingulation"] = dvResults4[0][3].ToString();
                Session["atwABFTAssembly"] = dvResults4[0][4].ToString();

                // Changing any existing values to blank.
                Session["atwUpperTool"] = "";
                Session["atwLowerTool"] = "";
                Session["atwShuttle"] = "";
                Session["atwVacuumAnvil"] = "";
                Session["atwMultiModule"] = "";

            }

        }

        protected void btnReset_Click(object sender, EventArgs e)
        {
            Response.Redirect("ATW.aspx");
        }


        // Method to query database for tooling pdf name
        public string getImages(string pdfType, string table, string componentType, string result)
        {
            string pdfName = "";
            string _pdfType = pdfType;
            string _componentType = componentType;
            string _table = table;
            string _result = result;

            try
            {
                // SQL query 
                pdfConn.Open();
                // Query and commands to retrieve image name
                string findImageQuery = "SELECT " + _pdfType + " FROM " + _table + " WHERE " + _componentType + " = '" + _result + "'";
                SqlCommand findImageCommand = new SqlCommand(findImageQuery, pdfConn);
                using (SqlDataReader tn = findImageCommand.ExecuteReader())
                {
                    while (tn.Read())
                    {
                        pdfName = tn[0].ToString();
                    }
                }
                if (pdfName == null)
                {
                    pdfName = "errorimage1";
                }

                pdfConn.Close();
            }
            catch (Exception)
            {
                pdfName = "errorimage2";
            }

            return pdfName;
        }

        // Button click event for grdResult1 toolingSet
        protected void lbtnTS1_Click(object sender, EventArgs e)
        {
            LinkButton btn = (LinkButton)grdResults1.Rows[0].FindControl("lbtnTS1");
            string result = btn.Text;

            var pdfName = getImages("toolingSetImages", "atwImages1$", "toolingSet", result);

            string link = "ToolingPDFs/" + pdfName + ".pdf";

            Response.Redirect(link);
        }

        // Button click event for grdResult1 flightTube
        protected void lbtnFT1_Click(object sender, EventArgs e)
        {
            LinkButton btn = (LinkButton)grdResults1.Rows[0].FindControl("lbtnFT1");
            string result = btn.Text;

            var pdfName = getImages("flightTubeImages", "atwImages1$", "flightTube", result);

            string link = "ToolingPDFs/" + pdfName + ".pdf";

            Response.Redirect(link);
        }

        // Button click event for grdResult1 multiModule
        protected void lbtnMM1_Click(object sender, EventArgs e)
        {
            LinkButton btn = (LinkButton)grdResults1.Rows[0].FindControl("lbtnMM1");
            string result = btn.Text;

            var pdfName = getImages("multiModuleImages", "atwImages1$", "multiModule", result);

            string link = "ToolingPDFs/" + pdfName + ".pdf";

            Response.Redirect(link);
        }

        // Button click event for grdResult1 Shuttle
        protected void lbtnSH1_Click(object sender, EventArgs e)
        {
            LinkButton btn = (LinkButton)grdResults1.Rows[0].FindControl("lbtnSH1");
            string result = btn.Text;

            var pdfName = getImages("ShuttleImages", "atwImages1$", "Shuttle", result);

            string link = "ToolingPDFs/" + pdfName + ".pdf";

            Response.Redirect(link);
        }

        // Button click event for grdResult1 vacuumAnvil
        protected void lbtnVA1_Click(object sender, EventArgs e)
        {
            LinkButton btn = (LinkButton)grdResults1.Rows[0].FindControl("lbtnVA1");
            string result = btn.Text;

            var pdfName = getImages("vacuumAnvilImages", "atwImages1$", "vacuumAnvil", result);

            string link = "ToolingPDFs/" + pdfName + ".pdf";

            Response.Redirect(link);
        }

        // Button click event for grdResult1 lowerTool
        protected void lbtnLT1_Click(object sender, EventArgs e)
        {
            LinkButton btn = (LinkButton)grdResults1.Rows[0].FindControl("lbtnLT1");
            string result = btn.Text;

            var pdfName = getImages("lowerToolImages", "atwImages1$", "lowerTool", result);

            string link = "ToolingPDFs/" + pdfName + ".pdf";

            Response.Redirect(link);
        }

        // Button click event for grdResult2 toolingSet
        protected void lbtnTS2_Click(object sender, EventArgs e)
        {
            LinkButton btn = (LinkButton)grdResults2.Rows[0].FindControl("lbtnTS2");
            string result = btn.Text;

            var pdfName = getImages("toolingSetImages", "atwImages2$", "toolingSet", result);

            string link = "ToolingPDFs/" + pdfName + ".pdf";

            Response.Redirect(link);
        }

        // Button click event for grdResult2 flightTube
        protected void lbtnFT2_Click(object sender, EventArgs e)
        {
            LinkButton btn = (LinkButton)grdResults2.Rows[0].FindControl("lbtnFT2");
            string result = btn.Text;

            var pdfName = getImages("flightTubeImages", "atwImages2$", "flightTube", result);

            string link = "ToolingPDFs/" + pdfName + ".pdf";

            Response.Redirect(link);
        }

        // Button click event for grdResult2 orientation
        protected void lbtnOR2_Click(object sender, EventArgs e)
        {
            LinkButton btn = (LinkButton)grdResults2.Rows[0].FindControl("lbtnOR2");
            string result = btn.Text;

            var pdfName = getImages("orientationImages", "atwImages2$", "orientation", result);

            string link = "ToolingPDFs/" + pdfName + ".pdf";

            Response.Redirect(link);
        }

        // Button click event for grdResult2 singulation
        protected void lbtnSG2_Click(object sender, EventArgs e)
        {
            LinkButton btn = (LinkButton)grdResults2.Rows[0].FindControl("lbtnSG2");
            string result = btn.Text;

            var pdfName = getImages("singulationImages", "atwImages2$", "singulation", result);

            string link = "ToolingPDFs/" + pdfName + ".pdf";

            Response.Redirect(link);
        }

        // Button click event for grdResult2 Shuttle
        protected void lbtnSH2_Click(object sender, EventArgs e)
        {
            LinkButton btn = (LinkButton)grdResults2.Rows[0].FindControl("lbtnSH2");
            string result = btn.Text;

            var pdfName = getImages("ShuttleImages", "atwImages2$", "Shuttle", result);

            string link = "ToolingPDFs/" + pdfName + ".pdf";

            Response.Redirect(link);
        }

        // Button click event for grdResult2 vacuumAnvil
        protected void lbtnVA2_Click(object sender, EventArgs e)
        {
            LinkButton btn = (LinkButton)grdResults2.Rows[0].FindControl("lbtnVA2");
            string result = btn.Text;

            var pdfName = getImages("vacuumAnvilImages", "atwImages2$", "vacuumAnvil", result);

            string link = "ToolingPDFs/" + pdfName + ".pdf";

            Response.Redirect(link);
        }

        // Button click event for grdResult2 lowerTool
        protected void lbtnLT2_Click(object sender, EventArgs e)
        {
            LinkButton btn = (LinkButton)grdResults2.Rows[0].FindControl("lbtnLT2");
            string result = btn.Text;

            var pdfName = getImages("lowerToolImages", "atwImages2$", "lowerTool", result);

            string link = "ToolingPDFs/" + pdfName + ".pdf";

            Response.Redirect(link);
        }

        // Button click event for grdResult3 toolingSet
        protected void lbtnTS3_Click(object sender, EventArgs e)
        {
            LinkButton btn = (LinkButton)grdResults3.Rows[0].FindControl("lbtnTS3");
            string result = btn.Text;

            var pdfName = getImages("toolingSetImages", "atwImages3$", "toolingSet", result);

            string link = "ToolingPDFs/" + pdfName + ".pdf";

            Response.Redirect(link);
        }

        // Button click event for grdResult3 flightTube
        protected void lbtnFT3_Click(object sender, EventArgs e)
        {
            LinkButton btn = (LinkButton)grdResults3.Rows[0].FindControl("lbtnFT3");
            string result = btn.Text;

            var pdfName = getImages("flightTubeImages", "atwImages3$", "flightTube", result);

            string link = "ToolingPDFs/" + pdfName + ".pdf";

            Response.Redirect(link);
        }

        // Button click event for grdResult3 multiModule
        protected void lbtnMM3_Click(object sender, EventArgs e)
        {
            LinkButton btn = (LinkButton)grdResults3.Rows[0].FindControl("lbtnMM3");
            string result = btn.Text;

            var pdfName = getImages("multiModuleImages", "atwImages3$", "multiModule", result);

            string link = "ToolingPDFs/" + pdfName + ".pdf";

            Response.Redirect(link);
        }

        // Button click event for grdResult3 lowerTool
        protected void lbtnLT3_Click(object sender, EventArgs e)
        {
            LinkButton btn = (LinkButton)grdResults3.Rows[0].FindControl("lbtnLT3");
            string result = btn.Text;

            var pdfName = getImages("lowerToolImages", "atwImages3$", "lowerTool", result);

            string link = "ToolingPDFs/" + pdfName + ".pdf";

            Response.Redirect(link);
        }

        // Button click event for grdResult3 upperTool
        protected void lbtnUT3_Click(object sender, EventArgs e)
        {
            LinkButton btn = (LinkButton)grdResults3.Rows[0].FindControl("lbtnUT3");
            string result = btn.Text;

            var pdfName = getImages("upperToolImages", "atwImages3$", "upperTool", result);

            string link = "ToolingPDFs/" + pdfName + ".pdf";

            Response.Redirect(link);
        }

        // Button click event for grdResult4 toolingSet
        protected void lbtnTS4_Click(object sender, EventArgs e)
        {
            LinkButton btn = (LinkButton)grdResults4.Rows[0].FindControl("lbtnTS4");
            string result = btn.Text;

            var pdfName = getImages("toolingSetImages", "atwImages4$", "toolingSet", result);

            string link = "ToolingPDFs/" + pdfName + ".pdf";

            Response.Redirect(link);
        }

        // Button click event for grdResult4 flightTube
        protected void lbtnFT4_Click(object sender, EventArgs e)
        {
            LinkButton btn = (LinkButton)grdResults4.Rows[0].FindControl("lbtnFT4");
            string result = btn.Text;

            var pdfName = getImages("flightTubeImages", "atwImages4$", "flightTube", result);

            string link = "ToolingPDFs/" + pdfName + ".pdf";

            Response.Redirect(link);
        }

        // Button click event for grdResult4 orientation
        protected void lbtnOR4_Click(object sender, EventArgs e)
        {
            LinkButton btn = (LinkButton)grdResults4.Rows[0].FindControl("lbtnOR4");
            string result = btn.Text;

            var pdfName = getImages("orientationImages", "atwImages4$", "orientation", result);

            string link = "ToolingPDFs/" + pdfName + ".pdf";

            Response.Redirect(link);
        }

        // Button click event for grdResult4 singulation
        protected void lbtnSG4_Click(object sender, EventArgs e)
        {
            LinkButton btn = (LinkButton)grdResults4.Rows[0].FindControl("lbtnSG4");
            string result = btn.Text;

            var pdfName = getImages("singulationImages", "atwImages4$", "singulation", result);

            string link = "ToolingPDFs/" + pdfName + ".pdf";

            Response.Redirect(link);
        }

        // Button click event for grdResult4 abftAssembly
        protected void lbtnAA4_Click(object sender, EventArgs e)
        {
            LinkButton btn = (LinkButton)grdResults4.Rows[0].FindControl("lbtnAA4");
            string result = btn.Text;

            var pdfName = getImages("abftAssemblyImages", "atwImages4$", "abftAssembly", result);

            string link = "ToolingPDFs/" + pdfName + ".pdf";

            Response.Redirect(link);
        }



    }
}